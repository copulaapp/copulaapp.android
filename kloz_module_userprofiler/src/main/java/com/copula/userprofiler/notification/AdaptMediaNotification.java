package com.copula.userprofiler.notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;
import com.copula.support.util.Formatter;
import com.copula.userprofiler.R;
import com.squareup.picasso.Picasso;

class AdaptMediaNotification extends BaseAdapter<MediaCaptionEntry> {
    static final int V_DP = R.id.img_profile_image;
    private int displayWidth;

    AdaptMediaNotification(Context context) {
        super(context);
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    private String getMediaTypeTitle(int mediaType) {
        if (mediaType == 1) return getString(R.string.txt_an_image);
        else if (mediaType == 2) return getString(R.string.txt_an_audio);
        else if (mediaType == 3) return getString(R.string.txt_a_video);
        else return getString(R.string.txt_media);
    }

    private String getActionString(int action) {
        if (action == MediaCaptionEntry.ACTION_LIKE)
            return getString(R.string.txt_liked);
        else return getString(R.string.txt_shared);
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_notification,
                    null, false);
            holder.img = (ImageView) convertView.findViewById(V_DP);
            holder.thumb = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.time = (TextView) convertView.findViewById(R.id.txt_caption_time);
            holder.msg = (TextView) convertView.findViewById(R.id.txt_caption_message);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        MediaCaptionEntry entry = getItem(position);

        String message;
        UserProfileEntry profile = (UserProfileEntry) entry.getTag();
        if (profile != null) {
            String url = MediaUtility.
                    appendSize(profile.imgUrl, displayWidth / 5);
            Picasso.with(getContext()).load(url)
                    .placeholder(R.drawable.img_person_white_150px).into(holder.img);

            String appUserId = new UserAccountBase(getContext()).getUserId();
            if (profile.userId.equals(appUserId)) profile.username = getString(R.string.txt_you);
            message = String.format("%s ", profile.username);
        } else message = String.format("%s ", getString(R.string.txt_a_friend));

        message += String.format("%s ", getActionString(entry.action));
        if (entry.mediaTitle != null) message += entry.mediaTitle;
        else message += getMediaTypeTitle(entry.mediaType);

        if (entry.mediaComposer != null)
            message += String.format(" by %s", entry.mediaComposer);

        holder.time.setText(Formatter.toElapsedDate(entry.createdAt));
        holder.msg.setText(message);

        int plh = DesignHelper.getPlaceholder(entry.mediaType);
        String url = MediaUtility
                .appendSize(entry.thumbnailUrl, displayWidth / 5);
        Picasso.with(getContext()).load(url).placeholder(plh).into(holder.thumb);

        final View fConvertView = convertView;
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) fConvertView.getParent())
                        .performItemClick(v, position, position);
            }
        };
        holder.thumb.setOnClickListener(clickListener);
        holder.img.setOnClickListener(clickListener);

        return convertView;
    }

    private class ViewHolder {
        ImageView img, thumb;
        TextView msg, time;
    }
}
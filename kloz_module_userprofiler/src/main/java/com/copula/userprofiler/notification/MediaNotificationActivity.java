package com.copula.userprofiler.notification;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.feeds.DaoLocalMediaCaption;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.localservice.profile.DaoConnectsProfile;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.genericlook.CustomActionBar;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.audio.player.AudioPlayerExtActivity;
import com.copula.mediasocial.image.player.ImagePlayerMainActivity;
import com.copula.mediasocial.video.player.VideoPlayerMainActivity;
import com.copula.support.android.view.widget.AbsListViewScrollListener;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;
import com.copula.userprofiler.R;

import java.util.List;

public class MediaNotificationActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener, AbsListViewScrollListener, View.OnClickListener {

    private AdaptMediaNotification adapter;
    private ListView listView;
    private View vEmptyView, vListProgress;
    private ListPopupWindow listPopupWindow;

    private DaoLocalMediaCaption doaMediaCaption;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, MediaNotificationActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_media_notification);
        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(R.string.notifications);
        actionBar.setBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }).compile();

        (listView = (ListView) findViewById(R.id.lst)).setHelperScrollListener(this);
        vEmptyView = findViewById(R.id.view_empty_list);
        vListProgress = findViewById(R.id.view_list_progress);
        listView.setPagerStep(50);

        ((TextView) (findViewById(R.id.txt_empty_list_message)))
                .setText(R.string.msg_empty_local_caption);
        vEmptyView.setOnClickListener(this);


        listView.setAdapter(adapter = new AdaptMediaNotification(this));
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        listPopupWindow = new ListPopupWindow(this);
        listPopupWindow.setAdapter(setupOptionAdapter());
        listPopupWindow.setWidth(DesignHelper.scaleInDP(this, 120));
        listPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        listPopupWindow.setModal(true);

        this.doaMediaCaption = new DaoLocalMediaCaption(this);

        String paging = DesignHelper.page(0, listView.getPagerStep());
        new GetCaptions(paging).execute();
    }

    private ArrayAdapter<String> setupOptionAdapter() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, R.layout.spnadp_studio_selector_opt);
        adapter.add(getString(R.string.txt_open));
        adapter.add(getString(R.string.act_delete));
        return adapter;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onPageScroll(int totalItem, int pageOffset, int pagerStep) {
        String paging = DesignHelper.page(pageOffset, pagerStep);
        new GetCaptions(paging).execute();
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MediaCaptionEntry model = adapter.getItem(position);
        if (model == null || model.dataUrl == null) {
            Toast.makeText(this, R.string.msg_media_not_available,
                    Toast.LENGTH_SHORT).show();
        } else doMediaOpen(model);
    }

    private void doMediaOpen(MediaModel m) {
        if (m.mediaType == MediaModel.AUDIO) {
            AudioPlayerExtActivity.launch(this, new AudioModel(m));
        } else if (m.mediaType == MediaModel.IMAGE) {
            ImagePlayerMainActivity.launch(this, new ImageModel(m));
        } else if (m.mediaType == MediaModel.VIDEO) {
            VideoPlayerMainActivity.launch(this, new VideoModel(m));
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        final MediaCaptionEntry media = adapter.getItem(position);

        listPopupWindow.setAnchorView(view);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> p, View v, int pos, long id) {
                if (pos == 0) doMediaOpen(media);
                else if (pos == 1) doItemDelete(media);

                listPopupWindow.dismiss();
            }
        });
        listPopupWindow.show();
        return true;
    }

    private void doItemDelete(final MediaCaptionEntry mc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.msg_confirm_action);
        DialogInterface.OnClickListener click = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    dialog.dismiss();
                } else if (which == DialogInterface.BUTTON_POSITIVE) {
                    doMediaCaptionDelete(mc);
                }
            }
        };
        builder.setNegativeButton(R.string.txt_no, click);
        builder.setPositiveButton(R.string.act_delete, click);
        builder.show();
    }

    private void doMediaCaptionDelete(final MediaCaptionEntry mc) {
        if (!doaMediaCaption.removeMediaCaptions(mc.mediaType, mc.mediaId)) return;
        adapter.removeHandler(mc);
        adapter.notifyDataSetChanged();
        if (adapter.getCount() <= 0) DesignHelper.showOnlyView(vEmptyView);
    }

    @Override
    public void onClick(View v) {
        if (v == vEmptyView) {
            String paging = DesignHelper.page(0, listView.getPagerStep());
            new GetCaptions(paging).execute();
        }
    }

    private class GetCaptions extends AsyncTask<String, Void, List<MediaCaptionEntry>> {
        private String limit;

        GetCaptions(String limit) {
            this.limit = limit;
            DesignHelper.showOnlyView(vListProgress);
        }

        @Override
        protected List<MediaCaptionEntry> doInBackground(String... params) {
            Context ctx = MediaNotificationActivity.this;
            DaoConnectsProfile daoConnects = new DaoConnectsProfile(ctx);
            UserProfileEntry user = new UserAccountBase(ctx).getUserProfile();

            List<MediaCaptionEntry> models =
                    doaMediaCaption.getCaptions(-1, user.userId, limit);
            if (models == null) return null;

            for (MediaCaptionEntry model : models) {
                if (model.actorId.equals(user.userId)) model.setTag(user);
                else model.setTag(daoConnects.getConnect(model.actorId));
            }
            return models;
        }

        @Override
        protected void onPostExecute(List<MediaCaptionEntry> response) {
            if (response != null && response.size() > 0) {
                DesignHelper.showOnlyView((View) listView.getParent());
                adapter.addOrReplaceHandlers(response);
                adapter.notifyDataSetChanged();
            }

            if (adapter.getCount() == 0) DesignHelper.showOnlyView(vEmptyView);
        }
    }//END
}
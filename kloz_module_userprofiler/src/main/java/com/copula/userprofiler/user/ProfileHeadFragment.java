package com.copula.userprofiler.user;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.localservice.profile.UsersProfileResource;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediagallary.MediaChooserGalleryActivity;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.profile.helper.AsyncAPIRequest;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.TextView;
import com.copula.userprofiler.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;

public class ProfileHeadFragment extends Fragment implements OnSharedPreferenceChangeListener,
        OnClickListener, AsyncAPIRequest.UserActCallback {
    private ImageView imgProfileImage;
    private TextView txtProfileName, txtProfileBio;
    private View prgImageUpload;

    private UserAccountBase userAccountBase;

    public static ProfileHeadFragment instantiate() {
        return new ProfileHeadFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_user_profile_head, container, false);
        txtProfileName = (TextView) view.findViewById(R.id.txt_profile_name);
        txtProfileBio = (TextView) view.findViewById(R.id.txt_profile_bio);
        imgProfileImage = (ImageView) view.findViewById(R.id.img_profile_image);
        prgImageUpload = view.findViewById(R.id.prg_image_upload);
        view.findViewById(R.id.btn_edit_profile).setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userAccountBase = new UserAccountBase(getContext());

        imgProfileImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int mediaType = MediaChooserGalleryActivity.CHOOSE_IMAGE;
                String title = getString(R.string.txt_select_profile_image);
                MediaChooserGalleryActivity.launch(getActivity(), mediaType, title, 100);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 919 && resultCode == Activity.RESULT_OK) {
            //TODO: requires check of image already uploaded on registration
        } else if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            MediaModel media = (MediaModel)
                    data.getSerializableExtra(MediaChooserGalleryActivity.EXTRA_MEDIA);
            this.setProfileImage(media);
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getActivity(), R.string.successful, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        UserProfileEntry profile = UserSession.getAppProfile(getActivity());
        txtProfileName.setText(profile.username);
        txtProfileBio.setText(profile.bio);

        int dw = (int) (getResources().getDisplayMetrics().widthPixels / 4.5);
        String imgUrl = MediaUtility.appendSize(profile.imgUrl, dw);
        setProfileImage(Picasso.with(getContext()).load(imgUrl));

        userAccountBase.getPreference().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        userAccountBase.getPreference().unregisterOnSharedPreferenceChangeListener(this);
    }

    private void setProfileImage(RequestCreator creator) {
        //cacheList was dumped so as to enabled update override cacheList
        creator.placeholder(R.drawable.img_person_white_150px)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(imgProfileImage);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        txtProfileBio.setText(userAccountBase.getProfileBio(getString(R.string.def_profile_bio)));
        txtProfileName.setText(userAccountBase.getUsername(getString(R.string.def_profile_name)));
    }


    private void setProfileImage(final MediaModel media) {
        if (!NettySocialActHelper.checkLaunchLogged(getActivity())) return;

        new ImageUpdateHelper(getActivity(), media, this)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void dispatchMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_edit_profile) {
            UserProfileUpdateActivity.launch(getActivity());
        }
    }

    @Override
    public void onPreAct(AsyncAPIRequest sf, int act) {
        MediaModel h = ((ImageUpdateHelper) sf).getMedia();
        prgImageUpload.setVisibility(View.VISIBLE);
        setProfileImage(Picasso.with(getContext()).load(new File(h.dataPath)));
    }

    @Override
    public void onPostAct(AsyncAPIRequest sf, int act) {
        prgImageUpload.setVisibility(View.GONE);
        String appUserId = new UserAccountBase(getActivity()).getUserId();

        if (sf.isSuccessful()) {
            MediaModel h = ((ImageUpdateHelper) sf).getMedia();
            dispatchMessage(getString(R.string.msg_profile_image_update_success));
            setProfileImage(Picasso.with(getContext()).load(new File(h.dataPath)));
        } else {
            dispatchMessage(getString(R.string.msg_profile_image_update_failed));
            String imgPath = UsersProfileResource.getProfileImagePath(appUserId);
            setProfileImage(Picasso.with(getContext()).load(new File(imgPath)));
        }
    }//END
}

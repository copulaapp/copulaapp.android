package com.copula.userprofiler.user;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.functionality.util.MediaUtility;
import com.copula.nettysocial.profile.helper.AsyncAPIRequest;

import java.io.File;

/**
 * Created by heeleaz on 7/23/17.
 */
public class ImageUpdateHelper extends AsyncTask<String, Object, RestResponse> implements AsyncAPIRequest {
    private MediaModel mediaModel;
    private boolean isSuccessful;
    private String appUserId;

    private UserActCallback callback;
    private Context context;

    ImageUpdateHelper(Context ctx, MediaModel mediaModel, UserActCallback callback) {
        this.callback = callback;
        this.mediaModel = mediaModel;
        this.context = ctx;
        this.appUserId = new UserAccountBase(ctx).getUserId();
    }

    @Override
    protected void onPreExecute() {
        if (callback != null) callback.onPreAct(this, 5);
    }

    @Override
    protected RestResponse doInBackground(String... strings) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();

        String imgUrl = MediaUtility
                .appendSize(mediaModel.dataUrl, MediaUtility.DP_UPLOAD_DIM);
        File imgFile = MediaUtility.makeTmpFile(imgUrl);
        if (imgFile == null || !imgFile.exists()) return null;

        RestResponse response =
                api.getProfileAPI().updateProfileImage(appUserId, imgFile);
        if (response != null && response.statusCode == 1) {
            new UserAccountBase(context).setProfileImage(imgFile);
        }
        return response;
    }

    @Override
    protected void onPostExecute(RestResponse result) {
        isSuccessful = (result != null && result.statusCode == 1);
        if (callback != null) callback.onPostAct(this, 5);
    }

    @Override
    public boolean isSuccessful() {
        return isSuccessful;
    }

    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public Object getTag(String key) {
        return null;
    }

    @Override
    public Object setTag(String key, Object value) {
        return null;
    }

    public MediaModel getMedia() {
        return mediaModel;
    }
}

package com.copula.userprofiler.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.genericlook.CopulaProgressDialog;
import com.copula.genericlook.CustomActionBar;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.profile.helper.AsyncAPIRequest;
import com.copula.nettysocial.profile.helper.GetProfileHelper;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.util.Formatter;
import com.copula.userprofiler.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliasigbalajobi on 4/5/16.
 */
public class UserProfileUpdateActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener,
        AsyncAPIRequest.UserActCallback {
    private List<String> usedUsername = new ArrayList<>();
    private EditText edtUsername, edtProfileBio;
    private View btnSave;

    public static void launch(Context context) {
        context.startActivity(new Intent(context, UserProfileUpdateActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_user_profile_update);

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setView(R.layout.bar_user_profile_update).setBackListener(this);
        actionBar.setTitle(getString(R.string.txt_update_profile)).compile();
        (btnSave = findViewById(R.id.btn_done)).setOnClickListener(this);
        btnSave.setTag(true);//this state true.. so as to allow update

        edtProfileBio = (EditText) findViewById(R.id.edt_profile_bio);
        edtUsername = (EditText) findViewById(R.id.edt_username);

        edtProfileBio.setText(UserSession.getAppProfile(this).bio);
        edtUsername.setText(UserSession.getAppProfile(this).username);
        edtUsername.addTextChangedListener(this);

        setCompoundDrawable(R.drawable.img_person_black_96dp, edtUsername);
        setCompoundDrawable(R.drawable.img_person_bio_black_48dp, edtProfileBio);

        if (NettySocialActHelper.isLogged(this)) {
            String userId = new UserAccountBase(this).getUserId();
            new GetProfileHelper(userId, userId, this);
        }
    }

    private void setCompoundDrawable(int drawable, EditText editText) {
        DesignHelper.setLeftDrawable(editText, DesignHelper.scaleDrawable(
                this, drawable, 18, 18));
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        String username = editable.toString();
        if (usedUsername.contains(username)) {
            setCompoundDrawable(R.drawable.img_person_red_96dp, edtUsername);
        } else if (username.equals(UserSession.getAppProfile(this).username)) {
            setCompoundDrawable(R.drawable.img_person_black_96dp, edtUsername);
            btnSave.setTag(true);
        } else if (Formatter.isValidUsername(username)) {
            new CheckUsername(username).execute();
        } else {
            setCompoundDrawable(R.drawable.img_person_red_96dp, edtUsername);
        }
    }

    private void dispatchMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnSave.getId()) {
            if (!NettySocialActHelper.checkLaunchLogged(this, 100))
                return;

            if ((Boolean) btnSave.getTag()) new ProfileUpdate().execute();
            else dispatchMessage(getString(R.string.msg_profile_update_failed));
        } else if (view.getId() == R.id.btn_bar_back) finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onPreAct(AsyncAPIRequest sf, int act) {
        if (act == GET_PROFILE) {
            findViewById(R.id.view_list_progress).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPostAct(AsyncAPIRequest sf, int act) {
        if (sf.isSuccessful() && act == GET_PROFILE) {
            UserProfileEntry u = ((GetProfileHelper) sf).getResult();
            edtProfileBio.setText(u.bio);
            edtUsername.setText(u.username);
        }
        findViewById(R.id.view_list_progress).setVisibility(View.GONE);
    }

    private class CheckUsername extends AsyncTask<String, Void, RestResponse<UserAccountEntry>> {
        private String username;

        CheckUsername(String username) {
            this.username = username;
            btnSave.setTag(false);//to be enabled if username is unique
        }

        @Override
        protected RestResponse<UserAccountEntry> doInBackground(String... params) {
            NettyAPIProvider nettyAPI = NettyAPIConnection.getInstance().connect();
            return nettyAPI.getAccountAPI().getAccountWithUser(username);
        }

        @Override
        protected void onPostExecute(RestResponse<UserAccountEntry> model) {
            if (model == null) {
                setCompoundDrawable(R.drawable.img_person_red_96dp, edtUsername);
                btnSave.setTag(false);
            } else if (model.statusCode == 1) {//used username
                usedUsername.add(model.result.username);
                setCompoundDrawable(R.drawable.img_person_red_96dp, edtUsername);
                btnSave.setTag(false);
            } else if (model.statusCode == 0) {//username is free
                setCompoundDrawable(R.drawable.img_person_black_96dp, edtUsername);
                btnSave.setTag(true);//enable saving if account is validated
            }
        }
    }//end

    private class ProfileUpdate extends AsyncTask<String, Void, RestResponse> {
        private String bio, username;
        private CopulaProgressDialog progressDialog;

        ProfileUpdate() {
            this.bio = edtProfileBio.getTextContent();
            this.username = edtUsername.getTextContent();

            progressDialog = new CopulaProgressDialog(UserProfileUpdateActivity.this);
            progressDialog.setMessage(R.string.prg_update);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected RestResponse doInBackground(String... args) {
            NettyAPIProvider nettyAPI = NettyAPIConnection.getInstance().connect();
            UserProfileEntry user =
                    UserSession.getAppProfile(UserProfileUpdateActivity.this);

            RestResponse response = new RestResponse();
            response.statusCode = 100;//defined. incase there is no change
            if (!bio.equals(user.bio)) {
                response = nettyAPI.getProfileAPI().updateProfile(user.userId, bio);
            }
            if (!username.equals(user.username)) {
                response = nettyAPI.getAccountAPI().updateUsername(user.userId, username);
            }
            return response;
        }

        @Override
        protected void onPostExecute(RestResponse result) {
            if (result == null) {
                dispatchMessage(getString(R.string.msg_profile_update_failed));
            } else if (result.statusCode == 1 || result.statusCode == 100) {
                UserAccountBase dao = new UserAccountBase(UserProfileUpdateActivity.this);
                dao.setUsername(edtUsername.getTextContent().trim());
                dao.setProfileBio(edtProfileBio.getTextContent().trim());
                finish();
            } else {
                dispatchMessage(result.statusMessage);
            }
            progressDialog.dismiss();
        }
    }//end
}

package com.copula.mediasocial.media;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.profile.DaoSharedCollection;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.MediaSocialActionHelper;
import com.copula.mediasocial.MediaSocialActionHelper.ActionListener;
import com.copula.mediasocial.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 3/10/17.
 */
public abstract class AbsMediaFragment<T extends MediaModel> extends Fragment implements AbsMediaHelper
        .HelperCallback {

    public abstract int getMediaType();

    private void doMediaDelete(T media) {
        new MediaDeleteHelper(getActivity(), media, this);
    }

    protected void doMediaLike(final T media) {
        MediaSocialActionHelper.doMediaLike(new ActionListener() {
            @Override
            public void onPreExecute() {
                onMediaAction(MediaOptions.LIKE, toArray(media));
            }

            @Override
            public void onPostExecute() {
            }
        }, new UserAccountBase(getActivity()).getUserId(), media);
    }

    private void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    protected void updateMediaStudio(T media) {
        DaoSharedCollection dao = new DaoSharedCollection(getActivity());
        if (media.isGalleryMedia = !media.isGalleryMedia) {
            dao.addMedia(media);
            dispatchMessage(getString(R.string.msg_media_show));
        } else {
            dao.removeMedia(media);
            dispatchMessage(getString(R.string.msg_media_hide));
        }

        onMediaAction(MediaOptions.UPDATE_STUDIO, toArray(media));
    }

    public List<T> toArray(T media) {
        List<T> mediaModels = new ArrayList<>(1);
        mediaModels.add(media);
        return mediaModels;
    }


    public void onMediaAction(MediaOptions action, List<T> mediaModels) {
    }

    protected void showMediaOptionActivity(T media, View v, int flags) {
        MediaOptionPopWindow window = new MediaOptionPopWindow(getActivity());
        window.setMedia(media);
        window.setFlags(flags);

        window.show(v, new MediaOptionPopWindow.MediaOptionListener() {
            @Override
            public void onAction(MediaOptions action, MediaModel media) {
                T m = (T) media;
                if (action == MediaOptions.PLAY) onMediaAction(action, toArray(m));
                if (action == MediaOptions.LIKE) doMediaLike(m);
                if (action == MediaOptions.DELETE) doMediaDelete(m);
                if (action == MediaOptions.SHARE) doMediaShare(m);
                if (action == MediaOptions.UPDATE_STUDIO) updateMediaStudio(m);
            }
        });
    }

    protected void doMediaShare(MediaModel media) {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_STREAM, Uri.parse(media.dataPath));
        i.setType(MediaUtility.getMimeType(media.mediaType));

        startActivity(Intent.createChooser(i, getString(R.string.txt_share)));
    }

    @Override
    public void onPostAction(AbsMediaHelper helper, MediaOptions action) {
        onMediaAction(action, helper.getMediaList());
    }

    @Override
    public void onPreAction(AbsMediaHelper helper, MediaOptions action) {
    }
}

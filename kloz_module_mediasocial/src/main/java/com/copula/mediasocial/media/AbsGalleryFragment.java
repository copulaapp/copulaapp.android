package com.copula.mediasocial.media;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.media.AudioController;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.AbsListViewScrollListener;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ImplHelperScrollview;

import java.util.List;

/**
 * Created by heeleaz on 3/10/17.
 */
public abstract class AbsGalleryFragment<T extends MediaModel> extends AbsMediaFragment<T> implements
        IFragmentBackPress, AbsListViewScrollListener, AdapterView.OnItemClickListener {

    public static final int RESUME_RELOAD = -2, EMPTY_RELOAD = -3;
    private View vMainContentHolder, vListProgress, vEmptyList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.abs_media_gallery_activity, container, false);

        LinearLayout ml = (LinearLayout) v.findViewById(R.id.view_main_content);
        ml.addView(initGalleryView(inflater, container, si));

        vMainContentHolder = v.findViewById(R.id.view_main_content_holder);
        vListProgress = v.findViewById(R.id.view_list_progress);
        vEmptyList = v.findViewById(R.id.view_empty_list);

        v.findViewById(R.id.view_empty_list).setOnClickListener(new ClickListener());

        ImageView imageView = (ImageView) v.findViewById(R.id.img_empty_list_image);
        if (getMediaType() == MediaModel.AUDIO) {
            imageView.setImageResource(R.drawable.img_audio_studio_empty_256dp);
        } else if (getMediaType() == MediaModel.IMAGE) {
            imageView.setImageResource(R.drawable.img_image_studio_empty_256dp);
        } else if (getMediaType() == MediaModel.VIDEO) {
            imageView.setImageResource(R.drawable.img_video_studio_empty_256dp);
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AbsHelperListView listView = getAbsListView();
        if (listView != null) {
            listView.setHelperScrollListener(this);
            listView.setOnItemClickListener(this);
        }
    }

    public void loadMediaList(int cause) {
    }

    @Override
    public int getMediaType() {
        return 0;
    }

    public abstract View initGalleryView(LayoutInflater inflater, ViewGroup container, Bundle si);

    public abstract T getItemAtPosition(AdapterView parent, int position);

    public void playAudio(List<AudioModel> songList, int streamType, int position) {
        AudioController c = AudioController.getInstance(getActivity());
        c.setQueue(songList);
        c.setStreamType(streamType);
        c.startPlay(position, true);
    }

    protected void playAudioLocal(List<AudioModel> songList, int position) {
        playAudio(songList, AudioController.LOCAL, position);
    }

    protected AbsHelperListView getAbsListView() {
        return null;
    }

    protected BaseAdapter<T> getAdapter() {
        return null;
    }

    protected void onEmptyViewClick() {
        this.loadMediaList(AbsGalleryFragment.EMPTY_RELOAD);
    }

    protected void showEmptyListView() {
        if (vEmptyList != null) DesignHelper.showOnlyView(vEmptyList);
    }

    protected void showListProgress(boolean show) {
        if (vMainContentHolder == null || vListProgress == null) return;
        DesignHelper.showOnlyView(vMainContentHolder);
        if (show) vListProgress.setVisibility(View.VISIBLE);
        else vListProgress.setVisibility(View.GONE);
    }

    @Override
    public void onMediaAction(MediaOptions action, List<T> mediaModels) {
        BaseAdapter<T> adapter = getAdapter();
        if (adapter == null) return;
        if (action == MediaOptions.DELETE) adapter.removeHandlers(mediaModels);
    }

    protected void showMediaOptionActivity(T media, View v) {
        int flags = MediaOptionPopWindow.FLAG_VIEW
                | MediaOptionPopWindow.FLAG_LIKE | MediaOptionPopWindow.FLAG_SHARE;
        super.showMediaOptionActivity(media, v, flags);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        ImplHelperScrollview.notifyParentOnScrollStateChanged(this, view, scrollState);
    }

    @Override
    public void onPageScroll(int totalItem, int pageOffset, int pagerStep) {
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {
        ImplHelperScrollview.notifyParentOnScroll(this, view, scrollAxis);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        T media = getItemAtPosition(parent, position);
        if (view.getId() == AdapterAudioSongList.BTN_OPT) {
            showMediaOptionActivity(media, view);
        } else if (view.getId() == AdapterAudioSongList.BTN_STUDIO_OPT) {
            updateMediaStudio(media);
        }
    }

    @Override
    public boolean allowBackPressed() {
        return true;
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.view_empty_list) onEmptyViewClick();
        }
    }//END
}
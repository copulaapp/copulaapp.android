package com.copula.mediasocial.media;

import android.content.Context;
import android.view.View;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.genericlook.AbsPopupWindow;
import com.copula.mediasocial.R;

/**
 * Created by heeleaz on 3/10/17.
 */
public class MediaOptionPopWindow extends AbsPopupWindow implements AbsPopupWindow.PopupClickListener {

    public static final int FLAG_VIEW = 0b000001;
    public static final int FLAG_LIKE = 0b000010;
    public static final int FLAG_SHARE = 0b000100;
    public static final int FLAG_POST = 0b001000;
    public static final int FLAG_DOWNLOAD = 0b010000;
    public static final int FLAG_STUDIO = 0b0100000;
    public static final int FLAG_DELETE = 0b1000000;

    public static final int FLAG_ALL = 0b1111111;

    private int flags = FLAG_ALL;

    private MediaModel media;
    private MediaOptionListener optionListener;
    private Context context;

    MediaOptionPopWindow(Context context) {
        super(context);
        this.context = context;
    }

    public void setMedia(MediaModel media) {
        this.media = media;
    }


    public void setFlags(int flags) {
        this.flags = flags;
    }

    private boolean isAllowed(int flag) {
        return ((flags & flag) == flag);
    }

    public void show(View anchor, MediaOptionListener listener) {
        this.optionListener = listener;

        this._setup(anchor);
        show(this);
    }

    public void _setup(View anchorView) {
        super._setup(anchorView);

        if (isAllowed(FLAG_VIEW)) setupPlayModel(media);
        if (isAllowed(FLAG_LIKE)) setupLikeModel(media);

        //if (isAllowed(FLAG_POST)) addModel(R.string.act_post,
        //        R.drawable.ic_share_feed_grey_64dp, MediaOptions.POST);
        if (isAllowed(FLAG_STUDIO)) setupStudioUpdateModel(media);
        if (isAllowed(FLAG_DOWNLOAD)) addModel(R.string.txt_download,
                R.drawable.ic_download_2_grey_64dp, MediaOptions.DOWNLOAD);
        if (isAllowed(FLAG_SHARE)) addModel(R.string.act_share,
                R.drawable.ic_share_local_grey_64dp, MediaOptions.SHARE);
        if (isAllowed(FLAG_DELETE)) addModel(R.string.act_delete,
                R.drawable.ic_delete_grey_64px, MediaOptions.DELETE);

    }

    private void setupStudioUpdateModel(MediaModel media) {
        if (media.isGalleryMedia) {
            addModel((R.string.act_studio_remove),
                    R.drawable.ic_studio_remove_grey_64px, MediaOptions.UPDATE_STUDIO);
        } else {
            addModel((R.string.act_studio_add),
                    R.drawable.ic_studio_add_grey_64px, MediaOptions.UPDATE_STUDIO);
        }
    }

    private void setupLikeModel(MediaModel media) {
        if (media.isLiked) {
            addModel((R.string.act_unlike),
                    R.drawable.ic_like_inactive_grey_96px, MediaOptions.LIKE);
        } else {
            addModel((R.string.act_like),
                    R.drawable.ic_like_active_appcolor_96px, MediaOptions.LIKE);
        }
    }

    private void setupPlayModel(MediaModel media) {
        int d = R.drawable.ic_media_play_grey_128dp;
        if (media.mediaType != MediaModel.IMAGE) {
            addModel(R.string.action_play, d, MediaOptions.PLAY);
        } else {
            addModel(R.string.act_image_view, d, MediaOptions.PLAY);
        }
    }

    @Override
    public void onAction(int position, AbsPopupWindow.OptionModel option) {
        optionListener.onAction((MediaOptions) option.tag, media);
    }

    public interface MediaOptionListener {
        void onAction(MediaOptions options, MediaModel media);
    }
}

package com.copula.mediasocial.media;

import com.copula.functionality.localservice.media.MediaModel;

import java.util.List;

/**
 * Created by heeleaz on 6/20/17.
 */
public interface AbsMediaHelper<T extends MediaModel> {
    boolean isSuccessful();

    List<T> getMediaList();

    interface HelperCallback<T extends MediaModel> {

        void onPostAction(AbsMediaHelper<T> helper, MediaOptions action);

        void onPreAction(AbsMediaHelper<T> helper, MediaOptions action);
    }
}

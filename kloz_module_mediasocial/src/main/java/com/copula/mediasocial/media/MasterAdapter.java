package com.copula.mediasocial.media;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.support.android.view.widget.BaseAdapter;

/**
 * Created by heeleaz on 6/24/17.
 */
public abstract class MasterAdapter<T extends MediaModel> extends BaseAdapter<T> {
    public MasterAdapter(Context context) {
        super(context);
    }

    public abstract View initAdapterView(LayoutInflater inflater);

    public abstract void initChildViewHolder(View view);

    public abstract void setData(View view, int position, T media);
}

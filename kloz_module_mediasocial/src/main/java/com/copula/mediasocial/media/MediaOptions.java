package com.copula.mediasocial.media;

import java.io.Serializable;

/**
 * Created by heeleaz on 3/11/17.
 */
public enum MediaOptions implements Serializable {
    DOWNLOAD, PLAY, LIKE, SHARE, DELETE, UPDATE_STUDIO
}

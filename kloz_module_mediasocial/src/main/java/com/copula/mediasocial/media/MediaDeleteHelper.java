package com.copula.mediasocial.media;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.util.MediaContentResolver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 6/20/17.
 */
public class MediaDeleteHelper extends AsyncTask<Void, Void, Void> implements AbsMediaHelper {
    private MediaModel media;
    private Context context;

    private AbsMediaHelper.HelperCallback callback;

    public MediaDeleteHelper(Context ctx, MediaModel media, AbsMediaHelper.HelperCallback callback) {
        this.context = ctx;
        this.callback = callback;
        this.media = media;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null)
            callback.onPreAction(this, MediaOptions.DELETE);
    }

    @Override
    protected Void doInBackground(Void... params) {
        new File(media.dataPath).delete();
        String[] paths = new String[]{media.dataPath};
        MediaContentResolver.updateContentProvider(context, paths);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (callback != null) callback.onPostAction(this, MediaOptions.DELETE);
    }

    @Override
    public List<MediaModel> getMediaList() {
        List<MediaModel> medias = new ArrayList<>(1);
        medias.add(media);
        return medias;
    }

    @Override
    public boolean isSuccessful() {
        return true;
    }
}

package com.copula.mediasocial.download;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.copula.functionality.downloader.DaoUMediaDownload;
import com.copula.functionality.downloader.MediaDownloadClient;
import com.copula.functionality.downloader.MediaDownloadEntry;
import com.copula.functionality.downloader.MediaDownloadListener;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.util.MediaContentResolver;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.util.FileManager;

import java.io.File;
import java.util.List;

/**
 * Created by eliasigbalajobi on 2/9/16.
 */
public class UserDownloadsFragment extends AbsDownloadFragment<MediaDownloadEntry> implements
        MediaDownloadListener.Listener, AdapterView.OnItemClickListener {

    private AdapterUserDownloads mAdapter;
    private DaoUMediaDownload daoMediaDownload;

    private ListView listView;
    private View vEmptyList, vListProgress;

    public static UserDownloadsFragment instantiate() {
        return new UserDownloadsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_user_media_download, null, false);
        listView = (ListView) view.findViewById(R.id.lst);
        vEmptyList = view.findViewById(R.id.view_empty_list);
        vListProgress = view.findViewById(R.id.view_list_progress);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listView.setAdapter(mAdapter = new AdapterUserDownloads(getActivity()));
        listView.setOnItemClickListener(this);
        listView.setHelperScrollListener(this);

        daoMediaDownload = new DaoUMediaDownload(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        MediaDownloadClient.getInstance().addListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MediaDownloadClient.getInstance().removeListener(this);
    }

    @Override
    protected ListView getListView() {
        return listView;
    }

    @Override
    protected BaseAdapter<MediaDownloadEntry> getListAdapter() {
        return mAdapter;
    }

    @Override
    protected void onDeleteRequest(MediaDownloadEntry mediaModel) {
        FileManager.deleteFile(new File(mediaModel.downloadTo));
        daoMediaDownload.removeDownload(mediaModel);

        mAdapter.removeHandler(mediaModel);
        mAdapter.notifyDataSetChanged();
        if (mAdapter.getCount() <= 0) DesignHelper.showOnlyView(vEmptyList);

        String[] path = new String[]{mediaModel.downloadTo};
        MediaContentResolver.updateContentProvider(getActivity(), path);
    }

    @Override
    protected void onClearAllRequest() {
        daoMediaDownload.truncateTable();

        mAdapter.clear();
        mAdapter.notifyDataSetChanged();
        if (mAdapter.getCount() <= 0) DesignHelper.showOnlyView(vEmptyList);
    }

    @Override
    public void downloadEvent(MediaDownloadEntry download, int state) {
        if (state != MediaDownloadEntry.STATE_QUEUED) {
            if (state == MediaDownloadEntry.STATE_COMPLETED) {
                MediaDownloadEntry model
                        = daoMediaDownload.getDownload(download.downloadId);
                download = (model != null) ? model : download;
                download.downloadState = MediaDownloadEntry.STATE_COMPLETED;
            }
            mAdapter.setHandler(download);
        } else mAdapter.addHandler(download);

        mAdapter.notifyDataSetChangedWorkerThread();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final MediaDownloadEntry model = mAdapter.getItem(position);
        if (model == null) return;

        if (model.downloadState == MediaDownloadEntry.STATE_COMPLETED) {
            doMediaOpen(model);
        } else {
            MediaDownloadClient.getInstance().stopDownload(model.downloadId);
            onClickDelete(model);
        }
    }

    private List<MediaDownloadEntry> merger(List<MediaDownloadEntry> models) {
        if (models == null) return null;

        for (MediaDownloadEntry m : models) {
            if (m.mediaType == MediaModel.IMAGE) m.setMedia(getImageMedia(m));
            if (m.mediaType == MediaModel.AUDIO) m.setMedia(getAudioMedia(m));
            if (m.mediaType == MediaModel.VIDEO) m.setMedia(getVideoMedia(m));
        }

        return models;
    }//END

    @Override
    protected void fetchDownloadList(String limit, boolean isScrolled) {
        new FetchDownloadList(limit, isScrolled).execute();
    }

    private class FetchDownloadList extends AsyncTask<Void, Void, List<MediaDownloadEntry>> {
        private String limit;

        FetchDownloadList(String limit, boolean isScrolled) {
            this.limit = limit;
            if (!isScrolled) DesignHelper.showOnlyView(vListProgress);
        }

        @Override
        protected List<MediaDownloadEntry> doInBackground(Void... params) {
            return merger(daoMediaDownload.getAllDownloads(limit));
        }

        @Override
        protected void onPostExecute(List<MediaDownloadEntry> results) {
            if (results != null && results.size() > 0) {
                mAdapter.addOrReplaceHandlers(results);
                mAdapter.notifyDataSetChanged();
                DesignHelper.showOnlyView(listView);
            }

            if (mAdapter.getCount() == 0) DesignHelper.showOnlyView(vEmptyList);
        }
    }//END
}

package com.copula.mediasocial.download;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListPopupWindow;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.player.AudioPlayerExtActivity;
import com.copula.mediasocial.image.player.ImagePlayerMainActivity;
import com.copula.mediasocial.video.player.VideoPlayerMainActivity;
import com.copula.support.android.view.widget.AbsListViewScrollListener;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ListView;

import java.util.List;

/**
 * Created by heeleaz on 1/29/17.
 */
abstract class AbsDownloadFragment<M> extends Fragment implements AbsListViewScrollListener, AdapterView
        .OnItemLongClickListener {

    private List<AudioModel> audioModels;
    private List<VideoModel> videoModels;
    private List<ImageModel> imageModels;

    private ListPopupWindow listPopupWindow;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView listView = getListView();
        listView.setOnItemLongClickListener(this);
        listView.setHelperScrollListener(this);

        Context ctx = getActivity();
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC";
        imageModels = SystemMediaProvider.getImages(ctx, null, orderBy);

        orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC";
        videoModels = SystemMediaProvider.getVideos(ctx, null, orderBy);

        orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC";
        String slc = MediaStore.Audio.Media.IS_MUSIC + " = 1";
        audioModels = SystemMediaProvider.getAudios(ctx, slc, orderBy);

        listPopupWindow = new ListPopupWindow(getActivity());
        listPopupWindow.setAdapter(setupOptionAdapter());
        listPopupWindow.setWidth(DesignHelper.scaleInDP(getActivity(), 120));
        listPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        listPopupWindow.setModal(true);
    }

    private ArrayAdapter<String> setupOptionAdapter() {
        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.spnadp_studio_selector_opt);
        spinnerAdapter.add(getString(R.string.txt_open));
        spinnerAdapter.add(getString(R.string.act_delete));
        return spinnerAdapter;
    }

    @Override
    public void onStart() {
        super.onStart();
        int pagerStep = getListView().getPagerStep();
        fetchDownloadList(DesignHelper.page(0, pagerStep), false);
    }

    protected abstract ListView getListView();

    protected abstract BaseAdapter<M> getListAdapter();

    protected abstract void fetchDownloadList(String limit, boolean isScrolled);

    protected abstract void onDeleteRequest(M mediaModel);

    protected abstract void onClearAllRequest();

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onPageScroll(int totalItem, int pageOffset, int pagerStep) {
        fetchDownloadList(DesignHelper.page(pageOffset, pagerStep), true);
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        final M model = getListAdapter().getItem(position);
        if (model == null) return false;

        listPopupWindow.setAnchorView(view);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> p, View v, int pos, long id) {
                if (pos == 0) doMediaOpen(model);
                else if (pos == 1) onClickDelete(model);

                listPopupWindow.dismiss();
            }
        });
        listPopupWindow.show();
        return true;
    }

    AudioModel getAudioMedia(MediaModel media) {
        if (media.mediaId == 0) {
            for (AudioModel a : audioModels) {
                if (a.dataPath.equals(media.dataPath)) return a;
            }
        } else {
            int i = audioModels.indexOf(new AudioModel(media));
            if (i > -1) return audioModels.get(i);
        }
        return null;
    }

    VideoModel getVideoMedia(MediaModel media) {
        if (media.mediaId == 0) {
            for (VideoModel a : videoModels) {
                if (a.dataPath.equals(media.dataPath)) return a;
            }
        } else {
            int i = videoModels.indexOf(new VideoModel(media));
            if (i > -1) return videoModels.get(i);
        }
        return null;
    }

    ImageModel getImageMedia(MediaModel media) {
        if (media.mediaId == 0) {
            for (ImageModel a : imageModels) {
                if (a.dataPath.equals(media.dataPath)) return a;
            }
        } else {
            int i = imageModels.indexOf(new ImageModel(media));
            if (i > -1) return imageModels.get(i);
        }
        return null;
    }


    boolean doClearHistory() {
        if (getListAdapter().getCount() <= 0) return false;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.msg_confirm_action);
        DialogInterface.OnClickListener click = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    dialog.dismiss();
                } else if (which == DialogInterface.BUTTON_POSITIVE) {
                    onClearAllRequest();
                }
            }
        };
        builder.setNegativeButton(R.string.txt_no, click);
        builder.setPositiveButton(R.string.act_clear, click);
        builder.show();
        return true;
    }

    void onClickDelete(final M mediaModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.msg_confirm_action);
        DialogInterface.OnClickListener click = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    dialog.dismiss();
                } else if (which == DialogInterface.BUTTON_POSITIVE) {
                    onDeleteRequest(mediaModel);
                }
            }
        };
        builder.setNegativeButton(R.string.txt_no, click);
        builder.setPositiveButton(R.string.act_delete, click);
        builder.show();
    }

    protected void doMediaOpen(M mediaModel) {
        MediaModel m = (MediaModel) mediaModel;
        if (m.mediaType == MediaModel.AUDIO) {
            AudioPlayerExtActivity.launch(getActivity(), new AudioModel(m));
        } else if (m.mediaType == MediaModel.IMAGE) {
            ImagePlayerMainActivity.launch(getActivity(), new ImageModel(m));
        } else if (m.mediaType == MediaModel.VIDEO) {
            VideoPlayerMainActivity.launch(getActivity(), new VideoModel(m));
        }
    }//END
}

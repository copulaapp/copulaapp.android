package com.copula.mediasocial.download;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.copula.functionality.localservice.connection.client.LocalServerActivePersist;
import com.copula.genericlook.CustomActionBar;
import com.copula.mediasocial.R;
import com.copula.support.android.content.TabActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 1/29/17.
 */
public class MediaDownloadActivity extends TabActivity implements View.OnClickListener {
    public static final int USER_DOWNLOAD = 0, HOST_DOWNLOAD = 1;
    public static final String INTENT_PAGE = "page";

    private static final int USER_DOWNLOAD_STR = R.string.tab_user_media_download;
    private static final int HOST_DOWNLOAD_STR = R.string.tab_host_media_download;

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MediaDownloadActivity.class));
    }

    @Override
    protected List<Fragment> getPagerFragments() {
        List<Fragment> fragments = new ArrayList<>(2);
        fragments.add(UserDownloadsFragment.instantiate());
        fragments.add(HostDownloadFragment.instantiate());
        return fragments;
    }

    @Override
    protected ViewPager getViewPager() {
        return (ViewPager) findViewById(R.id.pager);
    }

    @Override
    protected void onPrepareActivityWindow() {
        setTabViewStyle(R.style.Widget_Button_TabSwitcher);
        getActionBarTab().setTextSelectedColor(Color.WHITE);
        getActionBarTab().setTextUnselectedColor(0xFF242424);

        addTab(USER_DOWNLOAD_STR).addTab(HOST_DOWNLOAD_STR);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_main_media_download);

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(R.string.label_downloads).setView(R.layout.bar_media_download);
        actionBar.compile();
        findViewById(R.id.btn_bar_back).setOnClickListener(this);
        findViewById(R.id.btn_clear_history).setOnClickListener(this);

        setCurrentPage(getIntent().getIntExtra(INTENT_PAGE, USER_DOWNLOAD));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_bar_back) {
            finish();
        } else if ((view.getId() == R.id.btn_clear_history)) {
            ((AbsDownloadFragment<?>)
                    getFragment(getCurrentItem())).doClearHistory();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentItem() == 1) setCurrentPage(0);
        else super.onBackPressed();
    }
}

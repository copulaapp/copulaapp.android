package com.copula.mediasocial.download;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.copula.functionality.downloader.MediaDownloadEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ListView;
import com.squareup.picasso.Picasso;

import java.util.Locale;

/**
 * Created by eliasigbalajobi on 2/9/16.
 */
class AdapterUserDownloads extends BaseAdapter<MediaDownloadEntry> {
    private int displayWidth;

    AdapterUserDownloads(Context context) {
        super(context);
        displayWidth = getContext().getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_user_media_download, null, false);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.fileName = (TextView) convertView.findViewById(R.id.txt_media_filename);
            holder.downloadState = (TextView) convertView.findViewById(R.id.txt_download_state);
            holder.fileSize = (TextView) convertView.findViewById(R.id.txt_media_size);
            holder.open = (Button) convertView.findViewById(R.id.btn_open_media);
            holder.progress = (ProgressBar) convertView.findViewById(R.id.prg_media_download);

            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        MediaDownloadEntry entry = getItem(position);
        holder.fileName.setText(entry.fileName);
        holder.fileSize.setText(MediaUtility.filesizeToString(entry.fileSize));

        int placeholder = DesignHelper.getPlaceholder(entry.mediaType);
        if (entry.downloadState == MediaDownloadEntry.STATE_PROGRESS) {
            prepareProgressView(holder, entry, placeholder);
        } else if (entry.downloadState == MediaDownloadEntry.STATE_QUEUED) {
            prepareQueuedView(holder, placeholder);
        } else if (entry.downloadState == MediaDownloadEntry.STATE_COMPLETED) {
            prepareCompletedView(holder, entry, placeholder);
        } else if (entry.downloadState == MediaDownloadEntry.ERROR) {
            prepareErrorView(holder, placeholder);
        }

        final View view = convertView;
        holder.open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int p = position;
                ((ListView) view.getParent()).performItemClick(v, p, p);
            }
        });

        return convertView;
    }

    private void prepareProgressView(ViewHolder holder, MediaDownloadEntry media, int placeholder) {
        showOnlyView(holder.progress);

        float dc = (media.downloadCount / 1048576f);
        float fs = (media.fileSize / 1048576f);

        holder.progress.setProgress((int) ((dc / fs) * 100));
        holder.downloadState.setText(String.format(Locale.getDefault(),
                "%.2f/%.2f%s", dc, fs, "MB"));
        holder.open.setText(R.string.act_cancel);
        Picasso.with(getContext()).load(placeholder).into(holder.thumbnail);
    }


    private void prepareQueuedView(ViewHolder holder, int placeholder) {
        showOnlyView(holder.fileSize);

        int grey = ContextCompat.getColor(getContext(), R.color.appGreyTextColor);
        holder.downloadState.setTextColor(grey);
        holder.downloadState.setText(R.string.msg_download_queued);
        holder.open.setText(R.string.act_cancel);
        Picasso.with(getContext()).load(placeholder).into(holder.thumbnail);
    }

    private void prepareCompletedView(ViewHolder holder, MediaDownloadEntry media
            , int placeholder) {
        showOnlyView(holder.fileSize);
        if (media.fileSize == 0) holder.fileSize.setVisibility(View.GONE);

        int grey = ContextCompat.getColor(getContext(), R.color.appGreyTextColor);
        holder.downloadState.setTextColor(grey);
        holder.downloadState.setText(R.string.msg_download_complete);
        holder.open.setText(R.string.txt_open);

        String url = MediaUtility.appendSize(media.thumbnailUrl, displayWidth / 6);
        Picasso.with(getContext()).load(url).placeholder(placeholder).into(holder.thumbnail);
    }

    private void prepareErrorView(ViewHolder holder, int placeholder) {
        holder.progress.setVisibility(View.GONE);
        holder.fileSize.setVisibility(View.GONE);

        int red = ContextCompat.getColor(getContext(), R.color.appColorPrimary);
        holder.downloadState.setTextColor(red);
        holder.downloadState.setText(R.string.msg_download_failed);
        holder.open.setText(R.string.act_delete);
        Picasso.with(getContext()).load(placeholder).into(holder.thumbnail);
    }

    private class ViewHolder {
        ImageView thumbnail;
        TextView fileName, fileSize, downloadState;
        Button open;
        ProgressBar progress;
    }
}

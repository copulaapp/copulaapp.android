package com.copula.mediasocial.download;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.copula.functionality.downloader.HMediaDownloadEntry;
import com.copula.functionality.localservice.profile.DaoConnectsProfile;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.util.Formatter;
import com.squareup.picasso.Picasso;

/**
 * Created by heeleaz on 1/29/17.
 */
class AdapterHostDownloads extends BaseAdapter<HMediaDownloadEntry> {
    private DaoConnectsProfile DAOConnectsProfile;
    private int displayWidth;

    AdapterHostDownloads(Context context) {
        super(context);
        DAOConnectsProfile = new DaoConnectsProfile(context);
        displayWidth = getContext().getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.
                    inflate(R.layout.adp_host_media_download, null, false);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.fileName = (TextView) convertView.findViewById(R.id.txt_media_filename);
            holder.createdAt = (TextView) convertView.findViewById(R.id.txt_created_at);
            holder.username = (TextView) convertView.findViewById(R.id.txt_username);

            Drawable d = DesignHelper.
                    scaleDrawable(getContext(), R.drawable.img_person_black_48dp, 12, 12);
            holder.username.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);

            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        HMediaDownloadEntry entry = getItem(position);
        holder.fileName.setText(entry.mediaTitle);

        String time = Formatter.toElapsedDate(entry.createdAt);
        holder.createdAt.setText(String.format("%s %s", time, getString(R.string.txt_ago)));

        UserProfileEntry fp = DAOConnectsProfile.getConnect(entry.userId);
        if (fp != null) holder.username.setText(fp.username);
        else holder.username.setText(R.string.txt_a_friend);


        int placeholder = DesignHelper.getPlaceholder(entry.mediaType);
        String thumbnailUrl = MediaUtility
                .appendSize(entry.thumbnailUrl, displayWidth / 6);
        Picasso.with(getContext()).load(thumbnailUrl)
                .placeholder(placeholder).into(holder.thumbnail);

        return convertView;
    }

    private class ViewHolder {
        ImageView thumbnail;
        TextView fileName, createdAt, username;
    }
}

package com.copula.mediasocial.download;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.downloader.DaoHMediaDownload;
import com.copula.functionality.downloader.HMediaDownloadEntry;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ListView;

import java.util.List;

/**
 * Created by heeleaz on 1/29/17.
 */
public class HostDownloadFragment extends AbsDownloadFragment<HMediaDownloadEntry> implements
        AdapterView.OnItemClickListener {
    private ListView listView;
    private View vEmptyList, vListProgress;
    private DaoHMediaDownload daoHostDownload;
    private AdapterHostDownloads adapter;

    public static HostDownloadFragment instantiate() {
        return new HostDownloadFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_host_media_download, null, false);
        listView = (ListView) view.findViewById(R.id.lst);
        vEmptyList = view.findViewById(R.id.view_empty_list);
        vListProgress = view.findViewById(R.id.view_list_progress);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView.setAdapter(adapter = new AdapterHostDownloads(getActivity()));
        listView.setOnItemClickListener(this);
        listView.setHelperScrollListener(this);

        UserAccountBase daoProfile = new UserAccountBase(getActivity());
        UserSession.setHostProfile(daoProfile.getUserProfile());

        daoHostDownload = new DaoHMediaDownload(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        UserAccountBase daoProfile = new UserAccountBase(getActivity());
        UserSession.setHostProfile(daoProfile.getUserProfile());
    }

    @Override
    protected ListView getListView() {
        return listView;
    }

    @Override
    protected BaseAdapter<HMediaDownloadEntry> getListAdapter() {
        return adapter;
    }

    @Override
    protected void fetchDownloadList(String limit, boolean isScrolled) {
        new FetchDownloadList(limit, isScrolled).execute();
    }

    @Override
    protected void onDeleteRequest(HMediaDownloadEntry mediaModel) {
        daoHostDownload.removeDownload(mediaModel);

        adapter.removeHandler(mediaModel);
        adapter.notifyDataSetChanged();
        if (adapter.getCount() <= 0) DesignHelper.showOnlyView(vEmptyList);
    }

    @Override
    protected void onClearAllRequest() {
        daoHostDownload.truncateTable();
        DesignHelper.showOnlyView(vEmptyList);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HMediaDownloadEntry model = adapter.getItem(position);
        if (model != null) doMediaOpen(model);
    }

    private List<HMediaDownloadEntry> merger(List<HMediaDownloadEntry> models) {
        if (models == null) return null;

        for (HMediaDownloadEntry m : models) {
            if (m.mediaType == MediaModel.IMAGE) m.setMedia(getImageMedia(m));
            if (m.mediaType == MediaModel.AUDIO) m.setMedia(getAudioMedia(m));
            if (m.mediaType == MediaModel.VIDEO) m.setMedia(getVideoMedia(m));
        }

        return models;
    }//END

    private class FetchDownloadList extends AsyncTask<Void, Void, List<HMediaDownloadEntry>> {
        private String limit;
        private boolean isScrolled;

        FetchDownloadList(String limit, boolean isScrolled) {
            this.limit = limit;
            this.isScrolled = isScrolled;
            if (!isScrolled) DesignHelper.showOnlyView(vListProgress);
        }

        @Override
        protected List<HMediaDownloadEntry> doInBackground(Void... params) {
            return merger(daoHostDownload.getAllDownloads(limit));
        }

        @Override
        protected void onPostExecute(List<HMediaDownloadEntry> results) {
            if (results != null && results.size() > 0) {
                adapter.addOrReplaceHandlers(results);
                adapter.notifyDataSetChanged();
                DesignHelper.showOnlyView(listView);
            } else if (!isScrolled) DesignHelper.showOnlyView(vEmptyList);
        }
    }//END
}

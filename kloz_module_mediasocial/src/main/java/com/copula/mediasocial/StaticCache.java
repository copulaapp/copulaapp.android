package com.copula.mediasocial;

import com.copula.functionality.localservice.media.MediaModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by heeleaz on 7/22/17.
 */
public class StaticCache<T extends MediaModel> {
    private static StaticCache sInstance;

    private Map<String, List<T>> cacheList = new HashMap<>();

    public static void initialize() {
        if (sInstance == null) sInstance = new StaticCache();
    }

    public static StaticCache getInstance() {
        initialize();
        return sInstance;
    }

    public List<T> getCache(String key) {
        return cacheList.get(key);
    }

    public void cache(String key, List<T> m) {
        cacheList.put(key, new ArrayList<>(m));
    }
}

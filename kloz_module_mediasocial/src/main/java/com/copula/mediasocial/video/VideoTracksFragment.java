package com.copula.mediasocial.video;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.media.AbsGalleryFragment;
import com.copula.mediasocial.media.MediaOptions;
import com.copula.mediasocial.video.player.VideoPlayerMainActivity;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.ListView;

import java.util.List;

public class VideoTracksFragment extends AbsGalleryFragment<VideoModel> implements OnItemClickListener,
        GetVideosHelper.Callback {
    private ListView listView;
    private AdapterVideoMediaList adapter;

    public static VideoTracksFragment instantiate() {
        return new VideoTracksFragment();
    }

    @Override
    public View initGalleryView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.act_video_library_list, container, false);
        (listView = (ListView) v.findViewById(R.id.lst)).setOnItemClickListener(this);

        int h = DesignHelper.scaleInDP(getActivity(), 70);
        DesignHelper.bottomSpace(listView, h);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listView.setAdapter(adapter = getAdapter());
        this.loadMediaList(AbsGalleryFragment.EMPTY_RELOAD);
    }

    @Override
    protected AbsHelperListView getAbsListView() {
        return listView;
    }

    @Override
    protected AdapterVideoMediaList getAdapter() {
        if (listView.getTag() == null) {
            listView.setTag(new AdapterVideoMediaList(getActivity()));
        }

        return (AdapterVideoMediaList) listView.getTag();
    }

    @Override
    public void onResume() {
        super.onResume();
        listView.resumeLastPosition();
    }

    @Override
    public void loadMediaList(int cause) {
        if (cause == EMPTY_RELOAD || cause == 0)
            new GetVideosHelper(getActivity(), this).execute();
    }

    @Override
    public int getMediaType() {
        return MediaModel.VIDEO;
    }

    protected void play(int position) {
        VideoPlayerMainActivity.launch(getActivity(), adapter.getItems(), position);
    }

    @Override
    public VideoModel getItemAtPosition(AdapterView parent, int position) {
        return (VideoModel) parent.getItemAtPosition(position);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        super.onItemClick(parent, view, position, id);

        if (view.getId() == AdapterAudioSongList.PLAY) play(position);
    }

    @Override
    public void onMediaAction(MediaOptions action, List<VideoModel> models) {
        if (action == MediaOptions.PLAY) play(adapter.indexOf(models.get(0)));
        else if (action == MediaOptions.UPDATE_STUDIO ||
                action == MediaOptions.LIKE) {
            adapter.notifyDataSetChanged();
        } else super.onMediaAction(action, models);
    }

    @Override
    public void onPreFetchVideos() {
        showListProgress(true);
    }

    @Override
    public void onFetchedVideos(List<VideoModel> media) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        showListProgress(false);

        if (media != null && media.size() > 0) {
            adapter.addOrReplaceHandlers(media);
            adapter.notifyDataSetChanged();
        }
        if (adapter.getCount() == 0) showEmptyListView();
    }
}
package com.copula.mediasocial.video.player;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.*;
import android.view.SurfaceHolder.Callback;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.Toast;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.media.MediaPlayerInterceptor;
import com.copula.functionality.media.VideoController;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.mediasocial.media.AbsMediaFragment;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public abstract class PlayControllerFragment extends AbsMediaFragment<VideoModel> implements OnClickListener,
        Callback, OnCompletionListener, MediaPlayer.OnBufferingUpdateListener, SeekBar.OnSeekBarChangeListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayerInterceptor.InterceptorListener {
    private static final String SAVED_SEEK = "current_position";

    public static List<VideoModel> mediaList = new ArrayList<>();
    SurfaceView mSurfaceView;
    SurfaceHolder mSurfaceHolder;
    private View vProgressBuffer;

    private Handler mHandler;
    private VideoController videoController;
    private TextView txtMediaDuration, txtCurrentDuration, txtMediaTitle;
    private Button btnPlayPause;
    private SeekBar seekbar;
    private boolean playCompleted;
    private MediaPlayerInterceptor mMediaPlayerInterceptor;

    protected VideoController getController() {
        return videoController;
    }

    @Override
    public int getMediaType() {
        return MediaModel.VIDEO;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.act_video_player_main, container, false);
        txtMediaDuration = (TextView) v.findViewById(R.id.txt_media_duration);
        txtCurrentDuration = (TextView) v.findViewById(R.id.txt_played_duration);
        txtMediaTitle = (TextView) v.findViewById(R.id.txt_media_title);

        vProgressBuffer = v.findViewById(R.id.prg_media_prepare_buffer);
        v.findViewById(R.id.btn_next_media).setOnClickListener(this);
        v.findViewById(R.id.btn_prev_media).setOnClickListener(this);
        v.findViewById(R.id.view_surface_holder_parent).setOnClickListener(this);

        (seekbar = (SeekBar) v.findViewById(R.id.seekbar)).setOnSeekBarChangeListener(this);
        (btnPlayPause = (Button) v.findViewById(R.id.btn_play_pause)).setOnClickListener(this);
        (mSurfaceView = (SurfaceView) v.findViewById(R.id.surface)).setOnClickListener(this);
        (mSurfaceHolder = mSurfaceView.getHolder()).addCallback(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AudioController.getInstance(getActivity()).pause();
        (videoController = new VideoController()).setScreenOnWhilePlaying(true);
        videoController.setOnBufferingUpdateListener(this);
        videoController.setOnPreparedListener(this);
        videoController.setOnErrorListener(this);
        videoController.setOnCompletionListener(this);
        videoController.setStreamType(getStreamType());

        videoController.setPlayList(mediaList, getArguments().getInt("position"));

        this.mHandler = new Handler(getActivity().getMainLooper());
        this.mMediaPlayerInterceptor = new MediaPlayerInterceptor(getActivity());
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        videoController.setDisplay(holder);


        videoController.prepare(videoController.getCurrentTrack());

    }

    protected abstract int getStreamType();

    @Override
    public void onResume() {
        super.onResume();
        mMediaPlayerInterceptor.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (videoController != null) videoController.pause();
        mMediaPlayerInterceptor.unregister();

        btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
    }


    protected void prepareMediaPlayerView(VideoModel media) {
        if (media == null) return;

        txtMediaTitle.setText(media.mediaTitle);
        txtMediaDuration.setText(MediaUtility.formatDuration(media.duration));


        if (videoController.isPlaying()) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_white_128dp);
        } else {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
        }

        media.height = media.height == 0 ? 9 : media.height;
        media.width = media.width == 0 ? 16 : media.width;

        //hide prev & next media since it is just one media present in list
        if (videoController.getTracksCount() < 2) {
            getView().findViewById(R.id.btn_next_media).setVisibility(View.GONE);
            getView().findViewById(R.id.btn_prev_media).setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_SEEK, videoController.getCurrentPosition());
    }

    /**
     * Method which updates the SeekBar primary progress by current video playing position
     */
    private synchronized void primarySeekBarProgressUpdater(int waitMilli) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (videoController == null || !videoController.isPlaying()) return;

                int position = videoController.getCurrentPosition();
                long duration = videoController.getDuration();
                seekbar.setProgress((int) ((position / (duration * 1F) * 100)));
                txtCurrentDuration.setText(MediaUtility.formatDuration(position));
                primarySeekBarProgressUpdater(1000);
            }
        }, waitMilli);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        playCompleted = false;

        VideoModel video = videoController.getCurrentTrack();

        videoController.start();

        if (getView() == null) return;//parent view as been destroyed
        vProgressBuffer.setVisibility(View.GONE);
        primarySeekBarProgressUpdater(0);
        txtMediaDuration.setText(MediaUtility.formatDuration(video.duration));
        btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_white_128dp);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        seekbar.setSecondaryProgress(percent);
    }

    private void doMediaPlayPause() {
        if (videoController.isPlaying()) {
            videoController.pause();
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
        } else if (playCompleted) {
            prepareMediaPlayerView(videoController.getCurrentTrack());
            videoController.prepare(videoController.getCurrentTrack());
        } else {
            videoController.start();
            primarySeekBarProgressUpdater(0);
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_white_128dp);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        videoController.stop();
        videoController.reset();

        if (getView() == null) return false;

        dispatchMessage(getString(R.string.msg_media_play_error_unknown));
        vProgressBuffer.setVisibility(View.GONE);
        return false;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (videoController != null) videoController.stop();
    }

    @Override
    public void onHoldIntercept(int actor) {
        videoController.pause();
    }

    @Override
    public void onFreeIntercept(int actor) {
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int seekToMill = (int) ((progress / 100f) * videoController.getDuration());
            videoController.seekTo(seekToMill);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        seekbar.setProgress(100);//fill completed progress bar
        btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
        txtCurrentDuration.setText(MediaUtility.formatDuration(mp.getDuration()));
        playCompleted = true;
    }

    private void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }


    @Override
    public void onClick(View v) {
        if (v == btnPlayPause) doMediaPlayPause();
        else if (v.getId() == R.id.btn_next_media) {
            prepareMediaPlayerView(videoController.getNextMedia());
            videoController.prepareNextMedia();
        } else if (v.getId() == R.id.btn_prev_media) {
            prepareMediaPlayerView(videoController.getPrevMedia());
            videoController.preparePrevMedia();
        }
    }

    protected void terminate() {
        try {
            //illegal state exception may be thrown if media is never started
            videoController.destroy();
            videoController = null;
        } catch (Exception e) {
        }
    }//end
}
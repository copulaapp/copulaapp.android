package com.copula.mediasocial.video.player.ext;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.media.VideoController;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.video.player.VideoPlayerFragment;

/**
 * Created by heeleaz on 6/4/17.
 */
public class VideoPlayerExtActivity extends FragmentActivity {
    public static void launch(Context ctx, String mime, String path) {
        Intent intent = new Intent(ctx, VideoPlayerExtActivity.class);
        intent.putExtra("mime", mime).putExtra("path", path);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DesignHelper.activityFullscreen(this);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_gen_fragement_holder);

        String dataString = getIntent().getDataString();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, PlayerFragment.instantiate(dataString));
        ft.commit();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public static class PlayerFragment extends VideoPlayerFragment {
        public static PlayerFragment instantiate(String dataString) {
            PlayerFragment playerFragment = new PlayerFragment();
            Bundle args = new Bundle(1);
            args.putString("data", dataString);
            playerFragment.setArguments(args);
            return playerFragment;
        }

        @Override
        public int getMediaType() {
            return MediaModel.VIDEO;
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            btnLike.setVisibility(View.GONE);
            btnDownload.setVisibility(View.GONE);

            String dataString = getArguments().getString("data");
            int mediaId = MediaUtility.getMediaIdFromUri(getActivity(), Uri.parse(dataString));
            MediaModel media = SystemMediaProvider.getVideo(getActivity(), mediaId);

            getController().setPlayList(new VideoModel(media));
            getController().setStreamType(VideoController.LOCAL);
        }
    }//END
}

package com.copula.mediasocial.video;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.media.MasterAdapter;
import com.copula.support.android.view.widget.Button;
import com.squareup.picasso.Picasso;

public class AdapterVideoMediaList extends MasterAdapter<VideoModel> {
    private int displayWidth;
    private ViewHolder holder;

    public AdapterVideoMediaList(Context context) {
        super(context);
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        if (convertView == null) {
            initChildViewHolder(convertView = initAdapterView(inflater));
            convertView.setTag(this.holder);
        } else holder = (ViewHolder) convertView.getTag();

        setData(convertView, position, getItem(position));
        return convertView;
    }

    @Override
    public View initAdapterView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.adp_video_library_list, null, false);
    }

    @Override
    public void initChildViewHolder(View view) {
        holder = new ViewHolder();
        holder.thumbnail = (ImageView) view.findViewById(R.id.img_media_thumbnail);
        holder.option = (Button) view.findViewById(AdapterAudioSongList.BTN_OPT);
        holder.title = (TextView) view.findViewById(R.id.txt_media_title);
        holder.size = (TextView) view.findViewById(R.id.txt_media_size);
        holder.duration = (TextView) view.findViewById(R.id.txt_media_duration);
        holder.likeCount = (TextView) view.findViewById(R.id.txt_like_count);
        holder.studio = (Button) view.findViewById(AdapterAudioSongList.BTN_STUDIO_OPT);
        holder.download = (Button) view.findViewById(AdapterAudioSongList.BTN_DOWNLOAD);
        holder.selectIndicator = (ImageView) view.findViewById(R.id.img_select_indicator);

        DesignHelper.showOnlyView(holder.studio);

    }

    @Override
    public void setData(final View view, final int position, VideoModel media) {
        holder.title.setText(media.mediaTitle);
        holder.duration.setText(MediaUtility.formatDuration(media.duration));
        holder.likeCount.setText(AdapterAudioSongList.formatLike(media.likeCount));
        holder.size.setText(AdapterAudioSongList.formatFileSize(getContext(), media.fileSize));

        AdapterAudioSongList.studioSelectOpt(media.isGalleryMedia, holder.studio);

        loadThumbnailImage(holder.thumbnail, media);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbsListView listView = ((AbsListView) view.getParent());
                if (listView != null) listView.performItemClick(v, position, position);
            }
        };
        holder.option.setOnClickListener(listener);
        holder.studio.setOnClickListener(listener);
        holder.download.setOnClickListener(listener);
    }

    public void loadThumbnailImage(ImageView thumbView, VideoModel media) {
        String url = MediaUtility.appendSize(media.thumbnailUrl, displayWidth / 6);
        Picasso.with(getContext()).load(url)
                .placeholder(R.drawable.img_media_video_thumb2_256dp).into(thumbView);
    }

    public class ViewHolder {
        public Button option, studio, download;
        ImageView thumbnail, selectIndicator;
        TextView title, duration, size, likeCount;
    }
}

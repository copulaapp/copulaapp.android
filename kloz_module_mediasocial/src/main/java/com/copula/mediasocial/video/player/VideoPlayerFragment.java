package com.copula.mediasocial.video.player;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.media.VideoController;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayMainFragment;
import com.copula.mediasocial.media.MediaOptions;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.view.widget.Button;

import java.util.List;

/**
 * Created by heeleaz on 6/7/17.
 */
public class VideoPlayerFragment extends PlayControllerFragment implements IFragmentBackPress {
    protected Button btnLike, btnDownload, btnStudioOpt;
    private View vControlHeader, vControlFooter;

    private boolean screenLock;
    private boolean fullAspectRatio = false;

    private ControllerHideRunnable controllerHideRunnable = new ControllerHideRunnable();
    private Handler handler = new Handler(Looper.getMainLooper());

    public static VideoPlayerFragment instantiate(List<VideoModel> m, int p) {
        Bundle args = new Bundle(1);
        args.putInt("position", p);
        PlayControllerFragment.mediaList = m;

        VideoPlayerFragment fragment = new VideoPlayerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = super.onCreateView(inflater, container, si);

        v.findViewById(R.id.btn_media_share).setOnClickListener(this);
        v.findViewById(R.id.btn_aspect_ratio).setOnClickListener(this);
        v.findViewById(R.id.btn_locked).setOnClickListener(this);
        v.findViewById(R.id.btn_lock).setOnClickListener(this);
        v.findViewById(R.id.btn_screen_orientation).setOnClickListener(this);
        vControlFooter = v.findViewById(R.id.view_media_player_foot);
        vControlHeader = v.findViewById(R.id.view_media_player_head);

        (btnLike = (Button)
                v.findViewById(R.id.btn_like)).setOnClickListener(this);
        (btnDownload = (Button)
                v.findViewById(R.id.btn_download)).setOnClickListener(this);
        (btnStudioOpt = (Button)
                v.findViewById(R.id.btn_studio_opt)).setOnClickListener(this);

        DesignHelper.showOnlyView(btnStudioOpt);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        surfaceLayoutHideInDelay(5000);
    }

    @Override
    protected int getStreamType() {
        return VideoController.LOCAL;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        VideoModel media = getController().getCurrentTrack();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setLandscapeVideoAspectRatio(media.width, media.height);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setPortraitVideoAspectRatio(media.width, media.height);
        }
    }

    private void setPortraitVideoAspectRatio(int aspectWidth, int aspectHeight) {
        float aspectRatio = (aspectHeight / (aspectWidth * 1F));

        int scaledWidth = getResources().getDisplayMetrics().widthPixels;
        int scaledHeight = (int) (aspectRatio * scaledWidth);

        mSurfaceHolder.setFixedSize(scaledWidth, scaledHeight);
    }

    private void setLandscapeVideoAspectRatio(int aspectWidth, int aspectHeight) {
        float aspectRatio = (aspectWidth / (aspectHeight * 1F));

        int scaledHeight = getResources().getDisplayMetrics().heightPixels;
        int scaledWidth = (int) (scaledHeight * aspectRatio);

        mSurfaceHolder.setFixedSize(scaledWidth, scaledHeight);
    }

    private void doStudioAction() {
        VideoModel m = getController().getCurrentTrack();
        updateMediaStudio(m);
        AudioPlayMainFragment.studioSelectOpt(m.isGalleryMedia, btnStudioOpt);
    }

    @Override
    protected void prepareMediaPlayerView(VideoModel media) {
        super.prepareMediaPlayerView(media);
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setLandscapeVideoAspectRatio(media.width, media.height);
        } else setPortraitVideoAspectRatio(media.width, media.height);

        AudioPlayMainFragment.likeActionOpt(media.isLiked, btnLike);
        AudioPlayMainFragment.studioSelectOpt(media.isGalleryMedia, btnStudioOpt);
    }


    private void switchAspectRatio(VideoModel media) {
        int w = fullAspectRatio ? 1 : media.width;
        int h = fullAspectRatio ? 1 : media.height;

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setLandscapeVideoAspectRatio(w, h);
        } else setPortraitVideoAspectRatio(w, h);

        fullAspectRatio = !fullAspectRatio;
    }

    private void onClickScreenLock() {
        this.onClickSurfaceLayout();

        View vLocked = getView().findViewById(R.id.btn_locked);
        if (screenLock = !screenLock) vLocked.setVisibility(View.VISIBLE);
        else vLocked.setVisibility(View.GONE);
    }

    private void onClickSurfaceLayout() {
        if (screenLock) return;//do not perform operation
        handler.removeCallbacks(controllerHideRunnable);

        showControlView(vControlHeader.getVisibility() != View.VISIBLE);
        this.surfaceLayoutHideInDelay(5000);//hide in 5s time
    }

    private void surfaceLayoutHideInDelay(int delayMillis) {
        handler.postDelayed(controllerHideRunnable, delayMillis);
    }

    private void switchScreenOrientation() {
        int orientation = getResources().getConfiguration().orientation;
        Activity act = getActivity();
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        super.onCompletion(mp);
        if (screenLock) onClickScreenLock();

        handler.removeCallbacks(controllerHideRunnable);
        if (vControlHeader.getVisibility() == View.GONE) showControlView(true);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        super.onPrepared(mp);
        prepareMediaPlayerView(getController().getCurrentTrack());
    }

    @Override
    public void onMediaAction(MediaOptions action, List<VideoModel> models) {
        if (action == MediaOptions.LIKE) {
            AudioPlayMainFragment.likeActionOpt(models.get(0).isLiked, btnLike);
        }
    }

    @Override
    public void onPause() {
        super.onPause();//cancel control hider. this is to avoid NPE
        handler.removeCallbacks(controllerHideRunnable);
    }

    @Override
    public boolean allowBackPressed() {
        return !screenLock;
    }

    @Override
    public void onClick(View v) {
        VideoModel media = getController().getCurrentTrack();
        if (v.getId() == R.id.btn_screen_orientation) switchScreenOrientation();
        else if (v == btnLike) doMediaLike(media);
        else if (v == btnStudioOpt) doStudioAction();
        else if (v.getId() == R.id.btn_media_share) doMediaShare(media);
        else if (v.getId() == R.id.view_surface_holder_parent || v == mSurfaceView) {
            onClickSurfaceLayout();
        } else if (v.getId() == R.id.btn_lock || v.getId() == R.id.btn_locked) {
            onClickScreenLock();
        } else if (v.getId() == R.id.btn_aspect_ratio) switchAspectRatio(media);
        else super.onClick(v);
    }

    private void showControlView(boolean show) {
        if (show) {
            vControlFooter.setVisibility(View.VISIBLE);
            vControlHeader.setVisibility(View.VISIBLE);
        } else {
            vControlHeader.setVisibility(View.GONE);
            vControlFooter.setVisibility(View.GONE);
        }
    }

    private class ControllerHideRunnable implements Runnable {
        @Override
        public void run() {
            if (vControlHeader.getVisibility() == View.VISIBLE) showControlView(false);
        }
    }
}

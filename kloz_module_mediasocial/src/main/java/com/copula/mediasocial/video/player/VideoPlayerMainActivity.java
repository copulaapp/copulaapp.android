package com.copula.mediasocial.video.player;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 5/31/17.
 */
public class VideoPlayerMainActivity extends FragmentActivity {
    protected static List<VideoModel> media;
    private VideoPlayerFragment playerFragment;

    public static void launch(Context ctx, VideoModel video) {
        List<VideoModel> media = new ArrayList<>(1);
        media.add(video);

        launch(ctx, media, 0);
    }

    public static void launch(Context ctx, List<VideoModel> videos, int start) {
        Intent intent = new Intent(ctx, VideoPlayerMainActivity.class);

        VideoPlayerMainActivity.media = videos;
        ctx.startActivity(intent.putExtra("startPosition", start));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DesignHelper.activityFullscreen(this);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_gen_fragement_holder);

        int position = getIntent().getIntExtra("startPosition", 0);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, playerFragment = getPlayerFragment(position));
        ft.commit();
    }

    protected VideoPlayerFragment getPlayerFragment(int p) {
        return VideoPlayerFragment.instantiate(media, p);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (playerFragment != null) playerFragment.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        playerFragment.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        if (playerFragment != null) playerFragment.terminate();
        super.onDestroy();
    }//end

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

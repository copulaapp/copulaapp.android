package com.copula.mediasocial.video;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.MediaStore;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.V2SystemMediaQuery;

import java.util.List;

/**
 * Created by heeleaz on 7/11/17.
 */
public class GetVideosHelper extends AsyncTask<Void, Void, List<VideoModel>> {
    private Callback callback;
    private Context context;

    public GetVideosHelper(Context ctx, Callback callback) {
        this.context = ctx;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callback != null) callback.onPreFetchVideos();
    }

    @Override
    protected List<VideoModel> doInBackground(Void... voids) {
        V2SystemMediaQuery v2Query = new V2SystemMediaQuery(context);
        String orderBy = MediaStore.Video.Media.DATE_ADDED + " DESC";
        return v2Query.getVideoMedia(null, orderBy);
    }

    @Override
    protected void onPostExecute(List<VideoModel> media) {
        if (callback != null) callback.onFetchedVideos(media);
    }

    public interface Callback {
        void onPreFetchVideos();

        void onFetchedVideos(List<VideoModel> media);
    }
}

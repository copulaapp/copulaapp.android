package com.copula.mediasocial;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.copula.functionality.IContextBootstrap;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.profile.DaoSharedCollection;
import com.copula.functionality.media.AudioController;
import com.copula.mediasocial.audio.songlist.GetAudioMedia;
import com.copula.mediasocial.video.GetVideosHelper;

import java.util.List;

/**
 * Created by heeleaz on 7/9/17.
 */
public class SocialMediaBootstrap implements GetAudioMedia.Callback, IContextBootstrap, GetVideosHelper.Callback {
    public static final int EMPTY_COLLECTION = 0x11123;
    private static final String TAG = SocialMediaBootstrap.class.getSimpleName();
    private String cacheKey;
    private Context context;

    private AudioController audioController;
    private BootstrapListener listener;

    public SocialMediaBootstrap(Context context) {
        this.context = context;
        this.audioController = AudioController.getInstance(context);
    }

    public static String makeCacheKey(String cacheKey, int mediaType) {
        return cacheKey + "_" + mediaType;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    @Override
    public void onPreFetch() {
        Log.d(TAG, "Bootstrapping Audio Playlist");
    }

    @Override
    public void onPostFetched(List<AudioModel> media) {
        if (media != null && media.size() > 0) {
            String cacheKey = makeCacheKey(MediaModel.AUDIO);
            StaticCache.getInstance().cache(cacheKey, media);
            audioController.setQueue(media);

            int lastTrackId = audioController.getLastTrackMediaId();
            int index = media.indexOf(new AudioModel(lastTrackId));
            if (index != -1) audioController.setPlayPosition(index);

            Log.d(TAG, "Bootstrapping Completed, Size: " + media.size());
        } else {
            Log.d(TAG, "Bootstrapping Completed, Size: " + 0);
        }

        if (listener != null)
            listener.onBootstrapCompleted(SocialMediaBootstrap.class);
    }

    @Override
    public void onPreFetchVideos() {
    }

    @Override
    public void onFetchedVideos(List<VideoModel> media) {
        if (media != null && media.size() > 0) {
            String cacheKey = makeCacheKey(MediaModel.VIDEO);
            StaticCache.getInstance().cache(cacheKey, media);
            Log.d(TAG, "Videos fetch Completed, Size: " + media.size());
        }
    }

    private void notifyOnEmptySharedCollection() {
        DaoSharedCollection mediaList = new DaoSharedCollection(context);
        if (mediaList.getCount() == 0) {
            NotificationHelper.getInstance().dispatch(EMPTY_COLLECTION, null);
        }
    }

    @Override
    public void boot(BootstrapListener listener) {
        if ((this.listener = listener) != null)
            listener.onBootstrapStarted(SocialMediaBootstrap.class);

        this.notifyOnEmptySharedCollection();

        String key = makeCacheKey(MediaModel.AUDIO);
        List cached = StaticCache.getInstance().getCache(key);
        if (cached == null || cached.size() == 0) {
            executeAudioFetchAsync(this);
            executeVideoFetchAsync(this);
        } else {
            if (listener != null)
                listener.onBootstrapCompleted(SocialMediaBootstrap.class);
        }
    }//END

    protected void executeVideoFetchAsync(GetVideosHelper.Callback callback) {
        GetVideosHelper v = new GetVideosHelper(context, callback);
        v.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    protected void executeAudioFetchAsync(GetAudioMedia.Callback callback) {
        GetAudioMedia a = new GetAudioMedia(context, 0, callback);
        a.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private String makeCacheKey(int mediaType) {
        return makeCacheKey(cacheKey, mediaType);
    }
}

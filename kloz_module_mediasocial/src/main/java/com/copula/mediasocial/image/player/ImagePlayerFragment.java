package com.copula.mediasocial.image.player;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.media.VideoController;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayMainFragment;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.media.AbsMediaFragment;
import com.copula.mediasocial.media.MediaOptionPopWindow;
import com.copula.mediasocial.media.MediaOptions;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliasigbalajobi on 4/24/16.
 */
public class ImagePlayerFragment extends AbsMediaFragment<ImageModel> implements View.OnClickListener {
    public static List<ImageModel> imageList = new ArrayList<>();

    protected ViewPager viewPager;
    private Button btnStudioOpt, btnLike;
    private TextView txtLikeCount;


    public static ImagePlayerFragment instantiate(List<ImageModel> m, int p) {
        ImagePlayerFragment.imageList = m;

        Bundle bundle = new Bundle();
        bundle.putInt("startPosition", p);
        ImagePlayerFragment fragment = new ImagePlayerFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getMediaType() {
        return MediaModel.IMAGE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_image_player, container, false);

        txtLikeCount = (TextView) v.findViewById(R.id.txt_like_count);
        viewPager = (ViewPager) v.findViewById(R.id.pager);
        v.findViewById(R.id.btn_media_option).setOnClickListener(this);
        v.findViewById(R.id.btn_bar_back).setOnClickListener(this);
        (btnLike = (Button) v.findViewById(R.id.btn_like)).setOnClickListener(this);
        (btnStudioOpt = (Button) v.findViewById(R.id.btn_studio_opt)).setOnClickListener(this);

        DesignHelper.showOnlyView(btnStudioOpt);
        return v;
    }

    private void switchHideControl() {
        View v = getView();
        if (v == null) return;

        View header = v.findViewById(R.id.view_header);
        View footer = v.findViewById(R.id.view_footer);

        if (header.getVisibility() == View.GONE) {
            header.setVisibility(View.VISIBLE);
            footer.setVisibility(View.VISIBLE);
        } else {
            header.setVisibility(View.GONE);
            footer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MyPagerAdapter adapter = new MyPagerAdapter(getFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(getArguments().getInt("startPosition", 0));
        adapter.notifyDataSetChanged();
    }

    private void processCurrentMediaView(int listPosition) {
        ImageModel media = imageList.get(listPosition);
        txtLikeCount.setText(AdapterAudioSongList.formatLike(media.likeCount));

        AudioPlayMainFragment.likeActionOpt(media.isLiked, btnLike);
        AudioPlayMainFragment.studioSelectOpt(media.isGalleryMedia, btnStudioOpt);
    }

    @Override
    public void onClick(View v) {
        ImageModel media = imageList.get(viewPager.getCurrentItem());
        if (media == null) return;

        if (v == btnLike) doMediaLike(media);
        else if (v.getId() == R.id.btn_media_option) showMediaOptionActivity(media, v, 0);
        else if (v == btnStudioOpt) {
            updateMediaStudio(media);
            AudioPlayMainFragment.studioSelectOpt(media.isGalleryMedia, btnStudioOpt);
        } else if (v.getId() == R.id.btn_bar_back) getActivity().finish();
    }

    @Override
    protected void showMediaOptionActivity(ImageModel media, View v, int flags) {
        flags = MediaOptionPopWindow.FLAG_SHARE | MediaOptionPopWindow.FLAG_POST;
        super.showMediaOptionActivity(media, v, flags);
    }

    @Override
    public void onMediaAction(MediaOptions action, List<ImageModel> models) {
        int currentPosition = viewPager.getCurrentItem();
        if (currentPosition >= 0) processCurrentMediaView(currentPosition);
    }

    protected int getStreamType() {
        return VideoController.LOCAL;
    }


    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            processCurrentMediaView(i);
            int st = getStreamType();
            MyPlayerPagerFragment fragment =
                    MyPlayerPagerFragment.newInstance(imageList.get(i), st);
            fragment.setClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchHideControl();
                }
            });
            return fragment;
        }

        @Override
        public int getCount() {
            return (imageList != null) ? imageList.size() : 0;
        }
    }//END
}
package com.copula.mediasocial.image;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.squareup.picasso.Picasso;

import java.io.File;

public class AdapterImageMediaList extends BaseAdapter<ImageModel> {
    private int displayWidth;

    public AdapterImageMediaList(Context context) {
        super(context);
        this.displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_image_media_list, null, false);
            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);

            ViewGroup.LayoutParams params = holder.img.getLayoutParams();
            params.width = params.height = (int) (displayWidth / 4.01);
            holder.img.setLayoutParams(params);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        loadImageThumbnail(holder.img, getItem(position));

        return convertView;
    }

    protected void loadImageThumbnail(ImageView imageView, ImageModel m) {
        int dim = (int) (displayWidth / 4.3);
        Picasso.with(getContext()).load(new File(m.dataPath))
                .resize(dim, dim).centerInside()
                .placeholder(R.drawable.img_media_image_thumb_256dp).into(imageView);
    }

    private class ViewHolder {
        ImageView img;
    }
}
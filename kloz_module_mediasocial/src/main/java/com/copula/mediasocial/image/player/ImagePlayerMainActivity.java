package com.copula.mediasocial.image.player;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 5/31/17.
 */
public class ImagePlayerMainActivity extends FragmentActivity {
    protected static List<ImageModel> media;

    public static void launch(Context ctx, List<ImageModel> images, int p) {
        if (images == null) return;

        Intent intent = new Intent(ctx, ImagePlayerMainActivity.class);
        ImagePlayerMainActivity.media = images;
        intent.putExtra("startPosition", p);
        ctx.startActivity(intent);
    }

    public static void launch(Context ctx, ImageModel media) {
        if (media == null) return;

        ArrayList<ImageModel> entry = new ArrayList<>(1);
        entry.add(media);
        launch(ctx, entry, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DesignHelper.activityFullscreen(this);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_gen_fragement_holder);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        int p = getIntent().getIntExtra("startPosition", 0);
        ft.replace(R.id.fragment, getPlayerFragment(p)).commit();

    }

    protected ImagePlayerFragment getPlayerFragment(int position) {
        return ImagePlayerFragment.instantiate(media, position);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

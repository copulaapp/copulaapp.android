package com.copula.mediasocial.image;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.MediaStore;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.provider.V2SystemMediaQuery;

import java.util.List;

/**
 * Created by heeleaz on 6/7/17.
 */
public class GetImageMedia extends AsyncTask<Void, Void, List<ImageModel>> {
    protected String libraryName;
    private Callback callback;
    private Context context;

    public GetImageMedia(Context context, String libraryName, Callback callback) {
        this.context = context;
        this.libraryName = libraryName;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null) callback.onPreFetchAudios();
    }

    @Override
    protected List<ImageModel> doInBackground(Void... params) {
        V2SystemMediaQuery v2Query = new V2SystemMediaQuery(context);
        String selection = null;
        if (libraryName != null) {
            selection = MediaStore.Video.Media.BUCKET_DISPLAY_NAME
                    + "=\"" + libraryName + "\"";
        }
        String orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC";
        return v2Query.getImageMedia(selection, orderBy);

    }

    @Override
    protected void onPostExecute(List<ImageModel> m) {
        if (callback != null) callback.onFetchedAudios(m);
    }

    public interface Callback {
        void onPreFetchAudios();

        void onFetchedAudios(List<ImageModel> media);
    }
}

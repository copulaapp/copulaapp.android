package com.copula.mediasocial.image.player;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.media.VideoController;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.ZoomableImageView;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by heeleaz on 7/22/17.
 */
public class MyPlayerPagerFragment extends Fragment {
    private View.OnClickListener clickListener;

    static MyPlayerPagerFragment newInstance(ImageModel media, int streamType) {
        MyPlayerPagerFragment fragment = new MyPlayerPagerFragment();
        Bundle args = new Bundle();
        args.putSerializable("media", media);
        args.putInt("streamType", streamType);
        fragment.setArguments(args);
        return fragment;
    }

    public void setClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.adp_plh_image_show_main, null, false);
        ZoomableImageView img = (ZoomableImageView) view.findViewById(R.id.img_media_image);
        img.setOnClickListener(clickListener);

        ImageModel media = (ImageModel) getArguments().getSerializable("media");
        int streamType = getArguments().getInt("streamType");

        int height = getContext().getResources().getDisplayMetrics().heightPixels;
        int width = getContext().getResources().getDisplayMetrics().widthPixels;
        if (streamType == VideoController.LOCAL) {
            Picasso.with(getContext()).load(new File(media.dataPath))
                    .resize(width, height).centerInside().into(img);
        } else {
            String url = MediaUtility.appendSize(media.dataUrl, width);
            Picasso.with(getContext())
                    .load(url).resize(width, height).centerInside().into(img);
        }

        return view;
    }
}//end

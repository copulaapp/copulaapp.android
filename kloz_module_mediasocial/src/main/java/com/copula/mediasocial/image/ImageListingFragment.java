package com.copula.mediasocial.image;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.mediasocial.R;
import com.copula.mediasocial.image.player.ImagePlayerMainActivity;
import com.copula.mediasocial.media.AbsGalleryFragment;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.GridView;

import java.util.List;

public class ImageListingFragment extends AbsGalleryFragment<ImageModel> implements
        AdapterView.OnItemClickListener, GetImageMedia.Callback {

    private AdapterImageMediaList adapter;
    private GridView gridView;

    public static ImageListingFragment instantiate() {
        return new ImageListingFragment();
    }

    @Override
    public View initGalleryView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_image_media_list, container, false);
        (gridView = (GridView) v.findViewById(R.id.grd)).setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        gridView.setAdapter(adapter = getAdapter());
        loadMediaList(AbsGalleryFragment.EMPTY_RELOAD);
    }

    @Override
    public void onResume() {
        super.onResume();
        gridView.resumeLastPosition();
    }

    @Override
    protected AbsHelperListView getAbsListView() {
        return gridView;
    }

    @Override
    protected AdapterImageMediaList getAdapter() {
        if (gridView.getTag() == null) {
            gridView.setTag(new AdapterImageMediaList(getActivity()));
        }
        return (AdapterImageMediaList) gridView.getTag();
    }

    @Override
    public ImageModel getItemAtPosition(AdapterView parent, int position) {
        return adapter.getItem(position);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        ImagePlayerMainActivity.launch(getActivity(), adapter.getItems(), position);
    }

    @Override
    public void loadMediaList(int cause) {
        if (cause == EMPTY_RELOAD || cause == 0)
            new GetImageMedia(getActivity(), null, this).execute();
    }

    @Override
    public int getMediaType() {
        return MediaModel.IMAGE;
    }

    @Override
    public void onPreFetchAudios() {
        showListProgress(true);
    }

    @Override
    public void onFetchedAudios(List<ImageModel> media) {
        if (getView() == null) return;
        showListProgress(false);

        if (media != null && media.size() > 0) {
            adapter.addOrReplaceHandlers(media);
            adapter.notifyDataSetChanged();
        }
        if (adapter.getCount() == 0) showEmptyListView();
    }
}

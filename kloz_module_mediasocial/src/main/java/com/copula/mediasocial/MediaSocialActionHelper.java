package com.copula.mediasocial;

import android.os.AsyncTask;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;

/**
 * Created by heeleeaz on 7/17/16.
 */
public class MediaSocialActionHelper {
    public static void doMediaLike(ActionListener l, String peeperId, MediaModel media) {
        if (media == null) return;

        if ((media.isLiked = !media.isLiked)) {//liked
            media.likeCount++;
            new DoLocalLike(peeperId, media, l).execute();
        } else {
            media.likeCount--;
            new DoLocalUnlike(peeperId, media, l).execute();
        }
    }

    public interface ActionListener {
        void onPreExecute();

        void onPostExecute();
    }

    private static class DoLocalLike extends AsyncTask<Void, Void, Boolean> {
        private String peeperId;
        private MediaModel media;
        private ActionListener actionListener;

        DoLocalLike(String peeperId, MediaModel media, ActionListener l) {
            this.peeperId = peeperId;
            this.media = media;
            this.actionListener = l;

            if (actionListener != null) actionListener.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            int action = MediaCaptionEntry.ACTION_LIKE;
            LocalAPIProvider
                    api = LocalAPIConnection.getInstance().connect();
            api.mediaAction(peeperId, media.mediaType, media.mediaId, action);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (actionListener != null) actionListener.onPostExecute();
        }
    }//end

    private static class DoLocalUnlike extends AsyncTask<Void, Void, Boolean> {
        private String peeperId;
        private MediaModel media;
        private ActionListener actionListener;

        DoLocalUnlike(String peeperId, MediaModel media, ActionListener l) {
            this.peeperId = peeperId;
            this.media = media;
            this.actionListener = l;

            if (actionListener != null) actionListener.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            int action = MediaCaptionEntry.ACTION_UNLIKE;
            LocalAPIProvider api = LocalAPIConnection.getInstance().connect();
            return api.mediaAction(peeperId, media.mediaType, media.mediaId, action);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (actionListener != null) actionListener.onPostExecute();
        }
    }//end
}

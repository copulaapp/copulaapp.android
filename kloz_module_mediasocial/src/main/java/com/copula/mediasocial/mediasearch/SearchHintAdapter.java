package com.copula.mediasocial.mediasearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 7/12/17.
 */
public class SearchHintAdapter extends BaseAdapter<MediaModel> {

    public SearchHintAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, LayoutInflater inflater) {
        TextView textView;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adp_search_hint, null, false);
            convertView.setTag(textView = (TextView) convertView);
        } else textView = (TextView) convertView.getTag();

        textView.setText(getItem(position).mediaTitle);
        return convertView;
    }
}

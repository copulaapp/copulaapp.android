package com.copula.mediasocial.mediasearch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.StaticCache;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.media.AbsGalleryFragment;
import com.copula.mediasocial.media.MediaOptions;
import com.copula.mediasocial.video.player.VideoPlayerMainActivity;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 3/15/17.
 */
public class MediaSearchFragment extends AbsGalleryFragment implements SearchMediaHelper.Callback {
    private ListView listView;
    private TextView txtEmptyListMessage;
    private View vEmptySearchList, vListProgress;

    private MediaSearchAdapter adapter;
    private HintResolver hintResolver;

    public static MediaSearchFragment instantiate() {
        return new MediaSearchFragment();
    }

    @Override
    public int getMediaType() {
        return -1;
    }

    @Override
    public View initGalleryView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_media_search, container, false);

        vEmptySearchList = v.findViewById(R.id.view_empty_search_list);
        vListProgress = v.findViewById(R.id.view_list_progress);
        txtEmptyListMessage = (TextView) v.findViewById(R.id.txt_empty_list_message);

        (listView = (ListView) v.findViewById(R.id.lst)).setOnItemClickListener(this);

        DesignHelper.showOnlyView(v.findViewById(R.id.view_empty_search_list));
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.listView.setAdapter(adapter = getSearchAdapter());

        this.hintResolver = new HintResolver();
        this.__showAllMedia();
    }

    protected MediaSearchAdapter getSearchAdapter() {
        return new MediaSearchAdapter(getActivity());
    }

    protected List<AudioModel> getCachedAudio() {
        String key = SocialMediaBootstrap
                .makeCacheKey("local", MediaModel.AUDIO);
        return StaticCache.getInstance().getCache(key);
    }

    protected List<VideoModel> getCachedVideo() {
        String key = SocialMediaBootstrap
                .makeCacheKey("local", MediaModel.VIDEO);
        return StaticCache.getInstance().getCache(key);
    }

    private void __showAllMedia() {
        List<AudioModel> am = getCachedAudio();
        if (am != null) {
            hintResolver.setAudioList(am);
            for (AudioModel a : am) adapter.addOrReplaceHandler(a);
        }

        List<VideoModel> vm = getCachedVideo();
        if (vm != null) {
            hintResolver.setVideoList(vm);
            for (VideoModel a : vm) adapter.addOrReplaceHandler(a);
        }

        if (adapter.getCount() > 0) DesignHelper.showOnlyView(listView);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPreSearch(SearchMediaHelper h) {
        adapter.clear();
        DesignHelper.showOnlyView(vListProgress);
    }

    @Override
    public void onCompleteSearch(SearchMediaHelper h, SearchMediaHelper.SearchBundle bundle) {
        if (h.isEmpty()) {
            txtEmptyListMessage.setText(R.string.msg_empty_media_search);
            DesignHelper.showOnlyView(vEmptySearchList);
            return;
        }

        if (bundle.audio != null)
            for (MediaModel a : bundle.audio) adapter.addOrReplaceHandler(a);
        if (bundle.video != null)
            for (MediaModel a : bundle.video) adapter.addOrReplaceHandler(a);

        DesignHelper.showOnlyView(listView);
        adapter.notifyDataSetChanged();
    }

    public void __searchInHint(String s) {
        if (s != null && s.length() > 0) {
            adapter.clear();
            List<MediaModel> m = hintResolver.resolve(s);
            if (m != null && m.size() > 0) {
                adapter.addHandlers(m);
                adapter.notifyDataSetChanged();
            }

            if (adapter.getCount() > 0) DesignHelper.showOnlyView(listView);
            else DesignHelper.showOnlyView(vEmptySearchList);
        } else {
            __showAllMedia();
        }
    }

    public void search(String q) {
        q = q.split("[|]+")[0];
        new SearchMediaHelper(getActivity(), q, this).execute();
    }

    @Override
    protected AbsHelperListView getAbsListView() {
        return listView;
    }

    @Override
    public MediaSearchAdapter getAdapter() {
        return adapter;
    }

    @Override
    public MediaModel getItemAtPosition(AdapterView parent, int position) {
        return adapter.getItem(position);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        if (view.getId() == AdapterAudioSongList.PLAY) {
            doMediaPlay(getItemAtPosition(parent, position));
        } else super.onItemClick(parent, view, position, id);
    }

    protected void doMediaPlay(MediaModel media) {
        if (media.mediaType == MediaModel.VIDEO) {
            VideoPlayerMainActivity.launch(getActivity(), new VideoModel(media));
        } else {
            List<AudioModel> m = new ArrayList<>(1);
            m.add(new AudioModel(media));
            playAudioLocal(m, 0);
        }
    }

    @Override
    public void onMediaAction(MediaOptions action, List mediaModels) {
        if (action == MediaOptions.PLAY) {
            doMediaPlay((MediaModel) mediaModels.get(0));
        } else if (action == MediaOptions.LIKE || action == MediaOptions.UPDATE_STUDIO) {
            adapter.notifyDataSetChanged();
        } else super.onMediaAction(action, mediaModels);
    }
}

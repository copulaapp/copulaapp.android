package com.copula.mediasocial.mediasearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.media.MasterAdapter;
import com.copula.mediasocial.video.AdapterVideoMediaList;
import com.copula.support.android.view.widget.BaseAdapter;

/**
 * Created by heeleaz on 6/24/17.
 */
public class MediaSearchAdapter extends BaseAdapter<MediaModel> {
    private static final int AUDIO_ITEM = 0;
    private static final int VIDEO_ITEM = 1;
    private static final int TYPE_MAX_COUNT = 2;

    public MediaSearchAdapter(Context context) {
        super(context);
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        MediaModel m = getItem(position);
        if (m.mediaType == MediaModel.AUDIO) return AUDIO_ITEM;
        else return VIDEO_ITEM;
    }

    protected MasterAdapter getAdapter(int mediaType) {
        if (mediaType == AUDIO_ITEM) {
            return new AdapterAudioSongList(getContext());
        } else {
            return new AdapterVideoMediaList(getContext());
        }
    }

    @Override
    public View getView(int position, View convertView, LayoutInflater inflater) {
        MasterAdapter adapter;
        if (convertView == null || convertView.getTag() == null) {
            adapter = getAdapter(getItemViewType(position));
            convertView = adapter.initAdapterView(inflater);
            adapter.initChildViewHolder(convertView);

            convertView.setTag(adapter);
        } else adapter = (MasterAdapter) convertView.getTag();

        MediaModel media = getItem(position);
        if (media.mediaType == MediaModel.AUDIO) {
            adapter.setData(convertView,
                    position, new AudioModel(getItem(position)));
        } else {
            adapter.setData(convertView,
                    position, new VideoModel(getItem(position)));
        }
        return convertView;
    }//END
}

package com.copula.mediasocial.mediasearch;

import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.support.android.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 7/11/17.
 */
public class HintResolver {
    private List<AudioModel> audioList = new ArrayList<>();
    private List<VideoModel> videoList = new ArrayList<>();
    private List<MediaModel> matchedEntry = new ArrayList<>();

    public void setAudioList(List<AudioModel> audioList) {
        this.audioList = audioList;
    }

    public void setVideoList(List<VideoModel> videoList) {
        this.videoList = videoList;
    }

    public List<MediaModel> resolve(String query) {
        matchedEntry.clear();

        resolveAudioSearch(query);
        resolveVideoSearch(query);

        return matchedEntry;
    }

    private void resolveAudioSearch(String query) {
        for (AudioModel audio : audioList) {
            if (StringUtil.containsIgnoreCase(audio.mediaTitle, query) ||
                    StringUtil.containsIgnoreCase(audio.mediaComposer, query) ||
                    StringUtil.containsIgnoreCase(audio.mediaLibrary, query)) {
                matchedEntry.add(audio);
            }
        }
    }//END

    private void resolveVideoSearch(String query) {
        for (VideoModel video : videoList) {
            if (StringUtil.containsIgnoreCase(video.fileName, query)) {
                matchedEntry.add(video);
            }
        }
    }//END
}

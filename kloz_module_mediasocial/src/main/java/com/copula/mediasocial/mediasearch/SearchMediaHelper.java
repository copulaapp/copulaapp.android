package com.copula.mediasocial.mediasearch;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.V2SystemMediaQuery;

import java.util.List;

/**
 * Created by heeleaz on 6/4/17.
 */
public class SearchMediaHelper extends AsyncTask<Void, Void, SearchMediaHelper.SearchBundle> {
    private String query;
    private Callback listener;
    private SearchBundle result;

    private Context context;

    public SearchMediaHelper(Context ctx, String query, Callback listener) {
        this.context = ctx;
        this.query = query;
        (this.listener = listener).onPreSearch(this);
    }

    @Override
    protected SearchBundle doInBackground(Void... params) {
        V2SystemMediaQuery v2Query = new V2SystemMediaQuery(context);

        SearchBundle bundle = new SearchBundle();
        bundle.audio = v2Query.searchAudioMedia(query, null);
        bundle.video = v2Query.searchVideoMedia(query, null);
        return bundle;
    }

    @Override
    protected void onPostExecute(SearchBundle response) {
        if (listener != null)
            listener.onCompleteSearch(this, result = response);
    }

    public boolean isEmpty() {
        return (result.audio == null || result.audio.size() == 0)
                && (result.video == null || result.video.size() == 0);
    }

    public interface Callback {
        void onPreSearch(SearchMediaHelper t);

        void onCompleteSearch(SearchMediaHelper t, SearchBundle bundle);
    }

    public static class SearchBundle {
        public List<AudioModel> audio;
        public List<VideoModel> video;
    }
}

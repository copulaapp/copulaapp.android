package com.copula.mediasocial.audio.album;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.provider.SystemImageProvider;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

public class AdapterAudioAlbums extends BaseAdapter<AudioModel> {
    private int displayWidth;
    private SystemImageProvider imageProvider;

    public AdapterAudioAlbums(Context context) {
        super(context);
        imageProvider = new SystemImageProvider(context);
        displayWidth = getContext().getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(int position, View convertView, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_audio_library_list, null,
                    false);
            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.artist = (TextView) convertView.findViewById(R.id.txt_media_artist);
            holder.album = (TextView) convertView.findViewById(R.id.txt_media_album);

            int maxDimension = DesignHelper.scaleInDP(getContext(), 150);
            int dimen = (int) Math.min(displayWidth / 3.40, maxDimension);
            convertView.setLayoutParams(new AbsListView.LayoutParams(dimen, -1));

            ViewGroup.LayoutParams params = holder.img.getLayoutParams();
            params.width = params.height = dimen;
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        AudioModel entry = getItem(position);
        holder.artist.setText(entry.mediaComposer);
        holder.album.setText(entry.mediaLibrary);

        this.loadThumbnail(holder.img, entry);
        return convertView;
    }

    protected void loadThumbnail(ImageView imageView, AudioModel m) {
        int dim = displayWidth / 3;
        Uri uri = imageProvider.getAlbumThumbnailUri(m.albumId);
        Glide.with(getContext()).loadFromMediaStore(uri).override(dim, dim)
                .placeholder(R.drawable.img_media_audio_album_256dp).into(imageView);
    }

    protected class ViewHolder {
        ImageView img;
        TextView album, artist;
    }
}
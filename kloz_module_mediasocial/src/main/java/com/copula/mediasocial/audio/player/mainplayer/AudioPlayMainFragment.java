package com.copula.mediasocial.audio.player.mainplayer;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.provider.SystemImageProvider;
import com.copula.functionality.media.AudioController;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.StaticCache;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.media.MediaOptions;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.util.BlurTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by eliasigbalajobi on 5/23/16.
 */
public class AudioPlayMainFragment extends PlayControllerFragment implements View.OnClickListener,
        AdapterView.OnItemClickListener, IFragmentBackPress {

    private ImageView imgAlbumArt, imgAlbumArtInBG;
    private Button btnLike;
    private Button btnStudioOpt;
    private ListView songListView;

    private AdapterAudioSongList playlistAdapter;

    public static AudioPlayMainFragment instantiate() {
        return new AudioPlayMainFragment();
    }

    public static void studioSelectOpt(boolean isStudio, Button btnStudio) {
        if (isStudio) {
            btnStudio.setBackgroundResource(R.drawable.ic_studio_remove_white_64px);
        } else btnStudio.setBackgroundResource(R.drawable.ic_studio_add_white_64px);
    }

    public static void likeActionOpt(boolean isLiked, Button btnLike) {
        if (isLiked) {
            btnLike.setBackgroundResource(R.drawable.ic_like_active_appcolor_96px);
        } else btnLike.setBackgroundResource(R.drawable.ic_like_inactive_white_96px);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = super.onCreateView(inflater, container, si);
        imgAlbumArt = (ImageView) v.findViewById(R.id.img_media_thumbnail);
        imgAlbumArtInBG = (ImageView) v.findViewById(R.id.img_media_thumbnail_blurbg);
        (btnLike = (Button) v.findViewById(R.id.btn_like)).setOnClickListener(this);
        (v.findViewById(R.id.btn_playlist)).setOnClickListener(this);

        (btnStudioOpt = (Button) v.findViewById(R.id.btn_studio_opt)).setOnClickListener(this);
        (songListView = (ListView) v.findViewById(R.id.lst_song_list)).setOnItemClickListener(this);

        songListView.setAdapter(playlistAdapter = getPlaylistAdapter());
        DesignHelper.showOnlyView(btnStudioOpt);

        return v;
    }

    protected AdapterAudioSongList getPlaylistAdapter() {
        if (songListView.getTag() == null)
            songListView.setTag(new PlayerQueueAdapter(getActivity()));
        return (AdapterAudioSongList) songListView.getTag();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String key = SocialMediaBootstrap
                .makeCacheKey("local", MediaModel.AUDIO);
        List cached = StaticCache.getInstance().getCache(key);
        if (cached != null) populateQueueListEntries(cached);

        int dw = getResources().getDisplayMetrics().widthPixels;
        ViewGroup.LayoutParams params = imgAlbumArt.getLayoutParams();
        params.height = params.width = (int) (dw * 0.85);
    }

    @Override
    public void onResume() {
        super.onResume();
        playlistAdapter.activatePlayPositionTracker();
    }

    @Override
    public void onPause() {
        super.onPause();
        playlistAdapter.deactivatePlayPositionTracker();
    }

    protected void populateQueueListEntries(List<AudioModel> media) {
        playlistAdapter.addOrReplaceHandlers(media);
        playlistAdapter.notifyDataSetChanged();
    }

    protected void configureListTrackAndPosition(List<AudioModel> mediaList) {
        if (mediaList == null) return;

        int p = getController().findPlayPositionInList(mediaList);
        if (p != -1) playlistAdapter.setCurrentPlayPosition(p);
        songListView.setSelection(p);
    }

    @Override
    public void processCurrentMediaPlayView(AudioModel media) {
        if (media == null) return;

        super.processCurrentMediaPlayView(media);
        likeActionOpt(media.isLiked, btnLike);
        studioSelectOpt(media.isGalleryMedia, btnStudioOpt);

        try {
            processMediaPlayerBackground(media, imgAlbumArtInBG, imgAlbumArt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void
    processMediaPlayerBackground(AudioModel media, ImageView big, ImageView small) {
        try {
            SystemImageProvider d = new SystemImageProvider(getContext());
            Uri uri = d.getAlbumThumbnailUri(media.albumId);
            Picasso.with(getActivity()).load(uri)
                    .placeholder(R.drawable.bkg_main_media_audio_player)
                    .transform(new BlurTransform(getActivity(), 25)).into(big);
            Picasso.with(getActivity()).load(uri)
                    .resize(350, 350).centerInside()
                    .placeholder(R.drawable.img_media_song_thumb_512dp).into(small);
        } catch (Exception e) {
        }
    }

    @Override
    public void onMediaAction(MediaOptions action, List<AudioModel> media) {
        if (action == MediaOptions.LIKE) {
            likeActionOpt(media.get(0).isLiked, btnLike);
        } else if (action == MediaOptions.UPDATE_STUDIO) {
            studioSelectOpt(media.get(0).isGalleryMedia, btnStudioOpt);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getController().setQueue(playlistAdapter.getItems());
        getController().setStreamType(AudioController.LOCAL);
        getController().startPlay(position, true);
    }

    @Override
    protected void doMediaLike(AudioModel media) {
        if (media != null) super.doMediaLike(media);
    }

    private void onClickPlaylist() {
        if (songListView.getVisibility() == View.VISIBLE) {
            songListView.setVisibility(View.GONE);
        } else {
            songListView.setVisibility(View.VISIBLE);

            String key = SocialMediaBootstrap
                    .makeCacheKey("local", MediaModel.AUDIO);
            List list = StaticCache.getInstance().getCache(key);
            if (list != null) configureListTrackAndPosition(list);
        }
    }

    @Override
    public void onClick(View v) {
        AudioModel m = getController().getCurrentTrack();

        if (v == btnLike) doMediaLike(m);
        else if (v.getId() == R.id.btn_playlist) onClickPlaylist();
        else if (v == btnStudioOpt) updateMediaStudio(m);
        else super.onClick(v);
    }

    @Override
    public boolean allowBackPressed() {
        if (songListView.getVisibility() == View.GONE) return true;
        else {
            songListView.setVisibility(View.GONE);
            return false;
        }
    }//END

    private class PlayerQueueAdapter extends AdapterAudioSongList {
        private int playColor = Color.parseColor("#ffffff");

        PlayerQueueAdapter(Context context) {
            super(context);
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            View view = super.getView(position, convertView, inflater);
            ViewHolder viewHolder = (ViewHolder) view.getTag();

            viewHolder.option.setVisibility(View.GONE);
            viewHolder.download.setVisibility(View.GONE);
            viewHolder.studio.setVisibility(View.GONE);
            viewHolder.size.setVisibility(View.GONE);
            viewHolder.duration.setVisibility(View.GONE);
            viewHolder.likeCount.setVisibility(View.GONE);

            viewHolder.title.setTextColor(playColor);
            viewHolder.artist.setTextColor(playColor);

            return view;
        }
    }//END
}

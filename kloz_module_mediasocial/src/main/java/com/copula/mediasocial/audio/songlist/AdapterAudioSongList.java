package com.copula.mediasocial.audio.songlist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.provider.SystemImageProvider;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.media.MasterAdapter;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ListView;

import java.util.Locale;

public class AdapterAudioSongList extends MasterAdapter<AudioModel> {
    public static final int BTN_OPT = R.id.btn_media_option;
    public static final int BTN_STUDIO_OPT = R.id.btn_studio_opt;
    public static final int BTN_DOWNLOAD = R.id.btn_download;
    public static final int PLAY = R.id.parent;

    private boolean showAlbumArt = true;
    private ViewHolder holder;
    private int currentPlayPosition = -1;
    private int displayWidth;

    private int playColor = Color.parseColor("#a11a1a");
    private int normColor = Color.parseColor("#2f2f2f");

    private AudioControllerReceiver audioReceiver;
    private SystemImageProvider imageProvider;

    public AdapterAudioSongList(Context ctx) {
        super(ctx);
        imageProvider = new SystemImageProvider(ctx);
        displayWidth = ctx.getResources().getDisplayMetrics().widthPixels;
    }

    public static String formatLike(int like) {
        like = (like < 0) ? 0 : like;
        return like + ((like > 1) ? " likes" : " like");
    }

    public static String formatFileSize(Context ctx, long size) {
        return String.format(Locale.getDefault(), "%.1f%s",
                (size / 1048576f), ctx.getString(R.string.txt_mb));
    }

    public static void studioSelectOpt(boolean isGalleryMedia, Button studio) {
        if (isGalleryMedia) {
            studio.setBackgroundResource(R.drawable.ic_studio_remove_grey_64px);
        } else {
            studio.setBackgroundResource(R.drawable.ic_studio_add_grey_64px);
        }
    }

    public void setShowAlbumArt(boolean show) {
        this.showAlbumArt = show;
    }

    public void activatePlayPositionTracker() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(AudioController.INTENT_AUDIO_CONTROLLER);

        audioReceiver = new AudioControllerReceiver();
        getContext().registerReceiver(audioReceiver, filter);
    }

    public void deactivatePlayPositionTracker() {
        if (audioReceiver != null) getContext().unregisterReceiver(audioReceiver);
    }

    @Override
    public View getView(int position, View convertView, LayoutInflater inflater) {
        if (convertView == null) {
            initChildViewHolder(convertView = initAdapterView(inflater));
            convertView.setTag(this.holder);
        } else holder = (ViewHolder) convertView.getTag();

        setData(convertView, position, getItem(position));
        return convertView;
    }

    @Override
    public View initAdapterView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.adp_audio_songs_list, null, false);
    }

    @Override
    public void initChildViewHolder(View view) {
        holder = new ViewHolder();
        holder.thumbnail = (ImageView) view.findViewById(R.id.img_media_thumbnail);
        holder.artist = (TextView) view.findViewById(R.id.txt_media_artist);
        holder.title = (TextView) view.findViewById(R.id.txt_media_title);
        holder.option = (Button) view.findViewById(BTN_OPT);
        holder.studio = (Button) view.findViewById(BTN_STUDIO_OPT);
        holder.download = (Button) view.findViewById(BTN_DOWNLOAD);
        holder.duration = (TextView) view.findViewById(R.id.txt_media_duration);
        holder.size = (TextView) view.findViewById(R.id.txt_media_size);
        holder.likeCount = (TextView) view.findViewById(R.id.txt_like_count);
        holder.selectIndicator = (ImageView) view.findViewById(R.id.img_select_indicator);

        DesignHelper.showOnlyView(holder.studio);
    }

    @Override
    public void setData(final View view, final int position, AudioModel media) {
        holder.artist.setText(media.mediaComposer);
        holder.title.setText(media.mediaTitle);
        holder.duration.setText(MediaUtility.formatDuration(media.duration));
        holder.likeCount.setText(formatLike(media.likeCount));
        holder.size.setText(formatFileSize(getContext(), media.fileSize));

        AdapterAudioSongList.studioSelectOpt(media.isGalleryMedia, holder.studio);

        if (position == currentPlayPosition) {
            holder.title.setTextColor(playColor);
            holder.artist.setTextColor(playColor);
        } else {
            holder.title.setTextColor(normColor);
            holder.artist.setTextColor(normColor);
        }

        if (showAlbumArt) loadThumbnail(holder.thumbnail, media);
        else holder.thumbnail.setVisibility(View.GONE);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int p = position;
                ((ListView) view.getParent()).performItemClick(v, p, p);
            }
        };
        holder.option.setOnClickListener(clickListener);
        holder.studio.setOnClickListener(clickListener);
        holder.download.setOnClickListener(clickListener);
    }

    protected void loadThumbnail(ImageView imageView, AudioModel m) {
        int dim = displayWidth / 6;
        Uri uri = imageProvider.getAlbumThumbnailUri(m.albumId);
        Glide.with(getContext()).loadFromMediaStore(uri).override(dim, dim)
                .placeholder(R.drawable.img_media_song_thumb_256dp).into(imageView);
    }

    protected int getCurrentPlayPosition() {
        return currentPlayPosition;
    }

    public void setCurrentPlayPosition(int position) {
        this.currentPlayPosition = position;
        notifyDataSetChanged();
    }

    protected class ViewHolder {
        public ImageView thumbnail, selectIndicator;
        public Button option, studio, download;
        public TextView title, artist, duration, size, likeCount;
    }

    private class AudioControllerReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int action = intent.getIntExtra(AudioController.INTENT_ACTION, -1);
            if (action == AudioController.EXTRA_PREPARING) return;

            int pInQueue = AudioController.getInstance(context).findPlayPositionInQueue();
            if (pInQueue != -1) setCurrentPlayPosition(pInQueue);
        }
    }//END
}
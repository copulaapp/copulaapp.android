package com.copula.mediasocial.audio.player.mainplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.mediasocial.media.AbsMediaFragment;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 6/5/17.
 */
public class PlayControllerFragment extends AbsMediaFragment<AudioModel> implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener, MediaPlayer.OnErrorListener {
    private Button btnPlayPause, btnRepeat, btnShuffle;
    private TextView txtMediaTitle, txtMediaArtist, txtPlayedDuration, txtMediaDuration;
    private TextView txtShuffleState, txtRepeatState;
    private SeekBar seekBar;
    private View btnNext, btnPrev;
    private Handler mHandler;

    private AudioController playController;
    private PlayBroadcastReceiver mBroadcastReceiver;

    @Override
    public int getMediaType() {
        return MediaModel.VIDEO;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_audio_player, container, false);
        this.mHandler = new Handler(Looper.getMainLooper());

        txtMediaArtist = (TextView) v.findViewById(R.id.txt_media_artist);
        txtPlayedDuration = (TextView) v.findViewById(R.id.txt_played_duration);
        txtMediaDuration = (TextView) v.findViewById(R.id.txt_media_duration);
        txtMediaTitle = (TextView) v.findViewById(R.id.txt_media_title);
        txtRepeatState = (TextView) v.findViewById(R.id.txt_repeat_state);
        txtShuffleState = (TextView) v.findViewById(R.id.txt_shuffle_state);

        (seekBar = (SeekBar) v.findViewById(R.id.seekbar)).setOnSeekBarChangeListener(this);
        (btnRepeat = (Button) v.findViewById(R.id.btn_media_repeat)).setOnClickListener(this);
        (btnShuffle = (Button) v.findViewById(R.id.btn_media_shuffle)).setOnClickListener(this);
        (btnPlayPause = (Button) v.findViewById(R.id.btn_play_pause)).setOnClickListener(this);

        (btnNext = v.findViewById(R.id.btn_next_media)).setOnClickListener(this);
        (btnPrev = v.findViewById(R.id.btn_prev_media)).setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.playController = AudioController.getInstance(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        processCurrentMediaPlayView(playController.getCurrentTrack());
        IntentFilter filter = new IntentFilter(AudioController.INTENT_AUDIO_CONTROLLER);
        getActivity().registerReceiver(mBroadcastReceiver = new PlayBroadcastReceiver(), filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mBroadcastReceiver);
    }

    private void doMediaPlay() {
        if (playController.isPlaying()) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
        } else {
            primarySeekBarProgressUpdater(0);//resume instantiate tracker
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_white_128dp);
        }
        playController.togglePlay(true);
    }

    protected void processCurrentMediaPlayView(AudioModel media) {
        if (media == null) return;

        if (playController.isPlaying()) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_white_128dp);
            primarySeekBarProgressUpdater(0);
        } else {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
        }

        txtMediaDuration.setText(MediaUtility.formatDuration(media.duration));
        txtMediaTitle.setText(media.mediaTitle);
        txtMediaArtist.setText(media.mediaComposer);

        controlShuffle(false);
        controlRepeat(false);
    }

    public AudioController getController() {
        return playController;
    }

    /**
     * Method which updates the SeekBar primary progress by current song playing position
     */
    private synchronized void primarySeekBarProgressUpdater(int waitMilli) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!playController.isPlaying()) return;

                int position = playController.getCurrentPosition();
                long duration = playController.getDuration();

                seekBar.setProgress((int) ((position / (duration * 1f) * 100)));
                txtPlayedDuration.setText(MediaUtility.formatDuration(position));
                primarySeekBarProgressUpdater(1000);
            }
        }, waitMilli);
    }

    private void controlRepeat(boolean repeat) {
        int repeatMode = playController.getRepeatMode();

        if (repeat) {
            repeatMode = ++repeatMode % 3;//switch using modulo counter
            playController.setRepeatMode(repeatMode);
        }

        if (repeatMode == AudioController.REPEAT_PLAYLIST) {
            btnRepeat.setBackgroundResource(R.drawable.ic_repeat_all_white_64px);
            txtRepeatState.setText(R.string.txt_repeat_all);
        } else if (repeatMode == AudioController.REPEAT_TRACK) {
            btnRepeat.setBackgroundResource(R.drawable.ic_repeat_track_white_64px);
            txtRepeatState.setText(R.string.txt_repeat_1);
        } else {
            txtRepeatState.setText(R.string.txt_repeat_off);
            btnRepeat.setBackgroundResource(R.drawable.ic_repeat_off_white_64px);
        }
    }

    private void controlShuffle(boolean shuffle) {
        if (shuffle) {
            if (playController.getShuffle() == null) {
                int count = playController.getPlayListCount();
                playController.shuffle(new AudioController.Randomize(count));
            } else playController.shuffle(null);
        }

        if (playController.getShuffle() == null) {
            btnShuffle.setBackgroundResource(R.drawable.ic_shuffle_off_white_64px);
            txtShuffleState.setText(R.string.txt_shuffle_off);
        } else {
            btnShuffle.setBackgroundResource(R.drawable.ic_shuffle_on_white_64px);
            txtShuffleState.setText(R.string.txt_shuffle_on);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int seekToMl = (int) ((progress / 100f) * playController.getDuration());
            playController.seekTo(seekToMl);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        btnPlayPause.clearAnimation();
        btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
        return true;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onClick(View v) {
        if (v == btnNext) playController.playNext(true);
        else if (v == btnPrev) playController.playPrev(true);
        else if (v == btnPlayPause) doMediaPlay();
        else if (v == btnRepeat) controlRepeat(true);
        else if (v == btnShuffle) controlShuffle(true);
    }

    private class PlayBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            btnPlayPause.clearAnimation();

            int action = intent.getIntExtra(AudioController.INTENT_ACTION, 0);
            if (action == AudioController.EXTRA_PLAY) {
                processCurrentMediaPlayView(playController.getCurrentTrack());
            } else if (action == AudioController.EXTRA_PREPARING) {
                btnPlayPause.setBackgroundResource(R.drawable.ic_media_progress_white_96px);
                btnPlayPause.setAnimation(R.anim.anim_media_play_progress, 12, 500);
            } else {
                btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_white_256dp);
            }
        }
    }//END
}

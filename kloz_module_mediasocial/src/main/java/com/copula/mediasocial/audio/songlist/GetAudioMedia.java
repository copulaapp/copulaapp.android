package com.copula.mediasocial.audio.songlist;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.MediaStore;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.provider.V2SystemMediaQuery;

import java.util.List;

/**
 * Created by heeleaz on 6/7/17.
 */
public class GetAudioMedia extends AsyncTask<Void, Void, List<AudioModel>> {
    private int albumId;
    private Context context;

    private Callback callback;

    public GetAudioMedia(Context context, int albumId, Callback callback) {
        this.context = context;
        this.albumId = albumId;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null) callback.onPreFetch();
    }

    @Override
    protected List<AudioModel> doInBackground(Void... params) {
        String selection = MediaStore.Audio.Media.IS_MUSIC + "=1";
        if (albumId != 0) {
            selection += (" AND " + MediaStore.Audio.Media.ALBUM_ID + "=" + albumId);
        }
        String orderBy = MediaStore.Audio.Media.TITLE;
        return new V2SystemMediaQuery(context).getAudioMedia(selection, orderBy);
    }

    @Override
    protected void onPostExecute(List<AudioModel> m) {
        if (callback != null) callback.onPostFetched(m);
    }

    public interface Callback {
        void onPreFetch();

        void onPostFetched(List<AudioModel> media);
    }
}

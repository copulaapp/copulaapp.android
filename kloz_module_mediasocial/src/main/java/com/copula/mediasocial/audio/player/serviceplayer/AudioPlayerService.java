package com.copula.mediasocial.audio.player.serviceplayer;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.media.MediaPlayerInterceptor;
import com.copula.mediasocial.StaticCache;

/**
 * Created by heeleaz on 6/18/17.
 */
public abstract class AudioPlayerService extends IntentService implements MediaPlayerInterceptor.InterceptorListener {
    /**
     * this is to indicate that service is active and running
     */
    private static boolean isAlive = false;

    /**
     * keep cache reference alive.
     */
    private static StaticCache staticCache;


    private MediaPlayerInterceptor mediaInterceptor;

    private AudioController audioController;
    private MyBinder myBinder = new MyBinder();
    private AudioControllerReceiver audioReceiver;

    public AudioPlayerService() {
        super("AudioPlayerService");
    }

    public static void startService(Context context) {
        context.startService(new Intent(context, AudioPlayerService.class));
    }


    @Override
    public void onCreate() {
        super.onCreate();
        //Initialize singleton static cache instance
        staticCache = StaticCache.getInstance();

        IntentFilter filter = new IntentFilter();
        filter.addAction(AudioController.INTENT_AUDIO_CONTROLLER);
        registerReceiver(audioReceiver = new AudioControllerReceiver(), filter);

        if (!isAlive) {
            mediaInterceptor = new MediaPlayerInterceptor(this);
            mediaInterceptor.register(this);
        }

        isAlive = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        audioController = AudioController.getInstance(this);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    @Override
    public void onHoldIntercept(int actor) {
        if (audioController.isPlaying()) audioController.pause();
    }

    @Override
    public void onFreeIntercept(int actor) {
    }

    protected void releaseServices() {
        try {
            if (audioController != null) {
                audioController.stop();
                audioController.reset();
                audioController.release();
            }

            if (mediaInterceptor != null) mediaInterceptor.unregister();
            unregisterReceiver(audioReceiver);
            mediaInterceptor = null;
        } catch (Exception e) {
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        isAlive = false;
    }

    class MyBinder extends Binder {
        AudioPlayerService getService() {
            return AudioPlayerService.this;
        }
    }

    private class AudioControllerReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int action = intent.getIntExtra(AudioController.INTENT_ACTION, -1);
            if (action == AudioController.EXTRA_PREPARING) return;

            NotificationAudioPlayer.startPlayer(context);
        }
    }//END
}

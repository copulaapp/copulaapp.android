package com.copula.mediasocial.audio.songlist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.TextView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.player.AudioDPlayerFragment;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayerMainActivity;
import com.copula.support.android.content.IScrollableHandler;

/**
 * Created by heeleaz on 5/31/17.
 */
public class AudioSongListActivity extends AppCompatActivity implements View.OnClickListener, IScrollableHandler {
    private AudioModel media;
    private View actionBarView;
    private TextView txtBarTitle;

    public static void launch(Context ctx, AudioModel media) {
        Intent intent = new Intent(ctx, AudioSongListActivity.class);
        ctx.startActivity(intent.putExtra("media", media));
    }

    public static void launch(Context ctx) {
        Intent intent = new Intent(ctx, AudioSongListActivity.class);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DesignHelper.activityFullscreen(this);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        setContentView(R.layout.act_audio_songs_list);

        findViewById(R.id.btn_search).setOnClickListener(this);
        findViewById(R.id.btn_bar_back).setOnClickListener(this);
        findViewById(R.id.frg_audio_player).setOnClickListener(this);
        actionBarView = findViewById(R.id.bar_audio_songs_list);
        txtBarTitle = (TextView) findViewById(R.id.txt_bar_title);

        media = (AudioModel) getIntent().getSerializableExtra("media");
        txtBarTitle.setText(media.mediaLibrary);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, setupSongsListFragment());
        ft.replace(R.id.frg_audio_player, AudioDPlayerFragment.instantiate()).commit();
    }

    protected AudioSongListFragment setupSongsListFragment() {
        return AudioSongListFragment.instantiate(media, true);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.frg_audio_player) {
            AudioPlayerMainActivity.launch(this);
        } else if (v.getId() == R.id.btn_search) {
            startActivity(new Intent("com.copula.MainSearch"));
        } else if (v.getId() == R.id.btn_bar_back) finish();
    }

    @Override
    public void onScroll(AbsListView absListView, int axis) {
        if (absListView.getFirstVisiblePosition() != 0) {
            actionBarView.setBackgroundResource(R.drawable.bkg_app_main_actionbar_shadow);
            txtBarTitle.setText(media.mediaLibrary);
        } else {
            actionBarView.setBackgroundResource(R.color.transparent);
            txtBarTitle.setText(" ");
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

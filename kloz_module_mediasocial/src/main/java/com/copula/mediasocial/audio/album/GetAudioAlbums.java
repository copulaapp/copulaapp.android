package com.copula.mediasocial.audio.album;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.MediaStore;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.provider.V2SystemMediaQuery;

import java.util.List;

/**
 * Created by heeleaz on 6/7/17.
 */
public class GetAudioAlbums extends AsyncTask<Void, Void, List<AudioModel>> {
    private Callback callback;
    private Context context;

    public GetAudioAlbums(Context context, Callback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void execute(Callback callback) {
        this.callback = callback;
        super.execute();
    }

    @Override
    protected void onPreExecute() {
        if (callback != null) callback.onPreLoad();
    }

    @Override
    protected List<AudioModel> doInBackground(Void... params) {
        String selection = MediaStore.Audio.Media.IS_MUSIC + "=1";
        String orderBy = MediaStore.Audio.Media.ARTIST + " DESC";
        return new V2SystemMediaQuery(context).getAudioAlbum(selection, orderBy);
    }

    @Override
    protected void onPostExecute(List<AudioModel> m) {
        if (callback != null) callback.onPostLoad(m);
    }

    public interface Callback {
        void onPreLoad();

        void onPostLoad(List<AudioModel> media);
    }
}

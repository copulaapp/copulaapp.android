package com.copula.mediasocial.audio.album;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.songlist.AudioSongListActivity;
import com.copula.mediasocial.media.AbsGalleryFragment;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.GridView;

import java.util.List;

/**
 * Created by heeleaz on 7/21/17.
 */
public class AudioAlbumFragment extends AbsGalleryFragment<AudioModel> implements GetAudioAlbums.Callback {
    private GridView gridView;
    private AdapterAudioAlbums adapter;

    public static AudioAlbumFragment instantiate() {
        return new AudioAlbumFragment();
    }

    @Override
    public View initGalleryView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_audio_album_list, container, false);
        gridView = (GridView) v.findViewById(R.id.grd);

        gridView.setAdapter(adapter = getAdapter());
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadMediaList(0);
    }

    @Override
    public int getMediaType() {
        return MediaModel.AUDIO;
    }

    @Override
    public void onResume() {
        super.onResume();
        gridView.resumeLastPosition();
    }

    @Override
    protected AdapterAudioAlbums getAdapter() {
        if (gridView.getTag() == null)
            gridView.setTag(new AdapterAudioAlbums(getActivity()));

        return (AdapterAudioAlbums) gridView.getTag();
    }

    @Override
    protected AbsHelperListView getAbsListView() {
        return gridView;
    }

    @Override
    public AudioModel getItemAtPosition(AdapterView parent, int position) {
        return adapter.getItem(position);
    }

    @Override
    public void loadMediaList(int cause) {
        if (cause == 0 || cause == EMPTY_RELOAD) {
            new GetAudioAlbums(getContext(), this).execute();
        }
    }

    @Override
    public void onPreLoad() {
        showListProgress(true);
    }

    @Override
    public void onPostLoad(List<AudioModel> media) {
        if (getView() == null) return;

        showListProgress(false);
        if (media != null && media.size() > 0) {
            adapter.clear();
            adapter.addHandlers(media);
            adapter.notifyDataSetChanged();
        } else showEmptyListView();
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        AudioModel media = getItemAtPosition(parent, position);
        AudioSongListActivity.launch(getActivity(), media);
    }
}

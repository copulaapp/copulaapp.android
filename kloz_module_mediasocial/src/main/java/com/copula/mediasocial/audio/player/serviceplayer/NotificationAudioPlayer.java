package com.copula.mediasocial.audio.player.serviceplayer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.provider.SystemImageProvider;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.StaticCache;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by eliasigbalajobi on 6/1/16.
 */
public class NotificationAudioPlayer extends AudioPlayerService {
    private static final int notificationId = 1999;

    private Context context;
    private NotificationManager notificationManager;
    private Notification notification;
    private RemoteViews remoteView;
    private AudioController audioController;

    public static void startPlayer(Context context) {
        context.startService(new Intent(context, NotificationAudioPlayer.class));
    }//end

    private static void onHitPlay(final Context ctx) {
        AudioController pc = AudioController.getInstance(ctx);
        if (!pc.isPlaylistEmpty()) pc.togglePlay(false);
    }

    /**
     * intent to launch when notification is clicked.
     * If Play Cache is empty. Launch the landing page for possible re-fetch on Bootstrap
     * else Launch the Main MediaPlayer
     */
    private static void launchIntentBaseOnAudioController(Context context) {
        AudioController controller = AudioController.getInstance(context);

        String cacheKey;
        if (controller.getStreamType() == AudioController.LOCAL) {
            cacheKey = SocialMediaBootstrap
                    .makeCacheKey("local", MediaModel.AUDIO);
        } else {
            cacheKey = SocialMediaBootstrap
                    .makeCacheKey("host", MediaModel.AUDIO);
        }

        List available = StaticCache.getInstance().getCache(cacheKey);
        if (available != null && available.size() > 0) {
            Intent intent = new Intent("com.copula.MainAudioPlayer");
            intent.putExtra("streamType", controller.getStreamType());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } else {
            Intent intent = new Intent("com.copula.LandingPage");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    private static void releaseAudioPlayer(Context context) {
        MediaPlayer mediaPlayer = AudioController.getInstance(context);
        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
    }

    private Notification __init(Context context) {
        this.context = context;
        this.notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        this.audioController = AudioController.getInstance(context);

        long when = System.currentTimeMillis();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this.context);
        builder.setWhen(when).setSmallIcon(R.drawable.ic_notification_audio_player);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);

        remoteView = new RemoteViews(
                this.context.getPackageName(), R.layout.nvw_small_audio_player);
        //this is the intent that is supposed to be called when the button is clicked
        Intent switchIntent = new Intent(this.context, PlayBroadcast.class);

        PendingIntent playPauseIntent = PendingIntent.getBroadcast(this.context, 0,
                switchIntent.setAction("play_pause"), 0);
        remoteView.setOnClickPendingIntent(R.id.btn_play_pause, playPauseIntent);

        PendingIntent playNextIntent = PendingIntent.getBroadcast(this.context, 0,
                switchIntent.setAction("play_next"), 0);
        remoteView.setOnClickPendingIntent(R.id.btn_next_media, playNextIntent);

        PendingIntent playPrevIntent = PendingIntent.getBroadcast(this.context, 0,
                switchIntent.setAction("play_prev"), 0);
        remoteView.setOnClickPendingIntent(R.id.btn_prev_media, playPrevIntent);

        builder.setContentIntent(PendingIntent.getBroadcast(this.context, 0,
                switchIntent.setAction("open"), 0));

        builder.setDeleteIntent(PendingIntent.getBroadcast(this.context, 0,
                switchIntent.setAction("clear"), 0));

        notification = _ProcessCurrentMediaView(builder.setContent(remoteView));
        notificationManager.notify(notificationId, notification);
        return notification;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        try {
            Notification notification = __init(this);
            if (AudioController.getInstance(this).isPlaying()) {
                startForeground(notificationId, notification);
            } else stopForeground(false);
        } catch (Exception e) {
            stopForeground(true);
        }

        return START_STICKY;
    }

    private Notification _ProcessCurrentMediaView(NotificationCompat.Builder builder) {
        AudioModel entry = audioController.getCurrentTrack();
        if (entry == null) return null;

        Notification notification = builder.setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentTitle(entry.mediaTitle).build();

        Uri thumbUrl;
        if (audioController.getStreamType() == AudioController.LOCAL) {
            SystemImageProvider im = new SystemImageProvider(context);
            thumbUrl = im.getAlbumThumbnailUri(entry.albumId);
        } else {
            String s = MediaUtility.appendSize(entry.thumbnailUrl, 100);
            thumbUrl = Uri.parse(s);
        }

        remoteView.setTextViewText(R.id.txt_media_title, entry.mediaTitle);
        remoteView.setTextViewText(R.id.txt_media_artist, entry.mediaComposer);

        if (audioController.getPlayerState() == AudioController.EXTRA_PLAY) {
            remoteView.setImageViewResource(
                    R.id.btn_play_pause, R.drawable.ic_nvw_btn_pause_media_128px);

            audioImagePlaceholder(notification);//placeholder media thumbnail first
            Picasso.with(context).load(thumbUrl)
                    .into(remoteView, R.id.img_media_thumbnail, notificationId, notification);
        } else {
            remoteView.setImageViewResource(
                    R.id.btn_play_pause, R.drawable.ic_nvw_btn_play_media_128px);
        }
        return notification;
    }

    private void audioImagePlaceholder(Notification notification) {
        Picasso.with(context).load(R.drawable.img_media_song_thumb_128dp)
                .into(remoteView, R.id.img_media_thumbnail, notificationId, notification);
    }

    public static class PlayBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case "play_pause":
                    onHitPlay(context);
                    break;
                case "play_next":
                    AudioController.getInstance(context).playNext(false);
                    break;
                case "play_prev":
                    AudioController.getInstance(context).playPrev(false);
                    break;
                case "open":
                    launchIntentBaseOnAudioController(context);
                    break;
                case "clear":
                    //TODO: Monitor behaviour after doing --> releaseAudioPlayer(context);
                    break;
            }
        }
    }//END
}

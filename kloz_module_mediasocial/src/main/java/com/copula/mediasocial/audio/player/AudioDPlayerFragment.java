package com.copula.mediasocial.audio.player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.media.AudioController;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AudioDPlayerFragment extends Fragment implements OnClickListener, OnErrorListener {
    private TextView txtMediaArtist, txtMediaTitle;
    private Button btnPlayPause, btnNextMedia, btnPrevMedia;
    private ImageView imgMediaThumb;
    private AudioController playController;
    private Handler mHandler;
    private PlayBroadcastReceiver mReceiver;

    public static AudioDPlayerFragment instantiate() {
        return new AudioDPlayerFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_audio_player_d, container, false);
        txtMediaArtist = (TextView) v.findViewById(R.id.txt_media_artist);
        txtMediaTitle = (TextView) v.findViewById(R.id.txt_media_title);
        imgMediaThumb = (ImageView) v.findViewById(R.id.img_media_thumbnail);

        (btnPlayPause = (Button)
                v.findViewById(R.id.btn_play_pause)).setOnClickListener(this);
        (btnNextMedia = (Button)
                v.findViewById(R.id.btn_next_media)).setOnClickListener(this);
        (btnPrevMedia = (Button)
                v.findViewById(R.id.btn_prev_media)).setOnClickListener(this);
        txtMediaTitle.setSelected(true);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mHandler = new Handler(getActivity().getMainLooper());

        playController = AudioController.getInstance(getActivity());
        playController.setOnErrorListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new
                IntentFilter(AudioController.INTENT_AUDIO_CONTROLLER);
        getActivity().registerReceiver(mReceiver = new PlayBroadcastReceiver(), filter);

        initCurrentMediaPlayView(playController.getCurrentTrack());
    }

    public void play(List<AudioModel> media, int position) {
        if (media == null) {
            playController.setQueue(media);
            playController.startPlay(position, false);
        }
    }

    public void play(AudioModel media) {
        if (media != null) {
            playController.setQueue(media);
            playController.startPlay(0, true);
        }
    }

    private void initCurrentMediaPlayView(AudioModel media) {
        if (media == null) return;

        if (playController.isPlaying()) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_black_128px);
        } else {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_black_128px);
        }

        txtMediaArtist.setText(media.mediaComposer);
        txtMediaTitle.setText(media.mediaTitle);

        Picasso.with(getActivity()).load(media.thumbnailUrl)
                .placeholder(R.drawable.img_media_song_thumb_128dp).into(imgMediaThumb);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        btnPlayPause.clearAnimation();
        btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_grey_128dp);
        return true;
    }

    private void onPlayClick() {
        if (playController.getPlayList().size() > 0) doMediaPlayPause();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnPlayPause.getId()) onPlayClick();
        if (v == btnNextMedia) playController.playNext(true);
        if (v == btnPrevMedia) playController.playPrev(true);
    }

    private void doMediaPlayPause() {
        if (playController.isPlaying()) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_black_128px);
        } else {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_black_128px);
        }
        playController.togglePlay(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
    }//end

    private class PlayBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            btnPlayPause.clearAnimation();

            int action = intent.getIntExtra(AudioController.INTENT_ACTION, 0);
            if (action == AudioController.EXTRA_PLAY) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        initCurrentMediaPlayView(playController.getCurrentTrack());
                    }
                });
            } else if (action == AudioController.EXTRA_PREPARING) {
                btnPlayPause.setBackgroundResource(R.drawable.img_mediaplay_prepare_black_128px);
                btnPlayPause.setAnimation(R.anim.anim_media_play_progress, 12, 500);
            } else {
                btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_black_128px);
            }
        }
    }//END
}
package com.copula.mediasocial.audio.player.mainplayer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;

/**
 * Created by heeleaz on 5/31/17.
 */
public class AudioPlayerMainActivity extends FragmentActivity {
    private AudioPlayMainFragment playerFragment;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, AudioPlayerMainActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        DesignHelper.activityFullscreen(this);
        if (getActionBar() != null) getActionBar().hide();
        setContentView(R.layout.act_gen_fragement_holder);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, playerFragment = initPlayerFragment());
        ft.commit();
    }

    protected AudioPlayMainFragment initPlayerFragment() {
        return new AudioPlayMainFragment();
    }

    @Override
    public void onBackPressed() {
        if (playerFragment == null || playerFragment.allowBackPressed())
            super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

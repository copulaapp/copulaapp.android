package com.copula.mediasocial.audio.songlist;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.support.android.view.PatchBase;
import com.copula.support.android.view.PatchBundle;
import com.copula.support.android.view.PatchSession;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;

/**
 * Created by eliasigbalajobi on 1/23/16.
 */

//TODO: PatchAudioAlbumInfo using on server media image processing.
//TODO: resolve to both server and local

public class PatchAudioAlbumInfo extends PatchBase implements View.OnClickListener {
    public static PatchAudioAlbumInfo instantiate(PatchSession session, AudioModel model) {
        PatchBundle bundle = new PatchBundle();
        bundle.put("media", model);
        return session.patch(PatchAudioAlbumInfo.class, bundle, false);
    }

    @Override
    protected void onCreate(PatchBundle bundle, Object... sessionObject) {
        setContentView(R.layout.pch_audio_album_info);
        int dw = getContext().getResources().getDisplayMetrics().widthPixels;

        ImageView imgThumbnailL = (ImageView) findViewById(R.id.img_media_thumb_b);
        ImageView imgThumbnail = (ImageView) findViewById(R.id.img_media_thumb_a);
        TextView albumTitle = (TextView) findViewById(R.id.txt_album_title);
        TextView artist = (TextView) findViewById(R.id.txt_song_artist);
        findViewById(R.id.btn_album_play).setOnClickListener(this);

        AudioModel media = getBundle().getObject("media");
        albumTitle.setText((media != null) ? media.mediaLibrary : "");
        artist.setText((media != null) ? media.mediaComposer : "");

        ViewGroup.LayoutParams clp = imgThumbnailL.getLayoutParams();
        clp.width = dw;
        clp.height = (int) (dw / 1.2);

        if (media == null) return;

        String bigThumbUrl =
                MediaUtility.appendSize(media.thumbnailUrl, (int) (dw / 1.2));
        Picasso.with(getContext()).load(bigThumbUrl).into(imgThumbnailL);

        String smallThumbUrl =
                MediaUtility.appendSize(media.thumbnailUrl, (dw / 5));
        Picasso.with(getContext()).load(smallThumbUrl).noFade()
                .placeholder(R.drawable.img_media_audio_album_256dp).into(imgThumbnail);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_download) {
            dispatchEventToFrame("download_all", null);
        } else if (view.getId() == R.id.btn_album_play) {
            dispatchEventToFrame("play_all", null);
        }
    }
}

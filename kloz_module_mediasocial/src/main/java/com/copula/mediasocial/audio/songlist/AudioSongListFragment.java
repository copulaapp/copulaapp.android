package com.copula.mediasocial.audio.songlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.mediasocial.media.AbsGalleryFragment;
import com.copula.mediasocial.media.MediaOptions;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.view.PatchBase;
import com.copula.support.android.view.PatchBundle;
import com.copula.support.android.view.PatchSession;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.ListView;

import java.util.List;

public class AudioSongListFragment extends AbsGalleryFragment<AudioModel> implements AdapterView
        .OnItemClickListener, PatchSession.PatchSessionListener, IFragmentBackPress, GetAudioMedia.Callback {
    private AdapterAudioSongList adapter;
    private ListView listView;
    private int listPosition;

    private AudioModel media;

    public static AudioSongListFragment instantiate(AudioModel m, boolean isStandAlone) {
        Bundle args = new Bundle();
        args.putSerializable("media", m);
        args.putBoolean("isStandAlone", isStandAlone);

        AudioSongListFragment f = new AudioSongListFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public View initGalleryView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_audio_songs_list, container, false);
        listView = (ListView) v.findViewById(R.id.lst);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        media = (AudioModel) getArguments().getSerializable("media");

        this.adapter = getAdapter();
        adapter.setShowAlbumArt(!(media != null && media.albumId != 0));

        if (getArguments().getBoolean("isStandAlone")) {
            PatchSession session = new PatchSession(getActivity());
            session.setPatchSessionListener(this);
            PatchAudioAlbumInfo p = PatchAudioAlbumInfo.instantiate(session, media);
            listView.addHeaderView(p.getView(), null, false);
        } else {
            //this fragment listview is shifted upward so as to allow space
            //floating button
            int h = DesignHelper.scaleInDP(listView.getContext(), 70);
            DesignHelper.bottomSpace(listView, h);
        }

        listView.setAdapter(adapter);//setAdapter called after setting Adapter

        this.loadMediaList(EMPTY_RELOAD);
    }

    public AudioModel getMedia() {
        return media;
    }

    @Override
    public int getMediaType() {
        return MediaModel.AUDIO;
    }

    @Override
    protected AbsHelperListView getAbsListView() {
        return listView;
    }

    @Override
    protected AdapterAudioSongList getAdapter() {
        if (listView.getTag() == null)
            listView.setTag(new AdapterAudioSongList(getActivity()));
        return (AdapterAudioSongList) listView.getTag();
    }

    @Override
    public void loadMediaList(int cause) {
        if (cause == 0 || cause == EMPTY_RELOAD) {
            int albumId = (media == null) ? 0 : media.albumId;
            new GetAudioMedia(getActivity(), albumId, this).execute();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.activatePlayPositionTracker();
        listView.setSelection((listPosition < 0) ? 0 : listPosition);
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.deactivatePlayPositionTracker();
    }

    @Override
    public void onMediaAction(MediaOptions action, List<AudioModel> models) {
        if (action == MediaOptions.PLAY) {
            List<AudioModel> audioList = adapter.getItems();
            playAudioLocal(audioList, audioList.indexOf(models.get(0)));
        } else if (action == MediaOptions.UPDATE_STUDIO
                | action == MediaOptions.LIKE) adapter.notifyDataSetChanged();
        else super.onMediaAction(action, models);
    }

    @Override
    public AudioModel getItemAtPosition(AdapterView parent, int position) {
        return adapter.getItem(position);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        if (view.getId() == AdapterAudioSongList.PLAY) {
            position = position - listView.getHeaderViewsCount();
            playAudioLocal(adapter.getItems(), position);
        } else super.onItemClick(parent, view, position, id);
    }

    public ListView getListView() {
        return listView;
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {
        super.onScroll(view, first, visibleCount, totalCount, scrollAxis);
        listPosition = first;
    }

    @Override
    public void onPreFetch() {
        showListProgress(true);
    }

    @Override
    public void onPostFetched(List<AudioModel> media) {
        if (getView() == null) return;

        showListProgress(false);
        if (media != null && media.size() > 0) {
            adapter.addOrReplaceHandlers(media);
            adapter.notifyDataSetChanged();
        }
        if (adapter.getCount() == 0) showEmptyListView();
    }

    @Override
    public void onEventToFrame(PatchBase p, String event, PatchBundle b) {
        if (event.equals("play_all"))
            playAudioLocal(adapter.getItems(), 0);
    }

    @Override
    public void onNewPatchInit(PatchBase patch) {
    }
}

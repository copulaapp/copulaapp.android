package com.copula.mediasocial.audio.player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;

public class AudioPlayerExtActivity extends FragmentActivity implements OnClickListener, OnErrorListener, SeekBar
        .OnSeekBarChangeListener {
    private TextView txtMediaArtist, txtMediaTitle;
    private Button btnPlayPause;
    private SeekBar seekBar;

    private Handler handler;

    private AudioController playController;
    private PlayBroadcastReceiver mReceiver;

    public static void launch(Context ctx, AudioModel media) {
        Intent intent = new Intent(ctx, AudioPlayerExtActivity.class);
        intent.putExtra("media", media);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_audio_player_ext);

        handler = new Handler(Looper.getMainLooper());
        txtMediaArtist = (TextView) findViewById(R.id.txt_media_artist);
        txtMediaTitle = (TextView) findViewById(R.id.txt_media_title);
        (btnPlayPause = (Button)
                findViewById(R.id.btn_play_pause)).setOnClickListener(this);
        (seekBar = (SeekBar) findViewById(R.id.seekbar)).setOnSeekBarChangeListener(this);

        MediaModel media = (MediaModel) getIntent().getSerializableExtra("media");
        if (media == null) {
            Uri data = getIntent().getData();
            int mediaId = MediaUtility.getMediaIdFromUri(this, data);
            media = SystemMediaProvider.getVideo(this, mediaId);
        }

        if (media == null) {
            finish();
            return;
        }

        playController = AudioController.newInstance(this);
        playController.setOnErrorListener(this);
        playController.setQueue(new AudioModel(media));
        playController.setStreamType(AudioController.LOCAL);
        playController.startPlay();
    }

    @Override
    public void onResume() {
        super.onResume();
        AudioController.getInstance(this).pause();//pause app global media
        initCurrentMediaPlayView(playController.getCurrentTrack());

        IntentFilter filter = new
                IntentFilter(AudioController.INTENT_AUDIO_CONTROLLER);
        registerReceiver(mReceiver = new PlayBroadcastReceiver(), filter);
    }

    private synchronized void primarySeekBarProgressUpdater(int waitMilli) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!playController.isPlaying()) return;

                int position = playController.getCurrentPosition();
                long duration = playController.getDuration();

                seekBar.setProgress((int) ((position / (duration * 1F) * 100)));
                primarySeekBarProgressUpdater(1000);
            }
        }, waitMilli);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int seekToMill = (int) ((progress / 100f) * playController.getDuration());
            playController.seekTo(seekToMill);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    private void initCurrentMediaPlayView(AudioModel media) {
        if (media == null) return;

        if (playController.isPlaying()) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_grey_128dp);
            primarySeekBarProgressUpdater(0);
        } else {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_grey_128dp);
        }

        txtMediaArtist.setText(media.mediaComposer);
        txtMediaTitle.setText(media.mediaTitle);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_grey_128dp);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnPlayPause.getId()) doMediaPlayPause();
    }

    private void doMediaPlayPause() {
        if (playController.isPlaying()) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_grey_128dp);
            primarySeekBarProgressUpdater(0);
        } else {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_grey_128dp);
        }
        playController.togglePlay(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            playController.stop();
            playController.reset();
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
    }//end

    private class PlayBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            btnPlayPause.clearAnimation();

            int action = intent.getIntExtra(AudioController.INTENT_ACTION, 0);
            if (action == AudioController.EXTRA_PLAY) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initCurrentMediaPlayView(playController.getCurrentTrack());
                    }
                });
            }
        }
    }//END
}
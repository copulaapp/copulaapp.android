package com.copula.mediagallary.observer;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.provider.SystemMediaObserver;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.mediagallary.studio.SharedCollectionActivity;
import com.copula.mediasocial.R;

public class MediaObserverService extends IntentService implements UriObserver.OnChangeListener {
    private MyBinder myBinder = new MyBinder();
    private int initialSongCount;
    private NotificationManager notificationM;

    private UriObserver uriObserver;
    private SystemMediaObserver mediaObserver;

    public MediaObserverService() {
        super("MediaObserverService");
    }

    public static void startService(Context context) {
        context.startService(new Intent(context, MediaObserverService.class));
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void ackLatestMedia() {
        initialSongCount = 0;
        mediaObserver.ackLatestMedia();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mediaObserver = new SystemMediaObserver(this);
        uriObserver = UriObserver.getInstance(this, SystemMediaProvider.AUDIO_URL, this);
        notificationM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (uriObserver != null) uriObserver.stop();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public void onChange(Uri uri) {
        int count = mediaObserver.getLatestMediaCount(MediaModel.AUDIO);
        if (count > initialSongCount) {
            Notification n = setupLibraryUpdateNotification(count).build();
            notificationM.notify(0x1, n);
            initialSongCount = count;
        }
    }

    private NotificationCompat.Builder setupLibraryUpdateNotification(int count) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(true).setGroup("action_notification");
        builder.setDefaults(0);

        String tit = getString(R.string.ntf_tit_media_observer);
        String msg = getString(R.string.ntf_msg_media_observer);
        builder.setContentTitle(tit
                .replace("?", "" + count).replace("#", count > 1 ? "s" : ""));
        builder.setContentText(msg.replace("?", count > 1 ? "them" : "it"));

        Intent intent = new Intent(this, SharedCollectionActivity.class);
        intent.putExtra("page", 1);//songs chooser page
        PendingIntent pIntent = PendingIntent
                .getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (pIntent != null) builder.setContentIntent(pIntent);

        builder.setSmallIcon(R.drawable.ic_notification_new_songs);
        return builder;
    }

    public class MyBinder extends Binder {
        public MediaObserverService getService() {
            return MediaObserverService.this;
        }
    }
}

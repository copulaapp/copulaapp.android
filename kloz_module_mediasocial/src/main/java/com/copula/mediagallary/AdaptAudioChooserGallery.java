package com.copula.mediagallary;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.provider.SystemImageProvider;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

public class AdaptAudioChooserGallery extends BaseAdapter<AudioModel> {
    private int displayWidth;
    private boolean showButtonRemove;
    private SystemImageProvider imageProvider;

    public AdaptAudioChooserGallery(Context context) {
        super(context);
        this.imageProvider = new SystemImageProvider(context);
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    public void setShowButtonRemove(boolean showButtonRemove) {
        this.showButtonRemove = showButtonRemove;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_a_v_gallery, null, false);
            holder.chk = (CheckBox) convertView.findViewById(R.id.chk_chooser);
            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.duration = (TextView) convertView.findViewById(R.id.txt_media_duration);
            holder.title = (TextView) convertView.findViewById(R.id.txt_media_title);

            View btnRemove = convertView.findViewById(R.id.btn_remove);
            if (showButtonRemove) btnRemove.setVisibility(View.VISIBLE);
            else btnRemove.setVisibility(View.GONE);

            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chk.performClick();
                ((AbsListView) v.getParent()).performItemClick(v, position, position);
            }
        });

        AudioModel entry = getItem(position);
        holder.chk.setChecked(entry.status);
        holder.duration.setText(MediaUtility.formatDuration(entry.duration));
        holder.title.setText(entry.mediaTitle + "\n" + entry.mediaLibrary);

        int dim = displayWidth / 6;
        Uri uri = imageProvider.getAlbumThumbnailUri(entry.albumId);
        Glide.with(getContext()).loadFromMediaStore(uri).override(dim, dim)
                .placeholder(R.drawable.img_media_song_thumb_256dp).into(holder.img);
        return convertView;
    }

    private class ViewHolder {
        ImageView img;
        CheckBox chk;
        TextView title, duration;
    }
}
package com.copula.mediagallary;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.AbsListViewScrollListener;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

import java.util.List;

/**
 * Created by heeleaz on 9/11/16.
 */
public abstract class AbsMediaGalleryActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        AbsListViewScrollListener {
    private BaseAdapter baseAdapter;
    private AbsListView absListView;

    private int mediaType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        super.onCreate(savedInstanceState);

        this.mediaType = getIntent().getIntExtra("mediaType", 1);
    }

    @Override
    protected void onStart() {
        super.onStart();
        absListView = getAbsListView(mediaType);
        absListView.setAdapter(baseAdapter = getAbsListViewAdapter(mediaType));

        fetchItem("0,999999");
    }

    public int getMediaType() {
        return mediaType;
    }

    private void fetchItem(String limit) {
        List<?> items;

        if (mediaType == MediaModel.IMAGE) items = initImage(limit);
        else if (mediaType == MediaModel.AUDIO) items = initAudio(limit);
        else items = initVideo(limit);

        if (items != null || items.size() > 0) baseAdapter.addHandlers(items);

        if (baseAdapter != null && baseAdapter.getCount() > 0) {
            DesignHelper.showOnlyView(absListView);
            absListView.setOnItemClickListener(this);
        } else {
            ((TextView) findViewById(R.id.txt_empty_list_message))
                    .setText(R.string.msg_media_not_available);
            DesignHelper.showOnlyView(findViewById(R.id.view_empty_list));
        }
    }

    protected abstract List<ImageModel> initImage(String limit);

    protected abstract List<AudioModel> initAudio(String limit);

    protected abstract List<VideoModel> initVideo(String limit);

    protected abstract AbsListView getAbsListView(int mediaType);

    protected abstract BaseAdapter getAbsListViewAdapter(int mediaType);

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onPageScroll(int totalItem, int pageOffset, int pagerStep) {
        fetchItem(DesignHelper.page(pageOffset, pagerStep));
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {

    }
}

package com.copula.mediagallary.studio;

import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.localservice.profile.DaoSharedCollection;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.AbsListViewScrollListener;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

import java.util.List;

public class MediaStudioSelectorFragment extends AbsMediaStudioFragment implements AbsListViewScrollListener,
        OnItemClickListener {
    private BaseAdapter baseAdapter;
    private AbsListView absListView;
    private AbsHelperListView absHelperListView;
    private View vListEmpty, vListProgress;
    private TextView txtEmptyMessage;

    private DaoSharedCollection daoStudioMedia;
    private int mediaType;

    public static MediaStudioSelectorFragment instantiate(int mediaType) {
        Bundle bundle = new Bundle(1);
        bundle.putInt("media_type", mediaType);

        MediaStudioSelectorFragment fragment = new MediaStudioSelectorFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_media_selector_studio, container, false);
        vListProgress = view.findViewById(R.id.view_list_progress);
        vListEmpty = view.findViewById(R.id.view_empty_list);

        this.mediaType = getArguments().getInt("media_type");

        int activitySelector = StudioActivityCallback.ACTIVITY_SELECTOR;
        if (mediaType == MediaModel.IMAGE) {
            absListView = (AbsListView) view.findViewById(R.id.grd);
            absListView.setAdapter(baseAdapter
                    = new AdaptImageStudioSelector(getActivity(), activitySelector));
        } else if (mediaType == MediaModel.AUDIO) {
            absListView = (AbsListView) view.findViewById(R.id.lst);
            absListView.setAdapter(baseAdapter
                    = new AdaptAudioStudioSelector(getActivity(), activitySelector));
        } else if (mediaType == MediaModel.VIDEO) {
            absListView = (AbsListView) view.findViewById(R.id.lst);
            absListView.setAdapter(baseAdapter
                    = new AdaptVideoStudioSelector(getActivity(), activitySelector));
        }

        (absHelperListView = (AbsHelperListView) absListView).setHelperScrollListener(this);
        absHelperListView.setPagerStep(500);
        absListView.setOnItemClickListener(this);

        txtEmptyMessage = ((TextView) view.findViewById(R.id.txt_empty_list_message));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.daoStudioMedia = new DaoSharedCollection(getActivity());

        final String limit = DesignHelper.page(0, absHelperListView.getPagerStep());
        vListEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopulateMediaList(mediaType, limit).execute();
            }
        });

        new PopulateMediaList(mediaType, limit).execute();
    }

    @Override
    protected List<?> readList(int size) {
        String limit = DesignHelper.page(getReadPosition(), size);
        List<?> mediaList = null;
        if (mediaType == MediaModel.IMAGE) mediaList = getImages(limit);
        else if (mediaType == MediaModel.AUDIO) mediaList = getAudios(limit);
        else if (mediaType == MediaModel.VIDEO) mediaList = getVideos(limit);


        if (mediaList != null && mediaList.size() > 0) {
            this.moveReadPosition(mediaList.size());
            return mediaList;
        }
        reset();
        return null;
    }

    @Override
    protected void addMedia(MediaModel model) {
        if (baseAdapter == null || model == null) return;

        baseAdapter.addHandler(model);
        baseAdapter.notifyDataSetChanged();

        if (absListView.getVisibility()
                == View.GONE) DesignHelper.showOnlyView(absListView);
    }

    @Override
    protected void addMedia(List<MediaModel> mediaModels) {
        if (getActivity() == null || baseAdapter == null) return;
        if (mediaModels == null || mediaModels.size() == 0) return;

        baseAdapter.addOrReplaceHandlers(mediaModels);
        if (baseAdapter.getCount() > 0) DesignHelper.showOnlyView(absListView);
        baseAdapter.notifyDataSetChanged();
    }

    private int getDefinedMessageResource(int t) {
        if (t == MediaModel.IMAGE) return R.string.msg_no_selector_studio_image;
        else if (t == MediaModel.AUDIO) return R.string.msg_no_selector_studio_audio;
        else return R.string.msg_no_selector_studio_video;
    }

    @Override
    protected void removeMedia(MediaModel model) {
        if (baseAdapter == null || model == null) return;

        baseAdapter.removeHandler(model);
        baseAdapter.notifyDataSetChanged();

        if (baseAdapter.getCount() == 0) {
            txtEmptyMessage.setText(getDefinedMessageResource(mediaType));
            DesignHelper.showOnlyView(vListEmpty);
        }
    }

    @Override
    protected boolean isReadable() {
        return !baseAdapter.isEmpty();
    }

    @Override
    protected void clearList() {
        baseAdapter.clear();
        txtEmptyMessage.setText(getDefinedMessageResource(mediaType));
        DesignHelper.showOnlyView(vListEmpty);
    }

    private int initImage(AdaptImageStudioSelector adapter, String limit) {
        List<ImageModel> medias = getImages(limit);
        if (medias.size() > 0) {
            medias.removeAll(daoStudioMedia.getMedias(MediaModel.IMAGE));
            if (medias.size() > 0) {
                adapter.addOrReplaceHandlers(medias);
                return -1;
            } else return R.string.msg_no_selector_studio_image;
        } else return R.string.msg_media_not_available;
    }

    private int initAudio(AdaptAudioStudioSelector adapter, String limit) {
        List<AudioModel> medias = getAudios(limit);
        if (medias.size() > 0) {
            medias.removeAll(daoStudioMedia.getMedias(MediaModel.AUDIO));
            if (medias.size() > 0) {
                adapter.addOrReplaceHandlers(medias);
                return -1;
            } else return R.string.msg_no_selector_studio_audio;
        } else return R.string.msg_media_not_available;
    }

    private int initVideo(AdaptVideoStudioSelector adapter, String limit) {
        List<VideoModel> medias = getVideos(limit);
        if (medias.size() > 0) {
            medias.removeAll(daoStudioMedia.getMedias(MediaModel.VIDEO));
            if (medias.size() > 0) {
                adapter.addOrReplaceHandlers(medias);
                return -1;
            } else return R.string.msg_no_selector_studio_video;
        } else return R.string.msg_media_not_available;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MediaModel media = (MediaModel) baseAdapter.getItem(position);
        if (media == null) return;

        if (getActivity() instanceof StudioActivityCallback) {
            StudioActivityCallback callback = (StudioActivityCallback) getActivity();
            callback.addMedia(StudioActivityCallback.ACTIVITY_SELECTOR, media.mediaType, media);

            removeMedia(media);
        }
    }

    @Override
    public int getAdapterCount() {
        return baseAdapter.getCount();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onPageScroll(int totalItem, int pageOffset, int pagerStep) {
        String limit = DesignHelper.page(pagerStep, pagerStep);
        new PopulateMediaList(mediaType, limit).execute();
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {
    }

    private List<ImageModel> getImages(String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        return SystemMediaProvider.getImages(getActivity(), null, orderBy);
    }

    private List<AudioModel> getAudios(String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " = 1";
        return SystemMediaProvider.getAudios(getActivity(), selection, orderBy);
    }

    private List<VideoModel> getVideos(String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        return SystemMediaProvider.getVideos(getActivity(), null, orderBy);
    }

    private class PopulateMediaList extends AsyncTask<Void, Void, Integer> {
        private String limit;
        private int mediaType;

        PopulateMediaList(int mediaType, String limit) {
            this.limit = limit;
            this.mediaType = mediaType;
            vListProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            if (mediaType == MediaModel.IMAGE)
                return initImage((AdaptImageStudioSelector) baseAdapter, limit);
            else if (mediaType == MediaModel.AUDIO)
                return initAudio((AdaptAudioStudioSelector) baseAdapter, limit);
            else return initVideo((AdaptVideoStudioSelector) baseAdapter, limit);
        }

        @Override
        protected void onPostExecute(Integer s) {
            if (getActivity() == null) return;

            if (baseAdapter != null && baseAdapter.getCount() > 0) {
                DesignHelper.showOnlyView(absListView);
                baseAdapter.notifyDataSetChanged();
                absHelperListView.setPagerStep(baseAdapter.getCount());
            } else {
                if (s != -1) txtEmptyMessage.setText(s);
                DesignHelper.showOnlyView(vListEmpty);
            }
            absHelperListView.nextPager();
            vListProgress.setVisibility(View.GONE);
        }
    }//END
}
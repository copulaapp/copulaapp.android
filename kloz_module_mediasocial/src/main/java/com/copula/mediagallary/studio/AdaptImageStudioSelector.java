package com.copula.mediagallary.studio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.squareup.picasso.Picasso;

import java.io.File;

public class AdaptImageStudioSelector extends BaseAdapter<ImageModel> {
    private int displayWidth;
    private int activity;

    public AdaptImageStudioSelector(Context context, int activity) {
        super(context);
        this.activity = activity;
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_image_gallery, null, false);

            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            LayoutParams params = (LayoutParams) holder.img.getLayoutParams();
            params.width = params.height = displayWidth / 4;
            holder.img.setLayoutParams(params);

            convertView.findViewById(R.id.chk_media_thumbnail).setVisibility(View.GONE);
            if (activity == StudioActivityCallback.ACTIVITY_SELECTOR)
                convertView.findViewById(R.id.btn_remove).setVisibility(View.GONE);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        ImageModel entry = getItem(position);

        int dim = displayWidth / 4;
        Picasso.with(getContext()).load(new File(entry.dataPath))
                .resize(dim, dim).centerCrop()
                .placeholder(R.drawable.img_media_image_thumb_256dp).into(holder.img);
        return convertView;
    }

    private class ViewHolder {
        ImageView img;
    }
}
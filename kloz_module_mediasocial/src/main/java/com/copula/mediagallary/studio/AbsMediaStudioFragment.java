package com.copula.mediagallary.studio;

import android.support.v4.app.Fragment;
import com.copula.functionality.localservice.media.MediaModel;

import java.util.List;

/**
 * Created by heeleaz on 2/22/17.
 */
abstract class AbsMediaStudioFragment extends Fragment {
    private int readPosition;

    /**
     * add media to record and adapter list
     *
     * @param model media to add
     */
    protected void addMedia(MediaModel model) {
    }

    /**
     * add media to record and adapter list
     *
     * @param mediaModels media to add
     */
    protected void addMedia(List<MediaModel> mediaModels) {
    }

    /**
     * @see {@method addMediaAsync}
     */
    protected void addMediaAsync(MediaModel model) {
    }

    /**
     * add media to record and adapter async. no changes will be made to view
     *
     * @param mediaModel media to add
     */
    protected void addMediaAsync(List<MediaModel> mediaModel) {
    }

    /**
     * remove all the element in the record
     */
    protected void clearRecord() {
    }

    /**
     * remove all element in record and adapter
     */
    protected void clearList() {
    }

    /**
     * remove media from record and adapter
     *
     * @param model element to remove
     * @param <T>
     */
    protected <T extends MediaModel> void removeMedia(T model) {
    }

    /**
     * @return element in the list adapter
     */
    public int getAdapterCount() {
        return 0;
    }

    /**
     * refresh adapter for update. this may be due to update of list async
     */
    protected void refreshAdapter() {
    }

    /**
     * read elements in list
     *
     * @param size max number of element to read
     * @param <T>  a type of {@code MediaModel}
     * @return fetched elements
     */
    protected <T extends MediaModel> List<T> readList(int size) {
        return null;
    }

    /**
     * set read position to 0
     */
    protected void reset() {
        readPosition = 0;
    }

    /**
     * move readPosition
     *
     * @param readLength
     */
    protected void moveReadPosition(int readLength) {
        readPosition += readLength;
    }


    /**
     * get current read position
     *
     * @return
     */
    protected int getReadPosition() {
        return readPosition;
    }

    /**
     * check if there exists any element in the list
     *
     * @return
     */
    protected boolean isReadable() {
        return false;
    }
}

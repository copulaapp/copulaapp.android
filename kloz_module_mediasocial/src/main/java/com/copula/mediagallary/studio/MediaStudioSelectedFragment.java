package com.copula.mediagallary.studio;

import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.localservice.profile.DaoSharedCollection;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.AbsHelperListView;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

import java.util.List;

/**
 * Created by heeleaz on 2/22/17.
 */
public class MediaStudioSelectedFragment extends AbsMediaStudioFragment implements AdapterView.OnItemClickListener {
    private BaseAdapter baseAdapter;
    private AbsListView absListView;
    private AbsHelperListView absHelperListView;

    private View vListEmpty, vListProgress;

    private DaoSharedCollection DaoSharedCollection;
    private int mediaType;

    public static MediaStudioSelectedFragment instantiate(int mediaType) {
        Bundle bundle = new Bundle(1);
        bundle.putInt("media_type", mediaType);

        MediaStudioSelectedFragment fragment = new MediaStudioSelectedFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_media_studio_selected, container, false);
        TextView txtEmptyList = (TextView) view.findViewById(R.id.txt_empty_list_message);
        ImageView imgEmptyList = (ImageView) view.findViewById(R.id.img_empty_list_image);
        vListEmpty = view.findViewById(R.id.view_empty_list);
        vListProgress = view.findViewById(R.id.view_list_progress);

        this.mediaType = getArguments().getInt("media_type");

        int activitySelected = StudioActivityCallback.ACTIVITY_SELECTED;
        if (mediaType == MediaModel.IMAGE) {
            txtEmptyList.setText(R.string.msg_studio_select_image);
            imgEmptyList.setImageResource(R.drawable.img_image_studio_empty_256dp);

            absListView = (AbsListView) view.findViewById(R.id.grd);
            absListView.setAdapter(baseAdapter
                    = new AdaptImageStudioSelector(getActivity(), activitySelected));
        } else if (mediaType == MediaModel.AUDIO) {
            txtEmptyList.setText(R.string.msg_studio_select_audio);
            imgEmptyList.setImageResource(R.drawable.img_audio_studio_empty_256dp);

            absListView = (AbsListView) view.findViewById(R.id.lst);
            absListView.setAdapter(baseAdapter
                    = new AdaptAudioStudioSelector(getActivity(), activitySelected));
        } else {
            txtEmptyList.setText(R.string.msg_studio_select_video);
            imgEmptyList.setImageResource(R.drawable.img_video_studio_empty_256dp);

            absListView = (AbsListView) view.findViewById(R.id.lst);
            absListView.setAdapter(baseAdapter
                    = new AdaptVideoStudioSelector(getActivity(), activitySelected));
        }

        absHelperListView = (AbsHelperListView) absListView;
        absListView.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DaoSharedCollection = new DaoSharedCollection(getActivity());

        new PopulateMediaAsync(mediaType, DesignHelper.page(0, 10000)).execute();

    }

    private void initImage(AdaptImageStudioSelector adapter, String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        List<ImageModel> medias =
                SystemMediaProvider.getImages(getActivity(), null, orderBy);

        if (medias.size() > 0) {
            medias.retainAll(DaoSharedCollection.getMedias(MediaModel.IMAGE));
            if (medias.size() > 0) {
                adapter.addHandlers(medias);
                absHelperListView.setPagerStep(adapter.getCount());
            }
        }
    }

    private void initAudio(AdaptAudioStudioSelector adapter, String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " = 1";
        List<AudioModel> medias =
                SystemMediaProvider.getAudios(getActivity(), selection, orderBy);

        if (medias.size() > 0) {
            medias.retainAll(DaoSharedCollection.getMedias(MediaModel.AUDIO));
            if (medias.size() > 0) {
                adapter.addHandlers(medias);
                absHelperListView.setPagerStep(adapter.getCount());
            }
        }
    }

    private void initVideo(AdaptVideoStudioSelector adapter, String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        List<VideoModel> medias =
                SystemMediaProvider.getVideos(getActivity(), null, orderBy);

        if (medias.size() > 0) {
            medias.retainAll(DaoSharedCollection.getMedias(MediaModel.VIDEO));
            if (medias.size() > 0) {
                adapter.addHandlers(medias);
                absHelperListView.setPagerStep(adapter.getCount());
            }
        }
    }

    protected void addMedia(MediaModel model) {
        if (baseAdapter == null || getActivity() == null || model == null) return;

        DaoSharedCollection.addMedia(model);
        baseAdapter.addOrReplaceHandler(model);
        this.refreshAdapter();

        absListView.smoothScrollToPosition(baseAdapter.getCount());
    }

    @Override
    protected void addMedia(List<MediaModel> mediaModels) {
        if (baseAdapter == null || getActivity() == null) return;
        if (mediaModels == null || mediaModels.size() == 0) return;

        DaoSharedCollection.addMediaList(mediaModels);
        baseAdapter.addOrReplaceHandlers(mediaModels);
        refreshAdapter();
    }

    @Override
    protected void removeMedia(MediaModel model) {
        if (baseAdapter == null || getActivity() == null) return;

        DaoSharedCollection.removeMedia(model);
        baseAdapter.removeHandler(model);
        this.refreshAdapter();
    }

    @Override
    protected void refreshAdapter() {
        baseAdapter.notifyDataSetChanged();
        if (baseAdapter.getCount() <= 0) DesignHelper.showOnlyView(vListEmpty);
        else DesignHelper.showOnlyView(absListView);

        StudioActivityCallback callback = (StudioActivityCallback) getActivity();
        callback.onPreparedCount(StudioActivityCallback.ACTIVITY_SELECTED, getAdapterCount());
    }

    @Override
    protected void addMediaAsync(List<MediaModel> mediaModel) {
        DaoSharedCollection.addMediaList(mediaModel);
        baseAdapter.addOrReplaceHandlers(mediaModel);
    }

    @Override
    protected void clearRecord() {
        DaoSharedCollection.clearMedia(this.mediaType);
    }

    @Override
    protected void clearList() {
        if (baseAdapter == null || getActivity() == null) return;

        DaoSharedCollection.clearMedia(mediaType);
        baseAdapter.clear();

        DesignHelper.showOnlyView(vListEmpty);

        StudioActivityCallback callback = (StudioActivityCallback) getActivity();
        callback.onPreparedCount(StudioActivityCallback.ACTIVITY_SELECTED, 0);
    }

    @Override
    public int getAdapterCount() {
        return (baseAdapter != null) ? baseAdapter.getCount() : 0;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MediaModel media = (MediaModel) baseAdapter.getItem(position);
        if (media == null) return;

        if (getActivity() instanceof StudioActivityCallback) {
            StudioActivityCallback callback = (StudioActivityCallback) getActivity();
            callback.addMedia(StudioActivityCallback.ACTIVITY_SELECTED, media.mediaType, media);

            removeMedia(media);
        }
    }//end

    @Override
    protected List<MediaModel> readList(int size) {
        return baseAdapter.getItems();
    }

    @Override
    protected boolean isReadable() {
        return !baseAdapter.isEmpty();
    }

    private class PopulateMediaAsync extends AsyncTask<Void, Void, Void> {
        private String limit;
        private int mediaType;

        PopulateMediaAsync(int mediaType, String limit) {
            this.limit = limit;
            this.mediaType = mediaType;

            DesignHelper.showOnlyView(vListProgress);
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (mediaType == MediaModel.IMAGE) {
                initImage((AdaptImageStudioSelector) baseAdapter, limit);
            } else if (mediaType == MediaModel.AUDIO) {
                initAudio((AdaptAudioStudioSelector) baseAdapter, limit);
            } else if (mediaType == MediaModel.VIDEO) {
                initVideo((AdaptVideoStudioSelector) baseAdapter, limit);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (getActivity() == null) return;

            if (baseAdapter != null && baseAdapter.getCount() > 0) {
                DesignHelper.showOnlyView(absListView);
                baseAdapter.notifyDataSetChanged();
            } else DesignHelper.showOnlyView(vListEmpty);
        }
    }//END
}

package com.copula.mediagallary.studio;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.provider.SystemImageProvider;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

public class AdaptAudioStudioSelector extends BaseAdapter<AudioModel> {
    private int displayWidth;
    private int activity;
    private SystemImageProvider imageProvider;

    public AdaptAudioStudioSelector(Context context, int activity) {
        super(context);
        this.activity = activity;
        imageProvider = new SystemImageProvider(context);
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_a_v_gallery, null, false);
            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.duration = (TextView) convertView.findViewById(R.id.txt_media_duration);
            holder.title = (TextView) convertView.findViewById(R.id.txt_media_title);

            convertView.findViewById(R.id.chk_chooser).setVisibility(View.GONE);
            if (activity == StudioActivityCallback.ACTIVITY_SELECTOR)
                convertView.findViewById(R.id.btn_remove).setVisibility(View.GONE);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        AudioModel entry = getItem(position);
        holder.duration.setText(MediaUtility.formatDuration(entry.duration));
        holder.title.setText(entry.mediaTitle + "\n" + entry.mediaLibrary);

        int dim = displayWidth / 6;
        Uri uri = imageProvider.getAlbumThumbnailUri(entry.albumId);
        Glide.with(getContext()).loadFromMediaStore(uri).override(dim, dim)
                .placeholder(R.drawable.img_media_song_thumb_256dp).into(holder.img);
        return convertView;
    }

    private class ViewHolder {
        ImageView img;
        TextView title, duration;
    }
}
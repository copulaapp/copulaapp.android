package com.copula.mediagallary.studio;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.provider.SystemMediaObserver;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.localservice.profile.DaoSharedCollection;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;

/**
 * Created by heeleaz on 2/28/17.
 */
public class MediaStudioSampleFragment extends Fragment implements View.OnClickListener {
    private TextView txtAudioCount, txtImageCount, txtVideoCount;
    private ImageView imgAudio, imgVideo, imgImage;
    private LinearLayout vContainerNewMedia;
    private Button btnOpenMedia;

    private DaoSharedCollection daoMediaList;
    private SystemMediaObserver systemMediaObserver;

    public static MediaStudioSampleFragment instantiate() {
        return new MediaStudioSampleFragment();
    }

    private String composeCount(int mediaType, int count) {
        int resString;
        if (mediaType == MediaModel.IMAGE) {
            resString = count > 1 ? R.string.txt_images : R.string.txt_image;
        } else if (mediaType == MediaModel.AUDIO) {
            resString = count > 1 ? R.string.txt_songs : R.string.txt_song;
        } else {
            resString = (count > 1) ? R.string.txt_videos : R.string.txt_video;
        }
        return count + " " + getString(resString);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_studio_media_sampleview, container, false);
        txtAudioCount = (TextView) view.findViewById(R.id.txt_studio_audio_count);
        txtImageCount = (TextView) view.findViewById(R.id.txt_studio_image_count);
        txtVideoCount = (TextView) view.findViewById(R.id.txt_studio_video_count);
        vContainerNewMedia = (LinearLayout) view.findViewById(R.id.view_container_newmedia);
        btnOpenMedia = (Button) view.findViewById(R.id.btn_open_studio);

        setCompoundDrawable(txtAudioCount, R.drawable.img_collection_grey_64dp);
        setCompoundDrawable(txtImageCount, R.drawable.img_collection_grey_64dp);
        setCompoundDrawable(txtVideoCount, R.drawable.img_collection_grey_64dp);

        (imgAudio = (ImageView) view.findViewById(R.id.img_studio_audio)).setOnClickListener(this);
        (imgVideo = (ImageView) view.findViewById(R.id.img_studio_video)).setOnClickListener(this);
        (imgImage = (ImageView) view.findViewById(R.id.img_studio_image)).setOnClickListener(this);

        ViewGroup.LayoutParams layoutParams = imgAudio.getLayoutParams();
        int pad = DesignHelper.scaleInDP(getActivity(), 15);
        layoutParams.height = ((getResources().getDisplayMetrics().widthPixels) / 3) - pad;
        imgAudio.setLayoutParams(layoutParams);
        imgVideo.setLayoutParams(layoutParams);
        imgImage.setLayoutParams(layoutParams);

        btnOpenMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedCollectionActivity.launch(getActivity(), 0);
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        daoMediaList = new DaoSharedCollection(getActivity());
        systemMediaObserver = new SystemMediaObserver(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();

        int ic = daoMediaList.getCount(MediaModel.IMAGE);
        txtImageCount.setText(composeCount(MediaModel.IMAGE, ic));

        int ac = daoMediaList.getCount(MediaModel.AUDIO);
        txtAudioCount.setText(composeCount(MediaModel.AUDIO, ac));

        int vc = daoMediaList.getCount(MediaModel.VIDEO);
        txtVideoCount.setText(composeCount(MediaModel.VIDEO, vc));


        loadThumbnail(MediaModel.IMAGE, imgImage, R.drawable.img_media_image_thumb_256dp);
        loadThumbnail(MediaModel.AUDIO, imgAudio, R.drawable.img_media_song_thumb_256dp);
        loadThumbnail(MediaModel.VIDEO, imgVideo, R.drawable.img_media_video_thumb2_256dp);

        if (!systemMediaObserver.hasAddedNewMedia()) {
            ((View) vContainerNewMedia.getParent()).setVisibility(View.GONE);
        } else {
            int imgDim = DesignHelper.scaleInDP(getActivity(), 24);
            for (MediaModel media : systemMediaObserver.createShuffledLatestMedia(3)) {
                ImageView imageView = produceImageView(imgDim, media);
                vContainerNewMedia.addView(imageView);
            }
            systemMediaObserver.ackLatestMedia();
        }
    }

    private void loadThumbnail(int mediaType, ImageView imageView, int plh) {
        MediaModel model = daoMediaList.getTopModel(mediaType);
        if (model != null) {
            model = SystemMediaProvider.getMedia(
                    getActivity(), model.mediaType, model.mediaId);
            if (model != null) {
                String url = MediaUtility
                        .appendSize(model.thumbnailUrl, imageView.getHeight());
                Picasso.with(getActivity()).load(url).placeholder(plh).into(imageView);
            }
        }
    }//end

    private void setCompoundDrawable(TextView textView, int drawable) {
        Drawable dr = DesignHelper.scaleDrawable(getContext(), drawable, 20, 20);
        textView.setCompoundDrawablesWithIntrinsicBounds(dr, null, null, null);
    }

    private ImageView produceImageView(int dim, MediaModel media) {
        String imgUrl = MediaUtility.appendSize(media.thumbnailUrl, dim);
        ImageView imageView = new ImageView(getActivity());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dim, dim);
        params.setMargins(-5, 0, 0, 0);
        imageView.setLayoutParams(params);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        int placeholder = 0;
        switch (media.mediaType) {
            case MediaModel.AUDIO:
                placeholder = R.drawable.img_media_song_thumb_128dp;
                break;
            case MediaModel.IMAGE:
                placeholder = R.drawable.img_media_image_thumb_128dp;
                break;
            case MediaModel.VIDEO:
                placeholder = R.drawable.img_media_video_thumb2_128dp;
                break;
        }

        Picasso.with(getActivity()).load(imgUrl).placeholder(placeholder).into(imageView);
        return imageView;
    }

    @Override
    public void onClick(View v) {
        int page = 0;
        if (v == imgAudio) page = 1;
        else if (v == imgVideo) page = 2;

        SharedCollectionActivity.launch(getActivity(), page);
    }
}

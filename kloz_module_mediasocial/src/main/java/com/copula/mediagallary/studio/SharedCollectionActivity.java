package com.copula.mediagallary.studio;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListPopupWindow;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.genericlook.CopulaProgressDialog;
import com.copula.genericlook.CustomActionBar;
import com.copula.genericlook.DesignHelper;
import com.copula.mediagallary.observer.MediaObserverService;
import com.copula.mediasocial.R;
import com.copula.support.android.content.TabActivity;
import com.copula.support.android.view.widget.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 2/22/17.
 */
public class SharedCollectionActivity extends TabActivity implements StudioActivityCallback, View.OnClickListener,
        AdapterView.OnItemClickListener, ServiceConnection {
    private List<AbsMediaStudioFragment> studioSelectedFragments = new ArrayList<>(3);

    private CustomActionBar actionBar;
    private ViewPager selectedStudioPager;
    private ListPopupWindow listPopupWindow;

    private boolean serviceStarted;

    public static void launch(Context context, int page) {
        Intent intent = new Intent(context, SharedCollectionActivity.class);
        context.startActivity(intent.putExtra("page", page));
    }

    public static void launchForResult(Activity activity, int resultCode) {
        Intent intent = new Intent(activity, SharedCollectionActivity.class);
        activity.startActivityForResult(intent, resultCode);
    }

    @Override
    protected void onPrepareActivityWindow() {
        setTabViewStyle(R.style.AppTheme_MediaGallery_StudioSelectorTab);
        addTab(R.string.tab_image).addTab(R.string.tab_audio).addTab(R.string.tab_video);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_shared_collection);

        actionBar = new CustomActionBar(this).setBackListener(this);
        actionBar.setView(R.layout.bar_media_studio_selector).compile();

        selectedStudioPager = (ViewPager) findViewById(R.id.pager_selected_media);
        selectedStudioPager.setAdapter(new SelectedPagerAdapter());
        selectedStudioPager.addOnPageChangeListener(this);

        Button btnOption = (Button) findViewById(R.id.btn_bar_option);
        listPopupWindow = new ListPopupWindow(this);
        listPopupWindow.setAdapter(setupOptionAdapter());
        listPopupWindow.setAnchorView(btnOption);
        listPopupWindow.setWidth(DesignHelper.scaleInDP(this, 120));
        listPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(this);

        btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listPopupWindow.isShowing()) listPopupWindow.dismiss();
                else listPopupWindow.show();
            }
        });


        onPageSelected(getIntent().getIntExtra("page", 0));
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, MediaObserverService.class), this, Context.BIND_AUTO_CREATE);
    }

    private ArrayAdapter<String> setupOptionAdapter() {
        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<>(this, R.layout.spnadp_studio_selector_opt);
        spinnerAdapter.add("Take back all");
        spinnerAdapter.add("Select all");
        return spinnerAdapter;
    }

    @Override
    protected List<Fragment> getPagerFragments() {
        List<Fragment> fragments = new ArrayList<>(3);
        fragments.add(MediaStudioSelectorFragment.instantiate(MediaModel.IMAGE));
        fragments.add(MediaStudioSelectorFragment.instantiate(MediaModel.AUDIO));
        fragments.add(MediaStudioSelectorFragment.instantiate(MediaModel.VIDEO));

        studioSelectedFragments.add(
                MediaStudioSelectedFragment.instantiate(MediaModel.IMAGE));
        studioSelectedFragments.add(
                MediaStudioSelectedFragment.instantiate(MediaModel.AUDIO));
        studioSelectedFragments.add(
                MediaStudioSelectedFragment.instantiate(MediaModel.VIDEO));

        return fragments;
    }

    @Override
    protected ViewPager getViewPager() {
        return (ViewPager) findViewById(R.id.pager);
    }


    @Override
    public void onPageSelected(int pos) {
        super.onPageSelected(pos);
        selectedStudioPager.setCurrentItem(pos);

        if (pos == 0) actionBar.setTitle(R.string.lbl_select_image);
        else if (pos == 1) actionBar.setTitle(R.string.lbl_select_audio);
        else actionBar.setTitle(R.string.lbl_select_video);
    }

    @Override
    public void removeMedia(int activity, int mediaType, MediaModel media) {
        int index = mediaType - 1;

        if (activity == ACTIVITY_SELECTOR) {
            ((AbsMediaStudioFragment) getFragment(index)).removeMedia(media);
        } else studioSelectedFragments.get(index).removeMedia(media);
    }

    @Override
    public void addMedia(int activity, int mediaType, MediaModel media) {
        int index = mediaType - 1;

        if (activity == ACTIVITY_SELECTOR) {
            studioSelectedFragments.get(index).addMedia(media);
        } else {
            ((AbsMediaStudioFragment) getFragment(index)).addMedia(media);
        }
    }

    @Override
    public void onPreparedCount(int activity, int count) {
        if (activity == ACTIVITY_SELECTED) makeSelectedCountTitle(count);
    }

    private void makeSelectedCountTitle(int count) {
        actionBar.setTitle(count + " " + getString(R.string.txt_selected));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_bar_back) finish();
    }//end

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listPopupWindow.dismiss();

        int page = getCurrentItem();
        final AbsMediaStudioFragment selector = (AbsMediaStudioFragment) getFragment(page);
        final AbsMediaStudioFragment selected = studioSelectedFragments.get(page);

        if (position == 0) {
            //fetch all the element in selected fragment, place them in selector
            selector.addMedia(selected.readList(-1));
            selected.clearList();
        } else {//select all: select from selector
            if (selector.isReadable()) new SelectAllAsync(selector, selected).execute();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        serviceStarted = true;
        MediaObserverService.MyBinder b = (MediaObserverService.MyBinder) service;
        b.getService().ackLatestMedia();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        serviceStarted = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (serviceStarted) unbindService(this);
    }

    private class SelectAllAsync extends AsyncTask<Void, Void, Void> {
        private CopulaProgressDialog progressDialog;
        private AbsMediaStudioFragment selector, selected;

        SelectAllAsync(AbsMediaStudioFragment s, AbsMediaStudioFragment t) {
            selector = s;
            selected = t;
            progressDialog = new CopulaProgressDialog(SharedCollectionActivity.this);
            progressDialog.setMessage(R.string.prg_update);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            selected.clearRecord();//remove all old data from record
            List<MediaModel> m;
            while ((m = selector.readList(500)) != null) selected.addMediaAsync(m);
            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            selected.refreshAdapter();
            selector.clearList();

            progressDialog.dismiss();
        }
    }//END

    private class SelectedPagerAdapter extends FragmentPagerAdapter {
        SelectedPagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public Fragment getItem(int i) {
            return studioSelectedFragments.get(i);
        }

        @Override
        public int getCount() {
            return studioSelectedFragments.size();
        }
    }//END
}

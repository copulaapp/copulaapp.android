package com.copula.mediagallary.studio;

import com.copula.functionality.localservice.media.MediaModel;

/**
 * Created by heeleaz on 2/22/17.
 */
public interface StudioActivityCallback {
    public static final int ACTIVITY_SELECTOR = 0;
    public static final int ACTIVITY_SELECTED = 1;

    void removeMedia(int activity, int mediaType, MediaModel media);

    void addMedia(int activity, int mediaType, MediaModel media);

    void onPreparedCount(int activity, int count);
}

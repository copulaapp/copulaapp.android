package com.copula.mediagallary.studio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;


public class AdaptVideoStudioSelector extends BaseAdapter<VideoModel> {
    private int displayWidth;
    private int activity;

    public AdaptVideoStudioSelector(Context context, int activity) {
        super(context);
        this.activity = activity;
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_a_v_gallery, null, false);
            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.duration = (TextView) convertView.findViewById(R.id.txt_media_duration);
            holder.title = (TextView) convertView.findViewById(R.id.txt_media_title);

            convertView.findViewById(R.id.chk_chooser).setVisibility(View.GONE);
            if (activity == StudioActivityCallback.ACTIVITY_SELECTOR)
                convertView.findViewById(R.id.btn_remove).setVisibility(View.GONE);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();


        VideoModel entry = getItem(position);
        holder.title.setText(entry.mediaTitle);
        holder.duration.setText(MediaUtility.formatDuration(entry.duration));

        String url = MediaUtility.appendSize(entry.thumbnailUrl, displayWidth / 6);
        Picasso.with(getContext()).load(url).placeholder(R.drawable.img_media_video_thumb2_256dp).into(holder.img);
        return convertView;
    }

    private class ViewHolder {
        ImageView img;
        TextView title, duration;
    }
}
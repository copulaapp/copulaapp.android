package com.copula.mediagallary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.genericlook.CustomActionBar;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;

import java.util.List;

public class MediaChooserGalleryActivity extends AbsMediaGalleryActivity implements OnItemClickListener {
    public static final String EXTRA_MEDIA = "mediaModel";
    public static final int CHOOSE_IMAGE = 1, CHOOSE_AUDIO = 2, CHOOSE_VIDEO = 3;

    public static void launch(Activity ctx, int mediaType, String title, int requestCode) {
        Intent intent = new Intent(ctx, MediaChooserGalleryActivity.class);
        intent.putExtra("mediaType", mediaType);
        intent.putExtra("title", title);
        ctx.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_media_chooser_gallery);

        String title = getIntent().getStringExtra("title");
        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(title);
        actionBar.setBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }).compile();

        UserAccountBase userAccountBase = new UserAccountBase(this);
        UserSession.setHostProfile(userAccountBase.getUserProfile());
    }

    @Override
    protected AbsListView getAbsListView(int mediaType) {
        if (mediaType == MediaModel.IMAGE)
            return (AbsListView) findViewById(R.id.grd);
        else return (AbsListView) findViewById(R.id.lst);
    }

    @Override
    protected BaseAdapter getAbsListViewAdapter(int mediaType) {
        if (mediaType == MediaModel.IMAGE) {
            AdaptImageChooserGallery adapter = new AdaptImageChooserGallery(this);
            adapter.setShowRemoveButton(false);
            return adapter;
        } else if (mediaType == MediaModel.AUDIO) {
            AdaptAudioChooserGallery adapter = new AdaptAudioChooserGallery(this);
            adapter.setShowButtonRemove(false);

            return adapter;
        } else {
            AdaptVideoChooserGallery adapter = new AdaptVideoChooserGallery(this);
            adapter.setShowButtonRemove(false);

            return adapter;
        }
    }

    @Override
    protected List<ImageModel> initImage(String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        return SystemMediaProvider.getImages(this, null, orderBy);
    }

    @Override
    protected List<AudioModel> initAudio(String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT " + limit;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " = 1";
        return SystemMediaProvider.getAudios(this, selection, orderBy);
    }

    @Override
    protected List<VideoModel> initVideo(String limit) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT" + limit;
        return SystemMediaProvider.getVideos(this, null, orderBy);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MediaModel media = (MediaModel) parent.getAdapter().getItem(position);
        if (media == null || media.dataPath == null) return;

        if (!media.status) {
            setResult(RESULT_OK, new Intent().putExtra(EXTRA_MEDIA, media));
            finish();
        }
    }//end onItemClick

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}
package com.copula.mediagallary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;


public class AdaptVideoChooserGallery extends BaseAdapter<VideoModel> {
    private int displayWidth;
    private boolean showButtonRemove;

    public AdaptVideoChooserGallery(Context context) {
        super(context);
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    public void setShowButtonRemove(boolean showButtonRemove) {
        this.showButtonRemove = showButtonRemove;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_a_v_gallery, null, false);
            holder.chk = (CheckBox) convertView.findViewById(R.id.chk_chooser);
            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);
            holder.duration = (TextView) convertView.findViewById(R.id.txt_media_duration);
            holder.title = (TextView) convertView.findViewById(R.id.txt_media_title);

            View btnRemove = convertView.findViewById(R.id.btn_remove);
            if (showButtonRemove) btnRemove.setVisibility(View.VISIBLE);
            else btnRemove.setVisibility(View.GONE);

            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chk.performClick();
                ((AbsListView) v.getParent()).performItemClick(v, position, position);
            }
        });

        VideoModel entry = getItem(position);
        holder.chk.setChecked(entry.status);
        holder.title.setText(entry.mediaTitle);
        holder.duration.setText(MediaUtility.formatDuration(entry.duration));

        String url = MediaUtility.appendSize(entry.thumbnailUrl, displayWidth / 6);
        Picasso.with(getContext()).load(url).placeholder(R.drawable.img_media_video_thumb2_256dp).into(holder.img);
        return convertView;
    }

    private class ViewHolder {
        ImageView img;
        CheckBox chk;
        TextView title, duration;
    }
}
package com.copula.mediagallary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.squareup.picasso.Picasso;

import java.io.File;

import static android.widget.CompoundButton.OnCheckedChangeListener;

public class AdaptImageChooserGallery extends BaseAdapter<ImageModel> {
    private int displayWidth;
    private boolean showRemoveButton = true;

    public AdaptImageChooserGallery(Context context) {
        super(context);
        displayWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    private static int chkBGBaseOnStatus(boolean b) {
        if (b) return R.drawable.bkg_block_media_chkcover;
        else return 0;
    }


    public void setShowRemoveButton(boolean showRemoveButton) {
        this.showRemoveButton = showRemoveButton;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_image_gallery, null, false);
            holder.chk = (CheckBox) convertView.findViewById(R.id.chk_media_thumbnail);
            holder.img = (ImageView) convertView.findViewById(R.id.img_media_thumbnail);

            LayoutParams img_p = (LayoutParams) holder.img.getLayoutParams();
            img_p.width = img_p.height = displayWidth / 4;
            holder.img.setLayoutParams(img_p);

            ViewGroup.LayoutParams chk_p = holder.chk.getLayoutParams();
            chk_p.width = chk_p.height = displayWidth / 4;
            holder.chk.setLayoutParams(chk_p);

            View btnRemove = convertView.findViewById(R.id.btn_remove);
            if (showRemoveButton) btnRemove.setVisibility(View.VISIBLE);
            else btnRemove.setVisibility(View.GONE);

            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        final View finalConvertView = convertView;
        holder.chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean c) {
                holder.chk.setBackgroundResource(chkBGBaseOnStatus(c));
                AbsListView lv = ((AbsListView) finalConvertView.getParent());
                if (lv != null) lv.performItemClick(buttonView, position, position);
            }
        });

        ImageModel entry = getItem(position);
        holder.chk.setChecked(entry.status);

        int dim = displayWidth / 4;
        Picasso.with(getContext()).load(new File(entry.dataPath))
                .resize(dim, dim).centerCrop()
                .placeholder(R.drawable.img_media_image_thumb_256dp).into(holder.img);
        return convertView;
    }

    private class ViewHolder {
        ImageView img;
        CheckBox chk;
    }
}
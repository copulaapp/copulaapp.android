package com.copula.nettysocial;

import android.content.Context;
import com.copula.functionality.IContextBootstrap;
import com.copula.nettysocial.nettyservice.feeds.DaoExploreFeeds;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.nettysocial.feed.helper.FeedCacheHelper;

import java.util.List;

/**
 * Created by heeleaz on 2/16/17.
 */
public class NettySocialBootstrap implements IContextBootstrap {
    private Context context;

    public NettySocialBootstrap(Context context) {
        this.context = context;
    }

    private static void clearImageCache(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FeedCacheHelper.clearImageRes(context);
            }
        }).start();
    }

    private void cleanTimelineCache() {
        DaoExploreFeeds dao = new DaoExploreFeeds(context);
        List<MediaFeedEntry> models = dao.getOutdatedDataList(20);
        if (models != null) {
            for (MediaFeedEntry model : models) dao.deleteFeed(model);
        }
        NettySocialBootstrap.clearImageCache(context);
    }

    @Override
    public void boot(BootstrapListener callback) {
        this.cleanTimelineCache();
    }
}

package com.copula.nettysocial.channel.subscriptions;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.R;
import com.copula.nettysocial.profile.helper.UserFollowHelper;
import com.copula.support.android.view.widget.*;
import com.squareup.picasso.Picasso;

class AdapterSubsChannels extends BaseAdapter<UserProfileEntry> {
    static final int VIEW_FOLLOW = R.id.btn_follow_toggle;

    AdapterSubsChannels(Activity context) {
        super(context);
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_subs_channels, null, false);
            holder.name = (TextView) convertView.findViewById(R.id.txt_username);
            holder.bio = (TextView) convertView.findViewById(R.id.txt_profile_bio);
            holder.dp = (ImageView) convertView.findViewById(R.id.img_profile_image);
            holder.followToggle = (Button) convertView.findViewById(VIEW_FOLLOW);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        UserProfileEntry ph = getItem(position);
        holder.name.setText(ph.username);
        holder.bio.setText(ph.bio);
        UserFollowHelper.friendButton(holder.followToggle, ph.isFollowing);

        Picasso.with(getContext()).load(ph.imgUrl)
                .placeholder(R.drawable.img_profile_grey_placeholder).into(holder.dp);

        final View fConvertView = convertView;
        holder.followToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) (fConvertView.getParent()))
                        .performItemClick(v, position, position);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView name, bio;
        ImageView dp;
        Button followToggle;
    }

}

package com.copula.nettysocial.channel.subscriptions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.CustomActionBar;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.channel.helpers.GetChannelsHelper;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.profile.UserSocialProfileActivity;
import com.copula.nettysocial.profile.helper.AsyncAPIRequest;
import com.copula.nettysocial.profile.helper.UserFollowHelper;
import com.copula.nettysocial.profile.helper.UserUnfollowHelper;
import com.copula.support.android.view.widget.AbsListViewScrollListener;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ListView;

/**
 * Created by heeleaz on 7/19/17.
 */
public class SubsChannelListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        AbsHelper.ActCallback, View.OnClickListener, AbsListViewScrollListener, AsyncAPIRequest.UserActCallback {
    private String pageToken;
    private View vProgress, vEmptyList;
    private ListView listView;
    private AdapterSubsChannels adapter;
    private String appUserId;

    public static void launch(Context context) {
        context.startActivity(new Intent(context, SubsChannelListActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_subs_channel_list);
        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(R.string.label_subscriptions).setBackListener(this);
        actionBar.compile();

        vProgress = findViewById(R.id.view_list_progress);
        (listView = (ListView) findViewById(R.id.lst)).setOnItemClickListener(this);
        (vEmptyList = findViewById(R.id.view_empty_list)).setOnClickListener(this);

        listView.setAdapter(adapter = new AdapterSubsChannels(this));
        listView.setHelperScrollListener(this);

        this.appUserId = new UserAccountBase(this).getUserId();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetChannelsHelper(appUserId, pageToken, this).execute();
    }

    @Override
    public void onPreAct(AbsHelper sf, int act, MediaFeedEntry media) {
        vProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPostAct(AbsHelper sf, int act, MediaFeedEntry media) {
        GetChannelsHelper helper = (GetChannelsHelper) sf;
        if (helper.isSuccessful()) {
            pageToken = helper.getPageToken();
            adapter.addOrReplaceHandlers(helper.getResult());
            adapter.notifyDataSetChanged();
        }

        if (adapter.getCount() == 0) DesignHelper.showOnlyView(vEmptyList);
        else DesignHelper.showOnlyView(listView);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_bar_back) finish();
    }

    private void dispatchMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserProfileEntry e = adapter.getItem(position);
        if (view.getId() == AdapterSubsChannels.VIEW_FOLLOW) {
            if (e.isFollowing) {
                UserUnfollowHelper f =
                        new UserUnfollowHelper(this, e.userId, this);
                f.setTag("view", view).setTag("user", e).execute();
            } else {
                UserFollowHelper f =
                        new UserFollowHelper(this, e.userId, this);
                f.setTag("view", view).setTag("user", e).execute();
            }
        } else UserSocialProfileActivity.launch(this, e);
    }

    @Override
    public void onPreAct(AsyncAPIRequest sf, int act) {
        if (act == UNFOLLOW || act == FOLLOW) {
            Button btn = (Button) sf.getTag("view");
            btn.startProgressing(R.layout.view_btnprg_follow_toggle);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPostAct(AsyncAPIRequest sf, int act) {
        if (act == FOLLOW || act == UNFOLLOW) {
            ((Button) sf.getTag("view")).finishProgressing();
            if (sf.isSuccessful()) {
                UserProfileEntry user = (UserProfileEntry) sf.getTag("user");
                user.isFollowing = !user.isFollowing;
                adapter.notifyDataSetChanged();
            } else {
                dispatchMessage(getString(R.string.msg_failed_social_follow));
            }
        }
    }//END

    @Override
    public void onPageScroll(int totalItem, int pageOffset, int pagerStep) {
        new GetChannelsHelper(appUserId, pageToken, this).execute();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {
    }
}

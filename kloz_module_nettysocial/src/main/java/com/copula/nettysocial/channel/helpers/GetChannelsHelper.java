package com.copula.nettysocial.channel.helpers;

import android.os.AsyncTask;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.feed.helper.AbsHelper;

import java.util.List;

/**
 * Created by heeleaz on 7/13/17.
 */
public class GetChannelsHelper extends AsyncTask<Void, Void, RestResponse<List<UserProfileEntry>>> implements AbsHelper {
    private String pageToken, userId;
    private ActCallback callback;
    private boolean isSuccessful;
    private List<UserProfileEntry> result;

    public GetChannelsHelper(String userId, String pageToken, ActCallback c) {
        this.callback = c;
        this.pageToken = pageToken;
        this.userId = userId;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null)
            callback.onPreAct(this, ActCallback.ACT_GET, null);
    }

    @Override
    protected RestResponse<List<UserProfileEntry>> doInBackground(Void... voids) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        return api.getProfileAPI().getFollowings(userId, pageToken);
    }

    @Override
    protected void onPostExecute(RestResponse<List<UserProfileEntry>> l) {
        if (isSuccessful = (l != null && l.statusCode == 1)) {
            this.pageToken = l.nextPageToken;
            result = l.result;
        }

        if (callback != null)
            callback.onPostAct(this, ActCallback.ACT_GET, null);
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public String getPageToken() {
        return pageToken;
    }

    public List<UserProfileEntry> getResult() {
        return result;
    }
}

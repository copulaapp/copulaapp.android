package com.copula.nettysocial.channel.timeline;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.CustomActionBar;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.channel.helpers.GetTimelineHelper;
import com.copula.nettysocial.channel.subscriptions.SubsChannelListActivity;
import com.copula.nettysocial.channel.subscriptions.SubsChannelShowPatch;
import com.copula.nettysocial.feed.abstimeline.MediaTimelineFragment;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 7/18/17.
 */
public class FeedTimelineActivity extends AppCompatActivity implements View.OnClickListener {
    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, FeedTimelineActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_gen_fragment_container);

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(R.string.label_subscriptions);
        actionBar.setBackListener(this).compile();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, FeedTimelineFragment.instantiate()).commit();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_bar_back) finish();
    }

    public static class FeedTimelineFragment extends MediaTimelineFragment implements View.OnClickListener {
        private String nextPageToken;
        private View btnReload;
        private TextView txtEmptyListMessage;

        public static FeedTimelineFragment instantiate() {
            return new FeedTimelineFragment();
        }

        @Override
        protected void fetchFeedInApproach(int fetchApproach) {
            GetTimelineHelper f = new GetTimelineHelper(getActivity(), fetchApproach);
            f.setCallback(this);
            f.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        @Override
        public View initEmptyView(LayoutInflater inflater, ListView listView) {
            View h = SubsChannelShowPatch.launch(getActivity()).getView();
            listView.addHeaderView(h, null, false);
            h.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubsChannelListActivity.launch(getActivity());
                }
            });

            View v = inflater.inflate(R.layout.view_empty_media_feed_list,
                    null, false);
            txtEmptyListMessage =
                    ((TextView) v.findViewById(R.id.txt_empty_list_message));
            btnReload = v.findViewById(R.id.btn_reload);
            listView.addHeaderView(v, null, false);
            return v;
        }

        @Override
        public View initListProgressView(LayoutInflater inflater, ListView listView) {
            View v = inflater.inflate(R.layout.view_app_export_prg_circle,
                    null, false);
            listView.addFooterView(v, null, false);
            return v;
        }

        @Override
        public void onPostAct(AbsHelper abs, int act, MediaFeedEntry feed) {
            if (act == ACT_GET) {
                GetTimelineHelper h = (GetTimelineHelper) abs;
                if (h.isSuccessful()) {
                    this.nextPageToken = h.getNextPageToken();
                    onFeedFetched(h.getFetchApproach(), h.getResult());
                } else onFeedFetchFailed(h.getStatusCode());
            } else super.onPostAct(abs, act, feed);
        }

        @Override
        protected String getNextPageToken() {
            return nextPageToken;
        }

        @Override
        protected void onFeedFetchFailed(int statusCode) {
            super.onFeedFetchFailed(statusCode);
            if (!DesignHelper.isFragmentAlive(this)) return;

            if (!DesignHelper.isDataEnabled(getActivity())) {
                txtEmptyListMessage.setText(R.string.msg_nonet_feed_timeline);
                btnReload.setVisibility(View.VISIBLE);
            } else if (statusCode == 0) {
                txtEmptyListMessage.setText(R.string.msg_empty_feed_timeline);
                btnReload.setVisibility(View.GONE);
            } else {
                txtEmptyListMessage.setText(R.string.msg_feed_not_available);
                btnReload.setVisibility(View.VISIBLE);
            }
        }
    }//END Fragment
}

package com.copula.nettysocial.channel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.R;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;

/**
 * Created by heeleaz on 7/13/17.
 */
public class AdaptSearchChannels extends BaseAdapter<UserProfileEntry> {
    public AdaptSearchChannels(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_search_channels, null, false);
            holder.img = (ImageView) convertView.findViewById(R.id.img_profile_image);
            holder.username = (TextView) convertView.findViewById(R.id.txt_username);
            holder.bio = (TextView) convertView.findViewById(R.id.txt_profile_bio);

            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        UserProfileEntry entry = getItem(position);
        holder.username.setText(entry.username);
        holder.bio.setText(entry.bio);

        Picasso.with(getContext()).load(entry.imgUrl)
                .placeholder(R.drawable.img_person_black_96dp).into(holder.img);
        return convertView;
    }

    private class ViewHolder {
        private ImageView img;
        private TextView username, bio;
    }
}

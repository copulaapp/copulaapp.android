package com.copula.nettysocial.channel.helpers;

import android.os.AsyncTask;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.feed.helper.AbsHelper;

import java.util.List;

/**
 * Created by heeleaz on 7/13/17.
 */
public class FindChannelHelper extends AsyncTask<Void, Void, RestResponse<List<UserProfileEntry>>>
        implements AbsHelper {
    private String pageToken, q;
    private AbsHelper.ActCallback callback;
    private boolean isSuccessful;
    private List<UserProfileEntry> result;

    public FindChannelHelper() {
    }

    public FindChannelHelper(String q, String pageToken, AbsHelper.ActCallback c) {
        this.callback = c;
        this.pageToken = pageToken;
        this.q = q;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null) callback.onPreAct(this, ActCallback.ACT_GET, null);
    }

    @Override
    protected RestResponse<List<UserProfileEntry>> doInBackground(Void... voids) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        return api.getProfileAPI().findProfile(q, pageToken);
    }

    @Override
    protected void onPostExecute(RestResponse<List<UserProfileEntry>> l) {
        if (isSuccessful = (l != null && l.statusCode == 1)) {
            this.pageToken = l.nextPageToken;
            result = l.result;
        }

        if (callback != null) callback.onPostAct(this, ActCallback.ACT_GET, null);
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public String getPageToken() {
        return pageToken;
    }

    public List<UserProfileEntry> getResult() {
        return result;
    }
}

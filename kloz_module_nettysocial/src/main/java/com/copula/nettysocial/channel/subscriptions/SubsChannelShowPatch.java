package com.copula.nettysocial.channel.subscriptions;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.channel.helpers.GetChannelsHelper;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.profile.UserSocialProfileActivity;
import com.copula.support.android.view.PatchBase;
import com.copula.support.android.view.PatchBundle;
import com.copula.support.android.view.PatchSession;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.GridView;
import com.copula.support.android.view.widget.ImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by heeleaz on 7/18/17.
 */
public class SubsChannelShowPatch extends PatchBase implements AbsHelper.ActCallback, AdapterView.OnItemClickListener {
    private GridView gridView;
    private Adapter adapter;

    public static SubsChannelShowPatch launch(Activity context) {
        PatchSession session = new PatchSession(context);
        return session.patch(SubsChannelShowPatch.class, null, false);
    }

    @Override
    protected void onCreate(PatchBundle bundle, Object... sessionObject) {
        super.onCreate(bundle, sessionObject);
        setContentView(R.layout.pch_subscriptions_show);
        gridView = (GridView) findViewById(R.id.grd);
        gridView.setAdapter(adapter = new Adapter(getContext()));
        gridView.setOnItemClickListener(this);

        int imgInDp = DesignHelper.scaleInDP(getContext(), 40);
        int dw = getContext().getResources().getDisplayMetrics().widthPixels;
        int itemBound = (dw / imgInDp) - 3;
        gridView.setNumColumns(itemBound);

        String limit = DesignHelper.page(0, itemBound);
        String userId = new UserAccountBase(getActivity()).getUserId();
        new GetChannelsHelper(userId, limit, this)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    @Override
    public void onPreAct(AbsHelper sf, int act, MediaFeedEntry media) {
    }

    @Override
    public void onPostAct(AbsHelper sf, int act, MediaFeedEntry media) {
        if (sf.isSuccessful()) {
            adapter.addHandlers(((GetChannelsHelper) sf).getResult());
            adapter.notifyDataSetChanged();
        } else {
            findViewById(R.id.view_show_all).setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserSocialProfileActivity.launch(getActivity(), adapter.getItem(position));
    }

    private class Adapter extends BaseAdapter<UserProfileEntry> {
        Adapter(Context context) {
            super(context);
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            ImageView imageView;
            if (convertView == null) {
                int view = R.layout.adp_subscriptions_show;
                convertView = inflater.inflate(view, null, false);
                imageView = (ImageView) convertView.findViewById(R.id.img_profile_image);
                convertView.setTag(imageView);
            } else imageView = (ImageView) convertView.getTag();

            Picasso.with(getContext()).load(getItem(position).imgUrl)
                    .placeholder(R.drawable.img_profile_grey_placeholder).into(imageView);

            return convertView;
        }
    }//END
}

package com.copula.nettysocial.channel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.channel.helpers.FindChannelHelper;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.profile.UserSocialProfileActivity;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 7/13/17.
 */
public class ChannelSearchFragment extends Fragment implements AdapterView.OnItemClickListener, AbsHelper.ActCallback {
    private ListView listView;
    private AdaptSearchChannels adapter;
    private View vListProgress, vEmptyList;
    private TextView txtEmptyListMessage;

    private FindChannelHelper findChannelHelper;

    public static ChannelSearchFragment instantiate() {
        return new ChannelSearchFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        View v = inflater.inflate(R.layout.frg_search_channel, container, false);
        listView = (ListView) v.findViewById(R.id.lst_channels);
        vListProgress = v.findViewById(R.id.view_list_progress);
        vEmptyList = v.findViewById(R.id.view_empty_list);
        txtEmptyListMessage = (TextView) v.findViewById(R.id.txt_empty_list_message);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView.setAdapter(adapter = new AdaptSearchChannels(getActivity()));
        listView.setOnItemClickListener(this);

        findChannelHelper = new FindChannelHelper();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter.getCount() == 0) DesignHelper.showOnlyView(vEmptyList);
    }

    public void search(String q) {
        findChannelHelper = new FindChannelHelper(q, null, this);
        findChannelHelper.execute();
    }

    @Override
    public void onPreAct(AbsHelper sf, int act, MediaFeedEntry media) {
        adapter.clear();
        DesignHelper.showOnlyView((View) vListProgress.getParent());
        vListProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPostAct(AbsHelper sf, int act, MediaFeedEntry media) {
        vListProgress.setVisibility(View.GONE);

        FindChannelHelper helper = (FindChannelHelper) sf;
        if (helper.isSuccessful() && helper.getResult().size() > 0) {
            adapter.addOrReplaceHandlers(helper.getResult());
            adapter.notifyDataSetChanged();
        }

        if (adapter.getCount() == 0) {
            DesignHelper.showOnlyView(vEmptyList);
            txtEmptyListMessage.setText(R.string.msg_channel_not_found);
        } else {
            DesignHelper.showOnlyView((View) listView.getParent());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserProfileEntry e = adapter.getItem(position);
        UserSocialProfileActivity.launch(getActivity(), e);
    }
}

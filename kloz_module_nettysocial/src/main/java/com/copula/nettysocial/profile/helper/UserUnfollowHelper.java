package com.copula.nettysocial.profile.helper;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by heeleaz on 6/15/17.
 */
public class UserUnfollowHelper extends AsyncTask<Void, Void, RestResponse> implements AsyncAPIRequest {
    private String accUserId, appUserId;
    private UserActCallback callback;

    private boolean isSuccessful;
    private String message;
    private Context context;
    private Map<String, Object> tag = new HashMap<>();

    public UserUnfollowHelper(Context ctx, String accUserId, UserActCallback callback) {
        this.accUserId = accUserId;
        this.appUserId = new UserAccountBase(ctx).getUserId();

        this.callback = callback;
        this.context = ctx;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null)
            callback.onPreAct(this, UserActCallback.UNFOLLOW);
    }

    public Object getTag(String key) {
        return tag.get(key);
    }

    public UserUnfollowHelper setTag(String key, Object value) {
        this.tag.put(key, value);
        return this;
    }

    @Override
    protected RestResponse doInBackground(Void... params) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        return api.getProfileAPI().unFollow(appUserId, accUserId);
    }

    @Override
    protected void onPostExecute(RestResponse rest) {
        if (rest == null) {
            message = context.getString(R.string.msg_failed_social_follow);
        } else if (rest.statusCode != 1) message = rest.statusMessage;
        else isSuccessful = true;

        if (callback != null)
            callback.onPostAct(this, UserActCallback.UNFOLLOW);
    }

    @Override
    public boolean isSuccessful() {
        return isSuccessful;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

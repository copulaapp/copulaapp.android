package com.copula.nettysocial.profile.helper;

/**
 * Created by heeleaz on 6/15/17.
 */
public interface AsyncAPIRequest {
    boolean isSuccessful();

    String getMessage();

    Object getTag(String key);

    Object setTag(String key, Object value);

    interface UserActCallback {
        int FOLLOW = 1, UNFOLLOW = 2, GET_PROFILE = 3;

        void onPreAct(AsyncAPIRequest sf, int act);

        void onPostAct(AsyncAPIRequest sf, int act);
    }
}

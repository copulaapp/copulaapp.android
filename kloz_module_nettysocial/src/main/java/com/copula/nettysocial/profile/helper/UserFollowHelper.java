package com.copula.nettysocial.profile.helper;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.R;
import com.copula.support.android.view.widget.Button;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by heeleaz on 6/15/17.
 */
public class UserFollowHelper extends AsyncTask<Void, Void, RestResponse> implements AsyncAPIRequest {
    private String accUserId, appUserId;
    private UserActCallback callback;

    private boolean isSuccessful;
    private String message;

    private Context context;
    private Map<String, Object> tag = new HashMap<>();

    public UserFollowHelper(Context ctx, String accUserId, UserActCallback callback) {
        this.accUserId = accUserId;
        this.appUserId = new UserAccountBase(ctx).getUserId();

        this.callback = callback;
        this.context = ctx;
    }

    public Object getTag(String key) {
        return tag.get(key);
    }

    public UserFollowHelper setTag(String key, Object value) {
        this.tag.put(key, value);
        return this;
    }

    public static void friendButton(Button btn, boolean following) {
        btn.setBackgroundResource(R.drawable.btn_appcolor_follow_toggle);
        btn.setTag(following);
        btn.setSelected(following);

        Context ctx = btn.getContext();
        if (following) {
            btn.setText(R.string.act_unsubscribe_user);
            btn.setTextColor(ContextCompat.getColor(ctx, R.color.appWhiteTextColor));
        } else {
            btn.setText(R.string.act_subscribe_user);
            btn.setTextColor(ContextCompat.getColor(ctx, R.color.appGreyTextColor));
        }
    }//end

    @Override
    protected void onPreExecute() {
        if (callback != null)
            callback.onPreAct(this, UserActCallback.FOLLOW);
    }

    @Override
    protected RestResponse doInBackground(Void... params) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        return api.getProfileAPI().follow(appUserId, accUserId);
    }

    @Override
    protected void onPostExecute(RestResponse rest) {
        if (rest == null) {
            message = context.getString(R.string.msg_failed_social_follow);
        } else if (isSuccessful = (rest.statusCode == 1)) {
            message = rest.statusMessage;
        }

        if (callback != null)
            callback.onPostAct(this, UserActCallback.FOLLOW);
    }

    @Override
    public boolean isSuccessful() {
        return isSuccessful;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

package com.copula.nettysocial.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.genericlook.CustomActionBar;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.UserFeedLineFragment;
import com.copula.support.android.view.widget.ResultBundle;

/**
 * Created by eliasigbalajobi on 4/12/16.
 */
public class UserSocialProfileActivity extends AppCompatActivity implements ResultBundle {
    private CustomActionBar actionBar;

    private Fragment feedLineFragment;

    public static void launch(Context ctx, UserProfileEntry user) {
        Intent intent = new Intent(ctx, UserSocialProfileActivity.class);
        intent.putExtra("user", user);
        ctx.startActivity(intent);
    }

    public static Intent intent(Context ctx, UserProfileEntry user) {
        Intent intent = new Intent(ctx, UserSocialProfileActivity.class);
        return intent.putExtra("user", user);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_gen_fragment_container);

        actionBar = new CustomActionBar(this);
        actionBar.setBackground(R.drawable.bkg_app_main_actionbar_shadow);
        actionBar.setBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }).compile();

        UserProfileEntry user =
                (UserProfileEntry) getIntent().getSerializableExtra("user");

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        feedLineFragment = UserFeedLineFragment.instantiate(user);
        ft.replace(R.id.fragment, feedLineFragment, "TIMELINE").commit();
    }

    @Override
    public void message(String action, Bundle bundle) {
        actionBar.setTitle(bundle.getString("title"));
    }//end

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        feedLineFragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

package com.copula.nettysocial.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.profile.helper.AsyncAPIRequest;
import com.copula.nettysocial.profile.helper.GetProfileHelper;
import com.copula.nettysocial.profile.helper.UserFollowHelper;
import com.copula.nettysocial.profile.helper.UserUnfollowHelper;
import com.copula.support.android.view.PatchBase;
import com.copula.support.android.view.PatchBundle;
import com.copula.support.android.view.PatchSession;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;

/**
 * Created by eliasigbalajobi on 4/12/16.
 */
public class PatchHeader extends PatchBase implements AsyncAPIRequest.UserActCallback {
    private ImageView imgProfileImage;
    private TextView txtUsername, txtBio, txtFeedCount, txtFollowerCount;
    private Button btnFollowToggle;
    private UserProfileEntry accUserProfile;
    private String appUserId;

    public static PatchHeader instantiate(PatchSession s, UserProfileEntry u) {
        PatchBundle b = new PatchBundle().put("user", u);
        return s.patch(PatchHeader.class, b, false);
    }

    @Override
    protected void onCreate(PatchBundle bundle, Object... sessionObject) {
        setContentView(R.layout.pch_user_feedline_header);
        btnFollowToggle = (Button) findViewById(R.id.btn_follow_toggle);
        txtUsername = (TextView) findViewById(R.id.txt_username);
        txtBio = (TextView) findViewById(R.id.txt_profile_bio);
        txtFeedCount = (TextView) findViewById(R.id.txt_feed_count);
        txtFollowerCount = (TextView) findViewById(R.id.txt_follower_count);
        imgProfileImage = (ImageView) findViewById(R.id.img_profile_image);

        this.accUserProfile = bundle.getObject("user");
        this.appUserId = new UserAccountBase(getActivity()).getUserId();

        this.__updateProfileInfoView(accUserProfile);

        new GetProfileHelper(accUserProfile.userId, appUserId, this)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void __updateProfileInfoView(final UserProfileEntry profile) {
        if (profile == null) return;

        PatchBundle b = new PatchBundle().put("title", profile.username);
        dispatchEventToFrame("update_page_title", b);

        if (profile.bio != null) txtBio.setText(profile.bio);
        if (profile.username != null) txtUsername.setText(profile.username);

        if (profile.feedPostCount != -1) {
            txtFeedCount.setText(String.format("%s", profile.feedPostCount));
        }

        if (profile.followerCount != -1) {
            txtFollowerCount.setText(String.format("%s", profile.followerCount));
        }

        if (!profile.userId.equals(appUserId)) {
            UserFollowHelper.friendButton(btnFollowToggle, profile.isFollowing);
        } else btnFollowToggle.setVisibility(View.GONE);

        int iw = getContext().getResources().getDisplayMetrics().widthPixels / 4;
        Picasso.with(getContext()).load(profile.imgUrl).noPlaceholder()
                .resize(iw, iw).centerInside().into(imgProfileImage);

        btnFollowToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doFollowToggle(profile.userId);
            }
        });
    }

    private void __updateProfileFollowState(UserProfileEntry profile, boolean f) {
        profile.followerCount += (profile.isFollowing = f) ? 1 : -1;
        UserFollowHelper.friendButton(btnFollowToggle, f);
        txtFollowerCount.setText(String.format("%s", profile.followerCount));
    }

    private void doFollowToggle(String userId) {
        if (!NettySocialActHelper.checkLaunchLogged(getActivity())) return;

        boolean isFollow = btnFollowToggle.getTag() == null ? false :
                (Boolean) btnFollowToggle.getTag();
        if (isFollow) {
            new UserUnfollowHelper(getActivity(), userId, this).execute();
        } else {
            new UserFollowHelper(getActivity(), userId, this).execute();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 919 && resultCode == Activity.RESULT_OK) {
            btnFollowToggle.performClick();
        }
    }

    private void dispatchMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPreAct(AsyncAPIRequest sf, int act) {
        if (act == FOLLOW || act == UNFOLLOW) {
            btnFollowToggle.startProgressing(R.layout.view_btnprg_follow_toggle);
        }
    }

    @Override
    public void onPostAct(AsyncAPIRequest sf, int act) {
        if (getView() == null || getActivity() == null) return;

        if (act == FOLLOW || act == UNFOLLOW) {
            btnFollowToggle.finishProgressing();
            if (sf.isSuccessful()) {
                __updateProfileFollowState(accUserProfile, act == FOLLOW);
            } else dispatchMessage(sf.getMessage());
        } else if (act == GET_PROFILE && sf.isSuccessful()) {
            UserProfileEntry user = ((GetProfileHelper) sf).getResult();
            __updateProfileInfoView(this.accUserProfile = user);
        }
    }//END
}

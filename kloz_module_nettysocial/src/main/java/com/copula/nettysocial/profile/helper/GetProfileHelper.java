package com.copula.nettysocial.profile.helper;

import android.os.AsyncTask;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;

/**
 * Created by heeleaz on 7/16/17.
 */
public class GetProfileHelper extends AsyncTask<Void, Void, RestResponse<UserProfileEntry>> implements AsyncAPIRequest {
    private String peeperId, accUserId;
    private boolean isSuccessful;
    private String message;

    private UserActCallback callback;
    private UserProfileEntry result;


    public GetProfileHelper(String accUserId, String peeperId, UserActCallback c) {
        this.accUserId = accUserId;
        this.peeperId = peeperId;
        this.callback = c;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null)
            callback.onPreAct(this, UserActCallback.GET_PROFILE);
    }

    @Override
    protected RestResponse<UserProfileEntry> doInBackground(Void... voids) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        return api.getProfileAPI().getProfile(accUserId, peeperId);
    }

    @Override
    protected void onPostExecute(RestResponse<UserProfileEntry> response) {
        if (response != null && response.statusCode == 1) {
            isSuccessful = true;
            result = response.result;
        } else if (response != null) message = response.statusMessage;

        if (callback != null)
            callback.onPostAct(this, UserActCallback.GET_PROFILE);
    }

    public UserProfileEntry getResult() {
        return result;
    }

    @Override
    public boolean isSuccessful() {
        return isSuccessful;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Object getTag(String key) {
        return null;
    }

    @Override
    public Object setTag(String key, Object value) {
        return null;
    }
}

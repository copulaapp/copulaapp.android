package com.copula.nettysocial.feed.helper;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.nettysocial.nettyservice.feeds.DaoExploreFeeds;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.R;

/**
 * Created by heeleaz on 6/14/17.
 */
public class FeedDeleteHelper extends AsyncTask<Void, Void, RestResponse> implements AbsHelper {
    private MediaFeedEntry feedModel;
    private String appUserId;

    private Context context;
    private ActCallback callback;

    private boolean isSuccessful;

    public FeedDeleteHelper(Context ctx, MediaFeedEntry media, ActCallback callback) {
        this.context = ctx;
        this.appUserId = new UserAccountBase(ctx).getUserId();
        this.feedModel = media;

        this.callback = callback;
    }

    private void dispatchMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (callback != null)
            callback.onPreAct(this, ActCallback.ACT_DELETE, feedModel);
    }

    @Override
    protected RestResponse doInBackground(Void... params) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        return api.getFeedAPI().removeFeed(feedModel.feedId, appUserId);
    }

    @Override
    protected void onPostExecute(RestResponse response) {
        if (response == null) {
            dispatchMessage(context.getString(R.string.msg_delete_failed));
        } else if (response.statusCode != 1) {
            dispatchMessage(response.statusMessage);
        } else {
            isSuccessful = true;
            new DaoExploreFeeds(context).deleteFeed(feedModel);
            dispatchMessage(context.getString(R.string.msg_feed_deleted));
        }

        if (callback != null)
            callback.onPostAct(this, ActCallback.ACT_DELETE, feedModel);
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }
}

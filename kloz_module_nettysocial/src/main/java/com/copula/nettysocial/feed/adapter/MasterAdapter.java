package com.copula.nettysocial.feed.adapter;

import android.view.LayoutInflater;
import android.view.View;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;

/**
 * Created by heeleaz on 6/9/17.
 */
public abstract class MasterAdapter {
    /**
     * Created by heeleaz on 6/9/17.
     */
    public abstract View getChildView(LayoutInflater inflater);

    public abstract void initChildViewHolder();

    public abstract View getView();

    public abstract void setData(MediaFeedEntry m, String appUserId, int p);

    public static class MasterViewHolder {
    }
}

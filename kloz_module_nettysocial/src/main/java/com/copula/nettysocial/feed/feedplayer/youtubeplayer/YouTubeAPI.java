package com.copula.nettysocial.feed.feedplayer.youtubeplayer;

/**
 * Created by heeleaz on 6/11/17.
 */
public class YouTubeAPI {
    public static final String DEVELOPER_KEY = "AIzaSyAfYz-mrhfDKWoSl05Isr91Gm-YYtfcaHU";

    public enum State {
        UNINITIALIZED,
        INITIALIZED,
        LOADING_THUMBNAILS,
        VIDEO_FLIPPED_OUT,
        VIDEO_LOADING,
        VIDEO_CUED,
        VIDEO_PLAYING,
        VIDEO_ENDED,
        VIDEO_BEING_FLIPPED_OUT,
    }
}

package com.copula.nettysocial.feed;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import com.copula.nettysocial.nettyservice.feeds.DaoExploreFeeds;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.abstimeline.MediaTimelineFragment;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.feed.helper.GetExploreHelper;
import com.copula.support.android.content.FragmentPagerStateListener;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;

import java.util.List;

/**
 * Created by eliasigbalajobi on 4/12/16.
 */
public class ExploreFeedsFragment extends MediaTimelineFragment implements IFragmentBackPress, View.OnClickListener,
        FragmentPagerStateListener {

    @Override
    protected void fetchFeedInApproach(int fetchApproach) {
        GetExploreHelper f = new GetExploreHelper(getActivity(), fetchApproach);
        f.setCallback(this);
        f.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected String getNextPageToken() {
        return new DaoExploreFeeds(getActivity()).getNextPageToken();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        DaoExploreFeeds dao = new DaoExploreFeeds(getActivity());
        List<MediaFeedEntry> entries = dao.getFeeds();
        if (entries != null) getAdapter().addHandlers(entries);

        getListView().setAdapter(getAdapter());
        onFeedFetchFailed(-100);
    }

    @Override
    public View initEmptyView(LayoutInflater inflater, ListView view) {
        View v = inflater.inflate(R.layout.view_empty_media_feed_list,
                null, false);
        view.addHeaderView(v, null, false);
        return v;
    }

    @Override
    public View initListProgressView(LayoutInflater inflater, ListView view) {
        View v = inflater.inflate(R.layout.view_app_export_prg_circle,
                null, false);
        view.addFooterView(v, null, false);
        return v;
    }

    @Override
    public void onPostAct(AbsHelper abs, int act, MediaFeedEntry feed) {
        if (act == ACT_GET) {
            GetExploreHelper h = (GetExploreHelper) abs;
            if (h.isSuccessful()) {
                onFeedFetched(h.getFetchApproach(), h.getResult());
            } else onFeedFetchFailed(h.getStatusCode());
        } else super.onPostAct(abs, act, feed);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200 && resultCode == Activity.RESULT_OK)
            fetchFeedInApproach(TOP_FETCH);
    }

    @Override
    protected void onFeedFetchFailed(int statusCode) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        super.onFeedFetchFailed(statusCode);

        TextView sm = ((TextView)
                getView().findViewById(R.id.txt_empty_list_submessage));
        TextView m = ((TextView) getView().findViewById(R.id.txt_empty_list_message));
        if (!DesignHelper.isDataEnabled(getActivity())) {
            sm.setVisibility(View.VISIBLE);
            m.setText(R.string.msg_empty_explore_feeds);
            sm.setText(R.string.msg_nonet_feed_timeline);
        } else {
            sm.setVisibility(View.GONE);
            m.setText(R.string.msg_empty_explore_feeds);
        }
    }

    @Override
    public void onVisibleToPager() {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public boolean allowBackPressed() {
        return true;
    }
}
package com.copula.nettysocial.feed.helper;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.feed.abstimeline.AbsTimelineFragment;

import java.util.List;

/**
 * Created by heeleaz on 6/14/17.
 */
public class GetFeedsHelper extends AsyncTask<Void, Void, RestResponse<List<MediaFeedEntry>>>
        implements AbsHelper {
    private int fetchApproach;
    private String appUserId, accUserId, mNextPageToken;

    private boolean isSuccessful;
    private ActCallback callback;

    private List<MediaFeedEntry> result;

    public GetFeedsHelper(Context context, String accUserId, int fetchApproach) {
        this.fetchApproach = fetchApproach;
        this.accUserId = accUserId;
        this.appUserId = new UserAccountBase(context).getUserId();
    }

    public void setCallback(ActCallback callback) {
        this.callback = callback;
    }

    public String getNextPageToken(int fetchApproach) {
        return fetchApproach == AbsTimelineFragment.BOTTOM_FETCH ? mNextPageToken : null;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public List<MediaFeedEntry> getResult() {
        return result;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null)
            callback.onPreAct(this, ActCallback.ACT_GET, null);
    }

    @Override
    protected RestResponse<List<MediaFeedEntry>> doInBackground(Void... params) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();

        String nextPageToken = getNextPageToken(fetchApproach);
        RestResponse<List<MediaFeedEntry>> response =
                api.getFeedAPI().getFeeds(accUserId, appUserId, nextPageToken);
        if (response == null) return response;

        if (response.result != null && response.statusCode == 1)
            mNextPageToken = response.nextPageToken;

        return response;
    }

    @Override
    protected void onPostExecute(RestResponse<List<MediaFeedEntry>> response) {
        if (response != null && response.result != null) {
            isSuccessful = response.statusCode == 1;
            result = response.result;
        }

        if (callback != null)
            callback.onPostAct(this, ActCallback.ACT_GET, null);
    }

    public int getFetchApproach() {
        return fetchApproach;
    }

    public String getNextPageToken() {
        return mNextPageToken;
    }
}

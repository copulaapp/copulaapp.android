package com.copula.nettysocial.feed.helper;

import android.content.Context;
import com.copula.nettysocial.nettyservice.feeds.DaoExploreFeeds;
import com.copula.functionality.util.MediaUtility;

/**
 * Created by heeleaz on 7/11/17.
 */
public class FeedCacheHelper {
    public static void clearImageRes(Context context) {
        //Glide.get(context).clearDiskCache();
        MediaUtility.tmpTimelineResourceDir().delete();
    }

    public static void clearExploreTimeline(Context context) {
        clearImageRes(context);
        DaoExploreFeeds dao = new DaoExploreFeeds(context);
        dao.truncateTable();
    }
}

package com.copula.nettysocial.feed.adapter.media;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.adapter.MasterAdapter;
import com.copula.nettysocial.feed.helper.FeedLikeHelper;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;
import com.copula.support.util.Formatter;

/**
 * Created by heeleaz on 6/9/17.
 */
public abstract class MediaAdapter extends MasterAdapter {
    public static final int POSTER_VIEW = R.id.view_feed_poster;
    public static final int LIKE_VIEW = R.id.btn_like;
    public static final int MEDIA_INFO_VIEW = R.id.view_media_info;
    public static final int FOLLOW_TOGGLE_VIEW = R.id.btn_follow_toggle;
    public static final int MEDIA_OPTION_VIEW = R.id.btn_media_option;

    private Context context;
    private View view;
    private MediaViewHolder viewHolder;

    private int readMoreTextColor = Color.parseColor("#616161");

    MediaAdapter(Context ctx, LayoutInflater inflater) {
        this.context = ctx;
        viewHolder = initViewHolder(view = initView(inflater));
    }

    public Context getContext() {
        return context;
    }

    private View initView(LayoutInflater inflater) {
        View v = inflater.inflate(R.layout.adp_feed_media_container, null, false);
        View childView = getChildView(inflater);

        ViewGroup container = (ViewGroup) v.findViewById(R.id.view_media_info);
        ViewGroup.LayoutParams p = new ViewGroup.LayoutParams(-1, -1);
        container.addView(childView, 0, p);

        return v;
    }

    private MediaViewHolder initViewHolder(View view) {
        MediaViewHolder holder = new MediaViewHolder();

        //header
        holder.posterDP = (ImageView) view.findViewById(R.id.img_profile_image);
        holder.brief = (TextView) view.findViewById(R.id.txt_brief);
        holder.username = (TextView) view.findViewById(R.id.txt_profile_name);
        holder.caption = (TextView) view.findViewById(R.id.txt_media_caption);
        holder.posterView = view.findViewById(MediaAdapter.POSTER_VIEW);
        holder.mediaInfo = view.findViewById(MediaAdapter.MEDIA_INFO_VIEW);
        holder.mediaInfo = view.findViewById(MediaAdapter.MEDIA_INFO_VIEW);

        //footer
        holder.like = (Button) view.findViewById(LIKE_VIEW);

        this.initChildViewHolder();
        return holder;
    }

    @Override
    public void setData(final MediaFeedEntry m, String appUserId, final int p) {
        viewHolder.username.setText(m.posterUsername);
        viewHolder.caption.setText(m.mediaCaption);

        FeedLikeHelper.setLikeStatusView(m, viewHolder.like);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) (view.getParent())).performItemClick(v, p, p);
            }
        };
        viewHolder.posterView.setOnClickListener(clickListener);
        viewHolder.like.setOnClickListener(clickListener);
        viewHolder.mediaInfo.setOnClickListener(clickListener);

        viewHolder.caption.truncateText(context.getString(
                R.string.act_see_more), readMoreTextColor, 2);
        viewHolder.caption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.caption.expandText();
            }
        });

        String time = Formatter.toElapsedDate(Formatter.dateToMilli(m.createdAt));
        String likeCount = FeedLikeHelper.composeLike(getContext(), m.likeCount);
        String foot = String.format("<html>%s %s &nbsp;&#8226;&nbsp; %s</html>",
                time, getContext().getString(R.string.txt_ago), likeCount);
        viewHolder.brief.setText(new SpannableString(Html.fromHtml(foot)),
                TextView.BufferType.SPANNABLE);

        Glide.with(getContext()).load(m.posterImageUrl).skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .override(100, 100)
                .fitCenter().into(viewHolder.posterDP);
    }

    @Override
    public View getView() {
        return view;
    }

    class MediaViewHolder extends MasterAdapter.MasterViewHolder {
        ImageView posterDP;
        Button like;
        TextView username, brief, caption;
        View posterView, mediaInfo;
    }
}

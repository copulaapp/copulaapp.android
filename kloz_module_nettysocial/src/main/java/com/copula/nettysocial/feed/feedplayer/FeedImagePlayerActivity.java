package com.copula.nettysocial.feed.feedplayer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.copula.nettysocial.R;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;

/**
 * Created by heeleaz on 3/6/17.
 */
public class FeedImagePlayerActivity extends AbsMediaPlayer implements RequestListener<String, GlideDrawable> {
    public static void launch(Context context, MediaFeedEntry media) {
        Intent intent = new Intent(context, FeedImagePlayerActivity.class);
        context.startActivity(intent.putExtra("media", media));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_feed_image_player);
        ImageView image = (ImageView) findViewById(R.id.img_media_image);

        Glide.with(this).load(getFeedModel().thumbnailUrl)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE).listener(this).into(image);
    }

    @Override
    public MediaFeedEntry getFeedModel() {
        return (MediaFeedEntry) getIntent().getSerializableExtra("media");
    }

    @Override
    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
        findViewById(R.id.view_progress).setVisibility(View.GONE);
        return false;
    }

    @Override
    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
        findViewById(R.id.view_progress).setVisibility(View.GONE);
        return false;
    }
}

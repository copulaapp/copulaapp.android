package com.copula.nettysocial.feed;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.abstimeline.MediaTimelineFragment;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.feed.helper.GetFeedsHelper;
import com.copula.nettysocial.profile.PatchHeader;
import com.copula.support.android.view.PatchBase;
import com.copula.support.android.view.PatchBundle;
import com.copula.support.android.view.PatchSession;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.ResultBundle;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by eliasigbalajobi on 4/12/16.
 */
public class UserFeedLineFragment extends MediaTimelineFragment implements View.OnClickListener, PatchSession
        .PatchSessionListener {
    private PatchHeader timelineHeader;
    private String nextPageToken;
    private TextView txtEmptyViewMsg;

    public static UserFeedLineFragment instantiate(UserProfileEntry user) {
        Bundle argument = new Bundle(1);
        argument.putSerializable("user", user);
        UserFeedLineFragment timelineFragment = new UserFeedLineFragment();
        timelineFragment.setArguments(argument);
        return timelineFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchFeedInApproach(TOP_FETCH);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        timelineHeader.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View initEmptyView(LayoutInflater inflater, ListView view) {
        UserProfileEntry accUser
                = (UserProfileEntry) getArguments().getSerializable("user");

        PatchSession patchSession = new PatchSession(getActivity());
        patchSession.setPatchSessionListener(this);
        timelineHeader = PatchHeader.instantiate(patchSession, accUser);
        view.addHeaderView(timelineHeader.getView(), null, false);

        View v = inflater.inflate(R.layout.view_empty_media_feed_list,
                null, false);
        txtEmptyViewMsg = (TextView) v.findViewById(R.id.txt_empty_list_message);
        view.addHeaderView(v, null, false);
        return v;
    }

    @Override
    public View initListProgressView(LayoutInflater inflater, ListView view) {
        View v = inflater.inflate(R.layout.view_app_export_prg_circle,
                null, false);
        view.addFooterView(v, null, false);
        return v;
    }

    @Override
    public void onPostAct(AbsHelper abs, int act, MediaFeedEntry feed) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        if (act == ACT_GET) {
            GetFeedsHelper h = (GetFeedsHelper) abs;
            if (h.isSuccessful()) {
                onFeedFetched(h.getFetchApproach(), h.getResult());
                this.nextPageToken = h.getNextPageToken();
            } else onFeedFetchFailed(0);
        } else super.onPostAct(abs, act, feed);
    }

    @Override
    protected void onFeedFetchFailed(int statusCode) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        super.onFeedFetchFailed(statusCode);
        TextView message = ((TextView) txtEmptyViewMsg
                .findViewById(R.id.txt_empty_list_message));
        if (!DesignHelper.isDataEnabled(getActivity())) {
            message.setText(R.string.msg_nonet_feed_timeline);
        } else message.setText(R.string.msg_empty_feed_line);
    }

    @Override
    protected void fetchFeedInApproach(int fetchApproach) {
        UserProfileEntry accUser
                = (UserProfileEntry) getArguments().getSerializable("user");
        GetFeedsHelper f = new GetFeedsHelper(
                getActivity(), accUser.userId, fetchApproach);
        f.setCallback(this);
        f.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected String getNextPageToken() {
        return nextPageToken;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.view_empty_list) fetchFeedInApproach(TOP_FETCH);
    }

    @Override
    public void onEventToFrame(PatchBase fromPatch, String event, PatchBundle pb) {
        ResultBundle binder = (ResultBundle) getActivity();

        Bundle bundle = new Bundle(1);
        bundle.putString("title", pb.getString("title"));
        if (binder != null) binder.message(event, bundle);
    }

    @Override
    public void onNewPatchInit(PatchBase patch) {
    }
}

package com.copula.nettysocial.feed.helper;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.feeds.DaoExploreFeeds;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.R;
import com.copula.support.android.view.widget.Button;

/**
 * Created by heeleaz on 6/14/17.
 */
public class FeedLikeHelper extends AsyncTask<Void, Void, RestResponse> implements AbsHelper {
    private MediaFeedEntry feedModel;
    private String peeperId;
    private Context context;

    private ActCallback callback;

    public FeedLikeHelper(Context ctx, MediaFeedEntry feedModel, ActCallback callback) {
        this.context = ctx;
        this.feedModel = feedModel;
        this.peeperId = new UserAccountBase(ctx).getUserId();

        this.callback = callback;
    }

    public static void setLikeStatusView(MediaFeedEntry model, Button button) {
        if (model.isLiked) {
            button.setBackgroundResource(R.drawable.ic_like_active_appcolor_96px);
        } else {
            button.setBackgroundResource(R.drawable.ic_like_inactive_grey_96px);
        }
    }

    public static String composeLike(Context ctx, int likeCount) {
        likeCount = likeCount < 0 ? 0 : likeCount;
        return (likeCount <= 1) ? likeCount + " " + ctx.getString(R.string.txt_like)
                : likeCount + " " + ctx.getString(R.string.txt_likes);
    }

    public static String composeLikeNaked(int likeCount) {
        likeCount = likeCount < 0 ? 0 : likeCount;
        return "" + likeCount;
    }

    public static void preActLikeAction(MediaFeedEntry model) {
        if ((model.isLiked = !model.isLiked)) model.likeCount += 1;
        else model.likeCount -= 1;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        preActLikeAction(feedModel);
        if (callback != null)
            callback.onPreAct(this, ActCallback.ACT_LIKE, feedModel);
    }

    @Override
    protected RestResponse doInBackground(Void... params) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        if (feedModel.isLiked) {
            return api.getFeedAPI().likeFeed(feedModel.feedId, peeperId);
        } else {
            return api.getFeedAPI().unlikeFeed(feedModel.feedId, peeperId);
        }
    }

    @Override
    protected void onPostExecute(RestResponse response) {
        boolean result = (response != null && response.statusCode == 1);
        postActLikeAction(feedModel, result);

        if (callback != null)
            callback.onPostAct(this, ActCallback.ACT_LIKE, feedModel);
    }

    public void postActLikeAction(MediaFeedEntry model, boolean doAction) {
        if (doAction) {
            new DaoExploreFeeds(context).updateLike(model);
        } else {//reverse like since it failed
            if ((model.isLiked = !model.isLiked)) model.likeCount += 1;
            else model.likeCount -= 1;
        }
    }

    @Override
    public boolean isSuccessful() {
        return false;
    }
}

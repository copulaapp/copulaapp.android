package com.copula.nettysocial.feed.feedplayer;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.media.MediaPlayerInterceptor;
import com.copula.functionality.media.VideoController;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.nettysocial.R;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 3/6/17.
 */
public class FeedVideoPlayerActivity extends AbsMediaPlayer implements MediaPlayer.OnErrorListener,
        MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener,
        SeekBar.OnSeekBarChangeListener, SurfaceHolder.Callback, MediaPlayerInterceptor.InterceptorListener {

    private VideoController videoController;
    private Button btnPlayPause;
    private TextView txtMediaDuration, txtPlayedDuration;
    private SeekBar seekBar;
    private Handler mHandler;

    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private MediaPlayerInterceptor mMediaPlayerInterceptor;
    private boolean playCompleted;

    public static void launch(Context context, MediaFeedEntry media) {
        Intent intent = new Intent(context, FeedVideoPlayerActivity.class);
        context.startActivity(intent.putExtra("media", media));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_feed_video_player);

        AudioController.getInstance(this).pause();
        (videoController = new VideoController()).setStreamType(VideoController.STREAM);
        videoController.setScreenOnWhilePlaying(true);
        videoController.setOnBufferingUpdateListener(this);
        videoController.setOnPreparedListener(this);
        videoController.setOnErrorListener(this);
        videoController.setOnCompletionListener(this);

        txtMediaDuration = (TextView) findViewById(R.id.txt_media_duration);
        txtPlayedDuration = (TextView) findViewById(R.id.txt_played_duration);
        findViewById(R.id.view_surface_holder).setOnClickListener(this);
        (seekBar = (SeekBar) findViewById(R.id.seekbar)).setOnSeekBarChangeListener(this);
        (btnPlayPause = (Button) findViewById(R.id.btn_play_pause)).setOnClickListener(this);
        (surfaceView = (SurfaceView) findViewById(R.id.surface)).setOnClickListener(this);
        (surfaceHolder = surfaceView.getHolder()).addCallback(this);

        this.mHandler = new Handler(getMainLooper());
        this.mMediaPlayerInterceptor = new MediaPlayerInterceptor(this);

        MediaFeedEntry media = (MediaFeedEntry) getIntent().getSerializableExtra("media");
        videoController.setPlayList(new VideoModel(media));
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        videoController.setDisplay(holder);
        videoController.prepareAsync(videoController.getCurrentTrack());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayerInterceptor.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (videoController != null) videoController.pause();
        mMediaPlayerInterceptor.unregister();

        btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_naked_white_128px);
    }

    /**
     * Method which updates the SeekBar primary progress by current video playing position
     */
    private synchronized void primarySeekBarProgressUpdater(int waitMilli) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (videoController == null || !videoController.isPlaying()) return;

                int position = videoController.getCurrentPosition();
                long duration = videoController.getDuration();
                seekBar.setProgress((int) ((position / (duration * 1F) * 100)));
                txtPlayedDuration.setText(MediaUtility.formatDuration(position));
                primarySeekBarProgressUpdater(1000);
            }
        }, waitMilli);
    }

    @Override
    public MediaFeedEntry getFeedModel() {
        return (MediaFeedEntry) getIntent().getSerializableExtra("media");
    }

    private void setPortraitVideoAspectRatio(int aspectWidth, int aspectHeight) {
        if ((aspectHeight & aspectWidth) == 0) return;

        float aspectRatio = (aspectHeight / (aspectWidth * 1F));

        int scaledWidth = getResources().getDisplayMetrics().widthPixels;
        int scaledHeight = (int) (aspectRatio * scaledWidth);

        surfaceHolder.setFixedSize(scaledWidth, scaledHeight);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        playCompleted = false;
        VideoModel m = videoController.getCurrentTrack();
        setPortraitVideoAspectRatio(m.width, m.height);

        findViewById(R.id.prg_media_prepare_buffer).setVisibility(View.GONE);
        txtMediaDuration.setText(MediaUtility.formatDuration(videoController.getDuration()));
        primarySeekBarProgressUpdater(0);

        btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_naked_white_128px);
        videoController.setLooping(true);
        videoController.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        playCompleted = true;
        btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_naked_white_128px);
        txtPlayedDuration.setText(txtMediaDuration.getText());
        seekBar.setProgress(100);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        videoController.stop();
        videoController.reset();

        dispatchMessage(getString(R.string.msg_media_play_error_unknown));
        findViewById(R.id.prg_media_prepare_buffer).setVisibility(View.GONE);
        return false;
    }

    private void dispatchMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int seekToMill = (int) ((progress / 100f) * videoController.getDuration());
            videoController.seekTo(seekToMill);
        }
    }

    private void doMediaPlayPause() {
        if (videoController.isPlaying()) {
            videoController.pause();
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_naked_white_128px);
        } else if (playCompleted) {
            //restart by seeking to beginning and then start player
            //videoController.seekTo(0);
            //onPrepared(videoController);//just an Hack to act playing

            videoController.prepareAsync(videoController.getCurrentTrack());
        } else if (videoController.getCurrentPosition() > 0) {//atleast as once started
            videoController.start();
            primarySeekBarProgressUpdater(0);
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_naked_white_128px);
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v == btnPlayPause) doMediaPlayPause();
    }

    @Override
    public void onHoldIntercept(int actor) {
        videoController.pause();
    }

    @Override
    public void onFreeIntercept(int actor) {
    }


    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        seekBar.setSecondaryProgress(percent);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (videoController != null) videoController.stop();
    }
}

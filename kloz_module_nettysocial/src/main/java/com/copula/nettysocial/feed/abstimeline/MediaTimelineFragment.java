package com.copula.nettysocial.feed.abstimeline;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.CopulaProgressDialog;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.Util;
import com.copula.nettysocial.feed.adapter.MediaTimelineAdapter;
import com.copula.nettysocial.feed.adapter.media.MediaAdapter;
import com.copula.nettysocial.feed.feedplayer.FeedImagePlayerActivity;
import com.copula.nettysocial.feed.feedplayer.FeedVideoPlayerActivity;
import com.copula.nettysocial.feed.feedplayer.youtubeplayer.YouTubePlayerActivity;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.feed.helper.FeedDeleteHelper;
import com.copula.nettysocial.feed.helper.FeedLikeHelper;
import com.copula.nettysocial.profile.UserSocialProfileActivity;
import com.copula.nettysocial.profile.helper.AsyncAPIRequest;
import com.copula.nettysocial.profile.helper.UserFollowHelper;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ListView;

import java.util.List;

/**
 * Created by heeleaz on 6/14/17.
 */
public abstract class MediaTimelineFragment extends AbsTimelineFragment implements AbsHelper.ActCallback,
        View.OnClickListener {
    private ListView listView;
    private SwipeRefreshLayout swipeLayout;
    private View vEmptyList, vProgress;
    private MediaTimelineAdapter adapter;
    private CopulaProgressDialog deleteProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_media_timeline, container, false);
        listView = (ListView) v.findViewById(R.id.lst);
        swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);

        vProgress = initListProgressView(inflater, listView);
        (vEmptyList = initEmptyView(inflater, listView)).setOnClickListener(this);

        //hide both progress view and empty view list for now.
        hideListViewChild(vEmptyList);
        hideListViewChild(vProgress);

        adapter = new MediaTimelineAdapter(getActivity(), getAppUserId());
        listView.setAdapter(adapter);

        swipeLayout.setOnRefreshListener(this);

        return v;
    }

    public abstract View initEmptyView(LayoutInflater inflater, ListView view);

    public abstract View initListProgressView(LayoutInflater inflater, ListView view);

    @Override
    public void onResume() {
        super.onResume();
        if (adapter.getCount() == 0) onEmptyTimeline();

        fetchFeedInApproach(TOP_FETCH);
    }

    @Override
    public void onPreAct(AbsHelper abs, int act, MediaFeedEntry feedModel) {
        if (act == ACT_GET) {
            if (adapter.getCount() == 0) hideListViewChild(vEmptyList);
            showAndResizeListViewChild(vProgress, -1, -2);
        } else if (act == ACT_DELETE) {
            deleteProgressDialog = new CopulaProgressDialog(getActivity());
            deleteProgressDialog.setMessage(R.string.prg_delete);
            deleteProgressDialog.setCancelable(false);
            deleteProgressDialog.show();
        } else if (act == ACT_LIKE) getAdapter().notifyDataSetChanged();
    }

    protected void onFeedFetched(int fetchApproach, List<MediaFeedEntry> feeds) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        swipeLayout.setRefreshing(false);//hide SwipeView progress bar
        hideListViewChild(vProgress);
        hideListViewChild(vEmptyList);

        if (fetchApproach == TOP_FETCH) adapter.clear();
        getAdapter().addOrReplaceHandlers(feeds);
        getAdapter().notifyDataSetChanged();
    }

    protected void onFeedFetchFailed(int statusCode) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        swipeLayout.setRefreshing(false);
        hideListViewChild(vProgress);
        if (adapter.getCount() == 0) onEmptyTimeline();
    }

    @Override
    public void onPostAct(AbsHelper abs, int act, MediaFeedEntry feed) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        if (act == ACT_DELETE && abs.isSuccessful()) {
            onFeedDeleted(feed);
            if (deleteProgressDialog != null) deleteProgressDialog.dismiss();

        } else if (act == ACT_LIKE) getAdapter().notifyDataSetChanged();
    }

    private void onFeedDeleted(MediaFeedEntry feedModel) {
        if (!DesignHelper.isFragmentAlive(this)) return;

        adapter.removeHandler(feedModel);
        if (adapter.getCount() > 0) adapter.notifyDataSetChanged();
        else onEmptyTimeline();
    }

    private void onEmptyTimeline() {
        if (!DesignHelper.isFragmentAlive(this)) return;

        swipeLayout.setRefreshing(false);
        hideListViewChild(vProgress);
        showAndResizeListViewChild(vEmptyList, -1, -1);
    }

    private void showFeedDeleteDialog(final MediaFeedEntry media) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.msg_confirm_action);
        DialogInterface.OnClickListener click = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    dialog.dismiss();
                } else if (which == DialogInterface.BUTTON_POSITIVE) {
                    new FeedDeleteHelper(getActivity(),
                            media, MediaTimelineFragment.this).execute();
                }
            }//end onClick
        };
        builder.setNegativeButton(R.string.txt_no, click);
        builder.setPositiveButton(R.string.act_delete, click);
        builder.show();
    }

    @Override
    protected abstract void fetchFeedInApproach(int fetchApproach);

    protected MediaTimelineAdapter getAdapter() {
        return adapter;
    }

    private void hideListViewChild(View view) {
        view.setVisibility(View.GONE);
        view.setLayoutParams(new AbsListView.LayoutParams(1, 1));
    }

    private void showAndResizeListViewChild(View view, int w, int h) {
        view.setVisibility(View.VISIBLE);
        view.setLayoutParams(new AbsListView.LayoutParams(w, h));
    }

    @Override
    protected ListView getListView() {
        return listView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        super.onItemClick(parent, view, position, id);

        final MediaFeedEntry media = getAdapter().getItem(position);
        if (view.getId() == MediaAdapter.POSTER_VIEW) {
            UserSocialProfileActivity.launch(getActivity(), media.getProfileEntry());
        } else if (view.getId() == MediaAdapter.MEDIA_OPTION_VIEW) {
            FeedOptionPopWindow popWindow = new FeedOptionPopWindow(getActivity());
            popWindow.setMedia(media);
            popWindow.show(view, new FeedOptionPopWindow.MediaOptionListener() {
                @Override
                public void onAction(int action, MediaFeedEntry m) {
                    if (action == 0) showFeedDeleteDialog(m);
                }
            });
        } else if (view.getId() == MediaAdapter.LIKE_VIEW) {
            if (!NettySocialActHelper.checkLaunchLogged(getActivity())) return;
            new FeedLikeHelper(getActivity(), media, this)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } else if (view.getId() == MediaAdapter.MEDIA_INFO_VIEW) {
            if (media.mediaType == MediaModel.IMAGE) {
                FeedImagePlayerActivity.launch(getActivity(), media);
            } else resolveVideoPlayerActivity(media);
        } else if (view.getId() == MediaAdapter.FOLLOW_TOGGLE_VIEW) {
            if (!NettySocialActHelper.checkLaunchLogged(getActivity())) return;

            new UserFollowHelper(getActivity(), media.posterId,
                    new FollowAsyncCallback((Button) view)).execute();
        }
    }//END

    private void resolveVideoPlayerActivity(MediaFeedEntry media) {
        Util.StreamSource source = Util.getStreamSource(media.streamUrl);
        if (source == Util.StreamSource.YOUTUBE) {
            YouTubePlayerActivity.launch(getActivity(), media);
        } else {
            FeedVideoPlayerActivity.launch(getActivity(), media);
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.view_empty_list) fetchFeedInApproach(TOP_FETCH);
    }

    private class FollowAsyncCallback implements AsyncAPIRequest.UserActCallback {
        private Button button;

        FollowAsyncCallback(Button button) {
            this.button = button;
        }

        @Override
        public void onPreAct(AsyncAPIRequest abs, int act) {
            button.startProgressing(R.layout.view_btnprg_follow_toggle);
        }

        @Override
        public void onPostAct(AsyncAPIRequest abs, int act) {
            if (abs.isSuccessful()) {
                UserFollowHelper.friendButton(button, true);
            } else dispatchMessage(abs.getMessage());

            button.finishProgressing();
        }
    }//END
}

package com.copula.nettysocial.feed.adapter.media;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.copula.nettysocial.R;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.nettysocial.nettyservice.entry.MediaMetaModel;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 6/9/17.
 */
public class ImageAdapter extends MediaAdapter {
    private ImageViewHolder holder;

    public ImageAdapter(Context ctx, LayoutInflater inflater) {
        super(ctx, inflater);
    }

    public static void resolveImageViewDimension(final ImageView imv, final int w, final int h) {
        ViewGroup.LayoutParams p = imv.getLayoutParams();
        int dw = imv.getContext().getResources().getDisplayMetrics().widthPixels;
        p.height = (h * dw) / w;
    }

    @Override
    public View getChildView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.adp_feed_media_inc_image, null, false);
    }

    @Override
    public void initChildViewHolder() {
        View view = getView();
        holder = new ImageViewHolder();
        holder.caption = (TextView) view.findViewById(R.id.txt_media_caption);
        holder.thumbnail = (ImageView) view.findViewById(R.id.img_media_thumbnail);
    }

    @Override
    public void setData(MediaFeedEntry m, String appUserId, int p) {
        super.setData(m, appUserId, p);

        if (m.mediaCaption != null && !m.mediaCaption.equals("")) {
            holder.caption.setVisibility(View.VISIBLE);
            holder.caption.setText(m.mediaCaption);
        } else holder.caption.setVisibility(View.GONE);


        MediaMetaModel meta = MediaMetaModel.toModel(m.mediaMeta);
        resolveImageViewDimension(holder.thumbnail, meta.width, meta.height);

        Glide.with(getContext()).load(m.thumbnailUrl)
                .placeholder(R.color.placeholderImageColor)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.thumbnail);
    }

    public class ImageViewHolder extends MediaViewHolder {
        TextView caption;
        ImageView thumbnail;
    }
}
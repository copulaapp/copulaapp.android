package com.copula.nettysocial.feed.helper;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.feed.abstimeline.AbsTimelineFragment;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.nettysocial.nettyservice.feeds.DaoExploreFeeds;

import java.util.List;

/**
 * Created by heeleaz on 6/14/17.
 */
public class GetExploreHelper extends AsyncTask<Void, Void, RestResponse<List<MediaFeedEntry>>> implements AbsHelper {
    private Context context;
    private int fetchApproach;
    private DaoExploreFeeds daoSocialFeeds;
    private String appUserId;

    private boolean isSuccessful;
    private ActCallback callback;

    private int statusCode = -100;

    private List<MediaFeedEntry> result;

    public GetExploreHelper(Context context, int fetchApproach) {
        this.context = context;
        this.fetchApproach = fetchApproach;
        this.daoSocialFeeds = new DaoExploreFeeds(context);
        this.appUserId = new UserAccountBase(context).getUserId();
    }

    public void setCallback(ActCallback callback) {
        this.callback = callback;
    }

    public String getNextPageToken(int fetchApproach) {
        return fetchApproach == AbsTimelineFragment.BOTTOM_FETCH
                ? daoSocialFeeds.getNextPageToken() : null;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public List<MediaFeedEntry> getResult() {
        return result;
    }

    @Override
    protected void onPreExecute() {
        if (callback != null)
            callback.onPreAct(this, ActCallback.ACT_GET, null);
    }

    @Override
    protected RestResponse<List<MediaFeedEntry>> doInBackground(Void... params) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();

        String nextPageToken = getNextPageToken(fetchApproach);
        RestResponse<List<MediaFeedEntry>> response =
                api.getFeedAPI().getExploreFeeds(appUserId, nextPageToken);
        if (response == null) return response;

        if (response.result != null && response.statusCode == 1) {
            if (fetchApproach == AbsTimelineFragment.TOP_FETCH) {
                FeedCacheHelper.clearExploreTimeline(context);
            }
            daoSocialFeeds.putFeeds(response.result);
            daoSocialFeeds.saveNextPageToken(response.nextPageToken);
        }

        statusCode = response.statusCode;

        return response;
    }

    @Override
    protected void onPostExecute(RestResponse<List<MediaFeedEntry>> response) {
        if (response != null && response.result != null) {
            isSuccessful = response.statusCode == 1;
            result = response.result;
        }

        if (callback != null)
            callback.onPostAct(this, ActCallback.ACT_GET, null);
    }

    public int getStatusCode() {
        return statusCode;
    }

    public int getFetchApproach() {
        return fetchApproach;
    }
}

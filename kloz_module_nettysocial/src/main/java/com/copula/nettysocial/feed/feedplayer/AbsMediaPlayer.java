package com.copula.nettysocial.feed.feedplayer;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;
import com.copula.functionality.downloader.MediaDownloadClient;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.feed.helper.FeedLikeHelper;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;

import java.util.List;

/**
 * Created by heeleaz on 6/16/17.
 */
public abstract class AbsMediaPlayer extends Activity implements View.OnClickListener, AbsHelper.ActCallback {
    private TextView txtLikeCount;
    private Button btnLike;

    private MediaFeedEntry feedModel;

    protected abstract MediaFeedEntry getFeedModel();

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        findViewById(R.id.btn_bar_back).setOnClickListener(this);
        (findViewById(R.id.btn_download)).setOnClickListener(this);
        txtLikeCount = (TextView) findViewById(R.id.txt_like_count);
        (btnLike = (Button) findViewById(R.id.btn_like)).setOnClickListener(this);

        FeedLikeHelper.setLikeStatusView(feedModel, btnLike);
        txtLikeCount.setText(FeedLikeHelper.composeLikeNaked(feedModel.likeCount));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        DesignHelper.activityFullscreen(this);

        this.feedModel = getFeedModel();
    }

    private void doMediaDownload(MediaFeedEntry media) {
        try {
            MediaDownloadClient dc = MediaDownloadClient.getInstance();
            List<String> parts = Uri.parse(media.dataUrl).getPathSegments();
            String fileName = "Copula-VID-" + parts.get(parts.size() - 1);
            String imgPath = MediaUtility.
                    getDownloadMediaDir(media.mediaType, fileName).getAbsolutePath();
            dc.queue(media.dataUrl, fileName, imgPath, 1);
            Toast.makeText(this, R.string.msg_download_start, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.msg_download_failed, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPreAct(AbsHelper abs, int act, MediaFeedEntry media) {
        FeedLikeHelper.setLikeStatusView(media, btnLike);
        txtLikeCount.setText(FeedLikeHelper.composeLikeNaked(feedModel.likeCount));
    }

    @Override
    public void onPostAct(AbsHelper abs, int act, MediaFeedEntry media) {
        FeedLikeHelper.setLikeStatusView(media, btnLike);
        txtLikeCount.setText(FeedLikeHelper.composeLikeNaked(feedModel.likeCount));
    }

    @Override
    public void onClick(View v) {
        if (v == btnLike) {
            if (NettySocialActHelper.checkLaunchLogged(this)) {
                new FeedLikeHelper(this, feedModel, this)
                        .execute();
            }
        } else if (v.getId() == R.id.btn_download) {
            doMediaDownload(feedModel);
        } else if (v.getId() == R.id.btn_bar_back) finish();
    }
}

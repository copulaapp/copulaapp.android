package com.copula.nettysocial.feed.helper;

import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;

/**
 * Created by heeleaz on 6/15/17.
 */
public interface AbsHelper {
    boolean isSuccessful();

    interface ActCallback {
        int ACT_DELETE = 0, ACT_LIKE = 1, ACT_GET = 2;

        void onPreAct(AbsHelper sf, int act, MediaFeedEntry media);

        void onPostAct(AbsHelper sf, int act, MediaFeedEntry media);
    }
}

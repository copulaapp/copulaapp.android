package com.copula.nettysocial.feed.feedplayer.youtubeplayer;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import com.copula.functionality.media.AudioController;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.helper.AbsHelper;
import com.copula.nettysocial.feed.helper.FeedLikeHelper;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by heeleaz on 3/6/17.
 */
@Deprecated
public class YouTubePlayerActivity extends YouTubeFailureRecoveryActivity implements YouTubePlayer
        .OnInitializedListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener, AbsHelper.ActCallback {
    private Button btnLike, btnPlayPause, btnDownload;
    private TextView txtLikeCount, txtMediaDuration, txtPlayedDuration;
    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer youTubePlayer;
    private SeekBar seekBar;
    private Handler mHandler;
    private MediaFeedEntry feedModel;

    private MyPlaybackEventListener playbackEventListener;
    private MyPlayerStateChangeListener playerStateChangeListener;

    private AudioController audioController;
    private boolean isAudioPlaying;

    public static void launch(Context context, MediaFeedEntry media) {
        Intent intent = new Intent(context, YouTubePlayerActivity.class);
        context.startActivity(intent.putExtra("media", media));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        DesignHelper.activityFullscreen(this);
        setContentView(R.layout.act_youtube_video_player);

        this.mHandler = new Handler(getMainLooper());
        this.feedModel = (MediaFeedEntry) getIntent().getSerializableExtra("media");
        this.audioController = AudioController.getInstance(this);

        findViewById(R.id.parent).setOnClickListener(this);
        (btnLike = (Button) findViewById(R.id.btn_like)).setOnClickListener(this);
        txtLikeCount = (TextView) findViewById(R.id.txt_like_count);
        (btnDownload = (Button) findViewById(R.id.btn_download)).setOnClickListener(this);
        findViewById(R.id.btn_bar_back).setOnClickListener(this);

        (youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player))
                .setOnClickListener(this);
        txtPlayedDuration = (TextView) findViewById(R.id.txt_played_duration);
        txtMediaDuration = (TextView) findViewById(R.id.txt_media_duration);
        (btnPlayPause = (Button) findViewById(R.id.btn_play_pause)).setOnClickListener(this);
        (seekBar = (SeekBar) findViewById(R.id.seekbar)).setOnSeekBarChangeListener(this);

        ViewGroup.LayoutParams lp = youTubePlayerView.getLayoutParams();
        int dw = getResources().getDisplayMetrics().widthPixels;
        lp.height = ((9 * dw) / 16);

        playerStateChangeListener = new MyPlayerStateChangeListener();
        playbackEventListener = new MyPlaybackEventListener();

        txtLikeCount.setText(FeedLikeHelper.composeLikeNaked(feedModel.likeCount));
        FeedLikeHelper.setLikeStatusView(feedModel, btnLike);

        this.youTubePlayerView.initialize(YouTubeAPI.DEVELOPER_KEY, this);

        if (isAudioPlaying = audioController.isPlaying()) audioController.pause();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player
            , boolean wasRestored) {
        youTubePlayer = player;
        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);

        player.setPlaybackEventListener(playbackEventListener);
        player.setPlayerStateChangeListener(playerStateChangeListener);

        if (!wasRestored) {
            txtPlayedDuration.setText(MediaUtility.formatDuration(0));
            txtMediaDuration.setText(MediaUtility.formatDuration(0));
            player.cueVideo(feedModel.thumbnailUrl);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider p, YouTubeInitializationResult ir) {
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubePlayerView;
    }

    @Override
    public void onPreAct(AbsHelper abs, int act, MediaFeedEntry feedModel) {
        FeedLikeHelper.setLikeStatusView(feedModel, btnLike);
        txtLikeCount.setText(FeedLikeHelper.composeLikeNaked(feedModel.likeCount));
    }

    @Override
    public void onPostAct(AbsHelper abs, int act, MediaFeedEntry feed) {
        FeedLikeHelper.setLikeStatusView(feed, btnLike);
        txtLikeCount.setText(FeedLikeHelper.composeLikeNaked(feed.likeCount));
    }

    @Override
    public void onClick(View v) {
        if (v == btnPlayPause && youTubePlayer != null) {
            if (!youTubePlayer.isPlaying()) youTubePlayer.play();
            else youTubePlayer.pause();
        } else if (v == btnLike) {
            if (!NettySocialActHelper.checkLaunchLogged(this)) return;
            new FeedLikeHelper(this, feedModel, this)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if (v.getId() == R.id.btn_bar_back) {
            finish();
        } else if (v == btnDownload) {
        } else toggleShowHeaderAndFooter();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int s = (int) ((progress / 100f) * youTubePlayer.getCurrentTimeMillis());
            youTubePlayer.seekToMillis(s);
        }
    }

    /**
     * Method which updates the SeekBar primary progress by current video playing position
     */
    private synchronized void primarySeekBarProgressUpdater(int waitMilli) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (youTubePlayer == null || !youTubePlayer.isPlaying()) return;

                int position = youTubePlayer.getCurrentTimeMillis();
                long duration = youTubePlayer.getDurationMillis();
                seekBar.setProgress((int) ((position / (duration * 1F) * 100)));
                txtPlayedDuration.setText(MediaUtility.formatDuration(position));
                primarySeekBarProgressUpdater(1000);
            }
        }, waitMilli);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    protected void onStop() {
        super.onStop();
        youTubePlayer = null;

        if (isAudioPlaying) audioController.start();
    }

    private void showHeaderAndFooter(boolean show) {
        int v = show ? View.VISIBLE : View.INVISIBLE;
        findViewById(R.id.view_header).setVisibility(v);
        findViewById(R.id.view_footer).setVisibility(v);
    }

    private void toggleShowHeaderAndFooter() {
        int v = findViewById(R.id.view_footer).getVisibility();
        showHeaderAndFooter(!(v == View.VISIBLE));
    }

    /**
     * Youtube Player Listener Classes
     */
    private class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {
        @Override
        public void onPlaying() {
            primarySeekBarProgressUpdater(0);
            showHeaderAndFooter(false);
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_pause_naked_white_128px);
        }

        @Override
        public void onPaused() {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_naked_white_128px);
        }

        @Override
        public void onStopped() {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_naked_white_128px);
            showHeaderAndFooter(true);
        }

        @Override
        public void onSeekTo(int i) {
            int progress = (int) ((i / youTubePlayer.getDurationMillis() * 1f) / 100f);
            seekBar.setProgress(progress);
        }

        @Override
        public void onBuffering(boolean b) {

        }
    }//END

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {
        @Override
        public void onLoading() {
        }

        @Override
        public void onLoaded(String videoId) {
            if (youTubePlayer == null) return;

            int vl = youTubePlayer.getDurationMillis();
            txtMediaDuration.setText(MediaUtility.formatDuration(vl));
            youTubePlayer.play();
        }

        @Override
        public void onVideoEnded() {
            btnPlayPause.setBackgroundResource(R.drawable.ic_media_play_naked_white_128px);
            txtPlayedDuration.setText(txtMediaDuration.getText());
            seekBar.setProgress(100);
        }

        @Override
        public void onVideoStarted() {
        }

        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
        }
    }//END
}

package com.copula.nettysocial.feed;

import android.net.Uri;

/**
 * Created by heeleaz on 7/20/17.
 */
public class Util {
    private static final String YOUTUBE_SOURCED = "www.youtube.com";

    public static StreamSource getStreamSource(String streamUrl) {
        Uri uri = Uri.parse(streamUrl);
        switch (uri.getHost()) {
            case YOUTUBE_SOURCED:
                return StreamSource.YOUTUBE;
            default:
                return StreamSource.AMAZON_S3;
        }
    }

    public enum StreamSource {
        YOUTUBE, AMAZON_S3
    }
}

package com.copula.nettysocial.feed.abstimeline;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.support.android.view.widget.AbsListViewScrollListener;
import com.copula.support.android.view.widget.ListView;

/**
 * Created by heeleaz on 8/26/16.
 */
public abstract class AbsTimelineFragment extends Fragment implements AdapterView.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener, AbsListViewScrollListener {

    public static final int TOP_FETCH = 1, BOTTOM_FETCH = -1;
    private int preLastItem;

    private String appUserId;

    protected abstract void fetchFeedInApproach(int fetchApproach);

    protected abstract ListView getListView();

    protected abstract String getNextPageToken();

    public String getAppUserId() {
        return appUserId;
    }

    protected void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.appUserId = new UserAccountBase(getActivity()).getUserId();

        getListView().setOnItemClickListener(this);
        getListView().setHelperScrollListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
    }

    @Override
    public void onRefresh() {
        //load first batch of new data and clearing or appending prev data
        this.fetchFeedInApproach(TOP_FETCH);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onPageScroll(int totalItem, int pageOffset, int pagerStep) {
    }

    @Override
    public void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis) {
        final int lastItem = first + visibleCount;

        //to avoid multiple calls for last item
        if (lastItem != totalCount || preLastItem == lastItem) return;
        preLastItem = lastItem;// updateLike last item position

        if (getNextPageToken() != null) this.fetchFeedInApproach(BOTTOM_FETCH);
    }
}

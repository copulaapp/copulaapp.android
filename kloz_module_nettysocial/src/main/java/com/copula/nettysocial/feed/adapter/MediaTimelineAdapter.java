package com.copula.nettysocial.feed.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.nettysocial.feed.adapter.media.ImageAdapter;
import com.copula.nettysocial.feed.adapter.media.MediaAdapter;
import com.copula.nettysocial.feed.adapter.media.VideoAdapter;
import com.copula.support.android.view.widget.BaseAdapter;

/**
 * Created by eliasigbalajobi on 2/28/16.
 */
public class MediaTimelineAdapter extends BaseAdapter<MediaFeedEntry> {
    private static final int AUDIO_ITEM = 0;
    private static final int IMAGE_ITEM = 1;
    private static final int VIDEO_ITEM = 2;

    private static final int TYPE_MAX_COUNT = 3;

    private String appUserId;

    public MediaTimelineAdapter(Context context, String appUserId) {
        super(context);
        this.appUserId = appUserId;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        MediaFeedEntry m = getItem(position);
        if (m.mediaType == MediaModel.IMAGE) return IMAGE_ITEM;
        else if (m.mediaType == MediaModel.AUDIO) return AUDIO_ITEM;
        else return VIDEO_ITEM;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        MasterAdapter adapter;
        if (convertView == null || convertView.getTag() == null) {
            int type = getItemViewType(position);
            if (type == VIDEO_ITEM) {
                adapter = new VideoAdapter(getContext(), inflater);
            } else {
                adapter = new ImageAdapter(getContext(), inflater);
            }

            convertView = adapter.getView();
            convertView.setTag(adapter);
        } else {
            adapter = (MediaAdapter) convertView.getTag();
        }

        adapter.setData(getItem(position), appUserId, position);
        return convertView;
    }
}

package com.copula.nettysocial.feed.adapter.media;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.copula.nettysocial.R;
import com.copula.nettysocial.feed.Util;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.nettysocial.nettyservice.entry.MediaMetaModel;
import com.copula.support.android.util.StringUtil;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 6/9/17.
 */
public class VideoAdapter extends MediaAdapter {
    private VideoViewHolder holder;
    private int displayWidth;

    public VideoAdapter(Context ctx, LayoutInflater inflater) {
        super(ctx, inflater);
        displayWidth = ctx.getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getChildView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.adp_feed_media_inc_video, null, false);
    }

    @Override
    public void initChildViewHolder() {
        View view = getView();
        holder = new VideoViewHolder();
        holder.caption = (TextView) view.findViewById(R.id.txt_media_caption);
        holder.thumb = (ImageView) view.findViewById(R.id.img_media_thumbnail);
    }

    @Override
    public void setData(MediaFeedEntry m, String appUserId, int p) {
        super.setData(m, appUserId, p);
        if (!StringUtil.isNullString(m.mediaCaption)) {
            holder.caption.setVisibility(View.VISIBLE);
            holder.caption.setText(m.mediaCaption);
        } else holder.caption.setVisibility(View.GONE);

        MediaMetaModel meta = MediaMetaModel.toModel(m.mediaMeta);
        ImageAdapter.resolveImageViewDimension(holder.thumb, meta.width, meta.height);

        String thumbnailUrl = decodeVideoThumbnailUrl(m.thumbnailUrl);
        Glide.with(getContext()).load(thumbnailUrl)
                .placeholder(R.drawable.img_video_thumbnail_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.RESULT).into(holder.thumb);
    }

    private String decodeVideoThumbnailUrl(String thumbnailUrl) {
        if (thumbnailUrl == null) return null;

        Util.StreamSource s = Util.getStreamSource(thumbnailUrl);
        if (s == Util.StreamSource.YOUTUBE) {
            return Uri.parse(thumbnailUrl).getQueryParameter("v");
        } else return thumbnailUrl;
    }

    private class VideoViewHolder extends MediaViewHolder {
        ImageView thumb;
        TextView caption;
    }
}

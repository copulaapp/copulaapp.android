package com.copula.nettysocial.feed.abstimeline;

import android.content.Context;
import android.view.View;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import com.copula.genericlook.AbsPopupWindow;
import com.copula.nettysocial.R;

/**
 * Created by heeleaz on 3/10/17.
 */
class FeedOptionPopWindow extends AbsPopupWindow implements AbsPopupWindow.PopupClickListener {

    private MediaFeedEntry media;
    private MediaOptionListener optionListener;

    FeedOptionPopWindow(Context context) {
        super(context);
    }

    public void setMedia(MediaFeedEntry media) {
        this.media = media;
    }

    public void show(View anchor, MediaOptionListener listener) {
        this.optionListener = listener;

        this._setup(anchor);
        show(this);
    }

    @Override
    protected void _setup(View anchor) {
        super._setup(anchor);

        addModel(R.string.act_delete, R.drawable.ic_delete_grey_64px, 0);
    }

    @Override
    public void onAction(int position, OptionModel option) {
        optionListener.onAction((int) option.tag, media);
    }

    interface MediaOptionListener {
        void onAction(int action, MediaFeedEntry media);
    }
}

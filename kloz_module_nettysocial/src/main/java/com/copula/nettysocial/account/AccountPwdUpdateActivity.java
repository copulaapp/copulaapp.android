package com.copula.nettysocial.account;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.copula.functionality.app.UserSession;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.genericlook.CopulaAlertDialog;
import com.copula.genericlook.DesignHelper;
import com.copula.genericlook.CustomActionBar;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.util.Formatter;

/**
 * Created by eliasigbalajobi on 5/31/16.
 */
public class AccountPwdUpdateActivity extends AppCompatActivity implements EditText.TextWatcher, View.OnClickListener,
        View.OnFocusChangeListener {
    private EditText edtCurrentPassword, edtNewPassword, edtNewPasswordRe;
    private Button btnSubmit;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, AccountPwdUpdateActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(DesignHelper.wrapInScrollable(this, R.layout.act_password_update));

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(R.string.txt_update_password).setBackListener(this).compile();

        edtCurrentPassword = (EditText) findViewById(R.id.edt_current_password);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        edtNewPassword = (EditText) findViewById(R.id.edt_password);
        edtNewPasswordRe = (EditText) findViewById(R.id.edt_password_re);

        edtNewPassword.setTextChangedListener(this);
        edtNewPasswordRe.setTextChangedListener(this);
        edtNewPassword.setOnFocusChangeListener(this);
        btnSubmit.setOnClickListener(this);
        btnSubmit.setEnabled(false);//enabled controlled by input validation
    }

    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        boolean valPassword, valPasswordRe;
        String password = edtNewPassword.getTextContent();
        String passwordRe = edtNewPasswordRe.getTextContent();

        valPassword = Formatter.computePasswordStrength(password) > 5;
        if (valPasswordRe = password.equals(passwordRe)) {
            edtNewPasswordRe.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        btnSubmit.setEnabled(valPassword && valPasswordRe);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String passwordRe = edtNewPasswordRe.getTextContent();
        String password = edtNewPassword.getTextContent();
        if (passwordRe.length() > 0 && !password.equals(passwordRe)) {
            DesignHelper.showEditTextInputError(edtNewPasswordRe, "");
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {
            new PasswordUpdate().execute();
        } else if (v.getId() == R.id.btn_bar_back) {
            finish();
        }
    }

    private void dispatchMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private class PasswordUpdate extends AsyncTask<Void, Void, RestResponse> {
        private String cPassword, nPassword;

        @Override
        protected void onPreExecute() {
            cPassword = edtCurrentPassword.getTextContent();
            nPassword = edtNewPassword.getTextContent();

            btnSubmit.startProgressing(NettySocialActHelper.setupAuthLoadButton
                    (AccountPwdUpdateActivity.this, R.string.prg_update));
        }

        @Override
        protected RestResponse doInBackground(Void... params) {
            String userId = UserSession.getAppUserId(AccountPwdUpdateActivity.this);
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            return api.getAccountAPI().updatePassword(userId, cPassword, nPassword);
        }

        @Override
        protected void onPostExecute(RestResponse response) {
            btnSubmit.finishProgressing();

            if (response == null) {
                dispatchMessage(getString(R.string.msg_request_failed));
            } else if (response.statusCode != 1) {
                dispatchMessage(response.statusMessage);
            } else {
                CopulaAlertDialog dialog = new CopulaAlertDialog(AccountPwdUpdateActivity.this);
                dialog.setCancelable(false);
                dialog.setMessage(R.string.msg_password_update_success);
                dialog.setNeutralButton(R.string.act_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
                dialog.show();
            }
        }
    }//END
}

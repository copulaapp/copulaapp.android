package com.copula.nettysocial.account.registration;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.account.authenticate.LoggedInPageFragment;
import com.copula.nettysocial.account.thirdparty.FacebookAuthHelper;
import com.copula.nettysocial.account.thirdparty.T3AccountAuthenticator;
import com.copula.nettysocial.account.thirdparty.TPartyRegisterFragment;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 4/3/17.
 */
public class AccountRegisterFragment extends Fragment implements RegisterCheckFragment.NativeRegisterCallback,
        FacebookAuthHelper.FacebookAuthCallback {
    private NativeRegisterFragment nativeFragment;
    private FacebookAuthHelper facebookAuthHelper;

    private View vRegisterOpt;
    private TextView txtFacebookButton;

    public static AccountRegisterFragment instantiate() {
        return new AccountRegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_account_register_main, container, false);
        txtFacebookButton = (TextView) view.findViewById(R.id.txt_facebook_button);
        vRegisterOpt = view.findViewById(R.id.view_register_opt);

        Drawable dr = DesignHelper.scaleDrawable(
                getActivity(), R.drawable.ic_facebook_logo_blue_48px, 24, 24);
        txtFacebookButton.setCompoundDrawablesWithIntrinsicBounds(dr, null, null, null);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Fragment nativeRegister = RegisterCheckFragment.instantiate(this);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.register_fragment, nativeRegister, "NFragment").commit();

        facebookAuthHelper = new FacebookAuthHelper(getActivity(), this);
        txtFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookAuthHelper.init();
            }
        });
    }

    public boolean allowBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm == null || fm.findFragmentByTag("RegisterStack") == null)
            return true;

        vRegisterOpt.setVisibility(View.VISIBLE);
        fm.popBackStackImmediate("Register", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        return false;
    }

    @Override
    public void onCompleteNativeRegister(String email, String password) {
        nativeFragment = NativeRegisterFragment.instantiate(email, password, null);
        stackFragmentB(nativeFragment);
    }

    @Override
    public void onFacebookAccountExists(UserAccountEntry account) {
        stackFragmentB(LoggedInPageFragment.instantiate(account));
    }

    @Override
    public void onFacebookAccountNonExists(String tpUserId, String email, String imgUrl) {
        nativeFragment = TPartyRegisterFragment.
                instantiate(T3AccountAuthenticator.FACEBOOK, tpUserId, email, imgUrl);
        stackFragmentB(nativeFragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (nativeFragment != null)
            nativeFragment.onActivityResult(requestCode, resultCode, data);

        if (facebookAuthHelper != null)
            facebookAuthHelper.onActivityResult(requestCode, resultCode, data);
    }

    private void stackFragmentB(Fragment fragment) {
        vRegisterOpt.setVisibility(View.GONE);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.register_fragment, fragment, "RegisterStack");
        ft.addToBackStack("Register").commit();
    }
}

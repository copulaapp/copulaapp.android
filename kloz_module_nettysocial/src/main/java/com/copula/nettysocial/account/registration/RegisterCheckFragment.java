package com.copula.nettysocial.account.registration;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.support.android.util.UserEmailFetcher;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.util.Formatter;

/**
 * Created by eliasigbalajobi on 3/23/16.
 */
public class RegisterCheckFragment extends Fragment implements EditText.TextWatcher, View.OnFocusChangeListener {
    private final int icPerson = R.drawable.img_person_black_96dp;
    private final int icPassword = R.drawable.img_password_black_96dp;
    private final int icWarning = R.drawable.ic_warning_red_64dp;

    private EditText edtEmail, edtPassword, edtRePassword;
    private Button btnSubmit;

    private NativeRegisterCallback callback;

    public static RegisterCheckFragment instantiate(NativeRegisterCallback callback) {
        RegisterCheckFragment f = new RegisterCheckFragment();
        f.callback = callback;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle s9) {
        View v = inflater.inflate(R.layout.frg_account_register_check, container, false);
        btnSubmit = (Button) v.findViewById(R.id.btn_submit);

        (edtEmail = (EditText)
                v.findViewById(R.id.edt_email)).setTextChangedListener(this);
        (edtPassword = (EditText)
                v.findViewById(R.id.edt_password)).setTextChangedListener(this);
        (edtRePassword = (EditText)
                v.findViewById(R.id.edt_re_password)).setTextChangedListener(this);

        setCompoundDrawable(icPerson, 0, edtEmail);
        setCompoundDrawable(icPassword, 0, edtPassword);
        setCompoundDrawable(icPassword, 0, edtRePassword);

        edtPassword.setOnFocusChangeListener(this);
        edtRePassword.setOnFocusChangeListener(this);
        return v;
    }

    public String getEmail() {
        return edtEmail.getTextContent();
    }

    public String getPassword() {
        return edtPassword.getTextContent();
    }

    private void setCompoundDrawable(int left, int right, EditText editText) {
        Drawable dl = DesignHelper.scaleDrawable(getActivity(), left, 20, 20);
        Drawable dr = DesignHelper.scaleDrawable(getActivity(), right, 20, 20);

        editText.setCompoundDrawablesWithIntrinsicBounds(dl, null, dr, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AccountVerification(edtEmail.getTextContent()).execute();
            }
        });

        edtRePassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (btnSubmit.isEnabled()) btnSubmit.performClick();
                }
                return true;
            }
        });

        String email = UserEmailFetcher.getEmail(getActivity());
        edtEmail.setText(email != null ? email : "");

        btnSubmit.setEnabled(false);
    }


    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        String email = edtEmail.getTextContent();
        String password = edtPassword.getTextContent();
        String rePassword = edtRePassword.getTextContent();

        boolean valEmail, valPassword, valRePassword;
        if (valEmail = Formatter.isValidEmailAddress(email)) {
            setCompoundDrawable(icPerson, 0, edtEmail);
        }

        if (valPassword = Formatter.computePasswordStrength(password) > 5) {
            setCompoundDrawable(icPassword, 0, edtPassword);
        }

        valRePassword = password.equals(rePassword);

        if (editText == edtRePassword && !valRePassword) {
            setCompoundDrawable(icPassword, icWarning, edtRePassword);
        } else {
            setCompoundDrawable(icPassword, 0, edtRePassword);
        }

        btnSubmit.setEnabled(valEmail && valPassword && valRePassword);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v == edtPassword && hasFocus)
                && !Formatter.isValidEmailAddress(edtEmail.getTextContent())) {
            setCompoundDrawable(icPerson, icWarning, edtEmail);
        } else if ((v == edtRePassword && hasFocus)
                && Formatter.computePasswordStrength(edtPassword.getTextContent()) < 6) {
            setCompoundDrawable(icPassword, icWarning, edtPassword);
        }
    }//end

    private void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    interface NativeRegisterCallback {
        void onCompleteNativeRegister(String email, String password);
    }

    private class AccountVerification extends AsyncTask<Void, Void, RestResponse<UserAccountEntry>> {
        private String email;

        AccountVerification(String email) {
            this.email = email;

            edtPassword.setEnabled(false);
            edtEmail.setEnabled(false);

            View progressingView = NettySocialActHelper
                    .setupAuthLoadButton(getActivity(), R.string.prg_account_checker);
            btnSubmit.startProgressing(progressingView);
        }

        @Override
        protected RestResponse<UserAccountEntry> doInBackground(Void... params) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            return api.getAccountAPI().getAccountWithUser(email);
        }

        @Override
        protected void onPostExecute(RestResponse<UserAccountEntry> entry) {
            if (getView() == null) return;

            if (entry == null) {
                dispatchMessage(getString(R.string.msg_account_register_error));
            } else if (entry.statusCode == 1) {//account exists
                setCompoundDrawable(icPerson, icWarning, edtEmail);
                dispatchMessage(getString(R.string.msg_email_already_exist));
            } else if (entry.statusCode == 0) {//not exists
                if (callback != null) callback.onCompleteNativeRegister(email, getPassword());
            }

            edtPassword.setEnabled(true);
            edtEmail.setEnabled(true);
            btnSubmit.finishProgressing();
        }
    }//end
}

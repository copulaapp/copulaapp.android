package com.copula.nettysocial.account.registration;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.util.MediaUtility;
import com.copula.functionality.webservice.RestResponse;
import com.copula.genericlook.DesignHelper;
import com.copula.mediagallary.MediaChooserGalleryActivity;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.util.Formatter;
import com.copula.support.validator.UrlValidator;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;

/**
 * Created by eliasigbalajobi on 3/23/16.
 */
public class NativeRegisterFragment extends Fragment implements View.OnFocusChangeListener, EditText.TextWatcher {
    private final int imgProfile = R.drawable.img_person_black_96dp;
    private final int imgBio = R.drawable.img_person_bio_black_48dp;

    protected EditText edtUsername, edtBio;
    protected Button btnSubmit;
    protected View vPrgImageUpload;
    protected String profileImagePath;

    private ImageView imgProfileDP;
    private String email, password;

    public static NativeRegisterFragment instantiate(String email, String password, String imageUrl) {
        NativeRegisterFragment fragment = new NativeRegisterFragment();
        Bundle argument = new Bundle(3);
        argument.putString("email", email);
        argument.putString("password", password);
        argument.putString("imgUrl", imageUrl);

        fragment.setArguments(argument);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_account_register_native, container, false);

        (edtUsername = (EditText) v.findViewById(R.id.edt_username)).setTextChangedListener(this);
        (edtBio = (EditText) v.findViewById(R.id.edt_profile_bio)).setOnFocusChangeListener(this);
        imgProfileDP = (ImageView) v.findViewById(R.id.img_profile_image);
        btnSubmit = (Button) v.findViewById(R.id.btn_submit);
        vPrgImageUpload = v.findViewById(R.id.prg_image_upload);


        setCompoundDrawable(imgProfile, 0, edtUsername);
        setCompoundDrawable(imgBio, 0, edtBio);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        password = getArguments().getString("password");
        email = getArguments().getString("email");
        profileImagePath = getArguments().getString("imgUrl");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateInput()) return;
                new AccountRegistration(email, edtUsername.getTextContent(),
                        edtBio.getTextContent(), password).execute();
            }
        });

        imgProfileDP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = getString(R.string.txt_select_profile_image);
                MediaChooserGalleryActivity
                        .launch(getActivity(), 1, title, 1);
            }
        });

        if (profileImagePath != null) loadImage(profileImagePath);
    }

    protected void setCompoundDrawable(int left, int right, EditText editText) {
        Drawable ld = DesignHelper.scaleDrawable(getActivity(), left, 20, 20);
        Drawable rd = DesignHelper.scaleDrawable(getActivity(), right, 20, 20);

        editText.setCompoundDrawablesWithIntrinsicBounds(ld, null, rd, null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((resultCode == Activity.RESULT_OK) && (requestCode == 1)) {
            MediaModel mediaModel = (MediaModel)
                    data.getSerializableExtra(MediaChooserGalleryActivity.EXTRA_MEDIA);
            profileImagePath = MediaUtility
                    .appendSize(mediaModel.dataUrl, MediaUtility.DP_UPLOAD_DIM);
            loadImage(mediaModel.dataPath);
        }
    }

    protected void loadImage(String imgUrl) {
        RequestCreator creator;
        if (UrlValidator.getInstance().isValid(imgUrl)) {
            creator = Picasso.with(getActivity()).load(imgUrl);
        } else creator = Picasso.with(getActivity()).load(new File(imgUrl));

        int dw = getResources().getDisplayMetrics().widthPixels;
        int dim = (int) (dw / 3.5);
        creator.resize(dim, dim).centerCrop()
                .placeholder(R.drawable.img_person_white_150px).into(imgProfileDP);
    }

    protected void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        if (Formatter.isValidUsername(edtUsername.getTextContent())) {
            setCompoundDrawable(imgProfile, 0, edtUsername);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v == edtBio && hasFocus) validateInput();
    }

    protected boolean validateInput() {
        if (!Formatter.isValidUsername(edtUsername.getTextContent())) {
            setCompoundDrawable(imgProfile, R.drawable.ic_warning_red_64dp, edtUsername);
            return false;
        } else {
            setCompoundDrawable(imgProfile, 0, edtUsername);
            return true;
        }
    }

    protected void enableInputs(boolean enable) {
        edtUsername.setEnabled(enable);
        edtBio.setEnabled(enable);
        imgProfileDP.setEnabled(false);
    }

    protected void uploadAndSaveImage(String userId, String imgUrl) {
        File imgFile = MediaUtility.makeTmpFile(imgUrl);
        if (imgFile != null && imgFile.exists()) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            api.getProfileAPI().updateProfileImage(userId, imgFile);
            new UserAccountBase(getActivity()).setProfileImage(userId, imgFile);
        }
    }

    private class AccountRegistration extends AsyncTask<Void, Void, RestResponse<UserAccountEntry>> {
        private String username, bio, email, password;

        AccountRegistration(String email, String username, String bio, String password) {
            this.username = username;
            this.bio = bio;
            this.email = email;
            this.password = password;

            enableInputs(false);
            btnSubmit.startProgressing(NettySocialActHelper
                    .setupAuthLoadButton(getActivity(), R.string.prg_account_register));
            if (profileImagePath != null) vPrgImageUpload.setVisibility(View.VISIBLE);
        }

        @Override
        protected RestResponse<UserAccountEntry> doInBackground(Void... params) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();

            String pnToken = new UserAccountBase(getActivity()).getPNToken();
            RestResponse<UserAccountEntry> ac =
                    api.getAccountAPI().newAccount(email, username, password, pnToken);

            if (ac != null && ac.statusCode == 1) {
                api.getProfileAPI().newProfile(ac.result.userId, bio);
                uploadAndSaveImage(ac.result.userId, profileImagePath);
            }
            return ac;
        }

        @Override
        protected void onPostExecute(RestResponse<UserAccountEntry> response) {
            if (getView() == null) return;

            if (response == null) {
                dispatchMessage(getString(R.string.msg_account_register_error));
            } else if (response.statusCode == 2) {
                setCompoundDrawable(imgProfile, R.drawable.ic_warning_red_64dp, edtUsername);
                dispatchMessage(getString(R.string.msg_username_already_exist));
            } else if (response.statusCode != 1) {
                dispatchMessage(response.statusMessage);
            } else {
                UserAccountEntry ac = response.result;
                new UserAccountBase(getActivity()).setupLogged(ac.userId, ac.username, bio);

                //TODO: Set a callback for activity finish
                Activity activity = getActivity();
                activity.setResult(Activity.RESULT_OK);
                activity.finish();
            }//end

            enableInputs(true);
            btnSubmit.finishProgressing();
            vPrgImageUpload.setVisibility(View.GONE);
        }
    }//end
}

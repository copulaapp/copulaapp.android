package com.copula.nettysocial.account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import com.copula.nettysocial.R;
import com.copula.nettysocial.account.authenticate.AccountLoginFragment;
import com.copula.nettysocial.account.registration.AccountRegisterFragment;
import com.copula.support.android.content.TabActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 4/3/17.
 */
public class AccountAuthPopupActivity extends TabActivity {
    private AccountRegisterFragment registerFragment;
    private AccountLoginFragment authMain;

    public static void launch(Activity context, int requestCode) {
        Intent intent = new Intent(context, AccountAuthPopupActivity.class);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected List<Fragment> getPagerFragments() {
        List<Fragment> fragments = new ArrayList<>(2);
        fragments.add(registerFragment = AccountRegisterFragment.instantiate());
        fragments.add(authMain = AccountLoginFragment.instantiate());
        return fragments;
    }

    @Override
    protected ViewPager getViewPager() {
        return (ViewPager) findViewById(R.id.pager);
    }

    @Override
    protected void onPrepareActivityWindow() {
        setTabViewStyle(R.style.AppTheme_MediaGallery_StudioSelectorTab);
        addTab(R.string.act_account_register).addTab(R.string.act_account_login);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_account_auth_popup);
        onPageSelected(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        getFragment(getCurrentItem()).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        int currentPage = getCurrentItem();
        if (currentPage == 0) {
            if (!registerFragment.allowBackPressed()) return;
            super.onBackPressed();
        }

        if (currentPage == 1) {
            if (!authMain.allowBackPressed()) return;
            setCurrentPage(0);
            return;
        }

        super.onBackPressed();
    }
}

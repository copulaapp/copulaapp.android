package com.copula.nettysocial.account.thirdparty;

import android.os.AsyncTask;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;

/**
 * Created by heeleaz on 10/3/16.
 */
public class T3AccountAuthenticator {
    public static final String FACEBOOK = "facebook";
    public static final String NATIVE = "native";

    public void authAccount(ThirdPartyCallback<UserAccountEntry> callback, String accountType, String tpUserId) {
        new Authenticator(accountType, tpUserId, callback).execute();
    }

    public interface ThirdPartyCallback<T> {
        void onResponse(int responseCode, String message, T extra);
    }

    private class Authenticator extends AsyncTask<Void, Void, RestResponse<UserAccountEntry>> {
        private String accountType, tpUserId;
        private ThirdPartyCallback<UserAccountEntry> callback;

        Authenticator(String accountType, String tpUserId, ThirdPartyCallback<UserAccountEntry> callback) {
            this.accountType = accountType;
            this.tpUserId = tpUserId;
            this.callback = callback;
        }

        @Override
        protected RestResponse<UserAccountEntry> doInBackground(Void... voids) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            return api.getAccountAPI().authAccountThirdParty(accountType, tpUserId);
        }

        @Override
        protected void onPostExecute(RestResponse<UserAccountEntry> response) {
            if (response == null) {
                callback.onResponse(-2, null, null);
            } else if (response.statusCode != 1) {
                callback.onResponse(response.statusCode, response.statusMessage, null);
            } else {
                callback.onResponse(response.statusCode, response.statusMessage, response.result);
            }
        }
    }//END
}

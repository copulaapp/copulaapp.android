package com.copula.nettysocial.account.authenticate;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.downloader.MediaDownloadClient;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.localservice.profile.UsersProfileResource;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.profile.helper.AsyncAPIRequest;
import com.copula.nettysocial.profile.helper.GetProfileHelper;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.concurrent.Executors;

/**
 * Created by eliasigbalajobi on 6/22/16.
 */
public class LoggedInPageFragment extends Fragment implements AsyncAPIRequest.UserActCallback {
    private ImageView imgProfileImage;
    private Button btnStart;
    private TextView txtPreparingAccount, txtWelcome;

    public static LoggedInPageFragment instantiate(UserAccountEntry account) {
        Bundle argument = new Bundle();
        argument.putSerializable("account", account);
        LoggedInPageFragment fragment = new LoggedInPageFragment();
        fragment.setArguments(argument);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_account_logged_in_page, container, false);
        imgProfileImage = (ImageView) v.findViewById(R.id.img_profile_image);
        btnStart = (Button) v.findViewById(R.id.btn_start);
        txtPreparingAccount = (TextView) v.findViewById(R.id.txt_preparing_account);
        txtWelcome = (TextView) v.findViewById(R.id.txt_welcome_message);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        UserAccountEntry model =
                (UserAccountEntry) getArguments().getSerializable("account");

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                activity.setResult(Activity.RESULT_OK);
                activity.finish();
            }
        });

        if (model != null) {
            new GetProfileHelper(model.userId, model.userId, this).execute();
            String msg = getString(R.string.msg_auth_success_welcome);
            txtWelcome.setText(msg.replace("?", model.username));
        }
    }

    private void doProfileImageDownload(final String imgUrl, final String userId) {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String path = UsersProfileResource.getProfileImagePath(userId);
                    MediaDownloadClient.download(imgUrl, path);
                } catch (Exception e) {
                }
            }
        });//end
    }

    @Override
    public void onPreAct(AsyncAPIRequest sf, int act) {
        DesignHelper.showOnlyView(txtPreparingAccount);
    }

    @Override
    public void onPostAct(AsyncAPIRequest sf, int act) {
        if (sf.isSuccessful()) {
            UserProfileEntry user = ((GetProfileHelper) sf).getResult();
            new UserAccountBase(getActivity()).setProfileBio(user.bio);
            doProfileImageDownload(user.imgUrl, user.userId);

            if (getView() != null) {
                Picasso.with(getActivity()).load(user.imgUrl)
                        .placeholder(R.drawable.img_person_white_150px).into(imgProfileImage);
            }
        }
        if (getView() != null) DesignHelper.showOnlyView(btnStart);
    }
}

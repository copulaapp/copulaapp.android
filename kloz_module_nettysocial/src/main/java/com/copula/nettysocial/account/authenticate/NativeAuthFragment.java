package com.copula.nettysocial.account.authenticate;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.account.AccountPwdForgetActivity;
import com.copula.support.android.util.UserEmailFetcher;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.EditText;

/**
 * Created by eliasigbalajobi on 3/23/16.
 */
public class NativeAuthFragment extends Fragment implements EditText.TextWatcher {
    private EditText edtUsername, edtPassword;
    private Button btnSubmit;

    private UserAccountEntry accountModel;
    private NativeAuthCallback callback;

    public static NativeAuthFragment instantiate(NativeAuthCallback callback) {
        NativeAuthFragment f = new NativeAuthFragment();
        f.callback = callback;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_account_native_auth_0, container, false);
        (edtPassword = (EditText) v.findViewById(R.id.edt_password))
                .setTextChangedListener(this);
        (edtUsername = (EditText) v.findViewById(R.id.edt_username))
                .setTextChangedListener(this);
        btnSubmit = (Button) v.findViewById(R.id.btn_submit);

        setCompoundDrawable(R.drawable.img_person_black_96dp, edtUsername);
        setCompoundDrawable(R.drawable.img_password_black_96dp, edtPassword);

        btnSubmit.setEnabled(false);//enabled controlled by input validation

        View txtForgetAccount = v.findViewById(R.id.txt_forget_account);
        txtForgetAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountPwdForgetActivity.launch(getActivity());
            }
        });
        return v;
    }

    public void setCallback(NativeAuthCallback callback) {
        this.callback = callback;
    }

    private void setCompoundDrawable(int drawable, EditText editText) {
        editText.setCompoundDrawablesWithIntrinsicBounds(DesignHelper.scaleDrawable(
                getActivity(), drawable, 20, 20), null, null, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String email = UserEmailFetcher.getEmail(getActivity());
        edtUsername.setText(email != null ? email : "");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edtUsername.getTextContent();
                String password = edtPassword.getTextContent();
                new AccountAuthenticate(username, password).execute();
            }
        });

        edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                btnSubmit.performClick();
                return true;
            }
        });
    }

    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        boolean valUsername, valPassword;
        valUsername = edtUsername.getTextContent().length() >= 2;
        valPassword = edtPassword.getTextContent().length() >= 2;
        btnSubmit.setEnabled(valUsername && valPassword);
    }

    private void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    UserAccountEntry getAccountModel() {
        return accountModel;
    }

    public interface NativeAuthCallback {
        void onNativeAuth(UserAccountEntry account);
    }

    private class AccountAuthenticate extends AsyncTask<Void, Void, RestResponse<UserAccountEntry>> {
        private String username, password;

        AccountAuthenticate(String username, String password) {
            this.username = username;
            this.password = password;

            edtPassword.setEnabled(false);
            edtUsername.setEnabled(false);

            btnSubmit.startProgressing(NettySocialActHelper
                    .setupAuthLoadButton(getActivity(), R.string.prg_login));
        }

        @Override
        protected RestResponse<UserAccountEntry> doInBackground(Void... params) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            return api.getAccountAPI().authAccount(username, password);
        }

        @Override
        protected void onPostExecute(RestResponse<UserAccountEntry> response) {
            if (getView() == null) return;

            if (response == null) {
                dispatchMessage(getString(R.string.msg_account_auth_failed));
            } else if (response.statusCode != 1) {
                dispatchMessage(response.statusMessage);
            } else {
                NativeAuthFragment.this.accountModel = response.result;
                new UserAccountBase(getActivity())
                        .setupLogged(accountModel.userId, accountModel.username, null);
                if (callback != null) callback.onNativeAuth(accountModel);
            }

            edtPassword.setEnabled(true);
            edtUsername.setEnabled(true);
            btnSubmit.finishProgressing();
        }
    }//END
}

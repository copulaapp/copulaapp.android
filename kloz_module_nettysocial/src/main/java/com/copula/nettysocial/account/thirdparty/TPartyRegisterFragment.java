package com.copula.nettysocial.account.thirdparty;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.account.registration.NativeRegisterFragment;

/**
 * Created by heeleaz on 10/7/16.
 */
public class TPartyRegisterFragment extends NativeRegisterFragment {

    public static TPartyRegisterFragment instantiate(String accountType, String tpUserId
            , String email, String imgUrl) {
        TPartyRegisterFragment fragment = new TPartyRegisterFragment();
        Bundle argument = new Bundle(4);
        argument.putString("tpUserId", tpUserId);
        argument.putString("accountType", accountType);
        argument.putString("email", email);
        argument.putString("imgUrl", imgUrl);

        fragment.setArguments(argument);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadImage(profileImagePath = getArguments().getString("imgUrl"));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tpUserId = getArguments().getString("tpUserId");
                String email = getArguments().getString("email");
                String accountType = getArguments().getString("accountType");
                String pnToken = new UserAccountBase(getActivity()).getPNToken();

                if (!validateInput()) return;
                new AccountRegistration(accountType, tpUserId, email,
                        edtUsername.getTextContent(), pnToken, edtBio.getTextContent()).execute();
            }
        });//END
    }

    private class AccountRegistration extends AsyncTask<Void, Void, RestResponse<UserAccountEntry>> {
        private String username, bio, accountType, tpUserId, email, pnToken;

        AccountRegistration(String accountType, String tpUserId, String email
                , String username, String pnToken, String bio) {
            this.username = username;
            this.bio = bio;
            this.accountType = accountType;
            this.tpUserId = tpUserId;
            this.email = email;
            this.pnToken = pnToken;

            enableInputs(false);
            View pv = NettySocialActHelper
                    .setupAuthLoadButton(getActivity(), R.string.prg_account_register);
            btnSubmit.startProgressing(pv);
            if (profileImagePath != null) vPrgImageUpload.setVisibility(View.VISIBLE);
        }

        @Override
        protected RestResponse<UserAccountEntry> doInBackground(Void... params) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            RestResponse<UserAccountEntry> ac = api.getAccountAPI()
                    .newAccountThirdParty(accountType, tpUserId, email, username, pnToken);

            if (ac != null && ac.statusCode == 1) {
                api.getProfileAPI().newProfile(ac.result.userId, bio);
                uploadAndSaveImage(ac.result.userId, profileImagePath);
            }
            return ac;
        }

        @Override
        protected void onPostExecute(RestResponse<UserAccountEntry> response) {
            if (response == null) {
                dispatchMessage(getString(R.string.msg_account_register_error));
            } else if (response.statusCode == 2) {//tpUserId already exists
                setCompoundDrawable(R.drawable.img_person_black_96dp,
                        R.drawable.ic_warning_red_64dp, edtUsername);
                dispatchMessage(getString(R.string.msg_username_already_exist));
            } else if (response.statusCode != 1) {
                dispatchMessage(response.statusMessage);
            } else {
                UserAccountBase accountBase = new UserAccountBase(getActivity());
                accountBase.setupLogged(response.result.userId, username, bio);
                accountBase.set3PAccount(accountType, tpUserId);

                //TODO: Set a callback for activity finish
                Activity activity = getActivity();
                activity.setResult(Activity.RESULT_OK);
                activity.finish();
            }//end

            enableInputs(true);
            btnSubmit.finishProgressing();
            vPrgImageUpload.setVisibility(View.GONE);
        }
    }//end
}

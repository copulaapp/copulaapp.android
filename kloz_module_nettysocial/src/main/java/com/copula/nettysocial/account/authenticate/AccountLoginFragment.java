package com.copula.nettysocial.account.authenticate;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.nettysocial.R;
import com.copula.nettysocial.account.thirdparty.FacebookAuthHelper;
import com.copula.nettysocial.account.thirdparty.T3AccountAuthenticator;
import com.copula.nettysocial.account.thirdparty.TPartyRegisterFragment;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 4/3/17.
 */
public class AccountLoginFragment extends Fragment implements NativeAuthFragment.NativeAuthCallback,
        FacebookAuthHelper.FacebookAuthCallback {
    private TextView txtFacebookButton;
    private View vLoginOpt;

    private FacebookAuthHelper facebookAuthHelper;
    private TPartyRegisterFragment t3registerFragment;

    public static AccountLoginFragment instantiate() {
        return new AccountLoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_account_authentic_main, container, false);
        txtFacebookButton = (TextView) view.findViewById(R.id.txt_facebook_button);
        vLoginOpt = view.findViewById(R.id.view_login_opt);

        Drawable dr = DesignHelper.scaleDrawable(
                getActivity(), R.drawable.ic_facebook_logo_blue_48px, 24, 24);
        txtFacebookButton.setCompoundDrawablesWithIntrinsicBounds(dr, null, null, null);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment fragment = NativeAuthFragment.instantiate(this);
        ft.replace(R.id.auth_fragment, fragment, "AFragment").commit();

        facebookAuthHelper = new FacebookAuthHelper(getActivity(), this);

        txtFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookAuthHelper.init();
            }
        });
    }

    @Override
    public void onNativeAuth(UserAccountEntry account) {
        Fragment fragment = LoggedInPageFragment.instantiate(account);

        vLoginOpt.setVisibility(View.GONE);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.auth_fragment, fragment, "BFRAGMENT");
        ft.addToBackStack("B").commit();
    }

    @Override
    public void onFacebookAccountExists(UserAccountEntry account) {
        onNativeAuth(account);
    }

    @Override
    public void onFacebookAccountNonExists(String tpUserId, String email, String imgUrl) {
        t3registerFragment = TPartyRegisterFragment.
                instantiate(T3AccountAuthenticator.FACEBOOK, tpUserId, email, imgUrl);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.auth_fragment, t3registerFragment, "BFRAGMENT");
        ft.addToBackStack("B").commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (facebookAuthHelper != null)
            facebookAuthHelper.onActivityResult(requestCode, resultCode, data);
        if (t3registerFragment != null)
            t3registerFragment.onActivityResult(requestCode, resultCode, data);
    }

    public boolean allowBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm != null && fm.findFragmentByTag("BFRAGMENT") != null) {
            vLoginOpt.setVisibility(View.VISIBLE);
            fm.popBackStackImmediate("B", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            return false;
        }
        return true;
    }
}

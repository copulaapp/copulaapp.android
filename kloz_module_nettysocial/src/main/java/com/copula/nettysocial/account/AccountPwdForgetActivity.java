package com.copula.nettysocial.account;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;
import com.copula.genericlook.CopulaAlertDialog;
import com.copula.genericlook.DesignHelper;
import com.copula.genericlook.CustomActionBar;
import com.copula.nettysocial.R;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.util.Formatter;

/**
 * Created by eliasigbalajobi on 5/28/16.
 */
public class AccountPwdForgetActivity extends AppCompatActivity implements EditText.TextWatcher, View.OnClickListener {
    private EditText edtUsername;
    private Button btnSubmit;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, AccountPwdForgetActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(DesignHelper.wrapInScrollable(this, R.layout.act_forget_password));

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(R.string.label_forget_account);
        actionBar.setBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }).compile();

        (edtUsername = (EditText) findViewById(R.id.edt_username)).setTextChangedListener(this);
        (btnSubmit = (Button) findViewById(R.id.btn_submit)).setOnClickListener(this);
        setCompoundDrawable(R.drawable.img_person_black_96dp, edtUsername);
    }

    private void setCompoundDrawable(int drawable, EditText editText) {
        editText.setCompoundDrawablesWithIntrinsicBounds(DesignHelper.scaleDrawable(
                this, drawable, 20, 20), null, null, null);
    }

    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        String user = s.toString();
        if (Formatter.isValidEmailAddress(user) || Formatter.isValidUsername(user)) {
            edtUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    private void dispatchMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showSuccessView() {
        CopulaAlertDialog dialog = new CopulaAlertDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage(R.string.msg_forget_account_success);
        dialog.setNeutralButton(R.string.act_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        String username = edtUsername.getTextContent();
        if (Formatter.isValidEmailAddress(username) || Formatter.isValidUsername(username)) {
            new ForgetPassword(username).execute();
        } else {
            DesignHelper.showEditTextInputError(edtUsername, "");
        }
    }//end onClick

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private class ForgetPassword extends AsyncTask<Void, Void, RestResponse> {
        private String username;

        ForgetPassword(String username) {
            this.username = username;

            View progressingView = View.inflate(AccountPwdForgetActivity.this,
                    R.layout.view_auth_form_button_progress, null);
            btnSubmit.startProgressing(progressingView);
        }

        @Override
        protected RestResponse doInBackground(Void... params) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            return api.getAccountAPI().forgetPassword(username);
        }

        @Override
        protected void onPostExecute(RestResponse response) {
            if (response == null) {
                dispatchMessage(getString(R.string.msg_request_failed));
            } else if (response.statusCode != 1) {
                dispatchMessage(response.statusMessage);
            } else showSuccessView();

            btnSubmit.finishProgressing();
        }
    }//end
}

package com.copula.nettysocial.account.thirdparty;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.genericlook.CopulaProgressDialog;
import com.copula.nettysocial.R;
import com.facebook.*;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by heeleaz on 10/7/16.
 */
public class FacebookAuthHelper implements GraphRequest.GraphJSONObjectCallback, FacebookCallback<LoginResult> {
    private CallbackManager mCallbackManager;
    private Activity context;

    private FacebookAuthCallback callback;

    public FacebookAuthHelper(Activity context, FacebookAuthCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void init() {
        LoginManager.getInstance().logOut();

        LoginManager loginManager = LoginManager.getInstance();
        loginManager.logInWithReadPermissions(context, Arrays.asList("public_profile", "email"));
        loginManager.registerCallback(mCallbackManager = CallbackManager.Factory.create(), this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        AccessToken token = loginResult.getAccessToken();
        GraphRequest request = GraphRequest.newMeRequest(token, this);

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onError(FacebookException e) {
        dispatchMessage(context.getString(R.string.msg_authentication_failed));
    }

    private void dispatchMessage(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
        try {
            String tpUserId = jsonObject.getString("id");
            String email = jsonObject.getString("email");
            String imgUrl = "https://graph.facebook.com/" + tpUserId + "/picture?type=normal";

            T3AccountAuthenticator authenticator = new T3AccountAuthenticator();
            authenticator.authAccount(new AccountAuthApiCallback(tpUserId, email, imgUrl),
                    T3AccountAuthenticator.FACEBOOK, tpUserId);
        } catch (JSONException e) {
            dispatchMessage("Facebook: " + context.getString(R.string.msg_authentication_failed));
        }
    }//end

    public interface FacebookAuthCallback {
        void onFacebookAccountExists(UserAccountEntry account);

        void onFacebookAccountNonExists(String tpUserId, String email, String imgUrl);
    }

    private class AccountAuthApiCallback implements T3AccountAuthenticator.ThirdPartyCallback<UserAccountEntry> {
        private CopulaProgressDialog progressDialog;
        private String tpUserId, email, imgUrl;

        AccountAuthApiCallback(String tpUserId, String email, String imgUrl) {
            this.tpUserId = tpUserId;
            this.email = email;
            this.imgUrl = imgUrl;

            progressDialog = new CopulaProgressDialog(context);
            progressDialog.setMessage(R.string.prg_3p_authentication);
            progressDialog.show();
        }

        @Override
        public void onResponse(int responseCode, String message, UserAccountEntry model) {
            progressDialog.dismiss();
            if (responseCode == 1) { //account exists. hence login
                UserAccountBase accountBase = new UserAccountBase(context);
                accountBase.setupLogged(model.userId, model.username, null);
                accountBase.set3PAccount("facebook", tpUserId);

                if (callback != null) callback.onFacebookAccountExists(model);
            } else if (responseCode == -1) { //account does not exists, hence register
                if (callback != null) {
                    callback.onFacebookAccountNonExists(tpUserId, email, imgUrl);
                }
            } else {//possibly connection error
                dispatchMessage(context.getString(R.string.msg_authentication_failed));
            }
        }
    }//end
}

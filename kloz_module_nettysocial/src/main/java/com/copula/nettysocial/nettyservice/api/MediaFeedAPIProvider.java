package com.copula.nettysocial.nettyservice.api;


import com.copula.functionality.restlite.ObjectJsonMapper;
import com.copula.functionality.restlite.QueryParam;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;
import okhttp3.FormBody;

import java.util.List;

/**
 * Created by heeleeaz on 7/12/16.
 */
public class MediaFeedAPIProvider {
    private NettyAPIProvider apiProvider;

    MediaFeedAPIProvider(NettyAPIProvider provider) {
        this.apiProvider = provider;
    }

    public static boolean isNullString(String s) {
        return s == null || s.equalsIgnoreCase("null")
                || s.equals("");
    }

    public RestResponse<List<MediaFeedEntry>> getFeeds(String posterId, String peeperId
            , String pageToken) {
        try {
            QueryParam params = new QueryParam();
            params.put("posterId", posterId);
            params.put("peeperId", peeperId);
            params.put("pageToken", pageToken);

            String url = apiProvider.bind(RequestRouteTable.GET_FEEDS);
            NettyAPIProvider.Result result = apiProvider.getRequest(url, params);
            RestResponse<List<MediaFeedEntry>> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);

            if (!isNullString(result.result)) {
                rest.result = ObjectJsonMapper.mapList(MediaFeedEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<List<MediaFeedEntry>> getFeedTimeline(String userId, String pageToken) {
        try {
            QueryParam params = new QueryParam();
            params.put("userId", userId);
            params.put("pageToken", pageToken);

            String url = apiProvider.bind(RequestRouteTable.GET_FEED_TIMELINE);
            NettyAPIProvider.Result result = apiProvider.getRequest(url, params);

            RestResponse<List<MediaFeedEntry>> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);

            if (!isNullString(result.result)) {
                rest.result = ObjectJsonMapper.mapList(MediaFeedEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<List<MediaFeedEntry>> getExploreFeeds(String userId, String pageToken) {
        try {
            QueryParam params = new QueryParam();
            params.put("userId", userId);
            params.put("pageToken", pageToken);

            String url = apiProvider.bind(RequestRouteTable.GET_EXPLORE_FEEDS);
            NettyAPIProvider.Result result = apiProvider.getRequest(url, params);


            RestResponse<List<MediaFeedEntry>> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (!isNullString(result.result)) {
                rest.result = ObjectJsonMapper.mapList(MediaFeedEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse likeFeed(String feedId, String likerId) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("feedId", feedId);
            params.add("likerId", likerId);
            String url = apiProvider.bind(RequestRouteTable.LIKE_FEED);

            NettyAPIProvider.Result result =
                    apiProvider.formRequest("POST", url, params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse unlikeFeed(String feedId, String likerId) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("feedId", feedId);
            params.add("likerId", likerId);

            String url = apiProvider.bind(RequestRouteTable.UNLIKE_FEED);
            NettyAPIProvider.Result result =
                    apiProvider.formRequest("POST", url, params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse removeFeed(String feedId, String posterId) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("feedId", feedId);
            params.add("posterId", posterId);

            String url = apiProvider.bind(RequestRouteTable.REMOVE_FEED);
            NettyAPIProvider.Result result = apiProvider
                    .formRequest("DELETE", url, params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse removeFeeds(String posterId) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("posterId", posterId);

            String url = apiProvider.bind(RequestRouteTable.REMOVE_FEEDS);
            NettyAPIProvider.Result result =
                    apiProvider.formRequest("DELETE", url, params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

}

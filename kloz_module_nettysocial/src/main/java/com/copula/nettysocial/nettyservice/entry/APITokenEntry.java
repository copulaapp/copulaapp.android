package com.copula.nettysocial.nettyservice.entry;

import com.copula.functionality.restlite.annotation.JSONInner;

/**
 * Created by heeleaz on 11/4/16.
 */
@JSONInner("result")
public class APITokenEntry {
    public String apiToken;
}

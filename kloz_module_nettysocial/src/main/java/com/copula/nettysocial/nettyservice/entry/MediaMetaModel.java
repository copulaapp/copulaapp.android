package com.copula.nettysocial.nettyservice.entry;

import com.copula.functionality.restlite.ObjectJsonMapper;

/**
 * Created by heeleaz on 9/6/17.
 */
public class MediaMetaModel {
    public int width = 1, height = 1, filesize, duration;

    public static MediaMetaModel toModel(String meta) {
        try {
            if (meta == null) return new MediaMetaModel();
            return ObjectJsonMapper.mapOne(MediaMetaModel.class, meta);
        } catch (Exception e) {
            return new MediaMetaModel();
        }
    }

    @Override
    public String toString() {
        return ObjectJsonMapper.toJSON(this);
    }

}

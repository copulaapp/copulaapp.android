package com.copula.nettysocial.nettyservice.api;

import android.util.Log;
import com.copula.functionality.restlite.ObjectJsonMapper;
import com.copula.functionality.restlite.QueryParam;
import com.copula.nettysocial.nettyservice.entry.APITokenEntry;
import okhttp3.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by eliasigbalajobi on 2/24/16.
 */
public class NettyAPIProvider {
    private static final String TAG = NettyAPIProvider.class.getSimpleName();
    private OkHttpClient connection;
    private String serverAddress;

    public NettyAPIProvider() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20000, TimeUnit.MILLISECONDS);
        builder.readTimeout(20000, TimeUnit.MILLISECONDS);
        builder.writeTimeout(20000, TimeUnit.MILLISECONDS);
        connection = builder.build();
    }

    public void setEndpoint(String address) {
        this.serverAddress = address;
    }

    Result getRequest(String path, QueryParam param) {
        try {
            Request.Builder builder = new Request.Builder();
            builder.url(path + "?" + param.toString());
            return makeRequest(builder.build());
        } catch (IOException e) {
            return null;
        }
    }

    Result formRequest(String method, String url, RequestBody request)
            throws IllegalAccessException, IOException {
        Request.Builder builder = new Request.Builder();
        builder.url(url).method(method, request);
        String apiAuthToken = getAPIToken();
        if (apiAuthToken == null) throw new IllegalAccessException("API Auth Failed");

        builder.addHeader("Authorization", "Bearer " + apiAuthToken);
        return makeRequest(builder.build());
    }

    public String getAPIToken() {
        FormBody.Builder form = new FormBody.Builder();
        form.add("user", NettyServiceAPI.getApiUsername());
        form.add("password", NettyServiceAPI.getApiPassword());

        String url = bind(RequestRouteTable.GET_API_AUTH_KEY);
        Request.Builder builder = new Request.Builder();
        builder.url(url).method("POST", form.build());

        try {
            String result = makeRequest(builder.build()).result;
            APITokenEntry model = ObjectJsonMapper.mapOne(APITokenEntry.class, result);
            return model.apiToken;
        } catch (Exception e) {
            Log.d(TAG, "Error generating API Token: " + e.getMessage());
        }
        return null;
    }

    private Result makeRequest(Request request) throws IOException {
        Result result = new Result();
        Response response = connection.newCall(request).execute();
        if ((result.resultCode = response.code()) == 200) {
            result.result = response.body().string();
        }
        return result;
    }

    String bind(String path) {
        return serverAddress + path;
    }

    String bind(String serverAddress, String path) {
        return serverAddress + path;
    }

    public MediaFeedAPIProvider getFeedAPI() {
        return new MediaFeedAPIProvider(this);
    }

    public UProfileAPIProvider getProfileAPI() {
        return new UProfileAPIProvider(this);
    }

    public UAccountAPIProvider getAccountAPI() {
        return new UAccountAPIProvider(this);
    }

    public SupportAPIProvider getSupportAPI() {
        return new SupportAPIProvider(this);
    }


    static class Result {
        String result;
        int resultCode;
    }
}

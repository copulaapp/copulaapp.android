package com.copula.nettysocial.nettyservice.api;


import android.util.Log;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.webservice.RestResponse;
import com.copula.functionality.restlite.ObjectJsonMapper;
import com.copula.functionality.restlite.QueryParam;
import okhttp3.FormBody;

import java.io.File;
import java.util.List;

/**
 * Created by heeleeaz on 7/12/16.
 */
public class UProfileAPIProvider {
    private NettyAPIProvider apiProvider;

    UProfileAPIProvider(NettyAPIProvider provider) {
        this.apiProvider = provider;
    }

    public RestResponse<List<UserProfileEntry>> getFollowings(String userId, String pageToken) {
        try {
            QueryParam params = new QueryParam();
            params.put("userId", userId);
            params.put("pageToken", pageToken);

            String url = apiProvider.bind(RequestRouteTable.GET_FOLLOWINGS);
            NettyAPIProvider.Result result = apiProvider.getRequest(url, params);

            RestResponse rest = ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null) {
                rest.result = ObjectJsonMapper.mapList(UserProfileEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse follow(String userId, String followUserId) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("userId", userId);
            params.add("followUserId", followUserId);

            String url = apiProvider.bind(RequestRouteTable.FOLLOW);
            NettyAPIProvider.Result result =
                    apiProvider.formRequest("PUT", url, params.build());

            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse unFollow(String userId, String followUserId) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("userId", userId);
            params.add("followUserId", followUserId);

            String url = apiProvider.bind(RequestRouteTable.UNFOLLOW_USER);
            NettyAPIProvider.Result result =
                    apiProvider.formRequest("DELETE", url, params.build());

            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<UserProfileEntry> getProfile(String userId, String peeperId) {
        try {
            QueryParam params = new QueryParam();
            params.put("userId", userId);
            params.put("peeperId", peeperId);

            String url = apiProvider.bind(RequestRouteTable.GET_PROFILE);
            NettyAPIProvider.Result result = apiProvider.getRequest(url, params);

            RestResponse rest = ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            Log.d("Hello", userId + " " + peeperId);
            if (rest != null) {
                rest.result = ObjectJsonMapper.mapOne(UserProfileEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<List<UserProfileEntry>> findProfile(String q, String pageToken) {
        try {
            QueryParam params = new QueryParam();
            params.put("q", q);
            if (pageToken != null) params.put("pageToken", pageToken);

            String url = apiProvider.bind(RequestRouteTable.FIND_PROFILE);
            NettyAPIProvider.Result result = apiProvider.getRequest(url, params);

            RestResponse rest = ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null) {
                rest.result = ObjectJsonMapper.mapList(UserProfileEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public RestResponse newProfile(String userId, String bio) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("userId", userId).add("bio", bio);

            String url = apiProvider.bind(RequestRouteTable.NEW_PROFILE);
            NettyAPIProvider.Result result =
                    apiProvider.formRequest("PUT", url, params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse updateProfile(String userId, String bio) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("userId", userId).add("bio", bio);

            String url = apiProvider.bind(RequestRouteTable.UPDATE_PROFILE);
            NettyAPIProvider.Result result =
                    apiProvider.formRequest("POST", url, params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse updateProfileImage(String userId, File file) {
        MultipartAPIProvider mq = new MultipartAPIProvider();
        mq.addForm("userId", userId);
        mq.addForm("file", file, "image/jpeg");
        mq.addHeader("Authorization", "Bearer " + apiProvider.getAPIToken());

        String endpoint = NettyServiceAPI.getWebServerAddress();
        mq.setUrl(apiProvider.bind(endpoint, RequestRouteTable.UPDATE_PROFILE_IMAGE));
        try {
            return ObjectJsonMapper.mapOne(RestResponse.class, mq.execute());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }//END
}

package com.copula.nettysocial.nettyservice.entry;

import com.copula.functionality.webservice.RestResponse;
import com.copula.functionality.restlite.annotation.JSONInner;

/**
 * Created by heeleaz on 8/31/17.
 */
@JSONInner("result")
public class MediaFeedFileEntry extends RestResponse {
    public String uploadMediaId;
}

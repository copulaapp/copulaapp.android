package com.copula.nettysocial.nettyservice;

import android.os.AsyncTask;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.functionality.webservice.RestResponse;

/**
 * Created by heeleaz on 7/5/17.
 */
public class CloudMsgTokenPusher extends AsyncTask<Void, Void, RestResponse> {
    private String userId, token;

    public static void push(String userId, String token) {
        CloudMsgTokenPusher pusher = new CloudMsgTokenPusher();
        pusher.token = token;
        pusher.userId = userId;

        pusher.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected RestResponse doInBackground(Void... voids) {
        NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
        return api.getAccountAPI().updatePNToken(userId, token);
    }

    @Override
    protected void onPostExecute(RestResponse restResponse) {
    }
}

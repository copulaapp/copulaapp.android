package com.copula.nettysocial.nettyservice.api;

import okhttp3.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by heeleeaz on 7/16/16.
 */
public class MultipartAPIProvider {
    private OkHttpClient client;
    private MultipartBody.Builder multipartBuilder = new MultipartBody.Builder();
    private Request.Builder requestBuilder = new Request.Builder();

    public MultipartAPIProvider() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);
        builder.writeTimeout(20, TimeUnit.SECONDS);
        this.client = builder.build();
    }

    public MultipartAPIProvider addForm(String name, String value) {
        multipartBuilder.addFormDataPart(name, value);
        return this;
    }

    public MultipartAPIProvider addForm(String name, File f, String mimeType) {
        MediaType mediaType = MediaType.parse(mimeType);
        multipartBuilder.addFormDataPart(name, f.getName(), RequestBody.create(mediaType, f));
        return this;
    }

    public MultipartAPIProvider addHeader(String key, String value) {
        requestBuilder.addHeader(key, value);
        return this;
    }

    public MultipartAPIProvider setUrl(String url) {
        requestBuilder.url(url);
        return this;
    }

    public String execute() throws IOException, IllegalAccessException {
        multipartBuilder.setType(MultipartBody.FORM);
        requestBuilder.post(multipartBuilder.build()).build();
        Response response = client.newCall(requestBuilder.build()).execute();
        if (response.isSuccessful()) return response.body().string();
        else throw new IOException(response.body().string());
    }
}

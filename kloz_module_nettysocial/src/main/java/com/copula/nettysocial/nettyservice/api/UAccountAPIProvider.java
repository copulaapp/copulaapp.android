package com.copula.nettysocial.nettyservice.api;

import android.util.Log;
import com.copula.functionality.webservice.RestResponse;
import com.copula.nettysocial.nettyservice.entry.UserAccountEntry;
import com.copula.functionality.restlite.ObjectJsonMapper;
import com.copula.functionality.restlite.QueryParam;
import okhttp3.FormBody;

/**
 * Created by heeleeaz on 7/12/16.
 */
public class UAccountAPIProvider {
    private NettyAPIProvider mProvider;

    UAccountAPIProvider(NettyAPIProvider provider) {
        mProvider = provider;
    }

    public RestResponse<UserAccountEntry> newAccount(String email, String username
            , String password, String pnToken) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("email", email);
            params.add("username", username);
            params.add("password", password);
            if (pnToken != null) params.add("pnToken", pnToken);

            String url = mProvider.bind(RequestRouteTable.NEW_ACCOUNT);
            NettyAPIProvider.Result result
                    = mProvider.formRequest("PUT", url, params.build());

            RestResponse<UserAccountEntry> rest
                    = ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null) {
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public RestResponse<UserAccountEntry> authAccount(String user, String password) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("user", user);
            params.add("password", password);
            String url = mProvider.bind(RequestRouteTable.AUTH_ACCOUNT);

            NettyAPIProvider.Result result
                    = mProvider.formRequest("POST", url, params.build());
            Log.d("password", password);

            RestResponse<UserAccountEntry> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null)
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            return rest;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public RestResponse<UserAccountEntry> authAccountThirdParty(String accountType, String userId) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("accountType", accountType);
            params.add("tpUserId", userId);
            String url = mProvider.bind(RequestRouteTable.AUTH_THIRD_PARTY_ACCOUNT);

            NettyAPIProvider.Result result =
                    mProvider.formRequest("POST", url, params.build());

            RestResponse<UserAccountEntry> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null)
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<UserAccountEntry> newAccountThirdParty(String accountType, String tpUserId
            , String email, String username, String pnToken) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("accountType", accountType);
            params.add("tpUserId", tpUserId);
            params.add("email", email);
            params.add("username", username);
            params.add("pnToken", pnToken);
            String url = mProvider.bind(RequestRouteTable.NEW_ACCOUNT_THIRD_PARTY);
            NettyAPIProvider.Result result =
                    mProvider.formRequest("PUT", url, params.build());
            RestResponse<UserAccountEntry> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null)
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<UserAccountEntry>
    updatePassword(String userId, String cPassword, String nPassword) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("userId", userId);
            params.add("currentPassword", cPassword);
            params.add("newPassword", nPassword);

            String url = mProvider.bind(RequestRouteTable.UPDATE_PASSWORD);
            NettyAPIProvider.Result result =
                    mProvider.formRequest("POST", url, params.build());

            RestResponse<UserAccountEntry> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null)
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<UserAccountEntry> updateUsername(String userId, String username) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("userId", userId);
            params.add("username", username);
            String url = mProvider.bind(RequestRouteTable.UPDATE_USERNAME);
            NettyAPIProvider.Result result =
                    mProvider.formRequest("POST", url, params.build());

            RestResponse<UserAccountEntry> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null)
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            return rest;
        } catch (Exception e) {
            return null;
        }
    }


    public RestResponse<UserAccountEntry> updatePNToken(String userId, String token) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("userId", userId);
            params.add("pnToken", token);
            String url = mProvider.bind(RequestRouteTable.UPDATE_PNTOKEN);
            NettyAPIProvider.Result result =
                    mProvider.formRequest("POST", url, params.build());

            RestResponse<UserAccountEntry> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null)
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            return rest;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public RestResponse<UserAccountEntry> getAccountWithUser(String user) {
        try {
            QueryParam params = new QueryParam().put("user", user);
            String url = mProvider.bind(RequestRouteTable.GET_ACCOUNT_WITH_USER);
            NettyAPIProvider.Result result = mProvider.getRequest(url, params);

            RestResponse<UserAccountEntry> rest =
                    ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null) {
                rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            }
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse<UserAccountEntry> getAccount(String userId) {
        try {
            QueryParam params = new QueryParam().put("userId", userId);
            String url = mProvider.bind(RequestRouteTable.GET_ACCOUNT);

            NettyAPIProvider.Result result = mProvider.getRequest(url, params);
            RestResponse<UserAccountEntry> rest = ObjectJsonMapper.mapOne(RestResponse.class, result.result);
            if (rest != null) rest.result = ObjectJsonMapper.mapOne(UserAccountEntry.class, result.result);
            return rest;
        } catch (Exception e) {
            return null;
        }
    }

    public RestResponse forgetPassword(String userId) {
        try {
            FormBody.Builder params = new FormBody.Builder().add("userId", userId);
            String url = mProvider.bind(RequestRouteTable.FORGET_PASSWORD);
            NettyAPIProvider.Result result =
                    mProvider.formRequest("POST", url, params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }
}

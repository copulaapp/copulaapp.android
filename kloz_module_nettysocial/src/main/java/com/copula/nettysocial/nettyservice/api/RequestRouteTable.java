package com.copula.nettysocial.nettyservice.api;

/**
 * Created by eliasigbalajobi on 2/24/16.
 */
class RequestRouteTable {

    static final String GET_API_AUTH_KEY = "/ws/api/authenticate";

    //User Profile
    static final String NEW_PROFILE = "/ws/profile/newProfile";
    static final String GET_PROFILE = "/ws/profile/getProfile";
    static final String FIND_PROFILE = "/ws/profile/findProfile";
    static final String UPDATE_PROFILE = "/ws/profile/updateProfile";
    static final String UPDATE_PROFILE_IMAGE = "/ws/profile/image/update";

    //User Friends
    static final String GET_FOLLOWINGS = "/ws/friends/getFollowings";
    static final String FOLLOW = "/ws/friends/follow";
    static final String UNFOLLOW_USER = "/ws/friends/unFollow";

    //User Account
    static final String NEW_ACCOUNT = "/ws/account/newAccount";
    static final String AUTH_ACCOUNT = "/ws/account/authAccount";
    static final String AUTH_THIRD_PARTY_ACCOUNT = "/ws/account/authAccountThirdParty";
    static final String NEW_ACCOUNT_THIRD_PARTY = "/ws/account/newAccountThirdParty";
    static final String GET_ACCOUNT_WITH_USER = "/ws/account/getAccountWithUser";
    static final String GET_ACCOUNT = "/ws/account/getAccount";
    static final String UPDATE_PASSWORD = "/ws/account/updatePassword";
    static final String FORGET_PASSWORD = "/ws/account/forgetPassword";
    static final String UPDATE_USERNAME = "/ws/account/updateUsername";
    static final String UPDATE_PNTOKEN = "/ws/account/updatePNToken";

    //Media Feed
    static final String POST_FEED = "/ws/feeds/postFeed";
    static final String GET_FEED = "/ws/feeds/getFeed";
    static final String GET_FEEDS = "/ws/feeds/getFeeds";
    static final String GET_FEED_TIMELINE = "/ws/feeds/getFeedTimeline";
    static final String GET_EXPLORE_FEEDS = "/ws/feeds/getExploreFeeds";
    static final String LIKE_FEED = "/ws/feeds/likeFeed";
    static final String UNLIKE_FEED = "/ws/feeds/unlikeFeed";
    static final String REMOVE_FEED = "/ws/feeds/removeFeed";
    static final String REMOVE_FEEDS = "/ws/feeds/removeFeeds";
    static final String UPLOAD_FEED_IMAGE = "/ws/feed/media/uploadImage";
    static final String UPLOAD_FEED_VIDEO = "/ws/feed/media/uploadVideo";


    //Support
    static final String NEW_USER_FEEDBACK = "/ws/support/userfeedback/newFeedback";
}
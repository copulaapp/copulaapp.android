package com.copula.nettysocial.nettyservice.entry;

import com.copula.functionality.restlite.annotation.JSONInner;

import java.io.Serializable;

/**
 * Created by eliasigbalajobi on 3/23/16.
 */

@JSONInner("result")
public class UserAccountEntry implements Serializable {
    public String username, email, userId;
}

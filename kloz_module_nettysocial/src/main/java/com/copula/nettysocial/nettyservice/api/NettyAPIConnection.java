package com.copula.nettysocial.nettyservice.api;


/**
 * Created by eliasigbalajobi on 2/28/16.
 */
public class
NettyAPIConnection {
    private NettyAPIProvider apiProvider;

    private NettyAPIConnection() {
        apiProvider = new NettyAPIProvider();
    }

    public static NettyAPIConnection getInstance() {
        return new NettyAPIConnection();
    }

    public NettyAPIProvider connect(String server) {
        apiProvider.setEndpoint(server);
        return apiProvider;
    }

    public NettyAPIProvider connect() {
        return connect(NettyServiceAPI.getWebServerAddress());
    }
}

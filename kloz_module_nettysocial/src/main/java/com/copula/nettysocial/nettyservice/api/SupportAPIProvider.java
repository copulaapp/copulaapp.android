package com.copula.nettysocial.nettyservice.api;

import com.copula.functionality.webservice.RestResponse;
import com.copula.functionality.restlite.ObjectJsonMapper;
import okhttp3.FormBody;

/**
 * Created by heeleaz on 12/11/16.
 */
public class SupportAPIProvider {
    private NettyAPIProvider mProvider;

    SupportAPIProvider(NettyAPIProvider provider) {
        this.mProvider = provider;
    }

    public RestResponse newUserFeedback(String email, String deviceId, String feature, String message) {
        try {
            FormBody.Builder params = new FormBody.Builder();
            params.add("email", email);
            params.add("deviceId", deviceId);
            params.add("message", message);
            params.add("feature", feature);

            String url = mProvider.bind(RequestRouteTable.NEW_USER_FEEDBACK);

            NettyAPIProvider.Result result = mProvider.formRequest("PUT", url,
                    params.build());
            return ObjectJsonMapper.mapOne(RestResponse.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }
}

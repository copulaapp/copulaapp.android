package com.copula.nettysocial.nettyservice.feeds;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.copula.functionality.restlite.DaoModelMapper;
import com.copula.functionality.util.DatabaseContext;
import com.copula.nettysocial.nettyservice.entry.MediaFeedEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliasigbalajobi on 3/1/16.
 */
public class DaoExploreFeeds extends DatabaseContext {
    private static final String TAG = DaoExploreFeeds.class.getName();
    private static final String TABLE = "explore_feeds_cache";
    private SharedPreferences mPreference;

    public DaoExploreFeeds(Context context) {
        super(context, "explore_feeds_cache.db", TABLE, 2);
        mPreference = context.getSharedPreferences("explore_feeds_cache", Context.MODE_PRIVATE);
    }

    public void saveNextPageToken(String token) {
        SharedPreferences.Editor editor = mPreference.edit();
        if (token == null) editor.remove("nextPageToken").apply();
        else editor.putString("nextPageToken", token).apply();
    }

    public String getNextPageToken() {
        return mPreference.getString("nextPageToken", null);
    }

    public List<MediaFeedEntry> getFeeds() {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TABLE, null, null,
                    null, null, null, null);
            if (cu == null || cu.getCount() <= 0) return null;

            List<MediaFeedEntry> models = new ArrayList<>(cu.getCount());
            while (cu.moveToNext()) {
                MediaFeedEntry m = DaoModelMapper.mapOne(cu, MediaFeedEntry.class);
                m.isLiked = cu.getInt(cu.getColumnIndex("isLiked")) == 1;
                m.isFollowing = cu.getInt(cu.getColumnIndex("isFollowing")) == 1;
                models.add(m);
            }
            return models;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if ((db != null)) db.close();
        }
    }

    public void updateLike(MediaFeedEntry entry) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("isLiked", (entry.isLiked) ? 1 : 0);
            values.put("likeCount", entry.likeCount);
            db.update(TABLE, values, "feedId=?", new String[]{entry.feedId});
        } catch (Exception e) {
            Log.d(TAG, "updateLike: " + e.getMessage());
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean deleteFeed(MediaFeedEntry entry) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            String[] whereArgs = new String[]{entry.feedId + ""};
            return db.delete(TABLE, "feedId=?", whereArgs) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean putFeeds(List<MediaFeedEntry> entries) {
        SQLiteDatabase db = null;
        try {
            ContentValues values = new ContentValues();
            db = getWritableDatabase();
            for (MediaFeedEntry entry : entries) {
                values.put("feedId", entry.feedId);
                values.put("posterId", entry.posterId);
                values.put("mediaType", entry.mediaType);
                values.put("feedType", entry.feedType);
                values.put("createdAt", entry.createdAt);
                values.put("posterUsername", entry.posterUsername);
                values.put("posterImageUrl", entry.posterImageUrl);
                values.put("mediaTitle", entry.mediaTitle);
                values.put("thumbnailUrl", entry.thumbnailUrl);
                values.put("streamUrl", entry.streamUrl);
                values.put("dataUrl", entry.dataUrl);
                values.put("likeCount", entry.likeCount);
                values.put("mediaCaption", entry.mediaCaption);
                values.put("isFollowing", entry.isFollowing ? 1 : 0);
                values.put("isLiked", entry.isLiked ? 1 : 0);
                values.put("mediaMeta", entry.mediaMeta);


                //delete feed if it already exists
                db.delete(TABLE, "feedId=?", new String[]{entry.feedId});
                db.insert(TABLE, null, values);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public List<MediaFeedEntry> getOutdatedDataList(int cleanupThreshold) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getWritableDatabase();
            cu = db.query(TABLE, null, null, null,
                    null, null, "_id asc");
            if (cu.getCount() <= cleanupThreshold) return null;

            //start deleting data from this position
            cu.moveToPosition(cleanupThreshold - 1);
            List<MediaFeedEntry> models = new ArrayList<>();
            while (cu.moveToNext()) {
                models.add(DaoModelMapper.mapOne(cu, MediaFeedEntry.class));
            }
            return models;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    @Override
    public boolean truncateTable() {
        mPreference.edit().clear().apply();
        return super.truncateTable();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TABLE;
        sql += "(_id integer primary key autoincrement,";
        sql += "feedId varchar(100), posterId varchar(100),";
        sql += "posterImageUrl varchar(300), posterUsername varchar(100),";
        sql += "mediaType integer(1), feedType integer(1),";
        sql += "thumbnailUrl varchar(300), dataUrl varchar(300),";
        sql += "streamUrl varchar(300), createdAt integer,";
        sql += "mediaTitle varchar(100), mediaCaption varchar(300),";
        sql += "mediaMeta varchar(300), likeCount integer(10),";
        sql += "isLiked integer(1), isFollowing integer(1))";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("ALTER TABLE " + TABLE + " ADD mediaMeta varchar(300)");
        super.onUpgrade(db, oldVersion, newVersion);
    }
}


package com.copula.nettysocial.nettyservice.feeds;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.*;

import java.util.Arrays;
import java.util.Set;

/**
 * Created by heeleaz on 4/16/17.
 */
public class FacebookShareHelper implements FacebookCallback<LoginResult> {
    private Callback callback;
    private CallbackManager callbackManager;

    public void requestSharePermission(Activity activity, Callback callback) {
        this.callback = callback;

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            Set<String> perms = accessToken.getPermissions();
            if (perms != null && perms.contains("publish_actions")) {
                callback.onResult(Callback.SUCCESS, "publish_actions");
                return;
            }
        }

        LoginManager loginManager = LoginManager.getInstance();
        loginManager.logInWithPublishPermissions(activity, Arrays.asList("publish_actions"));
        callbackManager = CallbackManager.Factory.create();
        loginManager.registerCallback(callbackManager, this);
    }

    private void shareCallback(ShareContent shareContent, final Callback callback) {
        ShareApi.share(shareContent, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                if (callback != null)
                    callback.onResult(Callback.SUCCESS, result.getPostId());
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.onResult(Callback.FAILED, "Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                if (callback != null)
                    callback.onResult(Callback.FAILED, error.getMessage());
            }
        });
    }

    private ShareHashtag getCopulaHashtags() {
        return new ShareHashtag.Builder().setHashtag("#CopulaApp").build();
    }

    public void sharePhoto(final Callback callback, Bitmap image, String caption, String url) {
        sharePhoto(callback, (Object) image, caption, url);
    }

    public void shareLink(Callback callback, Uri imageUrl, Uri contentUrl, String title, String desc) {
        ShareLinkContent.Builder linkContent = new ShareLinkContent.Builder();
        linkContent.setImageUrl(imageUrl).setContentUrl(contentUrl);
        linkContent.setContentTitle(title).setContentDescription(desc);

        shareCallback(linkContent.build(), callback);
    }

    public void sharePhoto(Callback callback, Object image, String caption, String url) {
        SharePhoto.Builder photo = new SharePhoto.Builder().setCaption(caption);
        if (image instanceof Uri) photo.setImageUrl((Uri) image);
        else if (image instanceof Bitmap) photo.setBitmap((Bitmap) image);

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo.build()).setContentUrl(Uri.parse(url))
                .setShareHashtag(getCopulaHashtags()).build();
        shareCallback(content, callback);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        if (callback != null) callback.onResult(Callback.SUCCESS, null);
    }

    @Override
    public void onCancel() {
        if (callback != null) callback.onResult(Callback.FAILED, null);
    }

    @Override
    public void onError(FacebookException error) {
        if (callback != null) callback.onResult(Callback.FAILED, error.getMessage());
    }

    public interface Callback {
        int FAILED = -1, SUCCESS = 1;

        void onResult(int status, String data);
    }
}

package com.copula.nettysocial.nettyservice.api;

/**
 * Created by heeleaz on 11/25/16.
 */
public class NettyServiceAPI {
    private static final String ENDPOINT = "http://35.163.99.100:8080";

    public static String getWebServerAddress() {
        return ENDPOINT;
    }

    public static String getApiPassword() {
        return "android_api_key";
    }

    public static String getApiUsername() {
        return "android_api_key";
    }
}

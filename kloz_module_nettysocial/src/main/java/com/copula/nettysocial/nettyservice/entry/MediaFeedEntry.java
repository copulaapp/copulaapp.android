package com.copula.nettysocial.nettyservice.entry;

import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.restlite.annotation.DaoColumn;
import com.copula.functionality.restlite.annotation.JSONInner;

import java.io.Serializable;

/**
 * Created by eliasigbalajobi on 2/24/16.
 */

@JSONInner("result")
public class MediaFeedEntry extends MediaModel implements Serializable {
    public static int EXPLORE_FEED = 0x1;
    public static int USER_FEED = 0x2;
    public static int SPONSORED_POST = 0x3;

    public String createdAt, mediaMeta;
    public String posterImageUrl, posterUsername, feedId, posterId = "";
    public int feedType;

    @DaoColumn(type = Boolean.class, name = "isLiked")
    public boolean isFollowing;

    public MediaFeedEntry() {
    }

    public MediaFeedEntry(String feedId) {
        this.feedId = feedId;
    }

    public MediaFeedEntry(MediaModel media) {
        super(media);
    }

    @Override
    public boolean equals(Object o) {
        return feedId.equals(((MediaFeedEntry) o).feedId);
    }

    /**
     * get the poster profile
     *
     * @return UserProfileEntry basic poster profile
     */
    public UserProfileEntry getProfileEntry() {
        UserProfileEntry e = new UserProfileEntry();
        e.userId = posterId;
        e.username = posterUsername;
        e.imgUrl = posterImageUrl;
        return e;
    }
}

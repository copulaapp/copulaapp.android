package com.copula.nettysocial;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.account.AccountAuthPopupActivity;

/**
 * Created by heeleeaz on 7/21/16.
 */
public class NettySocialActHelper {
    public static View setupAuthLoadButton(Context context, int string) {
        View progressingView =
                View.inflate(context, R.layout.view_auth_form_button_progress, null);
        ((TextView) progressingView.findViewById(R.id.title)).setText(string);
        return progressingView;
    }

    public static boolean checkLaunchLogged(Activity activity) {
        return checkLaunchLogged(activity, 919);
    }

    public static boolean checkLaunchLogged(Activity activity, int requestCode) {
        if (!isLogged(activity)) {
            AccountAuthPopupActivity.launch(activity, requestCode);
            return false;
        }
        return true;
    }

    public static boolean isLogged(Context context) {
        return new UserAccountBase(context).isLogged();
    }
}

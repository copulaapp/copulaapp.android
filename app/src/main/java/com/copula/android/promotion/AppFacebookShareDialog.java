package com.copula.android.promotion;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.copula.android.R;
import com.copula.functionality.CopulaUrl;
import com.copula.nettysocial.nettyservice.feeds.FacebookShareHelper;
import com.copula.genericlook.CopulaProgressDialog;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 4/20/17.
 */
public class AppFacebookShareDialog extends DialogFragment implements FacebookShareHelper.Callback, View.OnClickListener {

    private View btnCancel, btnShare;
    private FacebookShareHelper facebookShareHelper;
    private PromotionMaster promotionMaster;

    public static AppFacebookShareDialog instantiate(FragmentManager fragmentManager) {
        String tag = "AppFacebookShareDialog";
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment oldfg = fragmentManager.findFragmentByTag(tag);
        if (oldfg != null) ft.remove(oldfg);

        AppFacebookShareDialog dialog = new AppFacebookShareDialog();
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        ft.add(dialog, tag).show(dialog).commit();

        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.diag_facebook_app_share, container, false);
        (btnCancel = view.findViewById(R.id.btn_cancel)).setOnClickListener(this);
        (btnShare = view.findViewById(R.id.btn_share)).setOnClickListener(this);

        ((TextView) view.findViewById(R.id.txt_caption_message)).setText(getShareMessage());
        ((ImageView) view.findViewById(R.id.img_caption_image)).setImageResource(R.drawable.ic_launcher);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.facebookShareHelper = new FacebookShareHelper();
        this.promotionMaster = new PromotionMaster(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            facebookShareHelper.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
        }
    }

    @Override
    public void onResult(int status, String data) {
        if (status == FAILED) return;
        final CopulaProgressDialog progressDialog = new CopulaProgressDialog(getActivity());
        progressDialog.setMessage(R.string.prg_app_promo_share);
        progressDialog.show();


        facebookShareHelper.shareLink(new FacebookShareHelper.Callback() {
            @Override
            public void onResult(int status, String data) {
                if (isDetached()) return;
                progressDialog.dismiss();

                if (status == SUCCESS) {
                    dispatchMessage(getString(R.string.msg_app_promo_share_success));
                    dismiss();
                } else {
                    dispatchMessage(getString(R.string.msg_app_promo_share_failed));
                }
            }
        }, null, Uri.parse(CopulaUrl.APP_URL), null, null);
    }

    private void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private String getShareMessage() {
        return "Hi friends, Checkout Copula. With Copula, you can play " +
                "and download nearby friends songs and videos directly from your device.";
    }

    @Override
    public void onClick(View v) {
        if (v == btnShare)
            facebookShareHelper.requestSharePermission(getActivity(), this);
        else if (v == btnCancel) {
            promotionMaster.resetReview(PromotionMaster.FB_SHARE, 10);
            dismiss();
        }
    }//END

    @Override
    public void onCancel(DialogInterface dialog) {
        promotionMaster.resetReview(PromotionMaster.FB_SHARE, 10);
        super.onCancel(dialog);
    }
}

package com.copula.android.promotion;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by heeleaz on 4/20/17.
 */
public class PromotionMaster {
    public static final String FB_SHARE = "fb_share_";
    public static final String STORE_RATING = "store_rating_";

    private static final int FB_PROM_THRESHOLD = 3;
    private static final int RATING_PROM_THRESHOLD = 5;

    private SharedPreferences preferences;

    PromotionMaster(Context context) {
        this.preferences = context.getSharedPreferences("promotion_master", Context.MODE_PRIVATE);
    }

    public static void initPromotionStrategy(Context context) {
        PromotionMaster master = new PromotionMaster(context);
        master.moveThresholdPointer(FB_SHARE);
        master.moveThresholdPointer(STORE_RATING);
    }

    public static AppFacebookShareDialog fbAppShareReview(Activity activity) {
        PromotionMaster pm = new PromotionMaster(activity);
        if (pm.getThresholdPointer(FB_SHARE) ==
                pm.getReviewThreshold(FB_SHARE, FB_PROM_THRESHOLD)) {
            return AppFacebookShareDialog.instantiate(activity.getFragmentManager());
        }
        return null;
    }

    public static boolean appRateReview(Activity activity) {
        PromotionMaster pm = new PromotionMaster(activity);
        if (pm.getThresholdPointer(STORE_RATING) ==
                pm.getReviewThreshold(STORE_RATING, RATING_PROM_THRESHOLD)) {
            AppStoreRatingDialog.instantiate(activity.getFragmentManager());
            return true;
        }
        return false;
    }

    private void moveThresholdPointer(String type) {
        int currentPos = getThresholdPointer(type);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(type + "threshold_pointer", currentPos + 1).apply();
    }

    private int getThresholdPointer(String type) {
        return preferences.getInt(type + "threshold_pointer", 0);
    }

    private int getReviewThreshold(String type, int def) {
        return preferences.getInt(type + "review_threshold", def);
    }

    public void resetReview(String type, int threshold) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(type + "threshold_pointer", 0);
        editor.putInt(type + "review_threshold", threshold).apply();
    }
}
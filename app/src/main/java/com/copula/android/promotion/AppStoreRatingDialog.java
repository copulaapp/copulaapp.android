package com.copula.android.promotion;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.android.R;
import com.copula.support.android.view.widget.Button;

/**
 * Created by heeleaz on 4/20/17.
 */
public class AppStoreRatingDialog extends DialogFragment implements View.OnClickListener {
    private Button btnCancel, btnRateUs;
    private PromotionMaster promotionMaster;

    public static void instantiate(FragmentManager fragmentManager) {
        AppStoreRatingDialog dialog = new AppStoreRatingDialog();

        FragmentTransaction ft = fragmentManager.beginTransaction();

        String tag = "AppStoreRatingDialog";
        Fragment oldfg = fragmentManager.findFragmentByTag(tag);
        if (oldfg != null) ft.remove(oldfg);

        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        ft.add(dialog, tag).show(dialog).commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.diag_app_store_rating, container, false);
        (btnCancel = (Button) view.findViewById(R.id.btn_cancel)).setOnClickListener(this);
        (btnRateUs = (Button) view.findViewById(R.id.btn_share)).setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.promotionMaster = new PromotionMaster(getActivity());
    }

    public static void launchPlayStore(Context context) {
        String packageName = context.getPackageName();
        Uri uri = Uri.parse("market://details?id=" + packageName);
        Intent market = new Intent(Intent.ACTION_VIEW, uri);
        market.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(market);
        } catch (ActivityNotFoundException e) {
            String url = "http://play.google.com/store/apps/details?id=" + packageName;
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnRateUs) {
            launchPlayStore(getActivity());
            dismiss();
        } else if (v == btnCancel) {
            promotionMaster.resetReview(PromotionMaster.STORE_RATING, 10);
            dismiss();
        }
    }//END
}

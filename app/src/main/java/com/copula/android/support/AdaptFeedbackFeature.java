package com.copula.android.support;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.copula.android.R;
import com.copula.genericlook.DesignHelper;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 12/11/16.
 */
public class AdaptFeedbackFeature extends BaseAdapter<AdaptFeedbackFeature.FeedbackEntry> {
    private int checkedPosition = 0;

    AdaptFeedbackFeature(Context context) {
        super(context);
    }

    protected void object(int title, String key, int icon) {
        addHandler(new FeedbackEntry(getString(title), key, icon));
    }

    protected void title(int title) {
        addHandler(new FeedbackEntry(getString(title), "_", 0));
    }

    int getCheckedPosition() {
        return checkedPosition;
    }

    void setCheckedPosition(int pos) {
        getItem(checkedPosition).checked = false;
        getItem(pos).checked = true;

        checkedPosition = pos;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).featureKey.equals("_")) return 0;
        else return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;

        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            int view;
            if (getItemViewType(position) == 0) view = R.layout.adp_feedback_title_item;
            else view = R.layout.adp_feedback_object_item;

            convertView = inflater.inflate(view, null, false);
            holder.feature = (TextView) convertView.findViewById(R.id.txt_title);
            holder.check = (ImageView) convertView.findViewById(R.id.img_check);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();


        FeedbackEntry model = getItem(position);
        holder.feature.setText(model.feature);

        if (model.featureIcon != 0) {//only object as feature icon
            Drawable dw = DesignHelper.
                    scaleDrawable(getContext(), model.featureIcon, 18, 18);
            holder.feature.setCompoundDrawablesWithIntrinsicBounds(dw,
                    null, null, null);

            if (checkedPosition == position) {
                holder.check.setImageResource(R.drawable.ic_appcolor_mark_64px);
            } else holder.check.setImageResource(0);
        } else {
            convertView.setOnClickListener(null);//disable click action on non object
        }
        return convertView;
    }

    static class FeedbackEntry {
        String feature, featureKey;
        int featureIcon;
        boolean checked;

        FeedbackEntry(String feature, String key, int icon) {
            this.feature = feature;
            this.featureKey = key;
            this.featureIcon = icon;
        }
    }

    private class ViewHolder {
        TextView feature;
        ImageView check;
    }
}//END

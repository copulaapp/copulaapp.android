package com.copula.android.support;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Toast;
import com.copula.android.R;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.api.NettyAPIConnection;
import com.copula.nettysocial.nettyservice.api.NettyAPIProvider;
import com.copula.functionality.webservice.RestResponse;
import com.copula.genericlook.CopulaAlertDialog;
import com.copula.genericlook.CopulaProgressDialog;
import com.copula.genericlook.CustomActionBar;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.android.view.widget.ListView;

/**
 * Created by heeleaz on 12/11/16.
 */
public class FeedbackSystemActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        View.OnClickListener {
    private AdaptFeedbackFeature adapter;
    private View vFeedbackMessage;
    private EditText edtFeedbackMessage;
    private ListView listView;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, FeedbackSystemActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_feedback_system);
        CustomActionBar bar = new CustomActionBar(this);
        bar.setTitle(R.string.label_feedback).setBackListener(this).compile();

        listView = (ListView) findViewById(R.id.lst);
        vFeedbackMessage = findViewById(R.id.view_feedback_message);
        edtFeedbackMessage = (EditText) findViewById(R.id.edt_feedback_message);

        listView.setAdapter(adapter = new AdaptFeedbackFeature(this));

        adapter.title(R.string.tit_feedback_feature);
        adapter.object(R.string.feedback_explore,
                "explore", R.drawable.ic_feedback_feature_feeds);
        adapter.object(R.string.feedback_mediaplayer,
                "media_player", R.drawable.ic_feedback_feature_mplayer);
        adapter.object(R.string.feedback_connection,
                "connect", R.drawable.ic_feedback_feature_connect);
        adapter.object(R.string.feedback_sharing,
                "share_and_downloads", R.drawable.ic_feedback_feature_mshare);
        adapter.object(R.string.feedback_notification,
                "notifications", R.drawable.ic_feedback_feature_notification);
        adapter.object(R.string.feedback_auth,
                "auth", R.drawable.ic_feedback_feature_profile);
        adapter.object(R.string.feedback_others,
                "others", R.drawable.ic_feedback_feature_others);

        listView.setOnItemClickListener(this);

        findViewById(R.id.btn_push_feedback).setOnClickListener(this);
    }

    private void validateAndPushFeedback() {
        if (adapter.getCheckedPosition() == 0) {
            dispatchMessage(getString(R.string.msg_empty_feedback_feature));
        } else if (edtFeedbackMessage.getTextContent().isEmpty()) {
            dispatchMessage(getString(R.string.msg_empty_feedback_message));
        } else {
            String message = edtFeedbackMessage.getTextContent();
            String username = new UserAccountBase(this).getUsername();
            String deviceId = "";
            String feature = adapter.getItem(adapter.getCheckedPosition()).feature;
            new SendFeedback(username, deviceId, feature, message).execute();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        adapter.setCheckedPosition(i);
        adapter.notifyDataSetChanged();

        vFeedbackMessage.setVisibility(View.VISIBLE);
        edtFeedbackMessage.requestFocus();

        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtFeedbackMessage, InputMethodManager.SHOW_IMPLICIT);

        listView.smoothScrollToPosition(i);
    }

    private void dispatchMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showFeedbackPushedDialog() {
        CopulaAlertDialog dialog = new CopulaAlertDialog(this);
        dialog.setMessage(R.string.msg_feedback_push_success);

        int sm = R.string.act_close_feedback_success_dialog;
        dialog.setNeutralButton(sm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        }).show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_bar_back) finish();
        else if (v.getId() == R.id.btn_push_feedback)
            validateAndPushFeedback();
    }

    private class SendFeedback extends AsyncTask<Void, Void, RestResponse> {
        private String message, deviceId, email, feature;
        private CopulaProgressDialog progressDialog;

        SendFeedback(String email, String deviceId, String feature, String message) {
            this.message = message;
            this.deviceId = deviceId;
            this.email = email;
            this.feature = feature;

            progressDialog = new CopulaProgressDialog(FeedbackSystemActivity.this);
            progressDialog.setMessage(getString(R.string.prg_feedback_push));
            progressDialog.show();
        }

        @Override
        protected RestResponse doInBackground(Void... voids) {
            NettyAPIProvider api = NettyAPIConnection.getInstance().connect();
            return api.getSupportAPI().newUserFeedback(email, deviceId, feature, message);
        }

        @Override
        protected void onPostExecute(RestResponse response) {
            super.onPostExecute(response);
            progressDialog.dismiss();

            if (response == null || response.statusCode != 1) {
                dispatchMessage(getString(R.string.msg_failed_feedback_push));
            } else {
                showFeedbackPushedDialog();
            }
        }
    }//END
}


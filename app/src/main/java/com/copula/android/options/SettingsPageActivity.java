package com.copula.android.options;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.copula.android.R;
import com.copula.genericlook.CustomActionBar;

/**
 * Created by eliasigbalajobi on 5/23/16.
 */
public class SettingsPageActivity extends AppCompatActivity {
    private SettingsPrefsFragment preferenceFragment;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, SettingsPageActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_settings_page);

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setTitle(R.string.label_settings);
        actionBar.setBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }).compile();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        preferenceFragment = new SettingsPrefsFragment();
        transaction.replace(R.id.fragment, preferenceFragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (preferenceFragment != null)
            preferenceFragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

package com.copula.android.options;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import com.copula.android.LandingPageActivity;
import com.copula.android.R;
import com.copula.android.app.AppBootstrapPrefs;
import com.copula.android.promotion.AppFacebookShareDialog;
import com.copula.android.promotion.AppStoreRatingDialog;
import com.copula.android.support.FeedbackSystemActivity;
import com.copula.hostworld.connection.appshare.CopulaShareOptionActivity;
import com.copula.functionality.CopulaUrl;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.NettySocialActHelper;
import com.copula.nettysocial.account.AccountPwdUpdateActivity;

/**
 * Created by heeleaz on 1/18/17.
 */
public class SettingsPrefsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {
    private AppFacebookShareDialog shareDialog;

    public static void launchWebPage(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    private static void launchInviteFriend(Context context) {
        String message = "Hi friend, Checkout Copula. With Copula, you can play" +
                " and download nearby friends songs and videos directly from your" +
                " device without internet.";
        message += " Download at https://play.google.com/store/apps/details?id=com.copula.android";


        Intent i = new Intent(Intent.ACTION_SEND);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra(Intent.EXTRA_TEXT, message).setType("text/plain");
        context.startActivity(i);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (shareDialog != null)
            shareDialog.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_preference);
        PreferenceCategory p =
                ((PreferenceCategory) getPreferenceScreen().getPreference(0));

        //remove update password prefs if user is not registered
        if (!NettySocialActHelper.isLogged(getActivity())) {
            p.removePreference(p.findPreference("pref_update_password"));
            p.removePreference(p.findPreference("pref_logout"));
        }

        //loop through preferences. since preference will not allow direct onclick
        for (int y = 0; y < p.getPreferenceCount(); y++) {
            Preference pref = p.getPreference(y);
            pref.setOnPreferenceClickListener(this);
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();
        if (key == null) return true;


        switch (key) {
            case "pref_update_password":
                AccountPwdUpdateActivity.launch(getActivity());
                break;

            case "pref_logout":
                UserAccountBase.logoutUser(getActivity());
                AppBootstrapPrefs.clear(getActivity());
                LandingPageActivity.launch(getActivity(),
                        Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;

            case "pref_invite_friends":
                launchInviteFriend(getActivity());
                break;

            case "pref_facebook_app_invite":
                shareDialog = AppFacebookShareDialog.instantiate(getFragmentManager());
                break;

            case "pref_feedback":
                FeedbackSystemActivity.launch(getActivity());
                break;

            case "pref_rate_app":
                AppStoreRatingDialog.instantiate(getFragmentManager());
                break;

            case "pref_contact_us":
                launchWebPage(getActivity(), CopulaUrl.HELP_URL);
                break;

            case "pref_terms":
                launchWebPage(getActivity(), CopulaUrl.TERMS_URL);
                break;

            case "pref_about":
                launchWebPage(getActivity(), CopulaUrl.ABOUT_URL);
                break;

            case "pref_share_copula_apk":
                CopulaShareOptionActivity.launch(getActivity());
                break;

            case "pref_blog":
                launchWebPage(getActivity(), CopulaUrl.BLOG_URL);
                break;
        }
        return true;
    }
}//end

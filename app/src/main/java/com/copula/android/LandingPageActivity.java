package com.copula.android;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import com.copula.android.app.AppBootstrapPrefs;
import com.copula.android.app.MessagingService;
import com.copula.android.intropage.splash.SplashScreenActivity;
import com.copula.android.mainpage.MainAppPageActivity;
import com.copula.android.options.SettingsPrefsFragment;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.profile.UserSocialProfileActivity;

/**
 * Created by eliasigbalajobi on 4/24/16.
 */
public class LandingPageActivity extends AbsBootstrapActivity {

    public static void launch(Context ctx, int flags) {
        Intent intent = new Intent(ctx, LandingPageActivity.class);
        intent.addFlags(flags);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_landing_page);
    }

    @Override
    protected void onPermitted() {
        routeIntentActionToPage(getIntent());
    }

    private void routeIntentActionToPage(Intent i) {
        String action = (i.getAction() != null) ? i.getAction() : "";
        String host = (i.getData() != null) ? i.getData().getHost() : "";
        Bundle extras = i.getExtras();

        if (action.equals(MessagingService.ACTION_NEW_EXPLORE)
                || host.equals("www.copulaapp.com")) {
            MainAppPageActivity.launch(this, 0, 0);

        } else if ((action.equals(MessagingService.ACTION_NEW_FEED)
                || action.equals(MessagingService.ACTION_FEED_LIKE))
                && extras != null) {
            UserProfileEntry user = new UserProfileEntry();
            user.userId = extras.getString("posterId");
            user.username = extras.getString("posterUsername");
            UserSocialProfileActivity.launch(this, user);

        } else if (action.equals(MessagingService.ACTION_NEW_FOLLOW)
                && extras != null) {
            UserProfileEntry user = new UserProfileEntry();
            user.userId = extras.getString("followUserId");
            user.username = extras.getString("followUsername");
            UserSocialProfileActivity.launch(this, user);

        } else if (action.equals(MessagingService.ACTION_UPDATES)
                && extras != null) {
            String uri = extras.getString("link");
            if (uri != null)
                SettingsPrefsFragment.launchWebPage(this, uri);
        } else MainAppPageActivity.launch(this, 0);

        finish();
    }

    @Override
    protected void onSystemServerInitCompleted() {
        AppBootstrapPrefs prefs = new AppBootstrapPrefs(this);
        if (!prefs.get(AppBootstrapPrefs.SPLASH_PASSED)) {
            SplashScreenActivity.launch(this, 0);
            finish();
        } else verifyV23AppPermission();//check if app permits app utility
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

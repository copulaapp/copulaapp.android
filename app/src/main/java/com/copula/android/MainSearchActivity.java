package com.copula.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import com.copula.genericlook.CustomActionBar;
import com.copula.mediasocial.audio.player.AudioDPlayerFragment;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayerMainActivity;
import com.copula.mediasocial.mediasearch.MediaSearchFragment;
import com.copula.nettysocial.channel.ChannelSearchFragment;
import com.copula.support.android.content.TabActivity;
import com.copula.support.android.view.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 5/31/17.
 */
public class MainSearchActivity extends TabActivity implements View.OnClickListener, TextView.OnEditorActionListener,
        EditText.TextWatcher {

    private EditText edtSearchQuery;
    private MediaSearchFragment mediaSearchFragment;
    private ChannelSearchFragment channelSearchFragment;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, MainSearchActivity.class));
    }

    @Override
    protected List<Fragment> getPagerFragments() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(mediaSearchFragment = MediaSearchFragment.instantiate());
        fragments.add(channelSearchFragment = ChannelSearchFragment.instantiate());

        return fragments;
    }

    @Override
    protected ViewPager getViewPager() {
        return (ViewPager) findViewById(R.id.pager);
    }

    @Override
    protected void onPrepareActivityWindow() {
        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setView(R.layout.bar_media_search).setBackListener(this);
        actionBar.setBackground(R.drawable.bkg_appwhite_bar_noshadow).compile();

        setTabViewStyle(R.style.ActivityTheme_MainPage_TabView);
        addTab(R.string.tab_my_media).addTab(R.string.tab_channel);
        getActionBarTab().setTextUnselectedColor(0xFF808080);
        getActionBarTab().setTextSelectedColor(Color.BLACK);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_search);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frg_audio_player, AudioDPlayerFragment.instantiate());
        ft.commit();

        findViewById(R.id.frg_audio_player).setOnClickListener(this);

        edtSearchQuery = (EditText) findViewById(R.id.edt_search_query);
        edtSearchQuery.setTextChangedListener(this);
        edtSearchQuery.setOnEditorActionListener(this);

        setCurrentPage(0);// start with media search fragment

        edtSearchQuery.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtSearchQuery, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.frg_audio_player) {
            AudioPlayerMainActivity.launch(this);
        } else if (v.getId() == R.id.btn_bar_back) finish();
    }

    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        if (getCurrentItem() == 0 && mediaSearchFragment != null)
            mediaSearchFragment.__searchInHint(s.toString());
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String q = edtSearchQuery.getTextContent();
            if (q == null || q.isEmpty()) return false;

            if (getCurrentItem() == 0 && mediaSearchFragment != null)
                mediaSearchFragment.search(q);

            if (channelSearchFragment != null) channelSearchFragment.search(q);
        }
        return false;
    }//END
}

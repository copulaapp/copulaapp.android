package com.copula.android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import com.copula.functionality.localservice.profile.PermissionEntry;
import com.copula.support.android.view.widget.ResultBundle;
import com.copula.hostworld.onlinehost.ConnectUserOptionDialog;

/**
 * Created by heeleaz on 8/28/16.
 */
public class DialogHolderActivity extends FragmentActivity implements ResultBundle {
    public static final String EXTRA_ACTION = "action";
    public static final String EXTRA_HOST_PERMISSION_MODEL = "host_permission_model";
    public static final int PERMISSION_UPDATER = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int action = getIntent().getIntExtra(EXTRA_ACTION, 0);
        if (action == PERMISSION_UPDATER) {
            PermissionEntry p = (PermissionEntry)
                    getIntent().getSerializableExtra(EXTRA_HOST_PERMISSION_MODEL);
            if (p != null) {
                ConnectUserOptionDialog.
                        launch(getSupportFragmentManager(), p.userId, p.rpAddress);
            }
        }
    }

    @Override
    public void message(String action, Bundle bundle) {
        if (action.equals("finish")) finish();
    }
}

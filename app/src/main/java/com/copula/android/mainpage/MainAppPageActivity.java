package com.copula.android.mainpage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import com.copula.android.MainSearchActivity;
import com.copula.android.R;
import com.copula.android.promotion.AppFacebookShareDialog;
import com.copula.android.promotion.PromotionMaster;
import com.copula.hostworld.onlinehost.ConnectionOpenFragment;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.audio.album.AudioAlbumFragment;
import com.copula.mediasocial.audio.player.AudioDPlayerFragment;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayerMainActivity;
import com.copula.mediasocial.audio.songlist.AudioSongListFragment;
import com.copula.mediasocial.video.VideoTracksFragment;
import com.copula.nettysocial.feed.ExploreFeedsFragment;
import com.copula.support.android.content.ActionBarTab;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.content.IScrollableHandler;
import com.copula.support.android.content.TabActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

public class MainAppPageActivity extends TabActivity implements OnClickListener, IScrollableHandler {
    public static final int ALBUM_PAGE = 1;
    public static final String INTENT_PAGE = "page";

    private AppFacebookShareDialog facebookShareDialog;
    private ConnectionOpenFragment connectionFragment;
    private ExploreFeedsFragment exploreFeedsFragment;
    private LeftDrawerFragment leftDrawerFragment;
    private DrawerLayout drawerLayout;

    public static void launch(Context ctx, int flags) {
        launch(ctx, 1, flags);
    }

    public static void launch(Context ctx, int page, int flags) {
        Intent intent = new Intent(ctx, MainAppPageActivity.class);
        intent.addFlags(flags).putExtra("page", page);
        ctx.startActivity(intent);
    }

    @Override
    protected void onPrepareActivityWindow() {
        setTabViewStyle(R.style.ActivityTheme_MainPage_TabView);
        addTab(R.string.tab_explore).addTab(R.string.tab_album)
                .addTab(R.string.tab_songs).addTab(R.string.tab_videos);
        getActionBarTab().setTextUnselectedColor(0xFF808080);
        getActionBarTab().setTextSelectedColor(Color.BLACK);
    }

    @Override
    protected ViewPager getViewPager() {
        return (ViewPager) findViewById(R.id.pager);
    }

    @Override
    protected List<Fragment> getPagerFragments() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(exploreFeedsFragment = new ExploreFeedsFragment());
        fragments.add(AudioAlbumFragment.instantiate());
        fragments.add(AudioSongListFragment.instantiate(null, false));
        fragments.add(VideoTracksFragment.instantiate());
        return fragments;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_main_app_page);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        findViewById(R.id.btn_bar_search).setOnClickListener(this);
        findViewById(R.id.btn_drawer_control).setOnClickListener(this);
        findViewById(R.id.frg_audio_player).setOnClickListener(this);

        setCurrentPage(getIntent().getIntExtra("page", ALBUM_PAGE));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frg_audio_player, AudioDPlayerFragment.instantiate());
        ft.replace(R.id.frg_open_connection,
                connectionFragment = ConnectionOpenFragment.instantiate());
        ft.replace(R.id.left_drawer,
                leftDrawerFragment = LeftDrawerFragment.instantiate()).commit();

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                Activity ctx = MainAppPageActivity.this;
                PromotionMaster.appRateReview(ctx);
                facebookShareDialog = PromotionMaster.fbAppShareReview(ctx);
            }
        });

        this.restoreCrashDamages();
    }

    private void restoreCrashDamages() {
        //bootstrap, just in case media cache is empty
        SocialMediaBootstrap bootstrap = new SocialMediaBootstrap(this);
        bootstrap.setCacheKey("local");
        bootstrap.boot(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        int pos = getCurrentItem();
        if (connectionFragment != null)
            connectionFragment.showFragment(!(pos == 0 || pos == 5));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.frg_audio_player) {
            AudioPlayerMainActivity.launch(v.getContext());
        } else if (v.getId() == R.id.btn_bar_search) {
            MainSearchActivity.launch(this);
        } else if (v.getId() == R.id.btn_drawer_control) doDrawerClose();
    }

    private void doDrawerClose() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT))
            drawerLayout.closeDrawers();
        else drawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (facebookShareDialog != null) {
            facebookShareDialog.onActivityResult(requestCode, resultCode, data);
        }
        if (connectionFragment != null) {
            connectionFragment.onActivityResult(requestCode, resultCode, data);
        }
        if (leftDrawerFragment != null) {
            leftDrawerFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
            return;//drawer closed..for this back operation
        }

        IFragmentBackPress fragment =
                (IFragmentBackPress) getPagerFragmentList().get(getCurrentItem());
        if (!fragment.allowBackPressed()) {
        } else if (getCurrentItem() != ALBUM_PAGE) setCurrentPage(ALBUM_PAGE);
        else super.onBackPressed();

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onPageSelected(int pos) {
        super.onPageSelected(pos);
        showConnectionFragment(!(pos == 0 || pos == 5));
    }

    @Override
    public void onSelected(int position, ActionBarTab.Tab tab) {
        super.onSelected(position, tab);
    }

    @Override
    public void onScroll(AbsListView view, int axis) {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
            showConnectionFragment(true);
        } else showConnectionFragment(false);
    }

    private void showConnectionFragment(boolean show) {
        if (connectionFragment != null) connectionFragment.showFragment(show);
    }
}

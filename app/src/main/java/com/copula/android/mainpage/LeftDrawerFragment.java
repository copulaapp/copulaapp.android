package com.copula.android.mainpage;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.copula.android.R;
import com.copula.android.options.SettingsPageActivity;
import com.copula.android.support.FeedbackSystemActivity;
import com.copula.genericlook.DesignHelper;
import com.copula.mediagallary.studio.SharedCollectionActivity;
import com.copula.mediasocial.download.MediaDownloadActivity;
import com.copula.nettysocial.channel.timeline.FeedTimelineActivity;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;
import com.copula.userprofiler.user.ProfileHeadFragment;

/**
 * Created by heeleaz on 7/6/17.
 */
public class LeftDrawerFragment extends Fragment implements AdapterView.OnItemClickListener {
    private ProfileHeadFragment profileHeader;
    private ListView listView;

    public static LeftDrawerFragment instantiate() {
        return new LeftDrawerFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_main_page_drawer, container, false);
        listView = (ListView) v.findViewById(R.id.drawer_list);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.replace(R.id.fragment_user_profile,
                profileHeader = ProfileHeadFragment.instantiate());
        ft.commit();

        Adapter adapter = new Adapter(getActivity());

        adapter.entry(R.string.drawer_download, R.drawable.ic_download_grey2_96dp);
        adapter.entry(R.string.drawer_media_studio, R.drawable.img_collection_grey2_64dp);
        adapter.entry(R.string.drawer_settings, R.drawable.ic_settings);
        adapter.entry(R.string.drawer_quick_feedback, 0);

        adapter.entry(0, 0);
        adapter.space(R.string.drawer_explore_space);
        adapter.entry(R.string.drawer_subscription, 0);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (profileHeader != null) {
            profileHeader.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Adapter.Entry entry = (Adapter.Entry) listView.getItemAtPosition(position);

        if (entry.name == R.string.drawer_media_studio) {
            SharedCollectionActivity.launch(getActivity(), 1);
        } else if (entry.name == R.string.drawer_settings) {
            SettingsPageActivity.launch(getActivity());
        } else if (entry.name == R.string.drawer_download) {
            MediaDownloadActivity.launch(getActivity());
        } else if (entry.name == R.string.drawer_subscription) {
            FeedTimelineActivity.launch(getActivity());
        } else if (entry.name == R.string.drawer_quick_feedback) {
            FeedbackSystemActivity.launch(getActivity());
        }
    }

    private class Adapter extends BaseAdapter<Adapter.Entry> {
        Adapter(Context context) {
            super(context);
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            Entry entry = getItem(position);
            if (entry.type == 0) return 0;
            else return 1;
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            TextView textView;
            if (convertView == null || convertView.getTag() == null) {
                int view;
                if (getItemViewType(position) == 0) view = R.layout.adp_main_page_drawer;
                else view = R.layout.adp_main_page_drawer_space;

                convertView = inflater.inflate(view, null, false);
                textView = (TextView) convertView.findViewById(R.id.textview);
                convertView.setTag(textView);
            } else textView = (TextView) convertView.getTag();

            Entry entry = getItem(position);
            if (entry.name != 0) textView.setText(entry.name);
            if (entry.drawable != 0) {
                Drawable d = DesignHelper.scaleDrawable(getContext(), entry.drawable, 20, 20);
                textView.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);
            }

            return convertView;
        }

        void entry(int text, int drawable) {
            addHandler(new Entry(drawable, text, 0));
        }

        void space(int optText) {
            addHandler(new Entry(0, optText, 1));
        }

        class Entry {
            int drawable, name, type;

            Entry(int drawable, int name, int type) {
                this.drawable = drawable;
                this.name = name;
                this.type = type;
            }
        }
    }//END
}

package com.copula.android;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import com.copula.android.app.NotificationService;
import com.copula.functionality.IContextBootstrap;
import com.copula.functionality.app.V23PermissionHelper;
import com.copula.functionality.downloader.MediaDownloadService;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.hostworld.connection.permission.RationalePermsRequestDialog;
import com.copula.mediagallary.observer.MediaObserverService;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.audio.player.serviceplayer.AudioPlayerService;
import com.copula.nettysocial.NettySocialBootstrap;

/**
 * Created by heeleaz on 12/13/16.
 */
public abstract class AbsBootstrapActivity extends FragmentActivity {
    private static int CORE_APP_PERM_REQ = 100;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MediaDownloadService.startService(this);
        NotificationService.startService(this);
        AudioPlayerService.startService(this);
        MediaObserverService.startService(this);

        UserAccountBase acc = new UserAccountBase(this);
        if (!acc.isLogged() && !acc.hasInitNonLogged()) {
            acc.setupNonLogged(getString(R.string.def_profile_bio));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        new NettySocialBootstrap(this).boot(null);
        this.onSystemServerInitCompleted();
    }


    protected abstract void onSystemServerInitCompleted();

    protected abstract void onPermitted();

    public void verifyV23AppPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            V23PermissionHelper helper = new V23PermissionHelper(this);
            int perm = helper.coreAppPermission(CORE_APP_PERM_REQ);
            if (perm == V23PermissionHelper.PERMITTED) {
                this.bootModuleNeedingPermissionResource();
            } else if (perm == V23PermissionHelper.RATIONALE_PERMISSION) {
                for (String permission : helper.getCoreRationalePermission())
                    RationalePermsRequestDialog
                            .launch(getSupportFragmentManager(), permission);
            }
        } else this.bootModuleNeedingPermissionResource();
    }//end

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions
            , @NonNull int[] grantResults) {
        if (requestCode == AbsBootstrapActivity.CORE_APP_PERM_REQ) {
            try {
                verifyV23AppPermission();
            } catch (Exception e) {
            }
        }
    }//END

    private void bootModuleNeedingPermissionResource() {
        SocialMediaBootstrap b = new SocialMediaBootstrap(this);
        b.setCacheKey("local");//local media cache
        b.boot(new IContextBootstrap.BootstrapListener() {
            @Override
            public void onBootstrapStarted(Class<?> contextName) {

            }

            @Override
            public void onBootstrapCompleted(Class<?> contextName) {
                onPermitted();
            }
        });
    }//END
}

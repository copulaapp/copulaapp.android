package com.copula.android.app;

import android.app.Application;
import com.copula.android.promotion.PromotionMaster;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

/**
 * Created by eliasigbalajobi on 6/4/16.
 */
public class CopulaApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PromotionMaster.initPromotionStrategy(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}

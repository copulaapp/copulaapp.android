package com.copula.android.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import com.copula.android.DialogHolderActivity;
import com.copula.android.R;
import com.copula.android.mainpage.MainAppPageActivity;
import com.copula.hostworld.connection.ConnectivityStateWidget;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.app.NotificationHelper.NotificationModel;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.downloader.MediaDownloadEntry;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.localservice.profile.DaoConnectsProfile;
import com.copula.functionality.localservice.profile.PermissionEntry;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediagallary.studio.SharedCollectionActivity;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.audio.player.serviceplayer.AudioPlayerService;
import com.copula.mediasocial.download.MediaDownloadActivity;
import com.copula.userprofiler.notification.MediaNotificationActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by eliasigbalajobi on 5/25/16.
 */
public class NotificationService extends AudioPlayerService implements NotificationHelper.NotificationPushListener {
    private static final String TAG = "NotificationService";
    private NotificationManager notificationManager;
    private int downloadnc = 0;

    public static void startService(Context context) {
        context.startService(new Intent(context, NotificationService.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationHelper.getInstance().addListener(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }


    @Override
    public void onPushNotification(int broadcast, NotificationModel model) {
        switch (broadcast) {
            case NotificationHelper.USER_DOWNLOAD_NOTIFICATION:
                downloadNotification((MediaDownloadEntry) model.data);
                break;
            case NotificationHelper.MEDIA_ACTION_NOTIFICATION:
                mediaActionNotification((MediaCaptionEntry) model.data);
                break;
            case NotificationHelper.HOST_CONNECT_NOTIFICATION:
                //hostConnectNotification((UserProfileEntry) model.data);
                break;
            case NotificationHelper.PERMISSION_UPDATE_NOTIFICATION:
                doHostPermissionUpdateRequest((PermissionEntry) model.data);
                break;
            case NotificationHelper.APP_WIFI_NOTIFICATION:
                runConnectionStatusNWidget();
                break;
            case SocialMediaBootstrap.EMPTY_COLLECTION:
                emptySharedCollectionNotification();
        }//end switch
    }

    private void runConnectionStatusNWidget() {
        if (ConnectivityHelper.isAppALIVE__(this)) {
            Intent intent = new Intent(this, MainAppPageActivity.class);
            ConnectivityStateWidget.showNotification(this, intent);
        } else {
            ConnectivityStateWidget.cancelNotification(this);
        }
    }

    private NotificationCompat.Builder
    prepare(String title, String message, Bitmap li, PendingIntent intent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(true).setGroup("action_notification");
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setContentTitle(title).setContentText(message);

        if (intent != null) builder.setContentIntent(intent);

        if (li != null) builder.setLargeIcon(li);
        builder.setSmallIcon(R.drawable.ic_notification_appicon);
        builder.setColor(ContextCompat.getColor(this, R.color.colorAccent));
        return builder;
    }

    private void doHostPermissionUpdateRequest(PermissionEntry model) {
        UserProfileEntry hp = new DaoConnectsProfile(this).getConnect(model.userId);
        if (hp == null) return;//account profile not exists

        String title = "Permission Request";
        String message = hp.username + " will like to ";
        if (model.inContextPermission == PermissionEntry.READ_PERM) {
            message += "view";
        } else if (model.inContextPermission == PermissionEntry.COPY_PERM) {
            message += "download";
        } else if (model.inContextPermission == PermissionEntry.SHARE_PERM) {
            message += "share";
        }

        Intent intent = new Intent(this, DialogHolderActivity.class);
        intent.putExtra(DialogHolderActivity.EXTRA_ACTION, DialogHolderActivity.PERMISSION_UPDATER);
        intent.putExtra(DialogHolderActivity.EXTRA_HOST_PERMISSION_MODEL, model);
        PendingIntent pIntent = PendingIntent
                .getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = this.prepare(title, message + " your media", null, pIntent);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        notificationManager.notify(0xA, builder.build());
    }

    private void downloadNotification(MediaDownloadEntry model) {
        if (model.downloadState != MediaDownloadEntry.STATE_COMPLETED) return;

        String title = "Download Completed";
        String message = model.fileName + " Download Completed";

        Intent intent = new Intent(this, MediaDownloadActivity.class);
        intent.putExtra(MediaDownloadActivity
                .INTENT_PAGE, MediaDownloadActivity.USER_DOWNLOAD);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = prepare(title, message, null, pendingIntent);
        builder.setSmallIcon(R.drawable.ic_notification_download);
        builder.setNumber(++downloadnc);

        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setDefaults(Notification.DEFAULT_SOUND);
        notificationManager.notify(0xC, builder.build());
    }

    private void emptySharedCollectionNotification() {
        String title = getString(R.string.tit_empty_shared_collection);
        String message = getString(R.string.msg_empty_shared_collection);

        Intent intent = new Intent(this, SharedCollectionActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = prepare(title, message, null, pendingIntent);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setDefaults(0);
        notificationManager.notify(0xC, builder.build());
    }

    private void mediaActionNotification(MediaCaptionEntry model) {
        String appUserId = new UserAccountBase(this).getUserId();
        if (appUserId.equals(model.actorId)) return;

        String title = getString(R.string.app_name), message, actionString = "", bigThumbUrl = null;
        int thumbPlaceholder;

        if (model.action == MediaCaptionEntry.ACTION_LIKE)
            actionString = " liked ";
        else if (model.action == MediaCaptionEntry.ACTION_SHARE)
            actionString = " shared ";
        else if (model.action == MediaCaptionEntry.ACTION_DOWNLOAD)
            actionString = " downloaded ";

        DaoConnectsProfile dfp = new DaoConnectsProfile(this);
        UserProfileEntry actorProfile = dfp.getConnect(model.actorId);
        message = (actorProfile != null) ? actorProfile.username : "Entry friend";

        if (model.mediaType == MediaCaptionEntry.IMAGE) {
            message += actionString + "your picture";
            ImageModel media = SystemMediaProvider.getImage(this, model.mediaId);
            if (media != null) {
                bigThumbUrl = media.thumbnailUrl;
                message += " - " + media.fileName;
            }
            thumbPlaceholder = R.drawable.img_media_image_thumb_128dp;
        } else if (model.mediaType == MediaCaptionEntry.AUDIO) {
            message += " " + actionString + "your music ";
            AudioModel media = SystemMediaProvider.getAudio(this, model.mediaId);
            if (media != null) {
                bigThumbUrl = media.thumbnailUrl;
                message += " - " + media.mediaTitle;
            }
            thumbPlaceholder = R.drawable.img_media_song_thumb_128dp;
        } else {
            message += " " + actionString + "your video ";
            VideoModel media = SystemMediaProvider.getVideo(this, model.mediaId);
            if (media != null) {
                bigThumbUrl = media.thumbnailUrl;
                message += " - " + media.mediaTitle;
            }
            thumbPlaceholder = R.drawable.img_media_video_thumb2_128dp;
        }

        Bitmap icon;
        try {
            String largeIconUrl = MediaUtility.appendSize(bigThumbUrl, 100);
            icon = Picasso.with(this).load(largeIconUrl).get();
        } catch (Exception e) {
            icon = BitmapFactory.decodeResource(getResources(), thumbPlaceholder);
        }

        Intent goIntent;
        if (model.action == MediaCaptionEntry.ACTION_DOWNLOAD) {
            goIntent = new Intent(this, MediaDownloadActivity.class);
            goIntent.putExtra(MediaDownloadActivity.INTENT_PAGE,
                    MediaDownloadActivity.HOST_DOWNLOAD);
        } else goIntent = new Intent(this, MediaNotificationActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, goIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder builder = prepare(title, message, icon, pendingIntent);
        builder.setDefaults(0);

        notificationManager.notify(0xC, builder.build());
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        NotificationManager manager = ((NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE));
        manager.cancel(0xA);
        manager.cancel(0xC);
    }
}
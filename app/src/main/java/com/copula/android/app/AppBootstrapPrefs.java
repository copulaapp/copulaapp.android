package com.copula.android.app;

import android.content.Context;
import android.content.SharedPreferences;

public class AppBootstrapPrefs {
    public static final String SPLASH_PASSED = "splash_passed";
    public static final String TUTOR_PASSED = "tutor_passed";
    private SharedPreferences mPreference;

    public AppBootstrapPrefs(Context context) {
        mPreference = context.getSharedPreferences("app_bootstrap_prefs", Context.MODE_PRIVATE);
    }

    public static void clear(Context context) {
        AppBootstrapPrefs prefs = new AppBootstrapPrefs(context);
        prefs.mPreference.edit().clear().apply();
    }

    public void set(String key, boolean passed) {
        mPreference.edit().putBoolean(key, passed).apply();
    }

    public boolean get(String key) {
        return mPreference.getBoolean(key, false);
    }
}
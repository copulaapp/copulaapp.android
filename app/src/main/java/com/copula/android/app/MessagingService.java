package com.copula.android.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import com.copula.android.R;
import com.copula.android.mainpage.MainAppPageActivity;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.nettysocial.profile.UserSocialProfileActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Map;

public class MessagingService extends FirebaseMessagingService {
    public static final String ACTION_NEW_EXPLORE = "newExplore";
    public static final String ACTION_NEW_FEED = "newFeed";
    public static final String ACTION_FEED_LIKE = "feedLike";
    public static final String ACTION_NEW_FOLLOW = "newFollow";
    public static final String ACTION_UPDATES = "updates";
    private static final int bigIconDimension = 100;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        RemoteMessage.Notification n = remoteMessage.getNotification();
        Map<String, String> dataPayload = remoteMessage.getData();

        String clickAction = n.getClickAction();
        if (clickAction == null) clickAction = "";

        switch (clickAction) {
            case ACTION_NEW_EXPLORE:
                showExploreNotification(n, dataPayload);
                break;
            case ACTION_NEW_FEED:
                showFeedNotification(n, dataPayload);
                break;
            case ACTION_FEED_LIKE:
                showFeedNotification(n, dataPayload);
                break;
            case ACTION_NEW_FOLLOW:
                showNewFollow(n, dataPayload);
                break;
            case ACTION_UPDATES:
                updateWithLinkNotification(n);
                break;
            default:
                showNotificationPayload(n);
        }
    }

    private void
    showNewFollow(RemoteMessage.Notification n, Map<String, String> data) {
        UserProfileEntry user = new UserProfileEntry();
        user.userId = data.get("followUserId");
        user.username = data.get("followUsername");
        Intent intent = UserSocialProfileActivity.intent(this, user);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap bigIcon = null;
        try {
            bigIcon = Picasso.with(this).load(data.get("followImgUrl"))
                    .resize(bigIconDimension, bigIconDimension).get();
        } catch (IOException e) {
        }

        prepare(n.getTitle(), n.getBody(), bigIcon, pendingIntent);
    }

    private void
    showFeedNotification(RemoteMessage.Notification n, Map<String, String> data) {
        UserProfileEntry user = new UserProfileEntry();
        user.userId = data.get("posterId");
        user.username = data.get("posterUsername");

        Intent intent = UserSocialProfileActivity.intent(this, user);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap bigIcon = null;
        try {
            bigIcon = Picasso.with(this).load(data.get("thumbnailUrl"))
                    .resize(bigIconDimension, bigIconDimension).get();
        } catch (IOException e) {
        }

        NotificationCompat.Builder builder
                = prepare(n.getTitle(), n.getBody(), bigIcon, pendingIntent);
        show(builder.build());
    }

    private void
    showExploreNotification(RemoteMessage.Notification n, Map<String, String> data) {
        Intent intent = new Intent(this, MainAppPageActivity.class);
        intent.putExtra(MainAppPageActivity.INTENT_PAGE, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap bigIcon = null;
        try {
            bigIcon = Picasso.with(this).load(data.get("thumbnailUrl"))
                    .resize(bigIconDimension, bigIconDimension).get();
        } catch (IOException e) {
        }

        NotificationCompat.Builder builder
                = prepare(n.getTitle(), n.getBody(), bigIcon, pendingIntent);
        show(builder.build());
    }

    private void showNotificationPayload(RemoteMessage.Notification n) {
        Intent intent = new Intent(this, MainAppPageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notification
                = prepare(n.getTitle(), n.getBody(), null, pendingIntent);
        show(notification.build());
    }

    private NotificationCompat.Builder
    prepare(String title, String message, Bitmap li, PendingIntent intent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(true).setGroup("action_notification");
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setContentTitle(title).setContentText(message);
        builder.setColor(ContextCompat.getColor(this, R.color.colorAccent));

        if (intent != null) builder.setContentIntent(intent);

        if (li != null) builder.setLargeIcon(li);
        builder.setSmallIcon(R.drawable.ic_notification_appicon);
        return builder;
    }

    private void updateWithLinkNotification(RemoteMessage.Notification n) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.setData(n.getLink());
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notification =
                prepare(n.getTitle(), n.getBody(), null, pendingIntent);
        show(notification.build());
    }

    private void show(Notification notification) {
        NotificationManager nm =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(0, notification);
    }
}
package com.copula.android.app;

import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.nettysocial.nettyservice.CloudMsgTokenPusher;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

public class InstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        UserAccountBase base = new UserAccountBase(this);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        base.setPNToken(refreshedToken);//save refreshed token

        FirebaseMessaging messaging = FirebaseMessaging.getInstance();
        messaging.subscribeToTopic("updatesWithLink");
        messaging.subscribeToTopic("followSuggestion");
        messaging.subscribeToTopic("exploreFeed");

        if (base.getUserId() != null) {//push refreshed token to ws
            CloudMsgTokenPusher.push(base.getUserId(), refreshedToken);
        }
    }//END
}
package com.copula.android.intropage.splash;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.copula.android.R;

/**
 * Created by heeleaz on 4/5/17.
 */
public class PlayerSplashSetupFragment extends ImplSplash {
    private LinearLayout vSplashDesc;

    static PlayerSplashSetupFragment instantiate() {
        return new PlayerSplashSetupFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_splash__player_setup, container, false);
        vSplashDesc = (LinearLayout) view.findViewById(R.id.view_splash_desc);

        setupSplashImage((ImageView) view.findViewById(R.id.img_splash_image));
        return view;
    }

    private void setupSplashImage(ImageView imageView) {
        Bitmap bitmap = BitmapFactory.
                decodeResource(getResources(), R.drawable.img_music_player_512px);
        ViewGroup.LayoutParams params = imageView.getLayoutParams();
        int[] specDimension = scaleImage(bitmap);
        params.width = specDimension[0];
        params.height = specDimension[1];
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onFragmentVisible() {
        runStageAnimation(vSplashDesc);
    }

    @Override
    public int getBackgroundColor() {
        return Color.parseColor("#5AC5CB");
    }
}

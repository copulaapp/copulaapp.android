package com.copula.android.intropage.tutor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.android.R;
import com.copula.android.intropage.splash.ImplSplash;
import com.copula.support.android.view.widget.Button;

/**
 * Created by heeleaz on 4/20/17.
 */
public class ConnectTutorFragment extends ImplSplash {
    private Button btnDone;
    private Callback callback;

    public static ConnectTutorFragment instantiate(Callback callback) {
        ConnectTutorFragment fragment = new ConnectTutorFragment();
        fragment.callback = callback;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_tutor_connect_host, container, false);
        btnDone = (Button) view.findViewById(R.id.btn_done);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) callback.onDoneClicked();
            }
        });
    }//END

    @Override
    public void onFragmentVisible() {
        if (getView() == null) return;
        runStageAnimation((ViewGroup) getView().findViewById(R.id.view_tutor_0));
        runStageAnimation((ViewGroup) getView().findViewById(R.id.view_tutor_1));
        runStageAnimation((ViewGroup) getView().findViewById(R.id.view_tutor_2));
    }

    @Override
    public int getBackgroundColor() {
        return 0;
    }

    public interface Callback {
        void onDoneClicked();
    }
}

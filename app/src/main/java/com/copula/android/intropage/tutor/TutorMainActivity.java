package com.copula.android.intropage.tutor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import com.copula.android.R;
import com.copula.android.app.AppBootstrapPrefs;
import com.copula.android.intropage.splash.ImplSplash;
import com.copula.android.mainpage.MainAppPageActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 4/29/17.
 */
public class TutorMainActivity extends FragmentActivity implements ViewPager.OnPageChangeListener,
        ConnectTutorFragment.Callback {
    private List<Fragment> fragmentList = new ArrayList<>();

    public static void launch(Context context) {
        context.startActivity(new Intent(context, TutorMainActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_tutor_main);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);

        fragmentList.add(ConnectTutorFragment.instantiate(this));
        pager.setAdapter(new MyPagerAdapter(fragmentList));
        pager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        ImplSplash splash = (ImplSplash) fragmentList.get(i);
        splash.onFragmentVisible();
    }

    @Override
    public void onPageScrollStateChanged(int i) {
    }

    @Override
    public void onDoneClicked() {
        new AppBootstrapPrefs(this).set(AppBootstrapPrefs.TUTOR_PASSED, true);
        MainAppPageActivity.launch(this, Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentList;

        MyPagerAdapter(List<Fragment> fragmentList) {
            super(getSupportFragmentManager());
            this.fragmentList = fragmentList;
        }

        @Override
        public int getCount() {
            return this.fragmentList.size();
        }

        @Override
        public Fragment getItem(int pos) {
            return fragmentList.get(pos);
        }
    }//END
}

package com.copula.android.intropage.splash;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import com.copula.android.AbsBootstrapActivity;
import com.copula.android.R;
import com.copula.android.app.AppBootstrapPrefs;
import com.copula.android.intropage.tutor.TutorMainActivity;
import com.copula.genericlook.DesignHelper;
import com.copula.support.android.view.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 12/3/16.
 */
public class SplashScreenActivity extends AbsBootstrapActivity implements ViewPager.OnPageChangeListener {
    private List<Fragment> fragmentList = new ArrayList<>();

    private ViewPager pager;
    private LinearLayout indicatorBox;
    private View parent;

    public static void launch(Context ctx, int flags) {
        Intent intent = new Intent(ctx, SplashScreenActivity.class);
        intent.addFlags(flags);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.act_splash_screen);

        indicatorBox = (LinearLayout) findViewById(R.id.view_indicator_box);
        pager = (ViewPager) findViewById(R.id.pager);

        fragmentList.add(ShareSplashSetupFragment.instantiate());
        fragmentList.add(PlayerSplashSetupFragment.instantiate());
        fragmentList.add(FeedsSplashSetupFragment.instantiate());

        int color = ((ImplSplash) fragmentList.get(0)).getBackgroundColor();
        (parent = findViewById(R.id.view_parent)).setBackgroundColor(color);

        setupPagerIndicator(indicatorBox, fragmentList.size());

        pager.setAdapter(new MyPagerAdapter(fragmentList));
        pager.addOnPageChangeListener(this);

        this.selectPagerIndicator(indicatorBox, 0);
    }

    @Override
    protected void onSystemServerInitCompleted() {
    }

    private void setupPagerIndicator(LinearLayout indicatorBox, int size) {
        if (indicatorBox == null) return;

        int h = DesignHelper.scaleInDP(this, 12);
        int w = DesignHelper.scaleInDP(this, 40);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w, h);
        params.setMargins(2, 2, 2, 2);
        for (int i = 0; i < size; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(params);

            imageView.setBackgroundResource(R.drawable.bkg_splash_navigator_selector);
            indicatorBox.addView(imageView);
        }
    }

    private void selectPagerIndicator(LinearLayout indicatorBox, int position) {
        if (indicatorBox == null) return;

        for (int i = 0; i < indicatorBox.getChildCount(); i++) {
            View childView = indicatorBox.getChildAt(i);
            childView.setHovered(false);

            if (position > i) childView.setSelected(true);
            else childView.setSelected(false);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int color = (((ImplSplash)
                    fragmentList.get(position)).getBackgroundColor());
            getWindow().setStatusBarColor(color);
        }

        indicatorBox.getChildAt(position).setHovered(true);

    }

    private void runBackgroundAnimation(final View parent, int from, int to) {
        ValueAnimator anim = ValueAnimator.ofInt(from, to);
        anim.setEvaluator(new ArgbEvaluator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                parent.setBackgroundColor((Integer) animation.getAnimatedValue());
            }
        });

        anim.setDuration(600).start();
    }

    @Override
    protected void onPermitted() {
        new AppBootstrapPrefs(this).set(AppBootstrapPrefs.SPLASH_PASSED, true);
        TutorMainActivity.launch(this);
        finish();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        this.selectPagerIndicator(indicatorBox, i);

        ImplSplash splash = (ImplSplash) fragmentList.get(i);
        int initColor = ((ColorDrawable) parent.getBackground()).getColor();
        runBackgroundAnimation(parent, initColor, splash.getBackgroundColor());
        splash.onFragmentVisible();
    }

    @Override
    public void onPageScrollStateChanged(int i) {
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentList;

        MyPagerAdapter(List<Fragment> fragmentList) {
            super(getSupportFragmentManager());
            this.fragmentList = fragmentList;
        }

        @Override
        public int getCount() {
            return this.fragmentList.size();
        }

        @Override
        public Fragment getItem(int pos) {
            return fragmentList.get(pos);
        }
    }//END
}

package com.copula.android.intropage.splash;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.copula.android.AbsBootstrapActivity;
import com.copula.android.R;
import com.copula.support.android.view.widget.Button;

/**
 * Created by heeleaz on 4/5/17.
 */
public class FeedsSplashSetupFragment extends ImplSplash {
    private LinearLayout vSplashDesc;
    private Button btnStart;

    static FeedsSplashSetupFragment instantiate() {
        return new FeedsSplashSetupFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_splash__browse_setup, container, false);
        vSplashDesc = (LinearLayout) view.findViewById(R.id.view_splash_desc);
        btnStart = (Button) view.findViewById(R.id.btn_start);

        setupSplashImage((ImageView) view.findViewById(R.id.img_splash_image));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof AbsBootstrapActivity) {
                    ((AbsBootstrapActivity) getActivity()).verifyV23AppPermission();
                }
            }
        });
    }

    private void setupSplashImage(ImageView imageView) {
        Bitmap bitmap = BitmapFactory.
                decodeResource(getResources(), R.drawable.img_feeds_explore_512px);
        ViewGroup.LayoutParams params = imageView.getLayoutParams();
        int[] specDimension = scaleImage(bitmap);
        params.width = specDimension[0];
        params.height = specDimension[1];
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onFragmentVisible() {
        runStageAnimation(vSplashDesc);
    }

    @Override
    public int getBackgroundColor() {
        return Color.parseColor("#63C5DA");
    }
}

package com.copula.android.intropage.splash;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import com.copula.genericlook.DesignHelper;

/**
 * Created by heeleaz on 4/5/17.
 */
public abstract class ImplSplash extends Fragment {
    public abstract void onFragmentVisible();

    public abstract int getBackgroundColor();

    public int[] scaleImage(Bitmap bitmap) {
        int padding = DesignHelper.scaleInDP(getActivity(), 40);
        int deviceSize = getResources().getDisplayMetrics().widthPixels - padding;
        int width = Math.min(650, deviceSize);

        int height = (int) (bitmap.getHeight() * (width * 1f) / (bitmap.getWidth() * 1f));

        return new int[]{width, height};
    }

    public void runStageAnimation(ViewGroup view) {
        for (int i = 0; i < view.getChildCount(); i++) {
            View childView = view.getChildAt(i);
            float yAxis = childView.getY();

            childView.setAlpha(0);
            childView.setY(view.getY() * 1.2f);

            ViewPropertyAnimator animator = childView.animate();
            animator.alpha(1).y(yAxis).setDuration(300 + (i * 170)).start();
        }
    }
}

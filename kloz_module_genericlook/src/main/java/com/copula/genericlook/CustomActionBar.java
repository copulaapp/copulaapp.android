package com.copula.genericlook;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 9/16/16.
 */
public class CustomActionBar {
    private ActionBar actionBar;
    private String title;
    private View.OnClickListener backClickListener;
    private Activity mActivity;

    private TextView txtTitle;
    private View mainView;
    private int backgroundResId;


    public CustomActionBar(AppCompatActivity activity) {
        this.mActivity = activity;
        this.actionBar = activity.getSupportActionBar();
    }

    public CustomActionBar setTitle(String title) {
        if (txtTitle != null) txtTitle.setText(title);
        else this.title = title;
        return this;
    }

    public CustomActionBar setBackListener(View.OnClickListener listener) {
        this.backClickListener = listener;
        return this;
    }

    public View getView() {
        return mainView;
    }

    public CustomActionBar setView(int resId) {
        this.mainView = LayoutInflater.from(mActivity).inflate(resId, null, false);
        return this;
    }

    public void compile() {
        View view = getView();
        if (view == null) {
            view = (setView(R.layout.view_app_custom_title_bar)).getView();
            if (backgroundResId != 0) view.setBackgroundResource(backgroundResId);
        }

        actionBar.setCustomView(view, new ActionBar.LayoutParams(-1, -1));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);


        txtTitle = (TextView) view.findViewById(R.id.txt_bar_title);
        if (txtTitle != null && title != null) txtTitle.setText(title);

        if (backClickListener != null) {
            view.findViewById(R.id.btn_bar_back).setOnClickListener(backClickListener);
        }

        Toolbar parent = (Toolbar) view.getParent();
        parent.setPadding(2, 0, 2, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);
    }

    public void setVisibility(int visibility) {
        if (visibility == View.VISIBLE) mActivity.getActionBar().show();
        else mActivity.getActionBar().hide();
    }

    public CustomActionBar setBackground(int resId) {
        if (mainView != null) mainView.setBackgroundResource(resId);
        else backgroundResId = resId;

        return this;
    }

    public String getTitle() {
        return title;
    }

    public CustomActionBar setTitle(int title) {
        return setTitle(mActivity.getString(title));
    }
}//end

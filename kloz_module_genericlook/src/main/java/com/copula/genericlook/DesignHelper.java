package com.copula.genericlook;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ScrollView;
import android.widget.Toast;
import com.copula.support.android.network.NetworkHelper;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.android.view.widget.ListView;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by eliasigbalajobi on 6/18/16.
 */
public class DesignHelper {

    public static boolean isFragmentAlive(Fragment fragment) {
        return fragment != null && fragment.getActivity() != null
                && fragment.getView() != null;
    }

    public static void bottomSpace(ListView listView, int height) {
        View v = new View(listView.getContext());

        v.setLayoutParams(new AbsListView.LayoutParams(-1, height));

        listView.addFooterView(v, null, false);
    }

    public static boolean isDataEnabled(Context context) {
        return NetworkHelper.isDataEnabled(context);
    }

    public static void forceShowSoftKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) editText.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void forceHideSoftKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) editText.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void showEditTextInputError(EditText editText, String message) {
        DesignHelper.setRightDrawable(editText, DesignHelper.scaleDrawable(
                editText.getContext(), R.drawable.ic_warning_red_64dp, 22, 22));
        if (message != null && message.length() > 0) {
            Toast.makeText(editText.getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public static void setLeftDrawable(EditText editText, Drawable ico) {
        editText.setCompoundDrawablesWithIntrinsicBounds(ico, null, null, null);
    }

    public static void setLeftDrawable(TextView editText, Drawable ico) {
        editText.setCompoundDrawablesWithIntrinsicBounds(ico, null, null, null);
    }

    public static void setRightDrawable(EditText editText, Drawable ico) {
        editText.setCompoundDrawablesWithIntrinsicBounds(null, null, ico, null);
    }


    public static void activityFullscreen(Activity activity) {
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) actionBar.hide();
    }

    public static void showOnlyView(View showView) {
        showOnlyView(showView, View.GONE);
    }

    public static void showOnlyView(View showView, int hi) {
        ViewGroup parent = (ViewGroup) showView.getParent();
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            parent.getChildAt(i).setVisibility(hi);
        }
        showView.setVisibility(View.VISIBLE);
    }

    public static String page(int offset, int limit) {
        return offset + ", " + limit;
    }

    public static Drawable scaleDrawable(Context context, int drawable, int w, int h) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), drawable);
        if (bitmap == null) return null;

        float scale = context.getResources().getDisplayMetrics().density;
        h = (int) (h * scale + 0.5f);
        w = (int) (w * scale + 0.5f);
        return new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    public static int scaleInDP(Context context, int dimension) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dimension * scale + 0.5f);
    }

    public static View wrapInScrollable(Context context, @LayoutRes int layout) {
        View layoutView = View.inflate(context, layout, null);

        ScrollView scrollView = (ScrollView) View.inflate(context,
                R.layout.view_app_layout_scrollable, null);
        layoutView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        scrollView.addView(layoutView, 0);
        return scrollView;
    }

    public static int getPlaceholder(int mediaType) {
        switch (mediaType) {
            case 3:
                return R.drawable.img_media_video_thumb_256dp;
            case 2:
                return R.drawable.img_media_song_thumb_256dp;
            case 1:
                return R.drawable.img_media_image_thumb_256dp;
            default:
                return R.color.placeholderImageColor;
        }
    }

}

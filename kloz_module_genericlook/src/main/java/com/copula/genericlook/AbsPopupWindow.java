package com.copula.genericlook;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListPopupWindow;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 3/24/17.
 */
public abstract class AbsPopupWindow implements AdapterView.OnItemClickListener {
    private Context context;
    private ListPopupWindow listPopupWindow;
    private Adapter adapter;

    private PopupClickListener optionListener;

    protected AbsPopupWindow(Context context) {
        this.context = context;
    }

    protected void addModel(String title, int imgRes, Object tag) {
        adapter.addHandler(new OptionModel(imgRes, title, tag));
    }

    protected void addModel(int title, int imgRes, Object tag) {
        addModel(context.getString(title), imgRes, tag);
    }

    protected void _setup(View anchorView) {
        listPopupWindow = new ListPopupWindow(context);
        listPopupWindow.setAdapter(adapter = new Adapter(context));
        listPopupWindow.setWidth(DesignHelper.scaleInDP(context, 140));
        listPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(this);
        listPopupWindow.setAnchorView(anchorView);
    }

    protected void show() {
        listPopupWindow.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listPopupWindow.dismiss();

        if (optionListener != null)
            optionListener.onAction(position, adapter.getItem(position));
    }

    public void show(PopupClickListener listener) {
        this.optionListener = listener;
        listPopupWindow.show();
    }

    public interface PopupClickListener {
        void onAction(int position, OptionModel option);
    }//END

    public static class OptionModel {
        public int img;
        public String text;
        public Object tag;

        public OptionModel(int img, String text, Object tag) {
            this.img = img;
            this.text = text;
            this.tag = tag;
        }
    }

    private class Adapter extends BaseAdapter<OptionModel> {
        Adapter(Context context) {
            super(context);
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.adp_popup_window, null, false);
                holder.txt = (TextView) convertView.findViewById(R.id.txt_list_option);
                convertView.setTag(holder);
            } else holder = (ViewHolder) convertView.getTag();

            OptionModel entry = getItem(position);
            holder.txt.setText(entry.text);

            Drawable dr = DesignHelper.scaleDrawable(getContext(), entry.img, 20, 20);
            holder.txt.setCompoundDrawablesWithIntrinsicBounds(dr, null, null, null);

            return convertView;
        }
    }

    private class ViewHolder {
        private TextView txt;
    }
}

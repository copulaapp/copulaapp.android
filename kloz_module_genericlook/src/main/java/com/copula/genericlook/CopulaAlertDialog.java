package com.copula.genericlook;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.view.View;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 12/25/16.
 */
public class CopulaAlertDialog extends AlertDialog.Builder implements DialogInterface {
    private TextView txtTitle, txtMessage;
    private Button btnNeutral;

    public CopulaAlertDialog(Context context) {
        super(context);
        View titleView = View.inflate(context, R.layout.app_custom_alert_dialog_title, null);
        txtTitle = (TextView) titleView.findViewById(R.id.txt_title);
        //setCustomTitle(titleView);

        View msgView = View.inflate(context, R.layout.app_custom_alert_dialog_msg, null);
        txtMessage = (TextView) msgView.findViewById(R.id.txt_message);
        btnNeutral = (Button) msgView.findViewById(R.id.btn_neutral);
        setView(msgView);
    }

    @Override
    public AlertDialog.Builder setTitle(CharSequence title) {
        txtTitle.setText(title);
        return this;
    }

    @Override
    public AlertDialog.Builder setTitle(@StringRes int titleId) {
        txtTitle.setText(titleId);
        return this;
    }

    @Override
    public AlertDialog.Builder setMessage(CharSequence message) {
        txtMessage.setText(message);
        return this;
    }

    @Override
    public AlertDialog.Builder setMessage(@StringRes int messageId) {
        txtMessage.setText(messageId);
        return this;
    }

    @Override
    public AlertDialog.Builder setNeutralButton(int text, DialogInterface.OnClickListener listener) {
        return setNeutralButton(getContext().getString(text), listener);
    }


    @Override
    public AlertDialog.Builder setNeutralButton(CharSequence text, final DialogInterface.OnClickListener listener) {
        btnNeutral.setText(text);
        btnNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(CopulaAlertDialog.this, 0);
            }
        });
        return this;
    }

    @Override
    public void cancel() {
    }

    @Override
    public void dismiss() {
    }
}

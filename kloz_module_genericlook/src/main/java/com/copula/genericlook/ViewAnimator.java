package com.copula.genericlook;

import android.animation.Animator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;

/**
 * Created by heeleaz on 6/4/17.
 */
public class ViewAnimator {
    public static void animateJumpIn(final View view, final int duration) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                float initY = view.getY();
                float initAlpha = view.getAlpha();

                view.setAlpha(0);
                view.setY(0);

                ViewPropertyAnimator animator = view.animate();
                animator.alpha(initAlpha).y(initY).setDuration(duration);
                animator.start();

                view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }

    public static void animationShiftUp(final View view, final int duration) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int height = view.getHeight();
                shiftYBy(view, -height, duration);
                view.getViewTreeObserver().removeGlobalOnLayoutListener(this);

            }
        });
    }

    private static void shiftYBy(View view, int height, int duration) {
        ViewPropertyAnimator animator = view.animate();
        animator.yBy(height).setDuration(duration).start();
    }


    public static void animationShiftDown(final View view, final int duration) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                shiftYBy(view, view.getHeight(), duration);
                view.getViewTreeObserver().removeGlobalOnLayoutListener(this);

            }
        });
    }

    public static void alpha(AnimationCallback callback, View view, int alpha, int duration) {
        ViewPropertyAnimator animator = view.animate();
        animator.alpha(alpha)
                .setDuration(duration)
                .setListener(new MyAnimationCallback(callback)).start();
    }

    public static void scaleXY(AnimationCallback callback, View view, float scale, int duration) {
        ViewPropertyAnimator animator = view.animate();
        animator.scaleX(scale).scaleY(scale).alpha(scale)
                .setDuration(duration)
                .setListener(new MyAnimationCallback(callback)).start();
    }

    public interface AnimationCallback {
        void onEnd();

        void onStart();
    }

    private static class MyAnimationCallback implements Animator.AnimatorListener {
        private AnimationCallback callback;

        MyAnimationCallback(AnimationCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onAnimationStart(Animator animation) {
            if (this.callback != null) callback.onStart();
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (callback != null) callback.onEnd();
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }//END
}

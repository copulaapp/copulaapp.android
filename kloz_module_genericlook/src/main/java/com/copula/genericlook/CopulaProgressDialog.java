package com.copula.genericlook;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.StringRes;
import android.view.View;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 1/1/17.
 */
public class CopulaProgressDialog extends AlertDialog {
    private TextView txtMessage;

    public CopulaProgressDialog(Context context) {
        super(context);
        View view = View.inflate(context, R.layout.app_custom_progress_dialog, null);
        txtMessage = (TextView) view.findViewById(R.id.txt_message);

        setView(view);
    }

    @Override
    public void setMessage(CharSequence message) {
        txtMessage.setText(message);
    }

    public void setMessage(@StringRes int messageId) {
        this.setMessage(getContext().getString(messageId));
    }
}

package com.copula.hostworld.connection;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.appshare.CopulaShareOptionActivity;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.hostworld.connection.core.HotspotConfigurationCache;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.genericlook.DesignHelper;
import com.copula.support.android.network.HotspotClientResult;
import com.copula.support.android.network.HotspotManager;
import com.copula.support.android.view.widget.TextView;

import java.util.List;

public class ConnectionCreateActivity extends FragmentActivity implements HotspotManager.HotspotClientScanListener,
        View.OnClickListener {
    private View vCreateSuccess, vCreateProgress;

    private HotspotManager hotspotManager;
    private boolean createdHere = false;
    private ConnectionCreateListener connectionListener;

    public static void launch(Activity context, int requestCode) {
        Intent intent = new Intent(context, ConnectionCreateActivity.class);
        context.startActivityForResult(intent, requestCode);
    }

    public static String createNetworkSSID(Context context) {
        String u = new UserAccountBase(context).getUsername();
        return ConnectivityHelper.networkSSIDEncoder(false, u);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_connection_create);

        findViewById(R.id.btn_hide).setOnClickListener(this);
        findViewById(R.id.view_app_share).setOnClickListener(this);

        vCreateSuccess = findViewById(R.id.view_network_create_success);
        vCreateProgress = findViewById(R.id.view_progress_network_create);
        TextView txtConnectionName = (TextView) findViewById(R.id.txt_connection_name);

        this.hotspotManager = new HotspotManager(this);
        HotspotConfigurationCache.cacheCurrentConfiguration(this);

        String username = new UserAccountBase(this).getUsername();
        txtConnectionName.setText(username);

        this.startOpenNetwork(createNetworkSSID(this));
    }

    @Override
    public void onResume() {
        super.onResume();

        this.connectionListener = new ConnectionCreateListener();
        IntentFilter filter = new IntentFilter(HotspotManager.NETWORK_AP_STATE_CHANGED_ACTION);
        registerReceiver(connectionListener, filter);
    }

    private void startOpenNetwork(String ssid) {
        createdHere = true;
        hotspotManager.hostOpenNetwork(ssid);
        DesignHelper.showOnlyView(vCreateProgress);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(connectionListener);
    }

    @Override
    public void onScanCompleted(List<HotspotClientResult> clients) {
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_hide) {
            hotspotManager.stopScan();
            setResult(RESULT_OK);
            finish();
        } else if (v.getId() == R.id.view_app_share) {
            CopulaShareOptionActivity.launch(this);
        }
    }

    private class ConnectionCreateListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(HotspotManager.NETWORK_AP_STATE_CHANGED_ACTION)) {
                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);
                if (WifiManager.WIFI_STATE_ENABLED == state % 10 && createdHere) {
                    DesignHelper.showOnlyView(vCreateSuccess);
                }
            }
        }//
    }//END
}

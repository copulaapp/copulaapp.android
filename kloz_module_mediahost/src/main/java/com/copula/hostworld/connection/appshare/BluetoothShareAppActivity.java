package com.copula.hostworld.connection.appshare;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.copula.hostworld.R;
import com.copula.functionality.localservice.media.provider.SystemAPKsProvider;
import com.copula.genericlook.CustomActionBar;

import java.io.File;

/**
 * Created by heeleaz on 5/16/17.
 */
public class BluetoothShareAppActivity extends AppCompatActivity implements View.OnClickListener {
    public static void launch(Context context) {
        context.startActivity(new Intent(context, BluetoothShareAppActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_bluetooth_share_app);
        findViewById(R.id.btn_share).setOnClickListener(this);

        CustomActionBar actionBar = new CustomActionBar(this).setBackListener(this);
        actionBar.setTitle(R.string.lbl_bluetooth_share_app).compile();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_share) {
            File file = SystemAPKsProvider.getCopulaAPK(this);
            shareFileViaBluetooth(file);
        } else if (v.getId() == R.id.btn_bar_back) finish();
    }

    private void shareFileViaBluetooth(File file) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        intent.setPackage("com.android.bluetooth");
        startActivity(intent);
    }
}

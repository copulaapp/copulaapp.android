package com.copula.hostworld.connection.core;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by heeleaz on 2/27/17.
 */
public class ConnectivityPreference {
    private SharedPreferences preferences;

    public ConnectivityPreference(Context context) {
        preferences = context.getSharedPreferences("connectivity_preference", Context.MODE_PRIVATE);
    }

    public String getConnectionKey() {
        return preferences.getString("connection_key", null);
    }

    public void setConnectionKey(String key) {
        preferences.edit().putString("connection_key", key).apply();
    }
}

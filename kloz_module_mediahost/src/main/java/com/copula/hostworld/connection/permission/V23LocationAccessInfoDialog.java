package com.copula.hostworld.connection.permission;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.hostworld.R;

/**
 * Created by heeleaz on 9/12/16.
 */
public class V23LocationAccessInfoDialog extends DialogFragment implements View.OnClickListener {
    public static void launch(FragmentManager fm) {
        V23LocationAccessInfoDialog dialog = new V23LocationAccessInfoDialog();
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        FragmentTransaction transaction = fm.beginTransaction();

        String tag = "V23LocationAccessInfoDialog";
        Fragment fragment = fm.findFragmentByTag(tag);
        if (fragment != null) transaction.remove(fragment);

        transaction.add(dialog, tag).show(dialog).commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup c, Bundle s) {
        View view = inflater.inflate(R.layout.frg_v23_location_access_info, c, false);

        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
        view.findViewById(R.id.btn_launch_permission).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_launch_permission) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().startActivity(intent);
        }
        this.dismiss();
    }//end
}

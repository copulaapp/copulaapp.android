package com.copula.hostworld.connection.appshare;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.copula.hostworld.connection.ConnectionCreateActivity;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.functionality.localservice.connection.server.CopulaAPKFetchServer;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.genericlook.CustomActionBar;
import com.copula.support.android.network.HotspotManager;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 5/16/17.
 */
public class Hot_SpotShareAppActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txtNetworkSSID, txtNetworkAddress;
    private ConnectionCreateListener connectionListener;

    public static void launch(Context context) {
        context.startActivity(new Intent(context, Hot_SpotShareAppActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_hotspot_share_app);
        txtNetworkAddress = (TextView) findViewById(R.id.txt_network_address);
        txtNetworkSSID = (TextView) findViewById(R.id.txt_network_ssid);

        CustomActionBar actionBar = new CustomActionBar(this).setBackListener(this);
        actionBar.setTitle(R.string.lbl_hot_spot_share_app).compile();

        if (ConnectivityHelper.isAppALIVE__(this)) {
            startServerWithConnectionInfo();
        } else {
            String ssid = ConnectionCreateActivity.createNetworkSSID(this);
            new HotspotManager(this).hostOpenNetwork(ssid);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        this.connectionListener = new ConnectionCreateListener();
        IntentFilter filter = new IntentFilter(HotspotManager.NETWORK_AP_STATE_CHANGED_ACTION);
        registerReceiver(connectionListener, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(connectionListener);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_bar_back) finish();
    }


    private void startServerWithConnectionInfo() {
        CopulaAPKFetchServer.getInstance(this).startServer(null);

        String host = UserAccountBase.systemAddress(this);
        txtNetworkAddress.setText(String.format("%s:%s", host, "55555"));

        String ssid = ConnectionCreateActivity.createNetworkSSID(this);
        txtNetworkSSID.setText(ssid);
    }

    private class ConnectionCreateListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(HotspotManager.NETWORK_AP_STATE_CHANGED_ACTION)) {
                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);
                if (WifiManager.WIFI_STATE_ENABLED == state % 10) {
                    startServerWithConnectionInfo();
                }
            }
        }//
    }//END
}

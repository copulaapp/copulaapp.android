package com.copula.hostworld.connection;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.app.NotificationHelper.NotificationModel;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by eliasigbalajobi on 3/1/16.
 */
public class ConnectionStatusFragment extends Fragment implements NotificationHelper.NotificationPushListener {
    private TextView txtConnectStatus;
    private View vBtnConnectionStatus;
    private Handler handler = new Handler(Looper.getMainLooper());

    public static ConnectionStatusFragment instantiate() {
        return new ConnectionStatusFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_connection_status, container, false);
        txtConnectStatus = (TextView) view.findViewById(R.id.txt_connect_status);
        vBtnConnectionStatus = view.findViewById(R.id.btn_connection_status);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        vBtnConnectionStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ConnectionDisconnectAsync().execute();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.__showConnectionStatus();
        NotificationHelper.getInstance().addListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        NotificationHelper.getInstance().removeListener(this);
    }

    @Override
    public void onPushNotification(int broadcast, NotificationModel m) {
        if (broadcast == NotificationHelper.APP_WIFI_NOTIFICATION)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    __showConnectionStatus();
                }
            });
    }//end

    private void __showConnectionStatus() {
        int network = ConnectivityHelper.getConnectedNetwork(getActivity());
        if (network != -1) {
            if (network == ConnectivityHelper.WIFI) {
                txtConnectStatus.setText(R.string.act_leave_group);
            } else if (network == ConnectivityHelper.HOTSPOT) {
                txtConnectStatus.setText(R.string.act_close_group);
            }
        }
    }//end

    private class ConnectionDisconnectAsync extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            return ConnectivityHelper.connectionDisconnect(getActivity());
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Intent i = new Intent("com.copula.MainAppPage");
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }//END
}

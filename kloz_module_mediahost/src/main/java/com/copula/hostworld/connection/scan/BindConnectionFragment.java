package com.copula.hostworld.connection.scan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.core.ConnectionDHCPWaiter;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.functionality.localservice.profile.DaoConnectsProfile;
import com.copula.functionality.localservice.profile.UsersProfileResource;
import com.copula.genericlook.DesignHelper;
import com.copula.support.android.network.NetConfigurator;
import com.copula.support.android.network.WifiManagerHelper;
import com.copula.support.android.view.widget.EditText;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.TextView;

public class BindConnectionFragment extends Fragment implements EditText.TextWatcher {
    private View btnSecureConnect, vConnectProgress;
    private TextView txtNetworkSSID, txtConnectMessage;
    private ImageView imgWifiNetworkThumb;

    private ConnectionListener mConnectionListener;
    private boolean bindedHere = false;

    public static BindConnectionFragment instantiate(ScanResult scanResult) {
        BindConnectionFragment fragment = new BindConnectionFragment();

        Bundle args = new Bundle();
        args.putParcelable("scan_result", scanResult);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_connection_bind, container, false);
        btnSecureConnect = view.findViewById(R.id.btn_secure_network_start);
        txtNetworkSSID = (TextView) view.findViewById(R.id.txt_network_ssid);
        txtConnectMessage = (TextView) view.findViewById(R.id.txt_connect_message);
        vConnectProgress = view.findViewById(R.id.view_open_network_bind);
        imgWifiNetworkThumb = (ImageView) view.findViewById(R.id.img_wifi_network_thumb);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnSecureConnect.setEnabled(false);

        ScanResult scanResult = getArguments().getParcelable("scan_result");
        if (scanResult == null) return;

        WifiManagerHelper wifiManager = new WifiManagerHelper(getActivity());

        String ssid = ConnectivityHelper.getNetworkSSID(scanResult.SSID);
        txtNetworkSSID.setText(String.format("Join %s", ssid));
        txtConnectMessage.setText(getString(R.string.prg_connection_binding)
                .replace("?", ssid));


        if (NetConfigurator.isOpenNetwork(scanResult)) {
            bindedHere = true;
            if (wifiManager.connect(scanResult, null) == -1) {
                dispatchMessage(getString(R.string.msg_wifi_bind_failed));
            } else {
                DesignHelper.showOnlyView(vConnectProgress);
            }
        }

        DaoConnectsProfile dao = new DaoConnectsProfile(getActivity());
        String userId = dao.getUserId(ssid);
        if (userId != null) {
            Bitmap bm = UsersProfileResource.getProfileImage(userId, 100);
            if (bm != null) imgWifiNetworkThumb.setImageBitmap(bm);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mConnectionListener = new ConnectionListener();
        getActivity().registerReceiver(mConnectionListener, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mConnectionListener);
    }

    private void dispatchMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        if (s.length() < 8) {//MIN_PASSWORD_LEN = 8
            btnSecureConnect.setEnabled(false);
        } else btnSecureConnect.setEnabled(true);
    }

    private class ConnectionListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (info == null) return;
            if (!info.isConnected() || !bindedHere) return;

            ConnectionDHCPWaiter balancer = new ConnectionDHCPWaiter(context);
            balancer.setListener(new ConnectionDHCPWaiter.BindBalanceListener() {
                @Override
                public void result(int network) {
                    if (network != -1) getActivity().finish();
                }
            });
            balancer.execute();
        }
    }//END
}

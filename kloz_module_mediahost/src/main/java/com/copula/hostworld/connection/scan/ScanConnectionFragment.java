package com.copula.hostworld.connection.scan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.hostworld.connection.core.SystemNetworkProfileCache;
import com.copula.genericlook.DesignHelper;
import com.copula.support.android.network.WifiManagerHelper;

import java.util.List;

public class ScanConnectionFragment extends Fragment implements OnItemClickListener {
    private AdaptScanConnection mAdapter;
    private WifiManagerHelper mWifiManager;
    private WifiScanListener mWifiScanListener;
    private View vScanProgress, vScanList;

    private GridView gridView;
    private SystemNetworkProfileCache netProfileCache;
    private Callback callback;

    public static ScanConnectionFragment instantiate(Callback callback) {
        ScanConnectionFragment fragment = new ScanConnectionFragment();
        fragment.callback = callback;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_connection_scan, container, false);
        gridView = (GridView) view.findViewById(R.id.grd);
        vScanList = view.findViewById(R.id.view_scan_list);
        vScanProgress = view.findViewById(R.id.view_scan_progress);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        gridView.setAdapter(mAdapter = new AdaptScanConnection(getActivity()));
        gridView.setOnItemClickListener(this);
        mWifiManager = new WifiManagerHelper(getActivity());

        netProfileCache = new SystemNetworkProfileCache(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();

        mWifiScanListener = new WifiScanListener();
        IntentFilter bi = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        getActivity().registerReceiver(mWifiScanListener, bi);

        new WifiScannerAsync().execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ScanResult scanResult = (ScanResult) parent.getItemAtPosition(position);
        if (scanResult == null) return;

        if (callback != null) callback.onSelect(scanResult);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mWifiScanListener);
    }

    interface Callback {
        void onSelect(ScanResult scanResult);
    }

    private class WifiScannerAsync extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            DesignHelper.showOnlyView(vScanProgress);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            mWifiManager.setWifiEnabled(true);
            List<SystemNetworkProfileCache.NetworkProfileModel>
                    ms = netProfileCache.getNetworks();
            for (SystemNetworkProfileCache.NetworkProfileModel m : ms) {
                if (mWifiManager.removeNetwork(m.networkId))
                    netProfileCache.removeNetwork(m.networkId);
            }
            mWifiManager.enableNetworks(false);//first disable all networks
            mWifiManager.scan();//only works on worker thread
            return true;
        }
    }//END

    private class WifiScanListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            mAdapter.clear();
            List<ScanResult> results = mWifiManager.getScanResult();
            if (results != null && results.size() > 0) {
                DesignHelper.showOnlyView(vScanList);
                for (ScanResult result : results) {
                    if (ConnectivityHelper.isAppNetworkSSID(result.SSID)) {
                        mAdapter.addOrReplaceHandler(result);
                        mAdapter.notifyDataSetChangedWorkerThread();
                    }
                }
            }//end
        }
    }//END
}
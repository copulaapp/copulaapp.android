package com.copula.hostworld.connection;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.support.android.network.NetworkHelper;

/**
 * Created by heeleaz on 12/16/16.
 */
public class ConnectivityStateWidget extends BroadcastReceiver {
    public static final int notificationId = 2000;
    private RemoteViews mRemoteView;
    private Context mContext;
    private NotificationManager mNotificationManager;
    private Notification mNotification;
    private NetworkHelper mNetworkHelper;

    /**
     * Default Constructor for Broadcast listener service
     */
    public ConnectivityStateWidget() {
    }

    private ConnectivityStateWidget(Context context, Intent launchIntent) {
        this.mContext = context;
        mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNetworkHelper = new NetworkHelper(context);

        long when = System.currentTimeMillis();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        builder.setWhen(when).setSmallIcon(R.drawable.ic_notification_appicon);
        builder.setOngoing(true).setPriority(NotificationCompat.PRIORITY_HIGH);

        builder.setContentIntent(PendingIntent.getActivity(mContext, 0, launchIntent,
                PendingIntent.FLAG_CANCEL_CURRENT));

        mRemoteView = new RemoteViews(mContext.getPackageName(), R.layout.nwidget_connection_status);
        mNotification = _ProcessWidgetView(builder.setContent(mRemoteView));
    }

    public static void cancelNotification(Context context) {
        NotificationManager nm = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(notificationId);

        //Intent intent = new Intent(context, MediaDownloadService.class);
        //context.startService(intent);
    }

    public static void showNotification(Context context, Intent launchIntent) {
        ConnectivityStateWidget c = new ConnectivityStateWidget(context, launchIntent);
        c.mNotificationManager.notify(notificationId, c.mNotification);

        //Intent intent = new Intent(context, MediaDownloadService.class);
        //intent.putExtra("NOTIFICATION_ID", ConnectivityStateWidget.notificationId);
        //intent.putExtra("NOTIFICATION", c.mNotification);
        //intent.putExtra("START_FOREGROUND", true);

        //context.startService(intent);
    }//end

    private Notification _ProcessWidgetView(NotificationCompat.Builder builder) {
        int networkType = ConnectivityHelper.getConnectedNetwork(mContext);
        if (networkType == -1) return null;

        String networkSSID, btnText;
        if (networkType == ConnectivityHelper.WIFI) {//wifi-connect
            networkSSID = mNetworkHelper.getWifiInfo().getSSID();
            btnText = mContext.getString(R.string.act_leave_group);
        } else if (networkType == ConnectivityHelper.HOTSPOT) {
            networkSSID = mNetworkHelper.getWifiConfiguration().SSID;
            btnText = mContext.getString(R.string.act_close_group);
        } else {
            networkSSID = mContext.getString(R.string.val_empty_wifi_network_ssid);
            btnText = mContext.getString(R.string.act_leave_group);
        }

        if (ConnectivityHelper.isAppNetworkSSID(networkSSID)) {
            String n = ConnectivityHelper.getNetworkSSID(networkSSID);
            mRemoteView.setTextViewText(R.id.txt_connection_name, n);
            mRemoteView.setTextViewText(R.id.btn_disconnect, btnText);
        }

        //this is the intent that is supposed to be called when the button is clicked
        Intent switchIntent = new Intent(mContext, getClass());
        PendingIntent disconnectIntent = PendingIntent.getBroadcast(mContext, 0,
                switchIntent.setAction("disconnect"), 0);
        mRemoteView.setOnClickPendingIntent(R.id.btn_disconnect, disconnectIntent);

        return builder.build();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("disconnect")) {
            new ConnectionDisconnectAsync(context).execute();
        }
    }

    private class ConnectionDisconnectAsync extends AsyncTask<Void, Void, Void> {
        private Context context;

        ConnectionDisconnectAsync(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ConnectivityHelper.connectionDisconnect(context);
            return null;
        }
    }//END
}

package com.copula.hostworld.connection.scan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.functionality.localservice.profile.DaoConnectsProfile;
import com.copula.functionality.localservice.profile.UsersProfileResource;
import com.copula.support.android.view.widget.BaseAdapter;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.TextView;

@SuppressLint("InflateParams")
class AdaptScanConnection extends BaseAdapter<ScanResult> {
    private DaoConnectsProfile DAOConnectsProfile;

    AdaptScanConnection(Context context) {
        super(context);
        this.DAOConnectsProfile = new DaoConnectsProfile(context);
    }

    @Override
    public View getView(int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_connection_scan, null, false);
            holder.ssid = (TextView) convertView.findViewById(R.id.txt_network_ssid);
            holder.netIcon = (ImageView) convertView.findViewById(R.id.img_wifi_network_thumb);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        ScanResult sr = getItem(position);

        String mainSSID = (ConnectivityHelper.getNetworkSSID(sr.SSID));
        holder.ssid.setText(mainSSID);

        String userId = DAOConnectsProfile.getUserId(mainSSID);
        if (userId != null) {
            Bitmap bm = UsersProfileResource.getProfileImage(userId, 100);
            if (bm != null) holder.netIcon.setImageBitmap(bm);
        }
        return convertView;
    }

    private class ViewHolder {
        TextView ssid;
        ImageView netIcon;
    }
}

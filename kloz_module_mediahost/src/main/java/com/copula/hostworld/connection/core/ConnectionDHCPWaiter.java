package com.copula.hostworld.connection.core;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.os.AsyncTask;
import android.util.Log;
import com.copula.support.android.network.WifiManagerHelper;

/**
 * Created by heeleaz on 1/21/17.
 */
public class ConnectionDHCPWaiter extends AsyncTask<Void, Void, Integer> {
    private static final String TAG = "ConnectionDHCPWaiter";
    private WifiManagerHelper wifiHelper;
    private SystemNetworkProfileCache cache;
    private BindBalanceListener listener;
    private Context context;
    private int checkCount = 10, checkBreak = 500;


    public ConnectionDHCPWaiter(Context context) {
        wifiHelper = new WifiManagerHelper(this.context = context);
        cache = new SystemNetworkProfileCache(context);
    }

    public void setCheckCount(int checkCount) {
        this.checkCount = checkCount;
    }

    public void setCheckBreak(int checkBreak) {
        this.checkBreak = checkBreak;
    }

    public void setListener(BindBalanceListener listener) {
        this.listener = listener;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        WifiInfo wifiInfo = wifiHelper.getWifiInfo();
        if (wifiInfo == null) return -1;

        cache.addNetwork(wifiInfo.getNetworkId());

        int count = 0, network = getNetwork();
        while (count < checkCount && (network = getNetwork()) == -1) {
            Log.d(TAG, "count: " + count + " network: " + network);
            sleep(checkBreak);
            ++count;
        }

        return network;
    }

    private int getNetwork() {
        return ConnectivityHelper.getConnectedNetwork(context);
    }

    private void sleep(int milli) {
        try {
            Thread.sleep(milli);//wait some time
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(Integer result) {
        if (listener != null) listener.result(result);
    }

    public interface BindBalanceListener {
        void result(int network);
    }
}//END

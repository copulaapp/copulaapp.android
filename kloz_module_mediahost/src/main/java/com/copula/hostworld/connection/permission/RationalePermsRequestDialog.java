package com.copula.hostworld.connection.permission;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.copula.hostworld.R;
import com.copula.functionality.app.V23PermissionHelper;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 9/12/16.
 */
public class RationalePermsRequestDialog extends DialogFragment implements View.OnClickListener {
    private View btnLaunch, btnCancel;
    private TextView txtTitle, txtMessage;
    private ImageView imgPermissionImage;
    private String permission;

    public static void launch(FragmentManager fm, String permission) {
        if (permission == null) return;

        RationalePermsRequestDialog dialog = new RationalePermsRequestDialog();
        Bundle bundle = new Bundle(1);
        bundle.putString("permission", permission);
        dialog.setArguments(bundle);

        FragmentTransaction ft = fm.beginTransaction();
        String tag = "RationalePerms";
        Fragment oldFragment = fm.findFragmentByTag(tag);
        if (oldFragment != null) ft.remove(oldFragment);

        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        ft.add(dialog, tag).show(dialog).commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_rationale_perms_request, container, false);
        (btnLaunch = view.findViewById(R.id.btn_launch_permission)).setOnClickListener(this);
        (btnCancel = view.findViewById(R.id.btn_cancel)).setOnClickListener(this);
        txtMessage = (TextView) view.findViewById(R.id.txt_message);
        txtTitle = (TextView) view.findViewById(R.id.txt_title);
        imgPermissionImage = (ImageView) view.findViewById(R.id.img_permission_image);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        permission = getArguments().getString("permission");

        int message = 0, title = 0, imgRes = 0;
        switch (permission) {
            case Manifest.permission.WRITE_SETTINGS:
                title = R.string.tit_rationale_request_hotspot;
                message = R.string.msg_rationale_request_hotspot;
                imgRes = R.drawable.img_tut_permission_phone_on;
                break;
            case Manifest.permission.ACCESS_FINE_LOCATION:
                title = R.string.tit_location_access_info;
                message = R.string.msg_location_access_info;
                imgRes = R.drawable.img_tut_permission_location_on;
                break;

            case Manifest.permission.READ_PHONE_STATE:
                title = R.string.tit_location_access_info;
                message = R.string.msg_phone_state_read_permission;
                imgRes = R.drawable.img_tut_permission_phone_on;

                break;
            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
            case Manifest.permission.READ_EXTERNAL_STORAGE:
                title = R.string.tit_perm_rational_write_setting;
                message = R.string.msg_perm_rational_write_setting;
                imgRes = R.drawable.img_tut_permission_storage_on;
                break;
        }

        txtTitle.setText(title);
        txtMessage.setText(message);
        imgPermissionImage.setImageResource(imgRes);
    }//end


    private void resolvePermissionActivatePage() {
        String action = "";
        switch (permission) {
            case Manifest.permission.ACCESS_FINE_LOCATION:
            case Manifest.permission.READ_PHONE_STATE:
            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS;
                break;
            case Manifest.permission.WRITE_SETTINGS:
                action = Settings.ACTION_MANAGE_WRITE_SETTINGS;
                break;
        }

        Intent intent = new Intent(action);
        intent.setData(Uri.parse("package:" + getActivity().getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (permission.equals(Manifest.permission.WRITE_SETTINGS)) {
            if (V23PermissionHelper.checkSettingsPermission(getActivity()))
                dismiss();
        }
        if (V23PermissionHelper.checkPermission(getActivity(), permission))
            dismiss();
    }

    @Override
    public void onClick(View v) {
        if (v == btnCancel) dismiss();
        else if (v == btnLaunch) resolvePermissionActivatePage();
    }
}

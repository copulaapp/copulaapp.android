package com.copula.hostworld.connection.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

public class SystemNetworkProfileCache extends DatabaseContext {

    private static final String TAG = SystemNetworkProfileCache.class.getSimpleName();
    private static final String TB_NAME = "network_profile_cache";

    public SystemNetworkProfileCache(Context context) {
        super(context, "network_profile_cache.db", TB_NAME, 1);
    }

    public boolean addNetwork(int networkId) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("network_id", networkId);
            values.put("created_at", System.currentTimeMillis());
            db.insert(TB_NAME, null, values);
            return true;
        } catch (Exception e) {
            Log.d(TAG, "addNetwork: " + e.getMessage());
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    private NetworkProfileModel populate(Cursor cu) {
        NetworkProfileModel model = new NetworkProfileModel();
        model.networkId = cu.getInt(cu.getColumnIndex("network_id"));
        model.createdAt = cu.getLong(cu.getColumnIndex("created_at"));
        return model;
    }

    public List<NetworkProfileModel> getNetworks() {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TB_NAME, null, null, null, null, null, null);
            if (cu == null || cu.getCount() <= 0) return new ArrayList<>();

            List<NetworkProfileModel> entries = new ArrayList<>(cu.getColumnCount());
            while (cu.moveToNext()) entries.add(populate(cu));
            return entries;
        } catch (Exception e) {
            return new ArrayList<>();
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }

    }

    public boolean removeNetwork(int networkId) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            return db.delete(TB_NAME, "network_id=" + networkId, null) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME;
        sql += "(_id integer primary key autoincrement,";
        sql += "network_id integer,";
        sql += "created_at integer)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public class NetworkProfileModel {
        public int networkId;
        public long createdAt;
    }

}
package com.copula.hostworld.connection.core;

import android.content.Context;
import com.copula.functionality.util.ConnectionConstant;
import com.copula.support.android.network.NetworkHelper;
import com.copula.support.android.network.WifiManagerHelper;

/**
 * Created by eliasigbalajobi on 6/12/16.
 */
public class ConnectivityHelper {
    public static final String APP_NETWORK_PREFIX = "klz";
    public static final int WIFI = 1, HOTSPOT = 0;
    private static final int appVersion = 1;

    public static int getConnectedNetwork(Context context) {
        NetworkHelper networkHelper = new NetworkHelper(context);
        if (networkHelper.isWifiConnected()) return WIFI;
        else if (networkHelper.isWifiApEnabled()) return HOTSPOT;
        return -1;
    }

    public static String getNetworkSSID(String ssid) {
        try {
            String[] decoded = networkSSIDDecoder(ssid = ssid.replace("\"", ""));
            if (checkDecodedString(decoded)) {
                return decoded[3];
            } else return ssid;
        } catch (Exception e) {
            e.printStackTrace();
            return ssid;
        }
    }

    private static boolean checkDecodedString(String[] decoded) {
        return (decoded.length == 4 && APP_NETWORK_PREFIX.equals(decoded[0]));
    }

    public static boolean isAppNetworkSSID(String ssid) {
        try {
            return ssid != null && checkDecodedString(networkSSIDDecoder(ssid.replace("\"", "")));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isAppInitiatedNetwork(Context ctx, int networkType) {
        NetworkHelper nHelper = new NetworkHelper(ctx);
        if (networkType == WIFI) {
            return isAppNetworkSSID(nHelper.getWifiInfo().getSSID());
        } else {
            return networkType == HOTSPOT &&
                    isAppNetworkSSID(nHelper.getWifiConfiguration().SSID);
        }
    }

    public static boolean isAppALIVE__(Context ctx) {
        return ConnectionConstant.HOST_ONLINE_STATUS
                && isAppInitiatedNetwork(ctx, getConnectedNetwork(ctx));
    }

    public static String networkSSIDEncoder(boolean secured, String username) {
        return networkSSIDEncoder(appVersion, secured, username);
    }

    private static String networkSSIDEncoder(int versionCode, boolean secured, String username) {
        String composer = APP_NETWORK_PREFIX + "V" + versionCode;
        return composer + "S" + (secured ? "t" : "f") + "U" + username;
    }

    private static String[] networkSSIDDecoder(String ssid) throws Exception {
        if (ssid == null) {
            throw new IllegalArgumentException("SSID cannot be null");
        }

        int versionOffset = ssid.indexOf('V');
        int securedOffset = ssid.indexOf('S', versionOffset);
        int userOffset = ssid.indexOf('U', securedOffset);

        try {
            String[] build = new String[4];
            build[0] = ssid.substring(0, versionOffset);
            build[1] = ssid.substring(versionOffset + 1, securedOffset);
            build[2] = ssid.substring(securedOffset + 1, userOffset);
            build[3] = ssid.substring(userOffset + 1);
            return build;
        } catch (Exception e) {
            throw new Exception("String format not accepted");
        }
    }

    public static boolean connectionDisconnect(Context context) {
        int network = ConnectivityHelper.getConnectedNetwork(context);
        if (network == -1) return false;

        if (network == ConnectivityHelper.HOTSPOT) {
            NetworkHelper networkHelper = new NetworkHelper(context);
            networkHelper.setHotspotEnabled(null, false);
            new HotspotConfigurationCache(context).restoreConfiguration();
        } else {
            new WifiManagerHelper(context).closeWifi();
        }

        return true;
    }
}

package com.copula.hostworld.connection.core;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import com.copula.functionality.util.MediaUtility;
import com.copula.support.android.network.NetConfigurator;
import com.copula.support.android.network.NetworkHelper;

import java.io.*;

/**
 * Created by heeleaz on 1/15/17.
 */
public class HotspotConfigurationCache {
    private NetworkHelper networkHelper;

    public HotspotConfigurationCache(Context context) {
        networkHelper = new NetworkHelper(context);
    }

    public static boolean cacheCurrentConfiguration(Context context) {
        return new HotspotConfigurationCache(context).cacheCurrentConfiguration();
    }

    public boolean restoreConfiguration() {
        try {
            File file = new File(MediaUtility.tmpDir(), "nc.tmp");
            if (!file.exists()) return false;

            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            WifiConfigHandler configHandler = (WifiConfigHandler) ois.readObject();
            if (configHandler == null) return false;

            WifiConfiguration configuration;
            if (configHandler.securityType.equals(NetConfigurator.Constants.OPEN)) {
                configuration = NetConfigurator.hostOpenNetwork(configHandler.ssid);
            } else {
                configuration = NetConfigurator
                        .hostWPASecuredNetwork(configHandler.ssid, configHandler.preSharedKey);
            }

            networkHelper.setWifiApConfiguration(configuration);
            ois.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean cacheCurrentConfiguration() {
        WifiConfiguration configuration = networkHelper.getWifiConfiguration();
        if (configuration == null) return false;

        if (ConnectivityHelper.isAppNetworkSSID(configuration.SSID))
            return false;

        WifiConfigHandler handler = new WifiConfigHandler();
        handler.bssid = configuration.BSSID;
        handler.preSharedKey = configuration.preSharedKey;
        handler.ssid = configuration.SSID;
        handler.securityType = NetConfigurator.getSecurityType(configuration);

        try {
            File file = new File(MediaUtility.tmpDir(), "nc.tmp");
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(handler);
            oos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static class WifiConfigHandler implements Serializable {
        String securityType, preSharedKey, ssid, bssid;
    }
}

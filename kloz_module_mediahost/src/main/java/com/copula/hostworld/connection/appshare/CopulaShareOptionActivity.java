package com.copula.hostworld.connection.appshare;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.copula.hostworld.R;
import com.copula.genericlook.CustomActionBar;

/**
 * Created by heeleaz on 5/16/17.
 */
public class CopulaShareOptionActivity extends AppCompatActivity implements View.OnClickListener {
    public static void launch(Context context) {
        context.startActivity(new Intent(context, CopulaShareOptionActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_copula_share_option);
        findViewById(R.id.view_hotspot_share).setOnClickListener(this);
        findViewById(R.id.view_bluetooth_share).setOnClickListener(this);

        CustomActionBar actionBar = new CustomActionBar(this).setBackListener(this);
        actionBar.setTitle(R.string.lbl_share_copula).compile();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.view_hotspot_share) {
            Hot_SpotShareAppActivity.launch(this);
        } else if (v.getId() == R.id.view_bluetooth_share) {
            BluetoothShareAppActivity.launch(this);
        } else {
            finish();
        }
    }
}

package com.copula.hostworld.connection;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.LinearLayout;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.permission.RationalePermsRequestDialog;
import com.copula.hostworld.connection.permission.V23LocationAccessInfoDialog;
import com.copula.hostworld.connection.scan.ScanConnectionMainActivity;
import com.copula.functionality.app.V23PermissionHelper;
import com.copula.support.android.network.WifiManagerHelper;

/**
 * Created by heeleaz on 3/1/17.
 */
public class ConnectionStarterActivity extends FragmentActivity {
    public static final int REQUEST_CODE = 0x123F;
    private int currentAction;

    public static void launch(Activity context) {
        Intent intent = new Intent(context, ConnectionStarterActivity.class);
        context.startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frg_connection_starter);
        LinearLayout vJoin = (LinearLayout) findViewById(R.id.btn_join_connection);
        LinearLayout vCreate = (LinearLayout) findViewById(R.id.btn_create_connection);

        vCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkV23PermissionAndCreateConnection();
            }
        });
        vJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkV23PermissionAndScanConnection();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (currentAction == 0xA) checkV23PermissionAndScanConnection();
        else if (currentAction == 0xC) checkV23PermissionAndCreateConnection();
    }

    private void checkV23PermissionAndScanConnection() {
        int state = V23PermissionHelper.wifiScanAccessPermission(this, 2);
        if (state == V23PermissionHelper.PERMITTED) {
            if (checkV23LocationServiceEnabled()) {
                currentAction = 0x0;//success action.....
                ScanConnectionMainActivity.launch(this);
                finish();
            } else {
                currentAction = 0xA;//location access info
                V23LocationAccessInfoDialog.launch(getSupportFragmentManager());
            }
        } else if (state == V23PermissionHelper.RATIONALE_PERMISSION) {
            currentAction = 0xA;// wifi scan rationale permission request
            RationalePermsRequestDialog.launch(getSupportFragmentManager(),
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    private void checkV23PermissionAndCreateConnection() {
        if (!V23PermissionHelper.checkSettingsPermission(this)) {
            currentAction = 0xC;//hotspot rationale permission request
            RationalePermsRequestDialog.launch(getSupportFragmentManager(),
                    Manifest.permission.WRITE_SETTINGS);
        } else {
            currentAction = 0x0;//success action.....
            ConnectionCreateActivity.launch(this, 200);
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    private boolean checkV23LocationServiceEnabled() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return WifiManagerHelper.checkLocationServiceEnabled(this);
        } else return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode
            , @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 2 && permissions.length > 0) {
            try {
                checkV23PermissionAndScanConnection();
            } catch (Exception e) {
                //some errors here i can't talk about....
            }
        }
    }//end
}

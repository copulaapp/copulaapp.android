package com.copula.hostworld.connection.scan;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.copula.hostworld.R;

/**
 * Created by heeleaz on 4/28/17.
 */
public class ScanConnectionMainActivity extends FragmentActivity implements ScanConnectionFragment.Callback {
    public static void launch(Context context) {
        context.startActivity(new Intent(context, ScanConnectionMainActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_connection_scan_main);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, ScanConnectionFragment.instantiate(this));
        ft.commit();
    }

    @Override
    public void onSelect(ScanResult scanResult) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment,
                BindConnectionFragment.instantiate(scanResult), "BIND");
        ft.addToBackStack("B").commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentByTag("SCAN") != null) {
            fm.popBackStackImmediate("B", FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else super.onBackPressed();
    }
}

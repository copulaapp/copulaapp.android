package com.copula.hostworld;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AbsListView;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.genericlook.CustomActionBar;
import com.copula.hostworld.media.HostedMediaBootstrap;
import com.copula.hostworld.media.audio.HAudioAlbumsFragment;
import com.copula.hostworld.media.audio.HAudioPlayerMainActivity;
import com.copula.hostworld.media.audio.HAudioSongListFragment;
import com.copula.hostworld.media.image.HImageListingFragment;
import com.copula.hostworld.media.search.HMediaSearchActivity;
import com.copula.hostworld.media.video.HVideoTrackFragment;
import com.copula.hostworld.onlinehost.ConnectionOpenFragment;
import com.copula.mediasocial.audio.player.AudioDPlayerFragment;
import com.copula.mediasocial.download.MediaDownloadActivity;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.content.IScrollableHandler;
import com.copula.support.android.content.TabActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 6/4/17.
 */
public class HostMediaPageActivity extends TabActivity implements View.OnClickListener, IScrollableHandler {
    public static final int ALBUM_PAGE = 0;

    private ConnectionOpenFragment hostListFragment;

    public static void launch(Context context, UserProfileEntry user) {
        UserSession.setHostProfile(user);

        Intent intent = new Intent(context, HostMediaPageActivity.class);
        intent.putExtra("user", user);
        context.startActivity(intent);
    }

    @Override
    protected void onPrepareActivityWindow() {
        setTabViewStyle(R.style.ActivityTheme_HostPortal_TabView);
        addTab(R.string.tab_album).addTab(R.string.tab_songs)
                .addTab(R.string.tab_videos).addTab(R.string.tab_images);

        getActionBarTab().setTextUnselectedColor(0xFF808080);
        getActionBarTab().setTextSelectedColor(Color.BLACK);
    }

    @Override
    protected ViewPager getViewPager() {
        return (ViewPager) findViewById(R.id.pager);
    }

    @Override
    protected List<Fragment> getPagerFragments() {
        UserProfileEntry user =
                (UserProfileEntry) getIntent().getSerializableExtra("user");
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(HAudioAlbumsFragment.instantiate(user));
        fragments.add(HAudioSongListFragment.instantiate(user,
                null, false));
        fragments.add(HVideoTrackFragment.instantiate(user));
        fragments.add(HImageListingFragment.instantiate(user));
        return fragments;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_host_media_page);

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setView(R.layout.bar_host_portal).setBackListener(this);
        actionBar.compile();
        findViewById(R.id.btn_bar_search).setOnClickListener(this);
        findViewById(R.id.btn_download).setOnClickListener(this);

        UserProfileEntry user = (UserProfileEntry)
                getIntent().getSerializableExtra("user");
        if (user != null) actionBar.setTitle(user.username);

        setCurrentPage(getIntent().getIntExtra("pager_page", 0));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frg_audio_player, AudioDPlayerFragment.instantiate());
        ft.replace(R.id.frg_connection_launcher,
                hostListFragment = ConnectionOpenFragment.instantiate()).commit();

        findViewById(R.id.frg_audio_player).setOnClickListener(this);

        cacheHostMedia();
    }

    private void cacheHostMedia() {
        HostedMediaBootstrap b = new HostedMediaBootstrap(this);
        b.setCacheKey("host");//local media cache
        b.boot(null);
    }//END

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_bar_back) finish();
        else if (v.getId() == R.id.frg_audio_player) {
            HAudioPlayerMainActivity.launch(this);
        } else if (v.getId() == R.id.btn_bar_search) {
            HMediaSearchActivity.launch(this);
        } else if (v.getId() == R.id.btn_download) {
            MediaDownloadActivity.launch(this);
        }
    }

    @Override
    public void onPageSelected(int pos) {
        super.onPageSelected(pos);
        showConnectionFragment(pos != 4);
    }

    @Override
    public void onScroll(AbsListView view, int axis) {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
            showConnectionFragment(true);
        } else showConnectionFragment(false);
    }

    @Override
    public void onBackPressed() {
        IFragmentBackPress fragment =
                (IFragmentBackPress) getPagerFragmentList().get(getCurrentItem());
        if (!fragment.allowBackPressed()) {
        } else if (getCurrentItem() != ALBUM_PAGE) setCurrentPage(ALBUM_PAGE);
        else super.onBackPressed();
    }

    private void showConnectionFragment(boolean show) {
        if (hostListFragment == null) return;
        hostListFragment.showFragment(show);
    }
}

package com.copula.hostworld.media.audio;

import android.content.Context;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.hostworld.media.HAbsGalleryAdapter;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.audio.songlist.GetAudioMedia;
import com.copula.mediasocial.media.MediaOptionPopWindow;
import com.copula.mediasocial.media.MediaOptions;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.view.PatchSession;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HAudioSongListFragment extends com.copula.mediasocial.audio.songlist.AudioSongListFragment
        implements AdapterView.OnItemClickListener, PatchSession.PatchSessionListener, IFragmentBackPress,
        GetAudioMedia.Callback {
    private AdapterAudioSongList adapter;
    private HAbsGalleryAdapter galleryAdapter;

    public static HAudioSongListFragment
    instantiate(UserProfileEntry user, AudioModel m, boolean showAlbumArt) {
        UserSession.setHostProfile(user);

        Bundle args = new Bundle();
        args.putSerializable("media", m);
        args.putBoolean("show_large_art", showAlbumArt);

        HAudioSongListFragment f = new HAudioSongListFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        galleryAdapter = new HAbsGalleryAdapter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        galleryAdapter.registerListeners();
    }

    @Override
    public void onPause() {
        super.onPause();
        galleryAdapter.unRegisterListeners();
    }

    @Override
    protected AdapterAudioSongList getAdapter() {
        if (adapter == null) adapter = new Adapter(getActivity());
        return adapter;
    }

    @Override
    public void loadMediaList(int cause) {
        int albumId = (getMedia() == null) ? 0 : getMedia().albumId;
        new HGetAudioMedia(getActivity(), albumId, this).execute();
    }

    @Override
    public void onMediaAction(MediaOptions action, List<AudioModel> models) {
        if (action == MediaOptions.PLAY) {
            List<AudioModel> audioList = adapter.getItems();
            int startIndex = audioList.indexOf(models.get(0));
            galleryAdapter.playAudioStream(audioList, startIndex);
        } else super.onMediaAction(action, models);
    }

    @Override
    protected void showMediaOptionActivity(AudioModel media, View v) {
        int flags = MediaOptionPopWindow.FLAG_VIEW
                | MediaOptionPopWindow.FLAG_LIKE | MediaOptionPopWindow.FLAG_DOWNLOAD;
        super.showMediaOptionActivity(media, v, flags);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        if (view.getId() == AdapterAudioSongList.PLAY) {
            position = position - getListView().getHeaderViewsCount();
            galleryAdapter.playAudioStream(adapter.getItems(), position);
        } else if (view.getId() == AdapterAudioSongList.BTN_DOWNLOAD) {
            position = position - getListView().getHeaderViewsCount();
            galleryAdapter.doMediaDownload(adapter.getItem(position));
        } else super.onItemClick(parent, view, position, id);
    }

    public static class Adapter extends AdapterAudioSongList {
        private int dw;

        public Adapter(Context ctx) {
            super(ctx);
            dw = ctx.getResources().getDisplayMetrics().widthPixels;
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            View view = super.getView(position, convertView, inflater);
            ViewHolder holder = (ViewHolder) view.getTag();

            holder.studio.setVisibility(View.GONE);
            holder.download.setVisibility(View.VISIBLE);

            return view;
        }

        @Override
        protected void loadThumbnail(ImageView imageView, AudioModel m) {
            String url = MediaUtility.appendSize(m.thumbnailUrl, dw / 6);
            Picasso.with(getContext())
                    .load(url).placeholder(R.drawable.img_media_song_thumb_256dp)
                    .into(imageView);
        }
    }//EN

    public static class HGetAudioMedia extends GetAudioMedia {
        private String appUserId;
        private int albumId;

        public HGetAudioMedia(Context context, int albumId, Callback callback) {
            super(context, albumId, callback);
            this.albumId = albumId;
            this.appUserId = new UserAccountBase(context).getUserId();
        }

        @Override
        protected List<AudioModel> doInBackground(Void... params) {
            LocalAPIProvider api = LocalAPIConnection.getInstance().connect();
            String selection = MediaStore.Audio.Media.IS_MUSIC + "=1";
            if (albumId != 0)
                selection += (" AND " + MediaStore.Audio.Media.ALBUM_ID + "=" + albumId);

            String orderBy = MediaStore.Audio.Media.TITLE;
            return api.qAudioMedia(selection, orderBy, appUserId, null);
        }
    }//END
}

package com.copula.hostworld.media.search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.hostworld.media.HAbsGalleryAdapter;
import com.copula.hostworld.media.audio.HAudioSongListFragment;
import com.copula.hostworld.media.video.HVideoTrackFragment;
import com.copula.mediasocial.R;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.StaticCache;
import com.copula.mediasocial.audio.songlist.AdapterAudioSongList;
import com.copula.mediasocial.media.MasterAdapter;
import com.copula.mediasocial.media.MediaOptionPopWindow;
import com.copula.mediasocial.mediasearch.MediaSearchAdapter;
import com.copula.mediasocial.mediasearch.MediaSearchFragment;
import com.copula.mediasocial.mediasearch.SearchMediaHelper;
import com.copula.mediasocial.video.player.VideoPlayerMainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 3/15/17.
 */
public class HMediaSearchFragment extends MediaSearchFragment {
    private Adapter adapter;
    private HAbsGalleryAdapter galleryAdapter;

    public static HMediaSearchFragment instantiate() {
        return new HMediaSearchFragment();
    }

    public void search(String q) {
        q = q.split("[|]+")[0];
        new HMediaSearchHelper(getActivity(), q, this).execute();
    }

    @Override
    protected List<AudioModel> getCachedAudio() {
        String key = SocialMediaBootstrap
                .makeCacheKey("host", MediaModel.AUDIO);
        return StaticCache.getInstance().getCache(key);
    }

    @Override
    protected List<VideoModel> getCachedVideo() {
        String key = SocialMediaBootstrap
                .makeCacheKey("host", MediaModel.VIDEO);
        return StaticCache.getInstance().getCache(key);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        galleryAdapter = new HAbsGalleryAdapter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        galleryAdapter.registerListeners();
    }

    @Override
    public void onPause() {
        super.onPause();
        galleryAdapter.unRegisterListeners();
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        MediaModel m = getItemAtPosition(parent, position);
        if (view.getId() == AdapterAudioSongList.PLAY) {
            if (m.mediaType == MediaModel.VIDEO) {
                VideoPlayerMainActivity.launch(getActivity(), new VideoModel(m));
            } else {
                galleryAdapter.playAudioStream(getAdapter().getItems(), position);
            }
        } else super.onItemClick(parent, view, position, id);
    }

    @Override
    protected void showMediaOptionActivity(MediaModel media, View v) {
        int flags = MediaOptionPopWindow.FLAG_VIEW
                | MediaOptionPopWindow.FLAG_LIKE | MediaOptionPopWindow.FLAG_DOWNLOAD;
        super.showMediaOptionActivity(media, v, flags);
    }

    @Override
    public Adapter getAdapter() {
        return adapter;
    }

    @Override
    public MediaSearchAdapter getSearchAdapter() {
        if (adapter == null) adapter = new Adapter(getActivity());
        return adapter;
    }

    @Override
    protected void doMediaPlay(MediaModel media) {
        if (media.mediaType == MediaModel.VIDEO) {
            VideoPlayerMainActivity.launch(getActivity(), new VideoModel(media));
        } else {
            List<AudioModel> m = new ArrayList<>(1);
            m.add(new AudioModel(media));
            galleryAdapter.playAudioStream(m, 0);
        }
    }

    public static class Adapter extends MediaSearchAdapter {
        private static final int AUDIO_ITEM = 0;

        Adapter(Context context) {
            super(context);
        }

        @Override
        protected MasterAdapter getAdapter(int mediaType) {
            if (mediaType == AUDIO_ITEM) {
                return new HAudioSongListFragment.Adapter(getContext());
            } else {
                return new HVideoTrackFragment.Adapter(getContext());
            }
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            View v = super.getView(position, convertView, inflater);
            v.findViewById(R.id.btn_studio_opt).setVisibility(View.GONE);
            return v;
        }
    }//END

    public static class HMediaSearchHelper extends SearchMediaHelper {
        private String userId, query;

        HMediaSearchHelper(Context ctx, String query, Callback listener) {
            super(ctx, query, listener);
            this.query = query;
            this.userId = new UserAccountBase(ctx).getUserId();
        }

        @Override
        protected SearchBundle doInBackground(Void... params) {
            LocalAPIProvider api = LocalAPIConnection.getInstance().connect();

            SearchBundle bundle = new SearchBundle();
            bundle.audio = api.searchAudio(query, userId);
            bundle.video = api.searchVideo(query, userId);

            return bundle;
        }
    }//END
}

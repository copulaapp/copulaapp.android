package com.copula.hostworld.media;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.hostworld.media.audio.HAudioSongListFragment;
import com.copula.hostworld.media.video.HVideoTrackFragment;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.audio.songlist.GetAudioMedia;
import com.copula.mediasocial.video.GetVideosHelper;

/**
 * Created by heeleaz on 7/9/17.
 */
public class HostedMediaBootstrap extends SocialMediaBootstrap {
    private Context context;

    public HostedMediaBootstrap(Context context) {
        super(context);
        this.context = context;
    }

    protected void executeVideoFetchAsync(GetVideosHelper.Callback callback) {
        GetVideosHelper v = new HVideoTrackFragment
                .HGetVideosHelper(context, callback);
        v.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    protected void executeAudioFetchAsync(GetAudioMedia.Callback callback) {
        GetAudioMedia a = new HAudioSongListFragment
                .HGetAudioMedia(context, 0, callback);
        a.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}

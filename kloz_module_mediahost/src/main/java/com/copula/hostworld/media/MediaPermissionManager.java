package com.copula.hostworld.media;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.app.NotificationHelper.NotificationModel;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.profile.PermissionEntry;

/**
 * Created by heeleaz on 3/11/17.
 */
public class MediaPermissionManager implements NotificationHelper.NotificationPushListener {
    private PermissionRequest pRequestAsync;

    static void
    checkPermission(PermissionListener l, String userId, String hAddr, int perm) {
        new PermissionChecker(userId, hAddr, perm, l).execute();
    }

    void requestPermission(PermissionListener l
            , String userId, String uAddr, String hAddr, int permission) {
        NotificationHelper.getInstance().addListener(this);

        pRequestAsync = new PermissionRequest(userId, uAddr, hAddr, permission, l);
        pRequestAsync.execute();
    }

    @Override
    public void onPushNotification(int broadcast, NotificationModel entry) {
        if (broadcast == NotificationHelper.PERMISSION_UPDATED_NOTIFICATION) {
            PermissionEntry p = (PermissionEntry) entry.data;
            dispatchListenerUiHandler(p.isPermitted(PermissionEntry.SHARE_PERM));
        }
    }//END

    private void dispatchListenerUiHandler(final boolean isPermitted) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                PermissionListener l = pRequestAsync.listener;
                if (isPermitted) {
                    l.onCheckedPermission(PermissionListener.ACTION_REQUEST,
                            PermissionListener.PERMITTED);
                } else {
                    l.onCheckedPermission(PermissionListener.ACTION_REQUEST,
                            PermissionListener.DECLINED);
                }
            }
        });
    }

    public interface PermissionListener {
        int PERMITTED = 1, DECLINED = 2;
        int ACTION_CHECK = 1, ACTION_REQUEST = 2;

        void onCheckedPermission(int action, int result);
    }

    private static class PermissionChecker extends AsyncTask<Object, Object, Integer> {
        private String hostAddr, userId;
        private int permission;
        private PermissionListener listener;

        PermissionChecker(String uid, String hAddr, int perm, PermissionListener l) {
            this.hostAddr = hAddr;
            this.userId = uid;
            this.permission = perm;
            this.listener = l;
        }

        @Override
        protected Integer doInBackground(Object... params) {
            LocalAPIProvider api =
                    LocalAPIConnection.getInstance().connect(hostAddr);
            return api.getPeepPermissionMask(userId);
        }

        @Override
        protected void onPostExecute(Integer result) {
            boolean permitted = (permission & result) == permission;
            listener.onCheckedPermission(PermissionListener.ACTION_CHECK,
                    permitted ? PermissionListener.PERMITTED : PermissionListener.DECLINED);
        }
    }//END

    private static class PermissionRequest extends AsyncTask<Void, Void, Integer> {
        private String hAddress, userId, uAddress;
        private int permission;
        private PermissionListener listener;

        PermissionRequest(String uid, String uAddr, String hAddr, int perm, PermissionListener l) {
            this.hAddress = hAddr;
            this.userId = uid;
            this.uAddress = uAddr;
            this.permission = perm;
            this.listener = l;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            LocalAPIProvider api =
                    LocalAPIConnection.getInstance().connect(hAddress);
            return api.requestPermissionUpdate(userId, uAddress, permission);
        }
    }//END
}

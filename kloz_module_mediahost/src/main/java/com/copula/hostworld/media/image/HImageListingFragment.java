package com.copula.hostworld.media.image;

import android.content.Context;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.mediasocial.image.AdapterImageMediaList;
import com.copula.mediasocial.image.GetImageMedia;
import com.copula.mediasocial.image.ImageListingFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HImageListingFragment extends ImageListingFragment implements AdapterView.OnItemClickListener,
        GetImageMedia.Callback {
    private AdapterImageMediaList adapter;

    public static HImageListingFragment instantiate(UserProfileEntry u) {
        UserSession.setHostProfile(u);
        return new HImageListingFragment();
    }

    @Override
    protected AdapterImageMediaList getAdapter() {
        if (adapter == null) adapter = new Adapter(getActivity());
        return adapter;
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        HImagePlayerMainActivity.launch(getActivity(), adapter.getItems(), position);
    }

    @Override
    public void loadMediaList(int cause) {
        new HGetImageMedia(getActivity(), null, this).execute();
    }

    public static class HGetImageMedia extends GetImageMedia {
        private String appUserId;

        HGetImageMedia(Context context, String libraryName, Callback callback) {
            super(context, libraryName, callback);
            this.appUserId = new UserAccountBase(context).getUserId();
        }

        @Override
        protected List<ImageModel> doInBackground(Void... params) {
            LocalAPIProvider api = LocalAPIConnection.getInstance().connect();

            String selection = null;
            if (libraryName != null) {
                selection = MediaStore.Video.Media.BUCKET_DISPLAY_NAME
                        + "=\"" + libraryName + "\"";
            }
            String orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC";
            return api.qImageMedia(selection, orderBy, appUserId, null);
        }
    }//END

    private class Adapter extends AdapterImageMediaList {
        private int dw;

        Adapter(Context context) {
            super(context);
            dw = context.getResources().getDisplayMetrics().widthPixels;
        }

        @Override
        protected void loadImageThumbnail(ImageView imageView, ImageModel m) {
            String url = MediaUtility.appendSize(m.dataUrl, (int) (dw / 4.3));
            Picasso.with(getContext()).load(url)
                    .placeholder(R.drawable.img_media_image_thumb_256dp).into(imageView);
        }
    }//END
}

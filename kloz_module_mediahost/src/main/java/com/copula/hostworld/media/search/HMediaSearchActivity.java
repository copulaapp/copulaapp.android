package com.copula.hostworld.media.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import com.copula.genericlook.CustomActionBar;
import com.copula.hostworld.media.audio.HAudioPlayerMainActivity;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.player.AudioDPlayerFragment;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayerMainActivity;
import com.copula.support.android.view.widget.EditText;

/**
 * Created by heeleaz on 7/23/17.
 */
public class HMediaSearchActivity extends AppCompatActivity implements View.OnClickListener,
        EditText.TextWatcher, TextView.OnEditorActionListener {
    private EditText edtSearchQuery;
    private HMediaSearchFragment searchFragment;

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, HMediaSearchActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_media_search);

        CustomActionBar actionBar = new CustomActionBar(this);
        actionBar.setView(R.layout.bar_media_search).setBackListener(this);
        actionBar.setBackground(R.drawable.bkg_app_main_actionbar_shadow);
        actionBar.compile();

        edtSearchQuery = (EditText) findViewById(R.id.edt_search_query);
        edtSearchQuery.setTextChangedListener(this);
        edtSearchQuery.setOnEditorActionListener(this);
        edtSearchQuery.requestFocus();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment,
                searchFragment = HMediaSearchFragment.instantiate()).commit();
        ft.replace(R.id.frg_audio_player, AudioDPlayerFragment.instantiate());
        findViewById(R.id.frg_audio_player).setOnClickListener(this);

        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtSearchQuery, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_bar_back) finish();
        else if (v.getId() == R.id.frg_audio_player) {
            HAudioPlayerMainActivity.launch(this);
        }
    }

    @Override
    public void onTextChanged(EditText editText, CharSequence s) {
        searchFragment.__searchInHint(s.toString());
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String q = edtSearchQuery.getTextContent();
            if (q == null || q.isEmpty()) return false;
            searchFragment.search(q);
        }
        return false;
    }
}

package com.copula.hostworld.media.video;

import android.content.Context;
import android.content.Intent;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.mediasocial.video.player.VideoPlayerFragment;
import com.copula.mediasocial.video.player.VideoPlayerMainActivity;

import java.util.List;

/**
 * Created by heeleaz on 9/5/17.
 */
public class HVideoPlayerMainActivity extends VideoPlayerMainActivity {
    public static void launch(Context ctx, List<VideoModel> videos, int start) {
        Intent intent = new Intent(ctx, HVideoPlayerMainActivity.class);

        VideoPlayerMainActivity.media = videos;
        ctx.startActivity(intent.putExtra("startPosition", start));
    }

    @Override
    protected VideoPlayerFragment getPlayerFragment(int p) {
        return HVideoPlayerFragment.instantiate(VideoPlayerMainActivity.media, p);
    }
}

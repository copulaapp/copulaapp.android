package com.copula.hostworld.media;

import android.os.Handler;
import android.os.Looper;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.util.ConnectionConstant;
import com.copula.mediasocial.media.AbsGalleryFragment;

import java.util.List;

/**
 * Created by heeleaz on 3/10/17.
 */
public class HAbsGalleryAdapter<T extends MediaModel> extends HAbsMediaAdapter<T> implements NotificationHelper
        .NotificationPushListener {
    private static final int NETWORK_RELOAD = -1;
    private boolean pendingPageReload = false;
    private AbsGalleryFragment activity;

    public HAbsGalleryAdapter(AbsGalleryFragment absGalleryFragment) {
        super(absGalleryFragment);
        this.activity = absGalleryFragment;
    }

    public void registerListeners() {
        NotificationHelper.getInstance().addListener(this);

        if (pendingPageReload) {
            activity.loadMediaList(AbsGalleryFragment.RESUME_RELOAD);
        }
        pendingPageReload = false;
    }

    public void unRegisterListeners() {
        NotificationHelper.getInstance().removeListener(this);
    }

    public void playAudioStream(List<AudioModel> songList, int position) {
        activity.playAudio(songList, AudioController.STREAM, position);
    }

    @Override
    public void onPushNotification(int b, NotificationHelper.NotificationModel entry) {
        if (b != ConnectionConstant.APP_WIFI_CONNECTION || activity == null)
            return;

        Looper looper = activity.getActivity().getMainLooper();
        new Handler(looper).post(new Runnable() {
            @Override
            public void run() {
                activity.loadMediaList(NETWORK_RELOAD);
            }
        });
    }

    public UserProfileEntry getHostProfile() {
        return UserSession.getHostProfile();
    }
}
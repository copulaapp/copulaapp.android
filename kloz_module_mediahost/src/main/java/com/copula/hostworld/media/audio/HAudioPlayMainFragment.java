package com.copula.hostworld.media.audio;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.util.MediaUtility;
import com.copula.hostworld.media.HAbsMediaAdapter;
import com.copula.mediasocial.R;
import com.copula.mediasocial.SocialMediaBootstrap;
import com.copula.mediasocial.StaticCache;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayMainFragment;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.util.BlurTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by eliasigbalajobi on 5/23/16.
 */
public class HAudioPlayMainFragment extends AudioPlayMainFragment
        implements View.OnClickListener, AdapterView.OnItemClickListener, IFragmentBackPress {
    private View btnDownload;
    private HAbsMediaAdapter mediaAdapter;

    public static HAudioPlayMainFragment instantiate() {
        return new HAudioPlayMainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = super.onCreateView(inflater, container, si);
        (btnDownload = v.findViewById(R.id.btn_download)).setOnClickListener(this);

        btnDownload.setVisibility(View.VISIBLE);
        v.findViewById(R.id.btn_studio_opt).setVisibility(View.GONE);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mediaAdapter = new HAbsMediaAdapter(this);
    }

    @Override
    protected void
    processMediaPlayerBackground(AudioModel media, ImageView big, ImageView small) {
        int h = getContext().getResources().getDisplayMetrics().widthPixels;
        String uri = MediaUtility.appendSize(media.thumbnailUrl, h);

        Picasso.with(getActivity()).load(uri)
                .placeholder(R.drawable.bkg_main_media_audio_player)
                .transform(new BlurTransform(getActivity(), 25)).into(big);
        Picasso.with(getActivity()).load(uri)
                .resize(350, 350).centerInside()
                .placeholder(R.drawable.img_media_song_thumb_512dp).into(small);
    }

    @Override
    protected void populateQueueListEntries(List<AudioModel> media) {
        String key = SocialMediaBootstrap
                .makeCacheKey("host", MediaModel.AUDIO);
        media = StaticCache.getInstance().getCache(key);
        if (media != null) super.populateQueueListEntries(media);
    }

    @Override
    protected void configureListTrackAndPosition(List<AudioModel> mediaList) {
        String key = SocialMediaBootstrap
                .makeCacheKey("host", MediaModel.AUDIO);
        mediaList = StaticCache.getInstance().getCache(key);
        if (mediaList != null) super.configureListTrackAndPosition(mediaList);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getController().setQueue(getPlaylistAdapter().getItems());
        getController().setStreamType(AudioController.STREAM);
        getController().startPlay(position, true);
    }

    @Override
    public void onClick(View v) {
        if (v == btnDownload) {
            mediaAdapter.doMediaDownload(getController().getCurrentTrack());
        } else super.onClick(v);
    }
}

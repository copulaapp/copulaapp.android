package com.copula.hostworld.media.image;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.media.VideoController;
import com.copula.hostworld.media.HAbsMediaAdapter;
import com.copula.mediasocial.R;
import com.copula.mediasocial.image.player.ImagePlayerFragment;

import java.util.List;

/**
 * Created by eliasigbalajobi on 4/24/16.
 */
public class HImagePlayerFragment extends ImagePlayerFragment implements View.OnClickListener {
    private View btnDownload;
    private HAbsMediaAdapter mediaAdapter;

    public static HImagePlayerFragment instantiate(List<ImageModel> m, int p) {
        com.copula.mediasocial.image.player.ImagePlayerFragment.imageList = m;

        Bundle bundle = new Bundle();
        bundle.putInt("startPosition", p);
        HImagePlayerFragment fragment = new HImagePlayerFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = super.onCreateView(inflater, container, si);
        (btnDownload = v.findViewById(R.id.btn_download)).setOnClickListener(this);
        v.findViewById(R.id.btn_studio_opt).setVisibility(View.GONE);
        btnDownload.setVisibility(View.VISIBLE);
        v.findViewById(R.id.btn_media_option).setVisibility(View.GONE);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mediaAdapter = new HAbsMediaAdapter(this);
    }

    @Override
    protected int getStreamType() {
        return VideoController.STREAM;
    }

    @Override
    public void onClick(View v) {
        if (v == btnDownload) {
            ImageModel m = imageList.get(viewPager.getCurrentItem());
            if (m != null) mediaAdapter.doMediaDownload(m);
        } else super.onClick(v);
    }
}
package com.copula.hostworld.media;

import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.downloader.MediaDownloadClient;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.localservice.profile.PermissionEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;

/**
 * Created by heeleaz on 6/26/17.
 */
public class DownloadRequestHelper extends AsyncTask<Void, Void, Boolean> implements MediaPermissionManager
        .PermissionListener, PermissionRequestDialog.DialogCallback {
    private MediaPermissionManager permissionManager;
    private MediaModel media;

    private FragmentActivity context;
    private PermissionRequestDialog permissionDialog;

    public DownloadRequestHelper(FragmentActivity context, MediaModel media) {
        this.context = context;
        this.media = media;
        permissionManager = new MediaPermissionManager();
    }

    public void startDownload() {
        String hAddr = UserSession.getHostAddress();
        String userId = UserSession.getAppUserId(context);
        MediaPermissionManager.checkPermission(
                this, userId, hAddr, PermissionEntry.COPY_PERM);
    }

    private PermissionRequestDialog requestDownloadPermission() {
        String uAddr = UserSession.getAppProfile(context).ipAddress;
        String hAddr = UserSession.getHostAddress();
        String userId = UserSession.getAppUserId(context);

        permissionManager.requestPermission(
                this, userId, uAddr, hAddr, PermissionEntry.COPY_PERM);
        return showPermissionDialog(PermissionRequestDialog.VIEW_PERMISSION_REQUEST);
    }

    private void doRealDownloadMedia() {
        new DownloadRequestHelper(context, media).execute();
    }

    private PermissionRequestDialog showPermissionDialog(int view) {
        return PermissionRequestDialog
                .show(context.getSupportFragmentManager(), view, this);
    }

    @Override
    public void onCheckedPermission(int action, int result) {
        if (permissionDialog != null) permissionDialog.dismiss();

        if (action == ACTION_CHECK && result == PERMITTED) {
            doRealDownloadMedia();
        } else if (action == ACTION_CHECK && result == DECLINED) {
            permissionDialog = requestDownloadPermission();
        } else if (action == ACTION_REQUEST) {
            if (result == PERMITTED) doRealDownloadMedia();
            else permissionDialog = showPermissionDialog(
                    PermissionRequestDialog.VIEW_PERMISSION_DENIED);
        }
    }

    @Override
    protected void onPreExecute() {
        Toast.makeText(context, R.string.msg_download_start, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        String path = getMediaDownloadPath(media);
        MediaDownloadClient.getInstance().queue(media, path);

        String userId = UserSession.getAppUserId(context);
        notifyDownload(media, userId);
        return true;
    }

    private String getMediaDownloadPath(MediaModel media) {
        return MediaUtility.getDownloadMediaDir(
                media.mediaType, media.fileName).getAbsolutePath();
    }

    private void notifyDownload(final MediaModel media, final String peeperId) {
        int action = MediaCaptionEntry.ACTION_DOWNLOAD;
        LocalAPIProvider api = LocalAPIConnection.getInstance().connect();
        api.mediaAction(peeperId, media.mediaType, media.mediaId, action);
    }

    @Override
    public void permissionRequestAction(PermissionRequestDialog d, int action) {
        if (action == RETRY) doRealDownloadMedia();
        else d.dismiss();
    }
}

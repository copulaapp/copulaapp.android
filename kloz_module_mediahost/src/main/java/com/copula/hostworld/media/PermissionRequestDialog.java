package com.copula.hostworld.media;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.copula.functionality.app.UserSession;
import com.copula.mediasocial.R;
import com.copula.support.android.view.widget.TextView;

/**
 * Created by heeleaz on 3/14/17.
 */
public class PermissionRequestDialog extends DialogFragment implements View.OnClickListener {
    private static final String TAG = PermissionRequestDialog.class.getSimpleName();
    static int VIEW_PERMISSION_DENIED = 1, VIEW_PERMISSION_REQUEST = 2;

    private TextView txtPermissionMessage;
    private View btnRetryPerm, btnCancelPerm;
    private ImageView imgResultEmocion;
    private DialogCallback callback;

    public static PermissionRequestDialog show(FragmentManager fm, int action, DialogCallback callback) {
        PermissionRequestDialog fragment = new PermissionRequestDialog();
        Bundle args = new Bundle(1);
        args.putInt("action", action);
        fragment.setArguments(args);

        fragment.callback = callback;

        FragmentTransaction ft = fm.beginTransaction();
        Fragment old = fm.findFragmentByTag(TAG);
        if (old != null) ft.remove(old);

        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        fragment.setCancelable(false);
        ft.add(fragment, TAG).show(fragment).commit();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.diag_permission_request, container, false);
        (btnCancelPerm = view.findViewById(R.id.btn_cancel)).setOnClickListener(this);
        (btnRetryPerm = view.findViewById(R.id.btn_retry)).setOnClickListener(this);
        txtPermissionMessage = (TextView) view.findViewById(R.id.txt_message);
        imgResultEmocion = (ImageView) view.findViewById(R.id.img_result_emocion);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments().getInt("action") == VIEW_PERMISSION_REQUEST) {
            btnRetryPerm.setVisibility(View.GONE);
            imgResultEmocion.setVisibility(View.GONE);

            String msg = getString(R.string.msg_permission_request);
            String host = UserSession.getHostProfile().username;
            txtPermissionMessage.setText(msg.replace("?", host));
        } else {
            imgResultEmocion.setImageResource(R.drawable.ic_sad_emocion_grey_128px);
            txtPermissionMessage.setText(R.string.msg_permission_rejected);
            btnRetryPerm.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnCancelPerm) {
            dismiss();
        } else if (v == btnRetryPerm) {
            if (callback != null)
                callback.permissionRequestAction(this, DialogCallback.RETRY);
        }
    }

    interface DialogCallback {
        int RETRY = 0;

        void permissionRequestAction(PermissionRequestDialog d, int action);
    }
}

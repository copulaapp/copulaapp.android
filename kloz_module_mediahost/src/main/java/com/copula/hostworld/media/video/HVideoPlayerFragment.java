package com.copula.hostworld.media.video;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.media.VideoController;
import com.copula.hostworld.media.HAbsMediaAdapter;
import com.copula.mediasocial.R;
import com.copula.mediasocial.video.player.PlayControllerFragment;
import com.copula.mediasocial.video.player.VideoPlayerFragment;
import com.copula.support.android.content.IFragmentBackPress;
import com.copula.support.android.view.widget.Button;

import java.util.List;

/**
 * Created by heeleaz on 6/7/17.
 */
public class HVideoPlayerFragment extends VideoPlayerFragment implements IFragmentBackPress {
    private Button btnDownload;
    private HAbsMediaAdapter mediaAdapter;

    public static HVideoPlayerFragment instantiate(List<VideoModel> m, int p) {
        PlayControllerFragment.mediaList = m;

        Bundle args = new Bundle(1);
        args.putInt("position", p);
        HVideoPlayerFragment fragment = new HVideoPlayerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mediaAdapter = new HAbsMediaAdapter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = super.onCreateView(inflater, container, si);
        (btnDownload = (Button)
                v.findViewById(R.id.btn_download)).setOnClickListener(this);
        btnDownload.setVisibility(View.VISIBLE);

        v.findViewById(R.id.btn_studio_opt).setVisibility(View.GONE);
        v.findViewById(R.id.btn_media_share).setVisibility(View.GONE);

        return v;
    }

    @Override
    protected int getStreamType() {
        return VideoController.STREAM;
    }

    @Override
    public void onClick(View v) {
        if (v == btnDownload) {
            mediaAdapter.doMediaDownload(getController().getCurrentTrack());
        } else super.onClick(v);
    }
}

package com.copula.hostworld.media.audio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.hostworld.media.search.HMediaSearchActivity;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.songlist.AudioSongListActivity;

/**
 * Created by heeleaz on 5/31/17.
 */
public class HAudioSongListActivity extends AudioSongListActivity {
    public static void launch(Context ctx, AudioModel m, UserProfileEntry u) {
        UserSession.setHostProfile(u);

        Intent intent = new Intent(ctx, HAudioSongListActivity.class);
        intent.putExtra("media", m).putExtra("user", u);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected HAudioSongListFragment setupSongsListFragment() {
        UserProfileEntry u = (UserProfileEntry)
                getIntent().getSerializableExtra("user");
        AudioModel m = (AudioModel) getIntent().getSerializableExtra("media");
        return HAudioSongListFragment.instantiate(u, m, true);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.frg_audio_player) {
            HAudioPlayerMainActivity.launch(this);
        } else if (v.getId() == R.id.btn_search) {
            HMediaSearchActivity.launch(this);
        } else super.onClick(v);
    }
}

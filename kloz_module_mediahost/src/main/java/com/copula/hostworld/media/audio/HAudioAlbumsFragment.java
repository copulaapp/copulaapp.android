package com.copula.hostworld.media.audio;

import android.content.Context;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.hostworld.media.HAbsGalleryAdapter;
import com.copula.mediasocial.R;
import com.copula.mediasocial.audio.album.AdapterAudioAlbums;
import com.copula.mediasocial.audio.album.AudioAlbumFragment;
import com.copula.mediasocial.audio.album.GetAudioAlbums;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by heeleaz on 7/21/17.
 */
public class HAudioAlbumsFragment extends AudioAlbumFragment {
    private Adapter adapter;
    private HAbsGalleryAdapter galleryAdapter;

    public static HAudioAlbumsFragment instantiate(UserProfileEntry user) {
        UserSession.setHostProfile(user);
        return new HAudioAlbumsFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        galleryAdapter = new HAbsGalleryAdapter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        galleryAdapter.registerListeners();
    }

    @Override
    public void onPause() {
        super.onPause();
        galleryAdapter.unRegisterListeners();
    }

    @Override
    public void loadMediaList(int cause) {
        new HGetAudioAlbums(getContext(), this).execute();
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        AudioModel media = getItemAtPosition(parent, position);
        HAudioSongListActivity.launch(getActivity(), media, galleryAdapter.getHostProfile());
    }

    @Override
    public Adapter getAdapter() {
        if (adapter == null) adapter = new Adapter(getActivity());
        return adapter;
    }

    public static class HGetAudioAlbums extends GetAudioAlbums {
        private String appUserId;

        HGetAudioAlbums(Context context, Callback callback) {
            super(context, callback);
            this.appUserId = new UserAccountBase(context).getUserId();
        }

        public void execute(Callback callback) {
            super.execute();
        }

        @Override
        protected List<AudioModel> doInBackground(Void... params) {
            LocalAPIProvider api = LocalAPIConnection.getInstance().connect();

            String orderBy = MediaStore.Audio.Media.ARTIST + " DESC";
            String selection = MediaStore.Audio.Media.IS_MUSIC + "=1";
            return api.qAudioMedia(selection, orderBy, appUserId);
        }
    }

    private class Adapter extends AdapterAudioAlbums {
        private int dw;

        Adapter(Context context) {
            super(context);
            dw = context.getResources().getDisplayMetrics().widthPixels;
        }

        @Override
        protected void loadThumbnail(ImageView imageView, AudioModel m) {
            String url = MediaUtility.appendSize(m.thumbnailUrl, dw / 3);
            Picasso.with(getContext()).load(url)
                    .placeholder(R.drawable.img_media_audio_album_256dp)
                    .into(imageView);
        }
    }//END
}

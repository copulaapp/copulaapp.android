package com.copula.hostworld.media.video;

import android.content.Context;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.mediasocial.R;
import com.copula.mediasocial.media.MediaOptionPopWindow;
import com.copula.mediasocial.video.AdapterVideoMediaList;
import com.copula.mediasocial.video.GetVideosHelper;
import com.copula.mediasocial.video.VideoTracksFragment;
import com.copula.mediasocial.video.player.VideoPlayerMainActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HVideoTrackFragment extends VideoTracksFragment implements OnItemClickListener, GetVideosHelper.Callback {

    private AdapterVideoMediaList adapter;

    public static HVideoTrackFragment instantiate(UserProfileEntry user) {
        UserSession.setHostProfile(user);
        return new HVideoTrackFragment();
    }

    @Override
    public void loadMediaList(int cause) {
        new HGetVideosHelper(getActivity(), this).execute();
    }

    @Override
    public AdapterVideoMediaList getAdapter() {
        if (adapter == null) adapter = new Adapter(getActivity());
        return adapter;
    }

    @Override
    protected void play(int position) {
        VideoPlayerMainActivity.launch(getActivity(), adapter.getItems(), position);
    }

    @Override
    protected void showMediaOptionActivity(VideoModel media, View v) {
        int flags = MediaOptionPopWindow.FLAG_VIEW
                | MediaOptionPopWindow.FLAG_LIKE | MediaOptionPopWindow.FLAG_DOWNLOAD;
        super.showMediaOptionActivity(media, v, flags);
    }

    public static class Adapter extends AdapterVideoMediaList {
        private int dw;

        public Adapter(Context context) {
            super(context);
            dw = context.getResources().getDisplayMetrics().widthPixels;
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            View view = super.getView(position, convertView, inflater);
            ViewHolder viewHolder = (ViewHolder) view.getTag();

            viewHolder.studio.setVisibility(View.GONE);
            viewHolder.download.setVisibility(View.VISIBLE);

            return view;
        }

        @Override
        public void loadThumbnailImage(ImageView thumbView, VideoModel media) {
            String url = MediaUtility.appendSize(media.thumbnailUrl, dw / 6);
            Picasso.with(getContext()).load(url)
                    .placeholder(R.drawable.img_media_video_thumb2_256dp).into(thumbView);
        }
    }//END

    public static class HGetVideosHelper extends GetVideosHelper {
        private String appUserId;

        public HGetVideosHelper(Context ctx, Callback callback) {
            super(ctx, callback);
            this.appUserId = new UserAccountBase(ctx).getUserId();
        }

        @Override
        protected List<VideoModel> doInBackground(Void... voids) {
            LocalAPIProvider api = LocalAPIConnection.getInstance().connect();
            String orderBy = MediaStore.Video.Media.DATE_ADDED + " DESC";
            return api.qVideoMedia(null, orderBy, appUserId, null);
        }
    }//END
}
package com.copula.hostworld.media;

import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.mediasocial.media.AbsMediaFragment;
import com.copula.mediasocial.media.MediaOptions;

/**
 * Created by heeleaz on 3/10/17.
 */
public class HAbsMediaAdapter<T extends MediaModel> {
    private AbsMediaFragment activity;

    public HAbsMediaAdapter(AbsMediaFragment parent) {
        this.activity = parent;
    }

    public void doMediaDownload(T media) {
        new DownloadRequestHelper(activity.getActivity(), media).startDownload();
        activity.onMediaAction(MediaOptions.DOWNLOAD, activity.toArray(media));
    }

    public UserProfileEntry getHostProfile() {
        return UserSession.getHostProfile();
    }
}
package com.copula.hostworld.media.image;

import android.content.Context;
import android.content.Intent;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.mediasocial.image.player.ImagePlayerMainActivity;

import java.util.List;

/**
 * Created by heeleaz on 9/5/17.
 */
public class HImagePlayerMainActivity extends ImagePlayerMainActivity {
    public static void launch(Context ctx, List<ImageModel> images, int p) {
        if (images == null) return;

        Intent intent = new Intent(ctx, HImagePlayerMainActivity.class);
        ImagePlayerMainActivity.media = images;
        intent.putExtra("startPosition", p);
        ctx.startActivity(intent);
    }

    @Override
    protected HImagePlayerFragment getPlayerFragment(int position) {
        return HImagePlayerFragment
                .instantiate(ImagePlayerMainActivity.media, position);
    }
}

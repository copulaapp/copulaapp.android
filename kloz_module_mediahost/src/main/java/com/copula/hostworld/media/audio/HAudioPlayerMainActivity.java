package com.copula.hostworld.media.audio;

import android.content.Context;
import android.content.Intent;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayMainFragment;
import com.copula.mediasocial.audio.player.mainplayer.AudioPlayerMainActivity;

/**
 * Created by heeleaz on 9/5/17.
 */
public class HAudioPlayerMainActivity extends AudioPlayerMainActivity {
    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, HAudioPlayerMainActivity.class));
    }

    @Override
    protected AudioPlayMainFragment initPlayerFragment() {
        return new HAudioPlayMainFragment();
    }
}

package com.copula.hostworld.onlinehost.core;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.app.NotificationHelper.NotificationModel;
import com.copula.functionality.app.UserSession;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.connection.client.LocalServerStateMonitor;
import com.copula.functionality.localservice.profile.*;
import com.copula.functionality.util.ConnectionConstant;
import com.copula.functionality.util.MediaUtility;
import com.copula.hostworld.connection.core.ConnectivityHelper;
import com.copula.support.android.network.HotspotManager;
import com.copula.support.android.network.NetworkHelper;
import com.copula.support.util.SimpleIPScanner;

import java.net.InetAddress;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by heeleaz on 1/14/17.
 */
public class HostScannerService extends IntentService implements SimpleIPScanner.PingResultListener,
        NotificationHelper.NotificationPushListener {
    private InetAddress systemAddress;
    private NetworkHelper networkHelper;
    private DaoConnectsProfile daoFriendProfile;
    private DaoPeepPermission daoPeepPermission;
    private OnlineHostCacheDao daoCache;
    private ExecutorService threadPool = Executors.newFixedThreadPool(2);
    private HostScannerListener listener;
    private Handler uiHandler;
    private int networkType;
    private LocalServerStateMonitor localServerMonitor;
    private MyBinder myBinder = new MyBinder();

    private LocalAPIProvider apiProvider;

    public HostScannerService() {
        super(HostScannerService.class.getName());
    }

    public void setListener(HostScannerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        networkHelper = new NetworkHelper(this);
        daoCache = new OnlineHostCacheDao(this);
        daoFriendProfile = new DaoConnectsProfile(this);
        daoPeepPermission = new DaoPeepPermission(this);
        localServerMonitor = new LocalServerStateMonitor(this);
        apiProvider = LocalAPIConnection.newInstance(500).connect();

        NotificationHelper.getInstance().addListener(this);
        uiHandler = new Handler(Looper.getMainLooper());

        //broadcast listening to.. hotspot and wifi change listener
        IntentFilter filter = new IntentFilter();
        filter.addAction(HotspotManager.NETWORK_AP_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(new ConnectionBroadcastReceiver(), filter);
    }

    public void checkConnectionStateAndScan() {
        systemAddress = networkHelper.getSystemAddress();
        if (systemAddress == null) return;


        networkType = ConnectivityHelper.getConnectedNetwork(this);
        if (!ConnectivityHelper.isAppInitiatedNetwork(this, networkType)) {
            daoCache.truncateTable();

            listener.onNonViableNetwork(-1);
            return;
        }

        List<UserProfileEntry> models = daoCache.getFriends(null);
        if (models != null && models.size() > 0) {
            listener.onListCachedHost(daoCache.getFriends(null));
        } else listener.onScanProgress();

        daoCache.setHostConnectStatus(false);
        SimpleIPScanner ipScanner = new SimpleIPScanner();
        ipScanner.setPingTimeout(300);
        ipScanner.startAsync(systemAddress, this);
    }

    private void prepareUserAsConnect(UserProfileEntry user) {
        String imgUrl = MediaUtility
                .appendSize(user.imgUrl, MediaUtility.DP_UPLOAD_DIM);
        UsersProfileResource.putProfileImage(user.userId, imgUrl);
        daoFriendProfile.putConnect(user);
        daoPeepPermission.addPermission(user.userId, PermissionEntry.READ_PERM);
    }

    @Override
    public void onAddressAlive(InetAddress address) {
        String hostAddress = address.getHostAddress();
        if (hostAddress.equals(systemAddress.getHostAddress())) return;

        apiProvider.setHostAddress(hostAddress);
        final UserProfileEntry profile = apiProvider.getUserProfile();
        if (profile == null) return;

        daoCache.addUser(profile);//add user to online list

        if (ConnectionConstant.SERVER_REFRESHED) {
            UserProfileEntry me = UserSession.getAppProfile(this);
            me.connectStatus = UserProfileEntry.ONLINE;
            apiProvider.hostConnectBeep(me);
        }

        threadPool.submit(new Runnable() {
            @Override
            public void run() {
                prepareUserAsConnect(profile);
            }
        });
    }

    @Override
    public void onScanComplete(int reachableHostCount, int scanCount) {
        this.networkType = ConnectivityHelper.getConnectedNetwork(this);
        if (networkType == -1) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onNonViableNetwork(-1);
                }
            });
            return;
        }

        final List<UserProfileEntry> hosts = daoCache.getFriends(null);
        if (hosts == null || hosts.size() == 0) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onEmptyHostList();
                }
            });
        } else {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (listener != null) listener.onListCachedHost(hosts);
                }
            });
        }

        ConnectionConstant.SERVER_REFRESHED = false;
    }


    @Override
    public void onPushNotification(int broadcast, NotificationModel model) {
        if (broadcast == NotificationHelper.HOST_CONNECT_NOTIFICATION) {
            UserProfileEntry profileModel = (UserProfileEntry) model.data;
            daoCache.addUser(profileModel);
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onListCachedHost(daoCache.getFriends(null));
                }
            });
            this.prepareUserAsConnect(profileModel);
        }
    }//END

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public interface HostScannerListener {
        void onNonViableNetwork(int cause);

        void onListCachedHost(List<UserProfileEntry> hosts);

        void onEmptyHostList();

        void onScanProgress();
    }

    private class ConnectionBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(HotspotManager.NETWORK_AP_STATE_CHANGED_ACTION)) {
                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);
                if (state == 12) return; //ignore if enabling

            } else if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (info == null || info.getState() == NetworkInfo.State.CONNECTING) return;
            }

            ConnectionConstant.SERVER_REFRESHED = true;
            localServerMonitor.startServer(true);//restart server::ip_changed

            new OnlineHostCacheDao(context).truncateTable();//reset online user cache
            if (listener != null) checkConnectionStateAndScan();
            NotificationHelper.getInstance().
                    dispatch(ConnectionConstant.APP_WIFI_CONNECTION, "connect");
        }
    }

    public class MyBinder extends Binder {
        public HostScannerService getService() {
            return HostScannerService.this;
        }
    }
}

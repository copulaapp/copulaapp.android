package com.copula.hostworld.onlinehost;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.genericlook.ViewAnimator;
import com.copula.hostworld.HostMediaPageActivity;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.ConnectionStatusFragment;
import com.copula.hostworld.onlinehost.core.HostScannerService;
import com.copula.support.android.view.widget.ListView;

import java.util.List;

/**
 * Created by heeleaz on 6/3/17.
 */
public class ConnectedHostListActivity extends FragmentActivity implements View.OnClickListener, AdapterView
        .OnItemClickListener, HostScannerService.HostScannerListener, ServiceConnection {
    private View vEmptyList;
    private ListView listView;
    private AdaptConnectedHostList adapter;

    private HostScannerService hostScannerService;
    private boolean serviceBound;


    public static void launch(Context context) {
        context.startActivity(new Intent(context, ConnectedHostListActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.frg_host_online_list);

        findViewById(R.id.btn_bar_back).setOnClickListener(this);
        findViewById(R.id.btn_reload).setOnClickListener(this);
        (listView = (ListView) findViewById(R.id.lst)).setOnItemClickListener(this);
        (vEmptyList = findViewById(R.id.view_empty_list)).setOnClickListener(this);

        listView.setAdapter(this.adapter = new AdaptConnectedHostList(this));
        listView.setOnItemClickListener(this);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frg_connection_status, ConnectionStatusFragment.instantiate());
        ft.commit();

        this.initMainContentViewDesign();
    }

    private void initMainContentViewDesign() {
        final View mainContent = findViewById(R.id.view_main_content);
        mainContent.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {//
                    @Override
                    public void onGlobalLayout() {
                        ViewGroup.LayoutParams p = mainContent.getLayoutParams();
                        p.height = mainContent.getMeasuredWidth();
                        mainContent.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                        ViewAnimator.animateJumpIn(mainContent, 700);
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent service = new Intent(this, HostScannerService.class);
        bindService(service, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (serviceBound) unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        hostScannerService
                = ((HostScannerService.MyBinder) service).getService();
        hostScannerService.setListener(this);
        hostScannerService.checkConnectionStateAndScan();
        serviceBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        serviceBound = false;
    }

    @Override
    public void onClick(View v) {
        if (v == vEmptyList) {
            if (hostScannerService != null)
                hostScannerService.checkConnectionStateAndScan();
        } else if (v.getId() == R.id.btn_reload) {
            if (hostScannerService != null)
                hostScannerService.checkConnectionStateAndScan();
        } else if (v.getId() == R.id.btn_bar_back) finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserProfileEntry model = (UserProfileEntry) parent.getItemAtPosition(position);
        if (model == null) return;

        if (view.getId() == AdaptConnectedHostList.OPTION_VIEW) {
            ConnectUserOptionDialog.launch(getSupportFragmentManager(),
                    model.userId, model.ipAddress);
        } else {
            HostMediaPageActivity.launch(this, model);
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onNonViableNetwork(int cause) {
    }

    @Override
    public void onListCachedHost(List<UserProfileEntry> hosts) {
        if (hosts == null) return;

        adapter.clear();//remove previous data from adapter
        adapter.addHandlers(hosts);
        listView.setListViewHeightBasedOnChildren();
        adapter.notifyDataSetChanged();

        DesignHelper.showOnlyView((View) listView.getParent());
    }

    @Override
    public void onScanProgress() {
        DesignHelper.showOnlyView(findViewById(R.id.view_list_progress));
    }

    @Override
    public void onEmptyHostList() {
        DesignHelper.showOnlyView(vEmptyList);
    }


}

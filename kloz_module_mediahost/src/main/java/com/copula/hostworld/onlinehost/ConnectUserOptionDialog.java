package com.copula.hostworld.onlinehost;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.localservice.connection.client.LocalAPIProvider;
import com.copula.functionality.localservice.connection.client.LocalAPIConnection;
import com.copula.functionality.localservice.profile.*;
import com.copula.hostworld.R;
import com.copula.support.android.view.widget.Button;
import com.copula.support.android.view.widget.ImageView;
import com.copula.support.android.view.widget.ResultBundle;
import com.copula.support.android.view.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.concurrent.Executors;

/**
 * Created by heeleaz on 4/17/17.
 */
public class ConnectUserOptionDialog extends DialogFragment {
    private static String TAG = ConnectUserOptionDialog.class.getSimpleName();

    private String userId, rpAddress, appUserId;

    private TextView txtUsername, txtMessage, txtBio;
    private ImageView imgProfileDP;
    private Button btnAllow, btnDisallow;

    private DaoPeepPermission DaoPeepsPermission;

    public static void launch(FragmentManager fm, String userId, String rpAddress) {
        ConnectUserOptionDialog dialog = new ConnectUserOptionDialog();

        Bundle bundle = new Bundle(2);
        bundle.putString("user_id", userId);
        bundle.putString("rp_address", rpAddress);
        dialog.setArguments(bundle);

        Fragment old = fm.findFragmentByTag(TAG);
        FragmentTransaction transaction = fm.beginTransaction();
        if (old != null) transaction.remove(old);

        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        transaction.add(dialog, TAG).show(dialog).commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.diag_connect_user_option, container, false);
        txtUsername = (TextView) view.findViewById(R.id.txt_username);
        txtMessage = (TextView) view.findViewById(R.id.txt_message);
        txtBio = (TextView) view.findViewById(R.id.txt_bio);
        imgProfileDP = (ImageView) view.findViewById(R.id.img_profile_image);
        btnAllow = (Button) view.findViewById(R.id.btn_allow);
        btnDisallow = (Button) view.findViewById(R.id.btn_disallow);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.DaoPeepsPermission = new DaoPeepPermission(getActivity());

        this.userId = getArguments().getString("user_id");
        this.rpAddress = getArguments().getString("rp_address");

        UserAccountBase userAccountBase = new UserAccountBase(getActivity());
        this.appUserId = userAccountBase.getUserId();

        if (userId == null) return;

        DaoConnectsProfile daoFriendProfile = new DaoConnectsProfile(getActivity());
        UserProfileEntry userProfile = daoFriendProfile.getConnect(userId);
        if (userProfile == null) {
            userProfile = userAccountBase.getUserProfile();
            if (!userProfile.userId.equals(userId)) return;//end
        }

        txtUsername.setText(userProfile.username);
        txtBio.setText(userProfile.bio);
        txtMessage.setText(getString(R.string.msg_launch_permission)
                .replace("?", userProfile.username));

        Picasso.with(getActivity()).load(userProfile.imgUrl)
                .placeholder(R.drawable.img_person_black_96dp).into(imgProfileDP);

        btnAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int perm = PermissionEntry.COPY_PERM | PermissionEntry.SHARE_PERM;
                DaoPeepsPermission.setPermission(userId, perm, true);
                dismiss();
            }
        });

        btnDisallow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int perm = PermissionEntry.COPY_PERM | PermissionEntry.SHARE_PERM;
                DaoPeepsPermission.setPermission(userId, perm, false);
                dismiss();
            }
        });
    }

    private void updatePermissionAsync() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                PermissionEntry p = DaoPeepsPermission.getPermission(userId);
                LocalAPIProvider api = LocalAPIConnection
                        .getInstance().connect(rpAddress);
                api.permissionUpdateBeep(appUserId, p.permissionMask);
            }
        });
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        if (rpAddress != null) updatePermissionAsync();

        Activity activity = getActivity();
        if (activity instanceof ResultBundle) {
            ResultBundle binder = (ResultBundle) activity;
            binder.message("finish", null);
        }
        super.onDismiss(dialog);
    }//end
}

package com.copula.hostworld.onlinehost;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.MediaUtility;
import com.copula.hostworld.R;
import com.copula.support.android.view.widget.*;
import com.squareup.picasso.Picasso;

public class AdaptConnectedHostList extends BaseAdapter<UserProfileEntry> {
    public static final int OPTION_VIEW = R.id.btn_option;
    private int displayWidth;

    AdaptConnectedHostList(Context context) {
        super(context);
        displayWidth = getContext().getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_online_users_list, null, false);
            holder.name = (TextView) convertView.findViewById(R.id.txt_profile_name);
            holder.bio = (TextView) convertView.findViewById(R.id.txt_profile_bio);
            holder.dp = (ImageView) convertView.findViewById(R.id.img_profile_image);
            holder.option = (Button) convertView.findViewById(R.id.btn_option);
            holder.onlineStatus = (ImageView) convertView.findViewById(R.id.img_online_status);


            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        final View fConvertView = convertView;
        holder.option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ListView) fConvertView.getParent()).performItemClick(view,
                        position, position);
            }
        });

        UserProfileEntry ph = getItem(position);
        holder.name.setText(ph.username);
        holder.bio.setText(ph.bio);

        holder.onlineStatus.setImageResource(R.drawable.ic_indicator_online_26dp);

        String url = MediaUtility.appendSize(ph.imgUrl, displayWidth / 5);
        Picasso.with(getContext()).load(url)
                .placeholder(R.drawable.img_person_white_150px).into(holder.dp);
        return convertView;
    }

    private class ViewHolder {
        TextView name, bio;
        ImageView dp, onlineStatus;
        Button option;
    }
}

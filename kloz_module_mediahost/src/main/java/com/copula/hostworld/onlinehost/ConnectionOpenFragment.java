package com.copula.hostworld.onlinehost;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.genericlook.DesignHelper;
import com.copula.genericlook.ViewAnimator;
import com.copula.hostworld.R;
import com.copula.hostworld.connection.ConnectionStarterActivity;
import com.copula.hostworld.onlinehost.core.HostScannerService;

import java.util.List;

public class ConnectionOpenFragment extends Fragment implements View.OnClickListener,
        HostScannerService.HostScannerListener, ServiceConnection {
    private View vConAbsent, vConActive;
    private boolean serviceBound;

    public static ConnectionOpenFragment instantiate() {
        return new ConnectionOpenFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle s) {
        View v = inflater.inflate(R.layout.frg_hostable_session, container, false);
        (vConAbsent = v.findViewById(R.id.v_con_absent)).setOnClickListener(this);
        (vConActive = v.findViewById(R.id.v_con_active)).setOnClickListener(this);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent service = new Intent(getActivity(), HostScannerService.class);
        getActivity().bindService(service, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (serviceBound) getActivity().unbindService(this);
    }

    @Override
    public void onNonViableNetwork(int cause) {
        DesignHelper.showOnlyView(vConAbsent);
    }

    @Override
    public void onListCachedHost(List<UserProfileEntry> hosts) {
        DesignHelper.showOnlyView(vConActive);
    }

    @Override
    public void onScanProgress() {
        DesignHelper.showOnlyView(vConActive);
    }

    @Override
    public void onEmptyHostList() {
        DesignHelper.showOnlyView(vConActive);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        HostScannerService s
                = ((HostScannerService.MyBinder) service).getService();
        s.setListener(this);
        s.checkConnectionStateAndScan();
        serviceBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        serviceBound = false;
    }

    @Override
    public void onClick(View v) {
        if (v == vConAbsent) {
            ConnectionStarterActivity.launch(getActivity());
        } else if (v == vConActive) {
            ConnectedHostListActivity.launch(getActivity());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ConnectionStarterActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ConnectedHostListActivity.launch(getActivity());
            }
        }
    }//END


    public void showFragment(boolean show) {
        final View view = getView();
        if (view == null) return;
        if (show) ViewAnimator.scaleXY(new ViewAnimator.AnimationCallback() {
            @Override
            public void onEnd() {
            }

            @Override
            public void onStart() {
                vConAbsent.setClickable(true);
                vConActive.setClickable(true);
            }
        }, view, 1, 400);
        else ViewAnimator.scaleXY(new ViewAnimator.AnimationCallback() {
            @Override
            public void onEnd() {
                vConAbsent.setClickable(false);
                vConActive.setClickable(false);
            }

            @Override
            public void onStart() {
            }
        }, view, 0, 300);
    }
}

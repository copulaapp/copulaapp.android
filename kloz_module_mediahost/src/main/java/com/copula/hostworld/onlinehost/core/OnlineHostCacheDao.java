package com.copula.hostworld.onlinehost.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.copula.functionality.localservice.profile.DaoConnectsProfile;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

public class OnlineHostCacheDao extends DatabaseContext {
    private static final String TABLE = "active_host_cache";
    private static final String TAG = OnlineHostCacheDao.class.getSimpleName();
    private DaoConnectsProfile daoConnectsProfile;

    public OnlineHostCacheDao(Context context) {
        super(context, "active_host_cache.db", TABLE, 1);
        this.daoConnectsProfile = new DaoConnectsProfile(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TABLE;
        sql += "(ipAddress varchar(20) primary key,";
        sql += "userId varchar(50),";
        sql += "connectStatus integer)";
        db.execSQL(sql);
    }

    public boolean addUser(UserProfileEntry user) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            if (this.isExists(db, user.ipAddress)) {
                values.put("userId", user.userId);
                values.put("connectStatus", user.connectStatus);
                return db.update(TABLE, values, "ipAddress=?",
                        new String[]{user.ipAddress}) > -1;
            } else {
                values.put("ipAddress", user.ipAddress);
                values.put("userId", user.userId);
                values.put("connectStatus", user.connectStatus);
                return db.insert(TABLE, null, values) > -1;
            }
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public List<UserProfileEntry> getFriends(String limit) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TABLE, null, null,
                    null, null, null, null, limit);
            if (cu == null || cu.getCount() <= 0) return null;

            List<UserProfileEntry> handlers = new ArrayList<>(cu.getCount());
            while (cu.moveToNext()) {
                String userId = cu.getString(cu.getColumnIndex("userId"));
                UserProfileEntry up = daoConnectsProfile.getConnect(userId);

                //these guz comes after profile fetch inother to avoid override
                up.ipAddress = cu.getString(cu.getColumnIndex("ipAddress"));
                up.connectStatus = cu.getInt(cu.getColumnIndex("connectStatus"));
                handlers.add(up);
            }
            return handlers;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if ((db != null)) db.close();
        }
    }

    public int getCount() {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getWritableDatabase();
            cu = db.query(TABLE, new String[]{"ipAddress"}, null,
                    null, null, null, null);
            return cu.getCount();
        } catch (Exception e) {
            return 0;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public boolean removeHost(String userId) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            return db.delete(TABLE, "userId=?", new String[]{userId}) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean setHostConnectStatus(boolean connectStatus) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("connectStatus", connectStatus);
            return db.update(TABLE, values, null, null) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    private boolean isExists(SQLiteDatabase db, String ipAddress) {
        Cursor cu = null;
        try {
            cu = db.query(TABLE, null, "ipAddress=?",
                    new String[]{ipAddress}, null, null, null);
            return cu != null && cu.getCount() > 0;
        } finally {
            if (cu != null) cu.close();
        }
    }
}
package com.copula.support.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class PatchView extends LinearLayout {

    public static final int MATCH_PARENT = LayoutParams.MATCH_PARENT;
    public static final int WRAP_CONTENT = LayoutParams.WRAP_CONTENT;
    private PatchBase patchOnView;

    public static final String TAG = PatchView.class.getSimpleName();

    public PatchView(Context context) {
        super(context);
    }

    public PatchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PatchView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * add a patch view the view is automatically updated on change note:
     * default layout_width=MATCH_PARENT and layout_height=MATCH_PATENT
     *
     * @param patch
     */
    public void setPatchView(final PatchBase patch) {
        setPatchView(patch, MATCH_PARENT, MATCH_PARENT);
    }

    /**
     * add a patch view the view is automatically updated on change
     *
     * @param patch
     */
    public void setPatchView(final PatchBase patch, int w, int h) {
        this.patchOnView = patch;
        ViewGroup.LayoutParams params = patch.getView().getLayoutParams();
        if (params == null) {
            params = new ViewGroup.LayoutParams(w, h);
        } else {
            params.width = w;
            params.height = h;
        }

        removeAllViews();
        addView(patch.getView(), 0, params);
    }

    public PatchBase getPatchBase() {
        return patchOnView;
    }

    public interface PatchViewListener {
        void onChange(View v);
    }
}

package com.copula.support.android.content;

import android.widget.AbsListView;

/**
 * Created by heeleaz on 6/1/17.
 */
public interface IScrollableHandler {
    void onScroll(AbsListView view, int axis);

    void onScrollStateChanged(AbsListView view, int scrollState);
}

package com.copula.support.android.view;

import android.app.Activity;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author elias igbalajobi an abstract class. that keeps session and bind all
 *         registered classes for one single instance of an object. this class
 *         is meant to be subclassed by a class that is willing to keep instance
 *         of PatchBase instances and share that single instance
 */
public class PatchSession {

    /** all registered patches list */

    private static final int MAX_PATCH_BASE_COUNT = 25;
    /**
     * all registered base patches list
     */
    private Stack<PatchBase> mPatchBase;
    private Context mContext;
    private Activity mActivity;
    private List<PatchSessionListener> patchSessionListeners = new ArrayList<>();

    public PatchSession(Context context) {
        this.mContext = context;
        mPatchBase = new Stack<>();
    }

    public PatchSession(Activity activity) {
        this((Context) activity);
        this.mActivity = activity;
    }

    public void setPatchSessionListener(PatchSessionListener sessionListener) {
        addPatchSessionListener(sessionListener);
    }

    public void addPatchSessionListener(PatchSessionListener sessionListener) {
        this.patchSessionListeners.add(sessionListener);
    }

    public Context getContext() {
        return mContext;
    }

    public Activity getActivity() {
        return mActivity;
    }

    /**
     * get patch bundle of the specified class
     *
     * @param cls class object for the specified PatchBase
     * @return
     * @throws Exception if base patch as not been registered
     */
    public <T extends PatchBase> T getBasePatch(Class<? extends PatchBase> cls) {
        for (PatchBase patchBase : mPatchBase) {
            if (patchBase.getClass().getName() == cls.getName())
                return (T) patchBase;
        }
        return null;
    }

    public void removePatchBase(Class<? extends PatchBase> cls) {
        for (int i = 0; i < mPatchBase.size(); i++) {
            String clsName = mPatchBase.get(i).getClass().getName();
            if (cls.getName().equals(clsName)) {
                mPatchBase.remove(i);
                return;
            }
        }//end
    }

    public void removePatchBase(PatchBase patch) {
        mPatchBase.remove(patch);
    }

    /**
     * get all registered base patchs
     *
     * @return {@code List<PatchBase>}
     */
    public List<PatchBase> getmPatchBase() {
        return mPatchBase;
    }

    public void dispatchOnPatchInit(PatchBase patch) {
        if (patchSessionListeners.size() == 0) return;
        for (PatchSessionListener listener : patchSessionListeners) {
            listener.onNewPatchInit(patch);
        }
    }

    public <T extends PatchBase> T patch(Class<? extends PatchBase> cls, PatchBundle bundle, boolean notifyFrame
            , Object... objects) {
        return doPatching(cls, bundle, notifyFrame, true, true, objects);
    }

    public <T extends PatchBase> T doPatching(Class<? extends PatchBase> cls, PatchBundle bundle, boolean notifyFrame
            , boolean removeOldInstance, boolean stackIt, Object... objects) {
        if (mPatchBase.size() == MAX_PATCH_BASE_COUNT)
            mPatchBase.removeElementAt(mPatchBase.size() - 1);

        if (removeOldInstance) {
            PatchBase patch = getBasePatch(cls);
            if (patch != null) {
                patch.finish();
            }
        } else {
            PatchBase pb = getBasePatch(cls);
            if (pb != null) {
                if (notifyFrame) dispatchOnPatchInit(pb);
                return (T) pb;
            }
        }

        try {
            PatchBase patchBase = cls.newInstance();
            if (stackIt) mPatchBase.add(patchBase);

            patchBase.contruct(this, bundle, objects);
            if (notifyFrame) dispatchOnPatchInit(patchBase);
            return (T) patchBase;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    protected void dispatchEvent(Class<? extends PatchBase> toPatch, final PatchBundle bundle, String event
            , boolean forceCreate, Object... instanceObjects) throws Exception {
        PatchBase pb = getBasePatch(toPatch);
        if (pb == null && forceCreate) {
            pb = patch(toPatch, bundle, true, instanceObjects);
        }
        if (pb != null) pb.onEventReceived(event, bundle);
    }

    protected void dispatchEventToFrame(PatchBase fromPatch, String event, PatchBundle bundle) {
        if (patchSessionListeners.size() == 0) return;
        for (PatchSessionListener listener : patchSessionListeners) {
            listener.onEventToFrame(fromPatch, event, bundle);
        }
    }

    public void dispatchFinishEvent(PatchBase patchBase, PatchBundle bundle) {
        if (patchSessionListeners.size() == 0) return;
        for (PatchSessionListener listener : patchSessionListeners) {
            listener.onEventToFrame(patchBase, PatchSessionListener.PATCH_FINISH, bundle);
        }
    }

    public void finishPatchBase(PatchBase patchBase, int result) {
        dispatchFinishEvent(patchBase, new PatchBundle().put("result", result));
        patchBase.onPause();
        popBackStack();
    }

    public void finishPatchBaseFromChild(Class<? extends PatchBase> child, int result) {
        int index = mPatchBase.indexOf(getBasePatch(child));
        if (index < 0 || index >= mPatchBase.size()) return;

        for (int i = index; i >= 0; i--) {
            PatchBase patch = mPatchBase.remove(i);
            patch.onPause();
            dispatchFinishEvent(patch, new PatchBundle().put("result", result));
        }
    }

    public PatchBase popBackStack() {
        if (mPatchBase != null && mPatchBase.size() > 0) {
            PatchBase pb = mPatchBase.peek();
            //onBackStack is meant to return false
            if (!pb.onPopBackStack()) {
                mPatchBase.remove(pb);
            }

            pb.onResume();
            return pb;
        } else return null;
    }

    public void dispatchOnStop() {
        for (PatchBase patchBase : mPatchBase) {
            patchBase.onStop();
        }
    }

    public void dispatchOnStop(Class<? extends PatchBase> cls) {
        PatchBase pb = getBasePatch(cls);
        if (pb != null) pb.onStop();
    }

    /**
     * call this to dispatch onDestroy protected method of all registered
     * BasePatches
     */
    public void dispatchOnDestroy() {
        for (PatchBase patchBase : mPatchBase) {
            patchBase.onDestroy();
            mPatchBase.remove(patchBase);
        }
    }

    public void dispatchOnDestroy(Class<? extends PatchBase> cls) {
        PatchBase patch = getBasePatch(cls);
        if (patch != null) patch.onDestroy();
    }

    /**
     * call this to dispatch onPause event to all registered BasePatches.
     */
    public void dispatchOnPause() {
        for (PatchBase patchBase : mPatchBase) {
            patchBase.onPause();
        }
    }


    public void dispatchOnPause(Class<? extends PatchBase> cls) {
        PatchBase patch = getBasePatch(cls);
        if (patch != null) patch.onPause();
    }

    /**
     * call this to dispatch onResume event to all registered BasePatches.
     */
    public void dispatchOnResume() {
        for (PatchBase patchBase : mPatchBase) {
            patchBase.onResume();
        }
    }

    public void dispatchOnResume(Class<? extends PatchBase> cls) {
        PatchBase patch = getBasePatch(cls);
        if (patch != null) patch.onResume();
    }


    public interface PatchSessionListener {
        String PATCH_FINISH = "patch_finish";
        String RESULT = "result";

        void onEventToFrame(PatchBase fromPatch, String event, PatchBundle bundle);

        void onNewPatchInit(PatchBase patch);
    }
}

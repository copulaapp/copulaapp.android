package com.copula.support.android.view.widget;

import android.graphics.Bitmap;

import java.util.BitSet;
import java.util.HashMap;

public class AdapterHandler {
    private HashMap<String, Bitmap> bitmapMap;
    private HashMap<String, String> stringMap;
    private HashMap<String, Integer> integerMap;
    private HashMap<String, Long> longMap;
    private HashMap<String, Object> objectMap;
    private HashMap<String, Boolean> booleanMap;

    private BitSet bitSet;

    public AdapterHandler() {
        bitSet = new BitSet();
    }

    public AdapterHandler put(String key, String value) {
        // initialize map if not initialized
        if (!bitSet.get(0)) {
            stringMap = new HashMap<>();
            bitSet.set(0);
        }
        stringMap.put(key, value);
        return this;
    }

    public String getString(String key, String def) {
        // initialize map if not initialized
        if (!bitSet.get(0)) return def;
        String value = stringMap.get(key);
        return (value == null) ? def : value;
    }

    public AdapterHandler put(String key, Bitmap value) {
        // initialize map if not initialized
        if (!bitSet.get(1)) {
            bitmapMap = new HashMap<>();
            bitSet.set(1);
        }
        bitmapMap.put(key, value);
        return this;
    }

    public Bitmap getBitmap(String key, Bitmap def) {
        // initialize map if not initialized
        if (!bitSet.get(1)) return null;
        Bitmap value = bitmapMap.get(key);
        return (value == null) ? def : value;
    }

    public AdapterHandler put(String key, int value) {
        if (!bitSet.get(2)) {
            integerMap = new HashMap<>();
            bitSet.set(2);
        }
        integerMap.put(key, value);
        return this;
    }

    public int getInteger(String key, int def) {
        if (!bitSet.get(2)) return def;
        Integer value = integerMap.get(key);
        return (value == null) ? def : value;
    }

    public AdapterHandler put(String key, long value) {
        if (!bitSet.get(3)) {
            longMap = new HashMap<>();
            bitSet.set(3);
        }
        longMap.put(key, value);
        return this;
    }

    public long getLong(String key, long def) {
        if (!bitSet.get(3)) return def;
        Long value = longMap.get(key);
        return (value == null) ? def : value;
    }

    public AdapterHandler put(String key, Object value) {
        if (!bitSet.get(4)) {
            objectMap = new HashMap<>();
            bitSet.set(4);
        }
        objectMap.put(key, value);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T getObject(String key) {
        if (!bitSet.get(4)) return null;
        return (T) objectMap.get(key);
    }

    public AdapterHandler put(String key, boolean value) {
        // initialize map if not initialized
        if (!bitSet.get(5)) {
            booleanMap = new HashMap<>();
            bitSet.set(5);
        }
        booleanMap.put(key, value);
        return this;
    }

    public boolean getBoolean(String key, boolean def) {
        if (!bitSet.get(5)) return def;
        Boolean value = booleanMap.get(key);
        return (value == null) ? def : value;
    }

    public String getString(String key) {
        return getString(key, null);
    }

    public Integer getInteger(String key) {
        return getInteger(key, 0);
    }

    public Bitmap getBitmap(String key) {
        return getBitmap(key, null);
    }

    public long getLong(String key) {
        return getLong(key, 0);
    }

    public void clear() {
        bitSet.clear();
    }
}

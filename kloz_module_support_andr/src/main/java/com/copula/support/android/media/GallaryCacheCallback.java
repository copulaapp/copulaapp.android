package com.copula.support.android.media;

import android.graphics.Bitmap;

public interface GallaryCacheCallback {
    void onBitmapLoaded(Bitmap bitmap);

}

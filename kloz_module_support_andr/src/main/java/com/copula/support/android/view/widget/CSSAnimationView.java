package com.copula.support.android.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebView;
import com.copula.support.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by heeleaz on 5/1/17.
 */
public class CSSAnimationView extends WebView {
    public CSSAnimationView(Context context) {
        this(context, null, R.styleable.CSSAnimationViewStyle_CSSAnimationViewDefault);
    }

    public CSSAnimationView(Context context, AttributeSet attrs) {
        this(context, attrs, R.styleable.CSSAnimationViewStyle_CSSAnimationViewDefault);

    }

    public CSSAnimationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        __init(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CSSAnimationView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        __init(context, attrs, defStyleAttr);
    }

    private void __init(Context context, AttributeSet attrs, int defStyle) {
        getSettings();

        setBackgroundColor(Color.TRANSPARENT);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CSSAnimationView, defStyle, 0);
        String assetFile = attributes.getString(R.styleable.CSSAnimationView_htmlAssetFile);
        try {
            if (assetFile != null) loadHtmlFromAsset(context.getAssets(), assetFile);
        } catch (IOException e) {
        }
        attributes.recycle();
    }

    public void loadHtmlFromAsset(AssetManager assetManager, String file) throws IOException {
        InputStream is = assetManager.open(file);
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }

        loadData(total.toString(), "text/html", "utf-8");
    }
}

package com.copula.support.android.view;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class PatchBase extends Object {
    private Context mContext;
    private View mView;

    private Object[] mInstanceObjects;
    private PatchBundle mBundle;
    private PatchSession mPatchSession;
    private Handler mHandler;
    private LayoutInflater layoutInflater;

    private void init(Context context, PatchBundle bundle, Object... instanceObjects) {
        this.mContext = context;
        this.mInstanceObjects = instanceObjects;
        this.mBundle = (bundle == null) ? new PatchBundle() : bundle;
        mHandler = new Handler(context.getMainLooper());
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.onCreate(bundle, instanceObjects);
        //this.onResume();
    }

    protected PatchBase contruct(PatchSession session, PatchBundle bundle, Object... objects) {
        this.mPatchSession = session;
        init(session.getContext(), bundle, objects);
        return this;
    }

    public PatchSession getPatchSession() {
        return mPatchSession;
    }

    public PatchBundle getBundle() {
        return mBundle;
    }

    public String getString(int resId) {
        return mContext.getString(resId);
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public void setContentView(int resid) {
        this.mView = layoutInflater.inflate(resid, null, false);
    }

    public void setContentView(View view) {
        this.mView = view;
    }

    public View findViewById(int id) {
        if (mView != null) return mView.findViewById(id);
        return null;
    }

    /**
     * called firstly, to do all Patch initialization. onStart is called next
     *
     * @param bundle        Bundle that contains data
     * @param sessionObject may contain controller etc
     */
    protected void onCreate(PatchBundle bundle, Object... sessionObject) {
    }

    /**
     * called when application resume back to mView after pausing
     */

    protected void onResume() {
    }

    /**
     * called if patch is going of mView.
     */
    protected void onPause() {
    }

    /**
     * called if application as gone out of mView
     */
    protected void onStop() {
    }

    /**
     * called if activity as been destroyed, or completely moved out of stack
     */
    protected void onDestroy() {
    }

    /**
     * return true to lock back stacking. otherwise return false
     */
    protected boolean onPopBackStack() {
        return false;
    }

    /**
     * run the specified Runnable in main thread Handler
     */
    protected void runOnUIThread(Runnable runnable) {
        mHandler.post(runnable);
    }


    public View getView() {
        return this.mView;
    }

    public Context getContext() {
        return mPatchSession.getContext();
    }

    public Activity getActivity() {
        return mPatchSession.getActivity();
    }

    public Handler getHandler() {
        return mHandler;
    }

    protected void showOnlyView(final View showView) {
        runOnUIThread(new Runnable() {
            @Override
            public void run() {
                ViewGroup parent = (ViewGroup) showView.getParent();
                int childCount = parent.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    parent.getChildAt(i).setVisibility(View.GONE);
                }
                showView.setVisibility(View.VISIBLE);
            }
        });
    }

    public String composePaging(int offset, int limit) {
        return offset + "," + limit;
    }

    public <T extends PatchBase> T initPatch(Class<? extends PatchBase> cls, PatchBundle bundle, Object... objects) {
        bundle = (bundle == null) ? new PatchBundle() : bundle;
        Object[] instObj = (objects.length > 0) ? objects : mInstanceObjects;

        this.onPause();//pause current patch
        return mPatchSession.doPatching(cls, bundle, true, true, true, instObj);
    }

    public <T extends PatchBase> T initDoPatch(Class<? extends PatchBase> cls, PatchBundle bundle
            , boolean removeOldInstance, boolean stackIt, Object... objects) {
        bundle = (bundle == null) ? new PatchBundle() : bundle;
        if (objects.length <= 0) {
            return mPatchSession.doPatching(cls, bundle, true, false, stackIt, mInstanceObjects);
        } else return mPatchSession.doPatching(cls, bundle, true, removeOldInstance, stackIt, objects);
    }

    protected void finish(int result) {
        mPatchSession.finishPatchBase(this, result);
    }

    protected void finishFromChild(Class<? extends PatchBase> child) {
        mPatchSession.finishPatchBaseFromChild(child, 0);
    }

    protected void finishFromChild(Class<? extends PatchBase> child, int result) {
        mPatchSession.finishPatchBaseFromChild(child, result);
    }

    protected void finish() {
        finish(-1);
    }

    public PatchBase popBackStack() {
        PatchBase pb = mPatchSession.popBackStack();
        if (pb == this) pb = mPatchSession.popBackStack();
        return pb;
    }

    /**
     * dispatch an event to specified PatchBase session base patch
     */
    public void dispatchEvent(String event, Class<? extends PatchBase> toPatch, PatchBundle bundle, boolean forceCreate) {
        try {
            mPatchSession.dispatchEvent(toPatch, bundle, event, forceCreate, mInstanceObjects);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dispatchEventToFrame(String event, PatchBundle bundle) {
        mPatchSession.dispatchEventToFrame(this, event, bundle);
    }

    /**
     * called when event is fired by other patch class of same class
     */
    public void onEventReceived(String event, PatchBundle bundle) {
    }
}

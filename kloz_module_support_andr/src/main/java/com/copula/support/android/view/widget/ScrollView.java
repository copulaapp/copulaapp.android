package com.copula.support.android.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import com.copula.support.R;

/**
 * Created by heeleaz on 3/19/17.
 */
public class ScrollView extends android.widget.ScrollView {
    private boolean stayTopScroll;

    public ScrollView(Context context) {
        this(context, null, 0);
    }

    public ScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (stayTopScroll) {
            scrollTo(0, 0);
            stayTopScroll = false;
        }
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        // Load the styled attributes and set their properties
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ScrollView, defStyle, 0);
        stayTopScroll = attributes.getBoolean(R.styleable.ScrollView_stayTopScroll, false);

        attributes.recycle();
    }

    public void stayTopScroll(boolean stay) {
        this.stayTopScroll = stay;
    }

    public void holdOnScrollAxis() {
        scrollTo(getScrollX(), getScrollY());
    }
}

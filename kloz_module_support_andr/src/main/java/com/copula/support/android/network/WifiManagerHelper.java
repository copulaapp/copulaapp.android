package com.copula.support.android.network;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import java.util.List;

public class WifiManagerHelper {
    private NetworkHelper mNetworkHelper;
    private WifiManager mWifiManager;

    public WifiManagerHelper(Context context) {
        mNetworkHelper = new NetworkHelper(context);
        mWifiManager = mNetworkHelper.getWifiManager();
    }

    public static boolean checkLocationServiceEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(),
                        Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void enableNetworks(boolean enable) {
        List<WifiConfiguration> networks = mWifiManager.getConfiguredNetworks();
        if (networks == null) return;

        for (WifiConfiguration wf : networks) {
            if (enable) mWifiManager.enableNetwork(wf.networkId, false);
            else mWifiManager.disableNetwork(wf.networkId);
        }
    }

    public void scan() {
        mWifiManager.setWifiEnabled(true);
        //mWifiManager.setWifiEnabled(true);
        mWifiManager.startScan();//start fresh scan
    }

    public boolean setWifiEnabled(boolean enable) {
        return mWifiManager.setWifiEnabled(enable);
    }

    public List<ScanResult> getScanResult() {
        return mWifiManager.getScanResults();
    }

    public WifiInfo getWifiInfo() {
        return mNetworkHelper.getWifiInfo();
    }

    public boolean isWifiConnected() {
        return mNetworkHelper.isWifiConnected();
    }

    private int isNetworkExists(WifiConfiguration configuration) {
        List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
        if (configs == null || configs.size() <= 0) return -1;

        for (WifiConfiguration config : configs) {
            if ((configuration.BSSID.equals(config.BSSID)) &&
                    (configuration.SSID.equals(config.SSID))) {
                return config.networkId;
            }
        }
        return -1;
    }

    public int connect(ScanResult result, String password) {
        return connect(NetConfigurator.createBindableWPANetwork(result, password));
    }

    public int connect(WifiConfiguration config) {
        int netId = mWifiManager.addNetwork(config);
        if (netId == -1) return -1;

        mWifiManager.disconnect();
        mWifiManager.enableNetwork(netId, true);
        mWifiManager.saveConfiguration();
        mWifiManager.reassociate();

        return netId;
    }

    public void disconnect(int netId) {
        mWifiManager.disconnect();
        mWifiManager.removeNetwork(netId);
    }

    public void closeWifi() {
        this.removeCreatedNetwork();
        mWifiManager.setWifiEnabled(false);
    }

    public boolean removeCreatedNetwork() {
        return removeNetwork(mWifiManager.getConnectionInfo().getNetworkId());
    }

    public boolean removeNetwork(int networkId) {
        try {
            boolean remove = mWifiManager.removeNetwork(networkId);
            mWifiManager.saveConfiguration();
            return remove;
        } catch (Exception e) {
            return false;
        }
    }
}
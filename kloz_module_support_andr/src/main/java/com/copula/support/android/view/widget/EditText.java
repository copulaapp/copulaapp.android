package com.copula.support.android.view.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class EditText extends android.widget.EditText {

    public EditText(Context context) {
        super(context);
    }

    public EditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public String getTextContent() {
        return getText().toString();
    }

    public void setTextChangedListener(final TextWatcher textWatcher) {
        addTextChangedListener(new android.text.TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textWatcher.onTextChanged(EditText.this, s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void setViewDrawableClick(final DrawableClickListener listener) {
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != MotionEvent.ACTION_UP) return false;
                Drawable l, r;
                l = getCompoundDrawables()[DrawableClickListener.DRAWABLE_LEFT];
                r = getCompoundDrawables()[DrawableClickListener.DRAWABLE_RIGHT];

                if (r != null && event.getRawX() >= (getRight() - r.getBounds().width())) {
                    listener.onDrawableClick(EditText.this, 2);
                    return true;
                } else if (l != null && event.getRawX() <= l.getBounds().width()) {
                    listener.onDrawableClick(EditText.this, 0);
                    return true;
                } else return false;
            }
        });//end onTouch
    }

    public interface TextWatcher {
        void onTextChanged(EditText editText, CharSequence s);
    }

    public interface DrawableClickListener {
        int DRAWABLE_LEFT = 0;
        int DRAWABLE_RIGHT = 2;

        void onDrawableClick(EditText editText, int position);
    }
}

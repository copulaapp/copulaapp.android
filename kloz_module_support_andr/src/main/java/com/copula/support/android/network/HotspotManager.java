package com.copula.support.android.network;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HotspotManager {
    public static final String NETWORK_AP_STATE_CHANGED_ACTION = "android.net.wifi.WIFI_AP_STATE_CHANGED";
    public static final String NETWORK_AP_CLIENT_UPDATE_ACTION = "com.support.net.hotspot.CLIENT_UPDATE";
    public static final String EXTRA_CLIENT_IP_LIST = "client_ip_list";
    private final ExecutorService threadPool = Executors.newCachedThreadPool();
    private NetworkHelper mNetworkHelper;
    private WifiConfiguration preWifiConfig;
    private boolean preDataState;
    private Context mContext;
    private boolean periodicScan;

    public HotspotManager(Context context) {
        this.mContext = context;
        mNetworkHelper = new NetworkHelper(context);
        preWifiConfig = mNetworkHelper.getWifiConfiguration();
    }

    public boolean hostOpenNetwork(String ssid) {
        if (isHotspotEnablingOrEnabled()) {//turn off hotspot
            mNetworkHelper.setHotspotEnabled(null, false);
        }

        WifiConfiguration config = NetConfigurator.hostOpenNetwork(ssid);
        return mNetworkHelper.setHotspotEnabled(config, true);
    }

    private boolean isHotspotEnablingOrEnabled() {
        NetworkHelper.WIFI_AP_STATE initState = mNetworkHelper.getWifiApState();
        return initState == NetworkHelper.WIFI_AP_STATE.WIFI_AP_STATE_ENABLED ||
                initState == NetworkHelper.WIFI_AP_STATE.WIFI_AP_STATE_ENABLING;
    }

    /**
     * enable hotspot com.kloz.connection.service, note ssid={@code UserProfile} ssid
     *
     * @param enabled
     */
    private void setHotspotEnabled(boolean enabled) {
        if (!enabled) {// false, restore state of connection
            mNetworkHelper.setHotspotEnabled(preWifiConfig, false);
            mNetworkHelper.setDataEnabled(preDataState);
        } else if (preDataState = mNetworkHelper.isDataEnabled()) {
            mNetworkHelper.setDataEnabled(false);
        }
    }

    public boolean hostSecuredNetwork(String ssid, String password) {
        if (isHotspotEnablingOrEnabled()) {//turn off hotspot
            mNetworkHelper.setHotspotEnabled(null, false);
        }

        setHotspotEnabled(true);
        WifiConfiguration config = NetConfigurator.hostWPASecuredNetwork(ssid, password);
        return mNetworkHelper.setHotspotEnabled(config, true);
    }

    private List<HotspotClientResult> scanResult(boolean onlyReachable) {
        BufferedReader br = null;
        List<HotspotClientResult> result = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted.length < 4) continue;
                // Basic sanity check
                if (splitted[3].matches("..:..:..:..:..:..")) {
                    boolean isReachable = InetAddress.getByName(splitted[0]).isReachable(300);
                    if (!onlyReachable || isReachable) {
                        result.add(new HotspotClientResult(splitted[0], splitted[3], splitted[5], isReachable));
                    }
                }
            }// end while
        } catch (Exception e) {
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {
            }
        }
        return result;
    }

    public void startLoopedScan(final boolean onlyReachable, final HotspotClientScanListener listener) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                periodicScan = true;
                while (periodicScan) {
                    List<HotspotClientResult> result = scanResult(onlyReachable);
                    if (result.size() > 0) listener.onScanCompleted(result);
                    try {//clear arp cache, to allow refresh(change)
                        Runtime.getRuntime().exec("ip neigh flush all");
                        synchronized (threadPool) {
                            threadPool.wait(300);
                        }
                    } catch (Exception e) {
                    }
                }//end while
            }//end run
        });//end threadPool
    }

    public void scanAndBroadcast(boolean onlyReachable) {
        startLoopedScan(onlyReachable, new HotspotClientScanListener() {
            @Override
            public void onScanCompleted(List<HotspotClientResult> clients) {
                Intent intent = new Intent(NETWORK_AP_CLIENT_UPDATE_ACTION);
                ArrayList<String> ipList = new ArrayList<>(clients.size());
                for (HotspotClientResult client : clients) {
                    ipList.add(client.getIpAddr());
                }
                intent.putStringArrayListExtra(EXTRA_CLIENT_IP_LIST, ipList);
                mContext.sendBroadcast(intent);
            }
        });
    }

    public void stopScan() {
        periodicScan = false;
    }

    public interface HotspotClientScanListener {
        void onScanCompleted(List<HotspotClientResult> clients);
    }
}

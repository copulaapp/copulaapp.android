package com.copula.support.android.media;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageBitmapOptimizer {
    public static int calcInSampleSize(InputStream stream, int maxSize) throws IOException {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(stream, null, o);

        int scale = 1;
        if (maxSize > 0 && (o.outHeight > maxSize || o.outWidth > maxSize)) {
            double log = Math.log(maxSize / (double) Math.max(o.outHeight, o.outWidth));
            scale = (int) Math.pow(2, (int) Math.ceil(log / Math.log(0.5)));
        }
        stream.close();
        return scale;
    }

    public static Bitmap decodeFile(File imageFile, int imageMaxSize) throws IOException {
        FileInputStream fis = new FileInputStream(imageFile);
        if (fis.available() <= 0) return null;

        int scale = calcInSampleSize(fis, imageMaxSize);

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        fis = new FileInputStream(imageFile);
        Bitmap b = BitmapFactory.decodeStream(fis, null, o2);

        fis.close();
        return b;
    }

    public static int calcInSampleScale(int outWidth, int outHeight, int maxSize) {
        int scale = 1;
        if (maxSize > 0 && (outHeight > maxSize || outWidth > maxSize)) {
            double log = Math.log(maxSize / (double) Math.max(outHeight, outWidth));
            scale = (int) Math.pow(2, (int) Math.ceil(log / Math.log(0.5)));
        }
        return scale;
    }
}
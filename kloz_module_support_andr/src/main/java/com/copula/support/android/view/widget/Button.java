package com.copula.support.android.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import com.copula.support.R;

public class Button extends android.widget.Button {
    private View mProgressView;
    private LinearLayout mPaddedWrapperView;
    private boolean touchPaddingArea, marginTouchArea;

    public Button(Context context) {
        this(context, null, R.styleable.ButtonStyle_ButtonDefault);
    }

    public Button(Context context, AttributeSet attrs) {
        this(context, attrs, R.styleable.ButtonStyle_ButtonDefault);
    }

    public Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.Button, defStyle, 0);
        touchPaddingArea = attributes.getBoolean(R.styleable.Button_touchPaddingArea, false);
        marginTouchArea = attributes.getBoolean(R.styleable.Button_marginTouchArea, false);
        attributes.recycle();
    }

    public void startProgressing(int layout) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        startProgressing(inflater.inflate(layout, null, false));
    }

    public void startProgressing(View progressView) {
        this.setEnabled(false);

        LinearLayout rootView = (LinearLayout) getParent();
        int viewPos = getViewPositionInParent();
        rootView.removeViewAt(viewPos);//remove this=>view from parent

        progressView.setLayoutParams(getLayoutParams());
        rootView.addView(mProgressView = progressView, viewPos);
    }

    public void finishProgressing() {
        this.setEnabled(true);

        LinearLayout rootView = (LinearLayout) mProgressView.getParent();
        for (int i = 0; i < rootView.getChildCount(); i++) {
            if (rootView.getChildAt(i) == mProgressView) {
                rootView.removeViewAt(i);
                rootView.addView(this, i);
            }
        }
    }//end

    private int getViewPositionInParent() {
        ViewGroup rootView = (ViewGroup) getParent();
        for (int i = 0; i < rootView.getChildCount(); i++) {
            if (rootView.getChildAt(i) == this) return i;
        }
        return -1;
    }

    private void setTouchPointSize(final OnClickListener listener, int l, int t, int r, int b) {
        if ((l | t | r | b) == -1) return;

        ViewGroup rootView = (ViewGroup) getParent();
        int viewPos = getViewPositionInParent();
        if (viewPos == -1) return;

        rootView.removeViewAt(viewPos);

        mPaddedWrapperView = new LinearLayout(getContext());
        mPaddedWrapperView.setLayoutParams(new LinearLayout.LayoutParams(-2,-2));
        mPaddedWrapperView.setGravity(Gravity.CENTER);
        mPaddedWrapperView.setPadding(l, t, r, b);
        mPaddedWrapperView.addView(this);
        rootView.addView(mPaddedWrapperView, viewPos);

        mPaddedWrapperView.setVisibility(getVisibility());
        mPaddedWrapperView.setBackgroundResource(R.drawable.background_btn_touch_area_default);

        setClickable(false);//so as to enable mPaddedWrapperView click only
        mPaddedWrapperView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(Button.this);
            }
        });
    }

    private int getPositionInParentView() {
        ViewParent parent = getParent();
        if (parent instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) parent;
            for (int i = 0; i < g.getChildCount(); i++) {
                if (this == g.getChildAt(i)) return i;
            }
        }
        return -1;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (marginTouchArea) {
            adjustInAreaViewBound(getPaddingLeft());
            marginTouchArea = false;//break in case of view call loop
        }
    }

    private void adjustInAreaViewBound(int d) {
        int myp = getPositionInParentView();

        ViewGroup parent = (ViewGroup) getParent();
        Object lp = null, rp = null;

        if (myp > 0)
            lp = parent.getChildAt(myp - 1).getLayoutParams();
        if (myp + 1 < parent.getChildCount()) {
            rp = parent.getChildAt(myp + 1).getLayoutParams();
        }

        if (lp != null && lp instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams ml = (ViewGroup.MarginLayoutParams) lp;
            ml.setMargins(ml.leftMargin, ml.topMargin, -d, ml.bottomMargin);
        }

        if (rp != null && rp instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams ml = (ViewGroup.MarginLayoutParams) rp;
            ml.setMargins(-d, ml.topMargin, ml.rightMargin, ml.bottomMargin);
        }
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        if (touchPaddingArea) {
            setTouchPointSize(l, getPaddingLeft(), getPaddingTop(), getPaddingRight(),
                    getPaddingBottom());
            touchPaddingArea = false;//break in case of view call loop
        } else super.setOnClickListener(l);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (mPaddedWrapperView != null) mPaddedWrapperView.setVisibility(visibility);
    }

    public void setAnimation(int animRes, final int frameCount, final int duration) {
        Animation a = AnimationUtils.loadAnimation(getContext(), animRes);
        a.setDuration(duration);
        a.setInterpolator(new android.view.animation.Interpolator() {
            @Override
            public float getInterpolation(float input) {
                return (float) Math.floor(input * frameCount) / frameCount;
            }
        });
        startAnimation(a);
    }
}

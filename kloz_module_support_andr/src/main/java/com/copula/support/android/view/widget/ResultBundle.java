package com.copula.support.android.view.widget;

import android.os.Bundle;

public interface ResultBundle {
    void message(String action, Bundle bundle);
}
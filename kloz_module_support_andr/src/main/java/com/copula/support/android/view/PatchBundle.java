package com.copula.support.android.view;

import android.graphics.Bitmap;
import com.copula.support.android.view.widget.AdapterHandler;

public class PatchBundle extends AdapterHandler {
    @Override
    public PatchBundle put(String key, Bitmap value) {
        return (PatchBundle) super.put(key, value);
    }

    @Override
    public PatchBundle put(String key, boolean value) {
        return (PatchBundle) super.put(key, value);
    }

    @Override
    public PatchBundle put(String key, int value) {
        return (PatchBundle) super.put(key, value);
    }

    @Override
    public PatchBundle put(String key, long value) {
        return (PatchBundle) super.put(key, value);
    }

    @Override
    public PatchBundle put(String key, Object value) {
        return (PatchBundle) super.put(key, value);
    }

    @Override
    public PatchBundle put(String key, String value) {
        return (PatchBundle) super.put(key, value);
    }
}

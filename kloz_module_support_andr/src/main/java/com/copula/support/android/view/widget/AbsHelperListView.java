package com.copula.support.android.view.widget;

import android.widget.AdapterView;

/**
 * Created by heeleaz on 3/18/17.
 */
public interface AbsHelperListView {
    void setOnItemClickListener(AdapterView.OnItemClickListener listener);

    void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener);

    boolean isMultipleSelectionEnabled();

    void setMultipleSelectionEnabled(boolean enable);

    int getPagerOffset();

    int getPagerStep();

    void setPagerStep(int step);

    void nextPager();

    void setHelperScrollListener(AbsListViewScrollListener l);

    void setListViewHeightBasedOnChildren();

    void setListViewHeightBasedOnChildren(int maxChild);

    public interface OnMultipleItemSelected {
        void onSelected(int count);
    }


}

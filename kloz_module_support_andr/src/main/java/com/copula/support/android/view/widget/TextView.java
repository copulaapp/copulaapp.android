package com.copula.support.android.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import com.copula.support.R;

public class TextView extends android.widget.TextView {
    public TextView(Context context) {
        this(context, null, R.styleable.TextViewStyle_TextViewDefault);
    }

    public TextView(Context context, AttributeSet attrs) {
        this(context, attrs, R.styleable.TextViewStyle_TextViewDefault);
    }

    public TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public void setTypeFaceFromAsset(String fontPath) {
        Typeface font = Typeface
                .createFromAsset(getContext().getAssets(), fontPath);
        setTypeface(font);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.TextView, defStyle, 0);
        String fontPath = attributes.getString(R.styleable.TextView_fontpath);
        if (fontPath != null) setTypeFaceFromAsset(fontPath);

        boolean marquee = attributes.getBoolean(R.styleable.TextView_scroll_marquee, false);
        if (marquee) {
            setMarqueeRepeatLimit(-1);
            setHorizontallyScrolling(true);
            setEllipsize(TextUtils.TruncateAt.MARQUEE);
            setSingleLine(true);
            setSelected(true);
        }

        attributes.recycle();
    }

    private void __truncateText(String readMoreText, int maxLines, int readMoreColor) {
        setTag(getText().toString());

        if (getLineCount() >= maxLines) {
            int lineEndIndex = getLayout().getLineEnd(maxLines - 1);
            String truncatedText = getText().subSequence(0,
                    lineEndIndex - readMoreText.length() + 1) + readMoreText;

            Spannable spannable = new SpannableString(truncatedText);
            spannable.setSpan(new ForegroundColorSpan(readMoreColor),
                    truncatedText.length() - readMoreText.length(),
                    truncatedText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            setText(spannable, TextView.BufferType.SPANNABLE);

            super.setMaxLines(maxLines);
        }
    }

    public void truncateText(final String readMoreText, final int color, final int maxLines) {
        final ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getViewTreeObserver().removeGlobalOnLayoutListener(this);
                __truncateText(readMoreText, maxLines, color);
            }
        });
    }

    public void expandText() {
        String origText = (String) getTag();
        if (origText != null) setText(origText);
        super.setMaxLines(10000);
    }
}

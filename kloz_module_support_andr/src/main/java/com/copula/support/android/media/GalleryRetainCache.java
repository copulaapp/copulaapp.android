package com.copula.support.android.media;

public class GalleryRetainCache {
    private static GalleryRetainCache sSingleton;
    public GalleryCache mRetainedCache;

    public static GalleryRetainCache getOrCreateRetainableCache(int size) {
        if (sSingleton == null) {
            sSingleton = new GalleryRetainCache();
            sSingleton.mRetainedCache = new GalleryCache(size);
        }

        return sSingleton;
    }
}

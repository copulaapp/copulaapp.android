package com.copula.support.android.media;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class GalleryCache {
    private LruCache<String, Bitmap> mBitmapCache;

    public GalleryCache(int size) {
        mBitmapCache = new LruCache<String, Bitmap>(size) {
            @Override
            protected int sizeOf(String key, Bitmap b) {
                // Assuming that one pixel contains four bytes.
                return b.getHeight() * b.getWidth() * 4;
            }
        };
    }

    public void addBitmapToCache(String key, Bitmap bitmap) {
        if (getBitmapFromCache(key) == null) {
            mBitmapCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromCache(String key) {
        return mBitmapCache.get(key);
    }

    public boolean isBitmapExists(String key) {
        if (getBitmapFromCache(key) != null) {
            return true;
        }
        return false;
    }

    /* end */
    public void clear() {
        mBitmapCache.evictAll();
    }
}

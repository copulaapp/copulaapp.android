package com.copula.support.android.network;

public class IPAddressListing {
    private int value;

    public IPAddressListing(int value) {
        this.value = value;
    }

    public IPAddressListing(String ipAddr) {
        value = analizeAddress(ipAddr);
    }

    private static int analizeAddress(String ipAddr) {
        String[] parts = ipAddr.split("\\.");
        if (parts.length != 4) {
            throw new IllegalArgumentException();
        }
        return (Integer.parseInt(parts[0], 10) << (8 * 3)) & 0xFF000000 | (Integer.parseInt(parts[1], 10) << (8 * 2))
                & 0x00FF0000 | (Integer.parseInt(parts[2], 10) << (8 * 1)) & 0x0000FF00
                | (Integer.parseInt(parts[3], 10) << (8 * 0)) & 0x000000FF;
    }

    private int getOctet(int i) {
        if (i < 0 || i >= 4)
            throw new IndexOutOfBoundsException();

        return (value >> (i * 8)) & 0x000000FF;
    }

    public String toString() {
        String ip = "";
        for (int i = 3; i >= 0; --i) {
            ip += getOctet(i);
            if (i != 0)
                ip += ".";
        }
        return ip;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IPAddressListing) {
            return value == ((IPAddressListing) obj).value;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return value;
    }

    public String next() {
        value = value + 1;
        return toString();
    }

    public boolean asNext(String limit) {
        if (limit == null)
            return false;
        return !(value == new IPAddressListing(limit).value);
    }
}

package com.copula.support.android.view.widget;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

public class GridView extends android.widget.GridView implements android.widget.AbsListView.OnScrollListener,
        AbsHelperListView {
    private ImplHelperScrollview implHelperScrollview;
    private AbsListViewScrollListener scrollListener;

    private boolean enableMultipleSelection = false;

    public GridView(Context context) {
        super(context);
        implHelperScrollview = new ImplHelperScrollview(this);
    }

    public GridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        implHelperScrollview = new ImplHelperScrollview(this);
    }

    public GridView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        implHelperScrollview = new ImplHelperScrollview(this);
    }

    private int getDividerHeight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return getVerticalSpacing();
        }
        return 1;
    }

    @Override
    public void nextPager() {
        this.implHelperScrollview.allowNext = true;
        this.implHelperScrollview.updateCallCount();
    }

    public int getPagerOffset() {
        return implHelperScrollview.offsetCount;
    }

    public void setListViewHeightBasedOnChildren(int maxChild) {
        implHelperScrollview.setListViewHeightBasedOnChildren(maxChild, 0);
    }

    @Override
    public int getPagerStep() {
        return implHelperScrollview.getPagerStep();
    }

    public void setPagerStep(int step) {
        this.implHelperScrollview.pagerStep = step;
    }

    @Override
    public boolean isMultipleSelectionEnabled() {
        return enableMultipleSelection;
    }

    @Override
    public void setMultipleSelectionEnabled(boolean enable) {
        enableMultipleSelection = enable;
    }

    public void setHelperScrollListener(AbsListViewScrollListener l) {
        this.scrollListener = l;
        super.setOnScrollListener(this);
    }

    @Override
    public void setListViewHeightBasedOnChildren() {
    }

    @Override
    public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {
        scrollListener.onScrollStateChanged(view, scrollState);
    }

    @Override
    public void onScroll(android.widget.AbsListView view, int firstVisibleItem
            , int visibleItemCount, int totalItemCount) {
        implHelperScrollview.onScroll(view, firstVisibleItem,
                visibleItemCount, totalItemCount, scrollListener);
    }

    public void resumeLastPosition() {
        implHelperScrollview.resumeLastPosition();
    }
}

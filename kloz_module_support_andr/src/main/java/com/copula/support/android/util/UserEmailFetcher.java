package com.copula.support.android.util;

/**
 * Created by eliasigbalajobi on 6/21/16.
 */

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public class UserEmailFetcher {
    public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) return null;
        else return account.name;
    }

    public static String getEmailName(Context context) {
        String email = getEmail(context);
        if (email == null) return null;


        String[] splitted = email.split("@");
        if (splitted.length > 0)
            return splitted[0];
        return null;
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        if (accounts.length > 0) return accounts[0];
        return null;
    }
}

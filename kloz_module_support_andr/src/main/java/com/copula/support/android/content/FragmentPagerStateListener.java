package com.copula.support.android.content;

/**
 * Created by heeleaz on 12/27/16.
 */
public interface FragmentPagerStateListener {
    void onVisibleToPager();
}

package com.copula.support.android.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by eliasigbalajobi on 4/16/16.
 */
public class LineSpaceView extends View {
    public LineSpaceView(Context context) {
        super(context);
        setMinimumHeight(1);
    }

    public LineSpaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LineSpaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


}

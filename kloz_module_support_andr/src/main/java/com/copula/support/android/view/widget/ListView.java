package com.copula.support.android.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class ListView extends android.widget.ListView implements android.widget.AbsListView.OnScrollListener,
        AbsHelperListView {

    private ImplHelperScrollview implHelperScrollview;
    private AbsListViewScrollListener scrollListener;

    private boolean enableMultipleSelection = false;

    public ListView(Context context) {
        super(context);
        implHelperScrollview = new ImplHelperScrollview(this);
    }

    public ListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        implHelperScrollview = new ImplHelperScrollview(this);
    }

    public ListView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        implHelperScrollview = new ImplHelperScrollview(this);
    }

    @Override
    public void setListViewHeightBasedOnChildren(int maxChild) {
        implHelperScrollview.setListViewHeightBasedOnChildren(maxChild, getDividerHeight());
    }

    @Override
    public void setListViewHeightBasedOnChildren() {
        implHelperScrollview.setListViewHeightBasedOnChildren(getAdapter().getCount(), getDividerHeight());
    }

    @Override
    public void setHelperScrollListener(AbsListViewScrollListener l) {
        this.scrollListener = l;
        super.setOnScrollListener(this);
    }

    public void resumeLastPosition() {
        implHelperScrollview.resumeLastPosition();
    }

    @Override
    public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {
        scrollListener.onScrollStateChanged(view, scrollState);
    }

    @Override
    public int getPagerStep() {
        return this.implHelperScrollview.pagerStep;
    }

    @Override
    public void setPagerStep(int step) {
        this.implHelperScrollview.pagerStep = step;
    }

    @Override
    public int getPagerOffset() {
        return implHelperScrollview.offsetCount;
    }


    @Override
    public void nextPager() {
        this.implHelperScrollview.updateCallCount();
        this.implHelperScrollview.allowNext = true;
    }

    @Override
    public void onScroll(android.widget.AbsListView view, int firstVisibleItem
            , int visibleItemCount, int totalItemCount) {
        implHelperScrollview.onScroll(view, firstVisibleItem,
                visibleItemCount, totalItemCount, scrollListener);
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
    }

    @Override
    public void addHeaderView(View v, Object data, boolean isSelectable) {
        super.addHeaderView(v, data, isSelectable);
    }

    @Override
    public boolean isMultipleSelectionEnabled() {
        return enableMultipleSelection;
    }

    @Override
    public void setMultipleSelectionEnabled(boolean enable) {
        this.enableMultipleSelection = enable;
    }

    @Override
    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        super.setOnItemLongClickListener(listener);
    }

    public int getMaxHeightBaseOnChild(int childCount) {
        View item = getAdapter().getView(0, null, this);
        if (item != null) {
            item.measure(0, 0);
            return item.getMeasuredHeight() * childCount + 1;
        }
        return 0;
    }
}
package com.copula.support.android.network;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.AuthAlgorithm;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiConfiguration.Status;

/**
 * Setting hotspot profile
 */
public class NetConfigurator {
    public static WifiConfiguration hostWPASecuredNetwork(String ssid, String password) {
        WifiConfiguration config = new WifiConfiguration();
        config.SSID = ssid;
        configAsWPA(config, password);
        return config;
    }

    public static WifiConfiguration hostOpenNetwork(String ssid) {
        WifiConfiguration config = new WifiConfiguration();
        config.SSID = ssid;
        configAsOpen(config);
        return config;
    }

    static WifiConfiguration createBindableWPANetwork(ScanResult result, String password) {
        WifiConfiguration config = new WifiConfiguration();
        config.status = Status.ENABLED;
        config.SSID = "\"" + result.SSID + "\"";
        config.BSSID = result.BSSID;

        String capability = getScanResultSecurity(result.capabilities);
        if (capability != null && capability.equals(Constants.PSK)) {
            configAsWPA(config, "\"" + password + "\"");
        } else {
            configAsOpen(config);
        }
        return config;
    }

    private static void configAsWPA(WifiConfiguration config, String password) {
        config.preSharedKey = password;
        config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
    }

    private static void configAsOpen(WifiConfiguration config) {
        config.allowedAuthAlgorithms.set(AuthAlgorithm.OPEN);
        config.allowedKeyManagement.set(KeyMgmt.NONE);
    }

    private static String getScanResultSecurity(String capabilities) {
        String[] securityModes = {Constants.PSK, Constants.WEP, Constants.EAP, Constants.OPEN};
        for (String s : securityModes) {
            if (capabilities.contains(s)) return s;
        }
        return null;
    }

    public static boolean isOpenNetwork(ScanResult result) {
        String security = getScanResultSecurity(result.capabilities);
        return security == null || security.equals(Constants.OPEN);
    }

    public static String getSecurityType(WifiConfiguration configuration) {
        if (configuration.preSharedKey == null)
            return Constants.OPEN;
        return Constants.WEP;
    }

    public class Constants {
        // Constants used for different security types
        public static final String PSK = "PSK";
        public static final String WEP = "WEP";
        public static final String EAP = "EAP";
        public static final String OPEN = "Open";
    }
}

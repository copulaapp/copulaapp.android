package com.copula.support.android.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import com.copula.support.util.Formatter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;

/**
 * @author eliasigbalajobi protected class
 */
public class NetworkHelper {
    private static final String TAG = NetworkHelper.class.getSimpleName();
    private WifiManager mWifiManager;
    private Context mContext;

    public NetworkHelper(Context context) {
        this.mContext = context;
        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    public static InetAddress intToInetAddress(int hostAddress) {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try {
            return InetAddress.getByAddress(addressBytes);
        } catch (UnknownHostException e) {
            throw new AssertionError();
        }
    }

    public static byte[] getLoopbackAddress() {
        return new byte[]{127, 0, 0, 1};
    }

    public static byte[] defSystemAddress() {
        return new byte[]{(byte) 192, (byte) 168, 43, 1};
    }

    public static InetAddress getSystemAddress(Context ctx) {
        return new NetworkHelper(ctx).getSystemAddress();
    }

    public static boolean isDataEnabled(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static InetAddress getInetAddress(WifiInfo info) {
        try {
            Field mIpAddress = info.getClass().getDeclaredField("mIpAddress");
            mIpAddress.setAccessible(true);
            return (InetAddress) mIpAddress.get(info);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getMacAddress(byte[] address) {
        try {
            NetworkInterface ni = NetworkInterface.getByInetAddress(InetAddress.getByAddress(address));
            if (ni == null) return null;

            byte[] mac = ni.getHardwareAddress();
            return Formatter.bytesToHex(mac, ":");
        } catch (Exception e) {
            return null;
        }
    }

    public WifiManager getWifiManager() {
        return mWifiManager;
    }

    public boolean setHotspotEnabled(WifiConfiguration config, boolean enabled) {
        if (mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(false);
        }

        try {
            Method method = mWifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class,
                    boolean.class);
            return (Boolean) method.invoke(mWifiManager, config, enabled);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setWifiApConfiguration(WifiConfiguration config) {
        try {
            Method method = mWifiManager.getClass()
                    .getMethod("setWifiApConfiguration", WifiConfiguration.class);
            return (Boolean) method.invoke(mWifiManager, config);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * disables WifiAp if enabled
     *
     * @return {@link WpsInfo}
     */
    public WifiInfo getWifiInfo() {
        return mWifiManager.getConnectionInfo();
    }

    public WifiConfiguration getWifiConfiguration() {
        try {
            Method method = mWifiManager.getClass().getMethod("getWifiApConfiguration");
            return (WifiConfiguration) method.invoke(mWifiManager);
        } catch (Exception e) {
            return null;
        }
    }

    public void setWifiConfiguration(WifiConfiguration config) {
        try {
            Method method = mWifiManager.getClass().getMethod("setWifiApConfiguration", WifiConfiguration.class);
            method.invoke(mWifiManager, config);
        } catch (Exception e) {
        }
    }

    /**
     * Gets the Wi-Fi enabled state.
     *
     * @return {@link WIFI_AP_STATE}
     * @see #isWifiApEnabled()
     */
    public WIFI_AP_STATE getWifiApState() {
        try {
            Method method = mWifiManager.getClass().getMethod("getWifiApState");
            int tmp = ((Integer) method.invoke(mWifiManager));

            // Fix for Android 4
            if (tmp >= 10) tmp = tmp - 10;

            return WIFI_AP_STATE.class.getEnumConstants()[tmp];
        } catch (Exception e) {
            return WIFI_AP_STATE.WIFI_AP_STATE_FAILED;
        }
    }

    public InetAddress getSystemAddress() {
        try {
            NetworkHelper NS = new NetworkHelper(mContext);
            if (NS.isWifiApEnabled()) {
                return InetAddress.getByAddress(defSystemAddress());
            } else if (NS.isWifiConnected()) {
                return getInetAddress(NS.getWifiInfo());
            } else {
                return InetAddress.getByAddress(getLoopbackAddress());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isDeviceConnected() {
        return isWifiApEnabled() || isWifiConnected();
    }

    /**
     * Return whether Wi-Fi AP is enabled or disabled.
     *
     * @return {@code true} if Wi-Fi AP is enabled
     * @hide Dont open yet
     * @see #getWifiApState()
     */
    public boolean isWifiApEnabled() {
        return getWifiApState() == WIFI_AP_STATE.WIFI_AP_STATE_ENABLED;
    }

    public boolean isDataEnabled() {
        return isDataEnabled(mContext);
    }

    public void setDataEnabled(boolean enabled) {
        try {
            ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            Method method = manager.getClass().getMethod("setMobileDataEnabled", Boolean.TYPE);
            method.invoke(manager, enabled);
        } catch (Exception e) {
        }
    }

    private boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean isWifiConnected() {
        ConnectivityManager con = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = con.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnectedOrConnecting();
    }

    public boolean startWifi() {
        try {
            Method startWifi = mWifiManager.getClass().getMethod("startWifi");
            return (Boolean) startWifi.invoke(mWifiManager);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean releaseWifi() {
        try {
            Method startWifi = mWifiManager.getClass().getMethod("stopWifi");
            return (Boolean) startWifi.invoke(mWifiManager);
        } catch (Exception e) {
            return false;
        }
    }

    public enum WIFI_AP_STATE {
        WIFI_AP_STATE_DISABLING, WIFI_AP_STATE_DISABLED,
        WIFI_AP_STATE_ENABLING, WIFI_AP_STATE_ENABLED, WIFI_AP_STATE_FAILED
    }
}

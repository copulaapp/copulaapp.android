package com.copula.support.android.content;

/**
 * Created by heeleaz on 5/31/17.
 */
public interface IFragmentBackPress {
    boolean allowBackPressed();
}

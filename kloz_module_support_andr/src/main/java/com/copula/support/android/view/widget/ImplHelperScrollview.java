package com.copula.support.android.view.widget;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import com.copula.support.android.content.IScrollableHandler;

/**
 * Created by heeleaz on 4/9/17.
 */
public class ImplHelperScrollview {
    protected int offsetCount = 0, pagerStep = 10, pagedCount = 0;
    protected boolean allowNext = true;

    private int mLastFirstVisibleItem;
    private AbsListView absListView;

    private int listPosition;

    ImplHelperScrollview(AbsListView listView) {
        this.absListView = listView;
    }

    public static void notifyParentOnScrollStateChanged(Fragment f, AbsListView v, int scrollState) {
        Fragment pf = f.getParentFragment();
        if (pf != null && pf instanceof IScrollableHandler) {
            ((IScrollableHandler) pf).onScrollStateChanged(v, scrollState);
        }

        Activity act = f.getActivity();
        if (act instanceof IScrollableHandler) {
            ((IScrollableHandler) act).onScrollStateChanged(v, scrollState);
        }
    }

    public static void notifyParentOnScroll(Fragment f, AbsListView v, int scrollAxis) {
        Fragment pf = f.getParentFragment();
        if (pf != null && pf instanceof IScrollableHandler) {
            ((IScrollableHandler) pf).onScroll(v, scrollAxis);
        }

        Activity act = f.getActivity();
        if (act instanceof IScrollableHandler) {
            ((IScrollableHandler) act).onScroll(v, scrollAxis);
        }
    }

    void onScroll(AbsListView view, int first
            , int visibleCount, int totalCount, AbsListViewScrollListener listener) {

        int currentFirstVisibleItem = absListView.getFirstVisiblePosition();
        int scrolledAxis;
        if (currentFirstVisibleItem > mLastFirstVisibleItem) {
            scrolledAxis = AbsListViewScrollListener.SCROLL_UP;
        } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
            scrolledAxis = AbsListViewScrollListener.SCROLL_DOWN;
        } else scrolledAxis = 0;
        mLastFirstVisibleItem = currentFirstVisibleItem;
        listener.onScroll(view, first, visibleCount, totalCount, scrolledAxis);


        //to avoid multiple calls for last item
        final int lastItem = first + visibleCount;
        if (scrolledAxis != AbsListViewScrollListener.SCROLL_UP ||
                lastItem != totalCount || !allowNext) return;

        allowNext = false;
        offsetCount = pagedCount * pagerStep;
        listener.onPageScroll(totalCount, offsetCount, pagerStep);

        if (first > 0) listPosition = first;
    }

    void updateCallCount() {
        pagedCount += 1;
        allowNext = true;
    }

    public int getPagerStep() {
        return pagerStep;
    }

    void setListViewHeightBasedOnChildren(int maxChild, int dividerHeight) {
        ListAdapter listAdapter = absListView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(absListView.getWidth(),
                View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;

        maxChild = Math.min(listAdapter.getCount(), maxChild);
        for (int i = 0; i < maxChild; i++) {
            view = listAdapter.getView(i, view, absListView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = absListView.getLayoutParams();
        params.height = totalHeight + (dividerHeight * (listAdapter.getCount() - 1));
        absListView.setLayoutParams(params);
    }

    void setListViewHeightBasedOnChildren(int dividerHeight) {
        this.setListViewHeightBasedOnChildren(absListView.getAdapter().getCount(), dividerHeight);
    }

    void allowScrollingInScrollableView() {
        absListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    public void resumeLastPosition() {
        if (listPosition != -1) {
            //absListView.setSelection(listPosition);
            ListAdapter listAdapter = absListView.getAdapter();
            if (listAdapter instanceof BaseAdapter) {
                ((BaseAdapter) listAdapter).notifyDataSetChanged();
            }
        }
    }
}

package com.copula.support.android.content;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.StyleRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import com.copula.support.R;

import java.util.List;

public abstract class TabActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener,
        ActionBarTab.TabListener {
    private ActionBarTab actionBarTab;
    private ViewPager mViewPager;

    private List<Fragment> pagerFragmentList;

    protected abstract List<Fragment> getPagerFragments();

    protected abstract ViewPager getViewPager();

    protected abstract void onPrepareActivityWindow();

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.onPrepareActivityWindow();

        LinearLayout layout = (LinearLayout) findViewById(R.id.tab_container);
        layout.addView(actionBarTab.getView(), 0);

        mViewPager = getViewPager();
        mViewPager.setAdapter(new MyPagerAdapter(pagerFragmentList = getPagerFragments()));
        mViewPager.setCurrentItem(0);
        mViewPager.addOnPageChangeListener(this);
    }

    public List<Fragment> getPagerFragmentList() {
        return pagerFragmentList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        (actionBarTab = new ActionBarTab(this)).setTabListener(this);
    }

    public Fragment getFragment(int i) {
        return pagerFragmentList.get(i);
    }

    public void setCurrentPage(int page) {
        actionBarTab.selectTab(page);
    }

    public TabActivity addTab(int text) {
        actionBarTab.newTab(getString(text));
        return this;
    }

    public TabActivity addTab(Drawable drawable) {
        actionBarTab.newTab(drawable);
        return this;
    }

    public void setTabBarStyle(@StyleRes int styleRes) {
        actionBarTab.setTabBarStyle(styleRes);
    }

    public ActionBarTab getActionBarTab() {
        return actionBarTab;
    }

    public void setTabViewStyle(@StyleRes int styleRes) {
        actionBarTab.setTabStyle(styleRes);
    }

    @Override
    public void onPageSelected(int pos) {
        actionBarTab.selectTab(pos);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {
    }

    @Override
    public void onPageScrollStateChanged(int i) {
    }

    @Override
    public void onSelected(int position, ActionBarTab.Tab tab) {
        mViewPager.setCurrentItem(position, true);
    }

    public int getCurrentItem() {
        return mViewPager.getCurrentItem();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> fragmentList;

        MyPagerAdapter(List<Fragment> fragmentList) {
            super(getSupportFragmentManager());
            this.fragmentList = fragmentList;
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public Fragment getItem(int pos) {
            return fragmentList.get(pos);
        }
    }//end
}

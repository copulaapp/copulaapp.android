package com.copula.support.android.media;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore.Video.Thumbnails;

import java.util.ArrayList;

public class VideoThumbnailCache extends MediaAsync<String, String, Bitmap> {

    private static GalleryCache mCache;
    private int mWidth;
    private ArrayList<String> mCurrentTasks;
    private GallaryCacheCallback mCallback;

    public VideoThumbnailCache(Context context, int width, GallaryCacheCallback callback) {
        mWidth = width;
        mCurrentTasks = new ArrayList<String>();
        mCallback = callback;

        int memClass = ((ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        int size = 1024 * 1024 * memClass / 8;

        // Handle orientation change.
        GalleryRetainCache c = GalleryRetainCache
                .getOrCreateRetainableCache(size);
        mCache = c.mRetainedCache;

        if (mCache == null) {
            // The maximum bitmap pixels allowed in respective direction.
            // If exceeding, the cache will automatically scale the
            // bitmaps.
            /*
             * final int MAX_PIXELS_WIDTH = 100; final int MAX_PIXELS_HEIGHT =
			 * 100;
			 */
            mCache = new GalleryCache(size);
            c.mRetainedCache = mCache;
        }
    }

    public void loadBitmap(String fileUrl, boolean isScrolling) {
        if (!isScrolling && !mCurrentTasks.contains(fileUrl)) {
            executeOnExecutor(THREAD_POOL_EXECUTOR, fileUrl);
            mCurrentTasks.add(fileUrl);
        }
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String imageKey = params[0];
        Bitmap bitmap = null;
        try {
            bitmap = mCache.getBitmapFromCache(imageKey);
            if (bitmap != null) {
                return bitmap;
            }

            bitmap = ThumbnailUtils.createVideoThumbnail(imageKey,
                    Thumbnails.FULL_SCREEN_KIND);
            if (bitmap != null) {
                bitmap = Bitmap.createScaledBitmap(bitmap, mWidth, mWidth,
                        false);
                mCache.addBitmapToCache(imageKey, bitmap);
                return bitmap;
            }
            return null;
        } catch (Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
            return null;
        } finally {
            mCurrentTasks.remove(imageKey);
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (mCallback != null) {
            mCallback.onBitmapLoaded(bitmap);
        }
    }
}

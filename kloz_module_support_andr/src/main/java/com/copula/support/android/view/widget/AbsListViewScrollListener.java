package com.copula.support.android.view.widget;

import android.widget.AbsListView;

/**
 * Created by eliasigbalajobi on 6/1/16.
 */
public interface AbsListViewScrollListener {
    int SCROLL_UP = 1, SCROLL_DOWN = -1;

    void onScrollStateChanged(AbsListView view, int scrollState);

    void onPageScroll(int totalItem, int pageOffset, int pagerStep);

    void onScroll(AbsListView view, int first, int visibleCount, int totalCount, int scrollAxis);


}


package com.copula.support.util;

import android.os.Environment;
import android.os.StatFs;

import java.io.File;

/**
 * Created by eliasigbalajobi on 6/21/16.
 */
public class MemoryManager {
    public static float megabytesAvailable(File f) {
        StatFs stat = new StatFs(f.getPath());
        long bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        return bytesAvailable / (1024.f * 1024.f);
    }

    public static float getUsableSpace(){
        return Environment.getDataDirectory().getUsableSpace();
    }
}

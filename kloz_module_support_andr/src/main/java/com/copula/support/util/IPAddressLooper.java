package com.copula.support.util;

public class IPAddressLooper {
    private int value, position;

    public IPAddressLooper(int value) {
        this.value = value;
    }

    public IPAddressLooper(String ipAddr) {
        value = analizeAddress(ipAddr);
    }

    private static int analizeAddress(String ipAddr) {
        String[] parts = ipAddr.split("\\.");
        if (parts.length != 4) {
            throw new IllegalArgumentException();
        }

        return (Integer.parseInt(parts[0], 10) << (8 * 3)) & 0xFF000000
                | (Integer.parseInt(parts[1], 10) << (8 * 2))
                & 0x00FF0000 | (Integer.parseInt(parts[2], 10) << 8) & 0x0000FF00
                | (Integer.parseInt(parts[3], 10)) & 0x000000FF;
    }

    private int getOctet(int i) {
        if (i < 0 || i >= 4)
            throw new IndexOutOfBoundsException();

        return (value >> (i * 8)) & 0x000000FF;
    }

    public String toString() {
        String ip = "";
        for (int i = 3; i >= 0; --i) {
            ip += getOctet(i);
            if (i != 0) ip += ".";
        }
        return ip;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IPAddressLooper) {
            return value == ((IPAddressLooper) obj).value;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return value;
    }

    public String next() {
        value = value + 1;
        return toString();
    }

    public boolean asNext(String limit) {
        if (limit == null) return false;

        position += 1;
        return !(value == new IPAddressLooper(limit).value);
    }

    public int getPosition() {
        return position;
    }
}
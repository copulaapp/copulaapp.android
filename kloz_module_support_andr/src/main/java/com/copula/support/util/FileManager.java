package com.copula.support.util;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileManager {
    private static ExecutorService threadPool = Executors.newCachedThreadPool();

    public static boolean copyFile(File sourceFile, File destFile) {
        BufferedOutputStream bos = null;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(sourceFile));
            bos = new BufferedOutputStream(new FileOutputStream(destFile));

            int read;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, read);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (bos != null) bos.close();
                if (bos != null) bis.close();
            } catch (IOException e) {
            }
        }
    }

    public static boolean channelCopyFile(File sourceFile, File destFile) {
        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            try {
                if (source != null) source.close();
                if (destination != null) destination.close();
            } catch (Exception e) {
            }
        }
    }

    public static boolean deleteFile(final File source) {
        if (source == null || !source.exists()) return false;
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                source.delete();
            }
        });
        return true;
    }
}

package com.copula.support.util;

import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleIPScanner {
    private final Object scanLock = new Object();
    private final Object concurrentScanLock = new Object();

    private PingResultListener mListener;
    private InetAddress mScanAddress;

    private int reachableHostCount = 0, scanCount = 0;
    private int pingTimeout = 1000;
    private ExecutorService mScanPool = null;

    public void start(InetAddress address, final PingResultListener listener) {
        this.mScanAddress = address;
        this.mListener = listener;
        this.mScanPool = Executors.newFixedThreadPool(80);

        synchronized (concurrentScanLock) {
            this.scanSubnetAddress(resolveSubnetLoopStart(), resolveSubnetLoopEnd());
            this.scanCompleteChecker();
        }
    }

    public void setPingTimeout(int timeout) {
        this.pingTimeout = timeout;
    }

    public void startAsync(final InetAddress address, final PingResultListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                start(address, listener);
            }
        }).start();
    }

    private String resolveSubnetLoopStart() {
        byte[] ba = mScanAddress.getAddress();
        return (int) ba[0] + "." + (int) ba[1] + "." + (int) ba[2] + ".0";
    }

    private String resolveSubnetLoopEnd() {
        byte[] ba = mScanAddress.getAddress();
        return (int) ba[0] + "." + (int) ba[1] + "." + (int) ba[2] + ".255";
    }

    private void scanSubnetAddress(String startAddress, String endAddress) {
        final IPAddressLooper fetcher = new IPAddressLooper(startAddress);

        while (fetcher.asNext(endAddress)) {
            final String next = fetcher.next();
            mScanPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        InetAddress address = InetAddress.getByName(next);
                        if (address.isReachable(pingTimeout)) {
                            reachableHostCount += 1;
                            mListener.onAddressAlive(address);
                        }

                        synchronized (scanLock) {
                            if (++scanCount > 254) scanLock.notify();
                        }
                    } catch (Exception e) {
                    }
                }
            });//end execute
        }// end while
    }

    private void scanCompleteChecker() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (scanLock) {
                    while (scanCount < 255) {
                        try {
                            scanLock.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                    mScanPool.shutdown();
                    mListener.onScanComplete(reachableHostCount, scanCount);
                }
            }
        }).start();
    }

    public interface PingResultListener {
        void onAddressAlive(InetAddress host);

        void onScanComplete(int reachableHostCount, int scanCount);
    }
}
package com.copula.support.util;

/**
 * Created by heeleaz on 12/19/16.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.RequiresApi;
import com.squareup.picasso.Transformation;

public class BlurTransform implements Transformation {
    private RenderScript rs;
    private int blurRadius;

    public BlurTransform(Context context, int blurRadius) {
        super();
        rs = RenderScript.create(context);
        this.blurRadius = blurRadius;
    }

    @Override
    public Bitmap transform(Bitmap bitmap) {
        try {
            if (Build.VERSION.SDK_INT
                    >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                return transformer(bitmap);
            } else return bitmap;
        } catch (Exception e) {
            return bitmap;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private Bitmap transformer(Bitmap bitmap) {
        // Create another bitmap that will hold the results of the filter.
        Bitmap blurredBitmap = Bitmap.createBitmap(bitmap);

        // Allocate memory for Renderscript to work with
        Allocation input = Allocation.createFromBitmap(rs,
                bitmap, Allocation.MipmapControl.MIPMAP_FULL, Allocation.USAGE_SHARED);
        Allocation output = Allocation.createTyped(rs, input.getType());

        // Load up an instance of the specific script that we want to use.
        ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            script.setInput(input);
            // Set the blur radius
            script.setRadius(blurRadius);
            // Start the ScriptIntrinisicBlur
            script.forEach(output);
        }

        // Copy the output to the blurred bitmap
        output.copyTo(blurredBitmap);

        return blurredBitmap;
    }

    @Override
    public String key() {
        return "blur";
    }
}

package com.copula.support.util;


import com.copula.support.validator.EmailValidator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class Formatter {
    public final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String bytesToHex(byte[] bytes, String seperator) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (int i = 0; i < bytes.length; i++)
            sb.append(String.format("%02x%s", bytes[i] & 0xff, (i < bytes.length - 1) ? ":" : ""));
        return sb.toString();
    }

    public static String IPToDBAccept(String ipAddress) {
        return ipAddress.replaceAll("[\\.:]", "_");
    }

    public static String fromDbReadable(String dbReadableMacAddr) {
        return dbReadableMacAddr.substring("mac".length() - 0).replaceAll("[\\_]", ":");
    }

    public static byte[] toByteAddress(String ipAddress) {
        String[] s_ip = ipAddress.split("\\.");
        if (s_ip == null || s_ip.length < 4) {
            return new byte[]{};
        }

        byte[] ip = new byte[4];
        for (int i = 0; i < s_ip.length; i++) {
            int v = (Integer.valueOf(s_ip[i]));
            ip[i] = (byte) v;
        }
        return ip;
    }

    public static long toNumberFormat(String ipAddress) {
        String[] s_ip = ipAddress.split("\\.");
        if (s_ip == null || s_ip.length < 4) {
            return 0;
        }

        long ip = 0;
        for (int i = 0; i < s_ip.length; i++) {
            ip += Integer.valueOf(s_ip[i]) * Math.pow(10, 9 - (i * 3));
        }
        return ip;
    }

    public static String toElapsedDate(long millis) {
        millis = System.currentTimeMillis() - millis;
        int weeks = (int) (millis / (1000 * 60 * 60 * 24 * 7));
        if (weeks > 0) return Math.abs(weeks) + "w";

        int days = (int) ((millis / (1000 * 60 * 60 * 24)) % 7);
        if (days > 0) return Math.abs(days) + "d";

        long hour = (millis / (1000 * 60 * 60)) % 24;
        if (hour > 0) return Math.abs(hour) + "h";

        long minute = (millis / (1000 * 60)) % 60;
        if (minute > 0) return Math.abs(minute) + "m";

        long second = (millis / 1000) % 60;
        return Math.abs(second) + "s";
    }

    public static boolean isValidEmailAddress(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean isValidFullname(String fullName) {
        if (fullName == null || fullName.length() < 4) {
            return false;
        }

        return true;
    }

    public static boolean isValidUsername(String username) {
        String regex = "[a-zA-Z_][a-zA-Z0-9][a-zA-Z0-9_]*";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(username).matches();
    }

    public static int computePasswordStrength(String password) {
        if (password == null || password.isEmpty()) return 0;
        if (password.length() < 5) return 5;

        Pattern hasUppercase = Pattern.compile("[A-Z]");
        Pattern hasLowercase = Pattern.compile("[a-z]");
        Pattern hasNumber = Pattern.compile("\\d");
        Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

        int uc_test, lc_test, d_test, sc_test;

        if (hasUppercase.matcher(password).find()) uc_test = 3;
        else uc_test = 0;

        if (hasLowercase.matcher(password).find()) lc_test = 3;
        else lc_test = 0;

        if (hasNumber.matcher(password).find()) d_test = 2;
        else d_test = 0;

        if (hasSpecialChar.matcher(password).find()) sc_test = 2;
        else sc_test = 0;

        int qt = uc_test + lc_test + d_test + sc_test;
        return (password.length() >= 8 ? 5 : 3) + qt;
    }

    public static long dateToMilli(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

}

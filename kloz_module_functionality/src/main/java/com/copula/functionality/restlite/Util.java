package com.copula.functionality.restlite;

/**
 * Created by heeleeaz on 7/29/16.
 */
public class Util {
    public static int chunkOptimize(int filesize, int maxByteSize) {
        double factor = 1, readSize;
        while ((readSize = (filesize / factor)) > maxByteSize) {
            ++factor;//INCREMENT FACTOR TO DIVIDE FILESIZE
        }
        int result = (int) Math.ceil(readSize);
        return result % 2 == 0 ? result : result + 1;
    }
}

package com.copula.functionality.restlite;

import com.copula.functionality.restlite.annotation.RoutePath;
import com.copula.functionality.restlite.annotation.RouteProduces;
import com.sun.net.httpserver.HttpExchange;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by heeleeaz on 7/10/16.
 */
public class LiteRouter {
    private Map<String, Method> methodPathMap = new HashMap<>();
    private Map<Class<?>, Object> clsInstMap = new HashMap<>();
    private ResponseSender mResponseSender = new ResponseSender();

    public void addObject(Object object) {
        clsInstMap.put(object.getClass(), object);
        load(object.getClass());
    }

    private void load(Class<?> cls) {
        for (Method m : cls.getMethods()) {
            RoutePath methodPath = m.getAnnotation(RoutePath.class);
            if (methodPath != null) {
                String path = resolvePathSyntax(methodPath.value());
                methodPathMap.put(path, m);
            }
        }
    }

    private String resolvePathSyntax(String path) {
        if (path.charAt(0) != '/') return '/' + path;
        return path;
    }

    public int route(HttpExchange httpExchange) {
        String route = httpExchange.getRequestURI().getPath();
        if (!methodPathMap.containsKey(route)) {
            httpExchange.close();
            return 404;
        }

        try {
            Method method = methodPathMap.get(route);
            String query = httpExchange.getRequestURI().getQuery();
            Class<?>[] parameters = method.getParameterTypes();
            Object[] arguments = new Object[parameters.length];
            for (int i = 0; i < parameters.length; i++) {
                Class<?> cls = parameters[i];
                if (cls == QueryParam.class) {
                    arguments[i] = QueryParam.createFromURIQuery(query);
                    continue;
                }

                if (cls == HttpExchange.class) {
                    arguments[i] = httpExchange;
                }
            }

            Object instance = clsInstMap.get(method.getDeclaringClass());
            RouteProduces rp = method.getAnnotation(RouteProduces.class);
            Object response = method.invoke(instance, arguments);
            return mResponseSender.resolveResponse(httpExchange, rp, response);
        } catch (Exception e) {
            return 402;
        } finally {
            httpExchange.close();
        }
    }
}
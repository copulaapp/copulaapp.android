package com.copula.functionality.restlite.annotation;

import java.lang.annotation.*;

/**
 * Created by heeleeaz on 7/11/16.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RouteProduces {
    String value() default "*/*";
}

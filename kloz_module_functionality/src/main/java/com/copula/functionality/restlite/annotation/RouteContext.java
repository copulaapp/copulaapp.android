package com.copula.functionality.restlite.annotation;

import java.lang.annotation.*;

/**
 * Created by heeleeaz on 7/11/16.
 */
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RouteContext {
}

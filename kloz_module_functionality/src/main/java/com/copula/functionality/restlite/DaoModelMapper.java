package com.copula.functionality.restlite;

import android.database.Cursor;
import com.copula.functionality.restlite.annotation.DaoColumn;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleeaz on 7/4/16.
 */
public class DaoModelMapper {
    private static <T> T map(Cursor row, Class<T> to) throws Exception {
        T obj = to.newInstance();
        for (Field field : to.getFields()) {
            try {
                resolve(field, obj, row);
            } catch (Exception e) {
            }
        }
        return obj;
    }

    private static void resolve(Field field, Object obj, Cursor row) throws IllegalAccessException {
        String fieldName;
        Class<?> fieldType;

        DaoColumn columnAnn = field.getAnnotation(DaoColumn.class);
        if (columnAnn != null) {
            fieldName = (columnAnn.name().equals("")) ? field.getName() : columnAnn.name();
            fieldType = (columnAnn.type() == Object.class) ? field.getType() : columnAnn.type();
        } else {
            fieldName = field.getName();
            fieldType = field.getType();
        }

        int columnIndex = row.getColumnIndex(fieldName);
        if (columnIndex < 0) return;

        if (fieldType == String.class) {
            field.set(obj, row.getString(columnIndex));
        } else if (fieldType == Integer.class) {
            field.set(obj, row.getInt(columnIndex));
        } else if (fieldType == Long.class) {
            field.set(obj, row.getLong(columnIndex));
        }
    }

    public static <T> T mapOne(Cursor cursor, Class<T> to) {
        if (cursor == null) return null;
        try {
            return map(cursor, to);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> List<T> mapList(Cursor cursor, Class<T> to) {
        if (cursor == null) return null;

        List<T> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(mapOne(cursor, to));
        }
        return list;
    }
}

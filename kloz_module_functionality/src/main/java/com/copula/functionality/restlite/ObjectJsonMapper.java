package com.copula.functionality.restlite;

import com.copula.functionality.restlite.annotation.JSONInner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by eliasigbalajobi on 2/28/16.
 */
public class ObjectJsonMapper {
    private static Object map(Object obj, String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        Field[] clsFields = obj.getClass().getFields();
        if (clsFields.length <= 0) return obj;

        for (Field f : clsFields) {
            if (!jsonObject.has(f.getName())) continue;

            Object result = jsonObject.get(f.getName());
            if (result == null) continue;
            try {
                f.set(obj, result); //set the field value
            } catch (Exception e) {
            }
        }
        return obj;
    }

    public static <T> List<T> mapList(Class<?> cls, String jsonString) throws Exception {
        if (jsonString == null) return null;

        String json = jsonString;
        JSONInner parent = cls.getAnnotation(JSONInner.class);
        if (parent != null) {
            json = new JSONObject(jsonString).get(parent.value()).toString();
        }

        JSONArray jsonArray = new JSONArray(json);
        Object[] objects = new Object[jsonArray.length()];
        List<T> list = new ArrayList<>(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            objects[i] = cls.newInstance();
            map(objects[i], jsonArray.get(i).toString());
            list.add((T) objects[i]);
        }
        return list;
    }

    public static <T> T mapOne(Class<?> cls, String jsonString) throws Exception {
        if (jsonString == null) return null;

        Object instance = cls.newInstance();

        JSONInner parent = cls.getAnnotation(JSONInner.class);
        if (parent != null) {
            try {
                JSONObject jObj = new JSONObject(jsonString);
                String inner = jObj.getJSONObject(parent.value()).toString();
                instance = map(instance, inner);
            } catch (Exception e) {
            }
        }
        return (T) map(instance, jsonString);//remark
    }

    public static void mapOne(Object object, String jsonString) throws Exception {
        map(object, jsonString);
    }

    private static JSONObject unMapObject(Object object) {
        if (object == null) return null;

        Class<?> cls = object.getClass();
        Field[] fields = cls.getFields();
        if (fields == null || fields.length <= 0)
            return null;

        JSONObject jObj = new JSONObject();
        for (Field f : fields) {
            try {
                jObj.put(f.getName(), f.get(object));
            } catch (Exception e) {
            }
        }
        return jObj;
    }

    public static String toJSON(Object object) {
        if (object == null) return null;

        if (object instanceof Object[]) {
            JSONArray jsonArray = new JSONArray();
            for (Object o : (Object[]) object) {
                JSONObject jsonObject = unMapObject(o);
                if (jsonObject != null) jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        } else if (object instanceof Collection) {
            JSONArray jsonArray = new JSONArray();
            for (Object o : (Collection) object) {
                JSONObject jsonObject = unMapObject(o);
                if (jsonObject != null) jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        } else {
            JSONObject j = unMapObject(object);
            if (j != null) return j.toString();
            return "";
        }
    }
}

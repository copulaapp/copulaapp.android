package com.copula.functionality.restlite;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eliasigbalajobi on 3/3/16.
 */
public class MediaType {
    public static final String APPLICATION_JSON = "application/json";
    public static final String TEXT_PLAIN = "text/plain";
    public static final String IMAGE_BITMAP = "image/x-bitmap";
    public static final String BINARY_FILE = "binary/file";
    public static final String STREAM_HTTP = "media/stream-http";

    private static Map<String, String> map = new HashMap<>();

    static {
        map.put("mp3", "audio/mp3");
        map.put("m4a", "audio/mp3");
        map.put("mp4", "video/mp4");
        map.put("mpeg", "video/mp4");
        map.put("mpga", "audio/mp3");
        map.put("mkv", "video/mp4");
        map.put("avi", "video/x-msvideo");
        map.put("avs", "video/avs-video");
        map.put("mid", "audio/x-mid");
        map.put("midi", "audio/midi");
        map.put("mjpg", "video/x-motion-jpeg");
        map.put("moov", "video/quicktime");
        map.put("mov", "video/quicktime");
        map.put("movie", "video/x-sgi-movie");
        map.put("jpg", "image/jpeg");
        map.put("jpeg", "image/jpeg");
        map.put("png", "image/png");
        map.put("apk", "*/*");
    }

    public static String getMIME(String fileName) {
        if (fileName == null) return null;

        int i = fileName.lastIndexOf('.');
        int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
        if (i > p) {
            String extension = fileName.substring(i + 1);
            return map.get(extension.toLowerCase());
        }
        return null;
    }
}

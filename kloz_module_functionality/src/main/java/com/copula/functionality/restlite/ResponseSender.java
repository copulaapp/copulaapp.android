package com.copula.functionality.restlite;

import android.graphics.Bitmap;
import android.util.Log;
import com.copula.functionality.restlite.annotation.RouteProduces;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import java.io.*;

/**
 * Created by heeleeaz on 7/11/16.
 */
public class ResponseSender {
    public int resolveResponse(HttpExchange exchange, RouteProduces produces, Object result) {
        switch (produces.value()) {
            case MediaType.APPLICATION_JSON:
                return sendPlainText(exchange, ObjectJsonMapper.toJSON(result));
            case MediaType.TEXT_PLAIN:
                return sendPlainText(exchange, String.valueOf(result));
            case MediaType.IMAGE_BITMAP:
                return sendBitmapImage(exchange, (Bitmap) result);
            case MediaType.BINARY_FILE:
                return sendBinaryFile(exchange, (File) result);
            case MediaType.STREAM_HTTP:
                return stream((HttpMediaStream) result);
            default:
                return 404;
        }
    }

    private int sendPlainText(HttpExchange httpExchange, String text) {
        if (text == null) {
            try {
                httpExchange.sendResponseHeaders(404, 0);
            } catch (IOException e1) {
            }
            return 404;
        }

        OutputStream os = null;
        byte[] buffer = null;
        try {
            os = httpExchange.getResponseBody();
            buffer = text.getBytes();
            httpExchange.sendResponseHeaders(200, buffer.length);
            os.write(buffer);
            os.flush();
            return 200;
        } catch (IOException e) {
            try {
                httpExchange.sendResponseHeaders(404, 0);
            } catch (IOException e1) {
            }
            return 404;
        } finally {
            try {
                buffer = null;
                if (os != null) os.close();
            } catch (IOException e) {
            }
        }
    }

    private int sendBinaryFile(HttpExchange httpExchange, File file) {
        if (file == null || !file.exists()) {
            try {
                httpExchange.sendResponseHeaders(404, 0);
            } catch (IOException e1) {
            }
            return 404;
        }

        Headers rh = httpExchange.getResponseHeaders();
        rh.set("Content-Type", MediaType.getMIME(file.getAbsolutePath()));
        rh.set("content-disposition", "attachment; filename=\"" + file.getName() + "\"");
        rh.set("Content-Name", file.getName());

        BufferedOutputStream bos = null;
        BufferedInputStream bis = null;
        try {
            bos = new BufferedOutputStream(httpExchange.getResponseBody());
            bis = new BufferedInputStream(new FileInputStream(file));
            int fs = bis.available();
            httpExchange.sendResponseHeaders(200, fs);

            //init with 1MB(MAX) to avoid expansion for small buffer
            int chunk = Util.chunkOptimize(fs, 1024 * 1024);
            byte[] buffer = new byte[chunk];
            int count;
            while ((count = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, count);
            }
            bos.flush();
            return 200;
        } catch (IOException e) {
            try {
                httpExchange.sendResponseHeaders(404, 0);
            } catch (IOException e1) {
            }
            return 404;
        } finally {
            try {
                if (bos != null) bos.close();
                if (bis != null) bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int sendBitmapImage(HttpExchange httpExchange, Bitmap image) {
        if (image == null) {
            try {
                httpExchange.sendResponseHeaders(404, 0);
            } catch (IOException e1) {
            }
        }

        Headers responseHeader = httpExchange.getResponseHeaders();
        responseHeader.set("Content-Type", "image/jpeg");

        OutputStream os = null;
        ByteArrayOutputStream bos = null;
        try {
            os = httpExchange.getResponseBody();
            bos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            httpExchange.sendResponseHeaders(200, bos.size());
            os.write(bos.toByteArray());
            os.flush();
            return 200;
        } catch (IOException e) {
            try {
                httpExchange.sendResponseHeaders(404, 0);
            } catch (IOException e1) {
            }
            return 404;
        } finally {
            try {
                if (os != null) os.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
            }
        }
    }

    private int stream(HttpMediaStream stream) {
        if (stream == null) return 404;

        stream.stream();
        return 200;
    }
}

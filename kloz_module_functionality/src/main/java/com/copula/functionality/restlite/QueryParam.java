package com.copula.functionality.restlite;

/**
 * Created by eliasigbalajobi on 1/16/16.
 */

import java.util.HashMap;
import java.util.Map;

public class QueryParam {
    private static final String AND_DELMER = "&";
    private static final String EQUAL_DELMER = "=";
    private Map<String, String> map = new HashMap<>();

    public QueryParam() {
    }

    public static QueryParam createFromURIQuery(String query) {
        QueryParam argMap = new QueryParam();

        if (query == null) return argMap;
        String[] queryParams = query.split(AND_DELMER);
        if (query.length() <= 0) return argMap;

        for (String s : queryParams) {
            String[] qParams = s.split(EQUAL_DELMER);
            if (qParams.length != 2) continue;
            argMap.put(qParams[0], qParams[1]);
        }
        return argMap;
    }

    public static String toUrlQuery(QueryParam argMap) {
        String query = "";
        Map<String, String> map = argMap.map;

        String[] keySet = new String[map.size()];
        map.keySet().toArray(keySet);

        for (int i = 0; i < keySet.length; i++) {
            String key = keySet[i];
            String value = map.get(key);
            if (i != keySet.length - 1) query += key + EQUAL_DELMER + value + AND_DELMER;
            else query += key + EQUAL_DELMER + value;
        }
        return query;
    }

    public QueryParam put(String key, String value) {
        try {
            if (value == null) return this;
            //replace any (=) by (&) if exists in the arg(value)
            this.map.put(String.valueOf(key), value.replaceAll("=", "::"));
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return this;
        }
    }

    public QueryParam put(String key, boolean value) {
        this.map.put(String.valueOf(key), String.valueOf(value));
        return this;
    }

    public QueryParam put(String key, int value) {
        this.map.put(String.valueOf(key), String.valueOf(value));
        return this;
    }

    public QueryParam put(String key, long value) {
        this.map.put(String.valueOf(key), String.valueOf(value));
        return this;
    }

    @Override
    public String toString() {
        return QueryParam.toUrlQuery(this);
    }

    public String getString(String key) {
        String value = this.map.get(key);
        if (value == null) return null;
        //replace any (&) by (=) if exists in the arg(value)
        return value.replaceAll("::", "=");
    }

    public int getInt(String key) {
        String value = this.map.get(key);
        if (value == null) return 0;
        return Integer.parseInt(value);
    }

    public int getInt(String key, int def) {
        String value = this.map.get(key);
        if (value == null) return def;
        return Integer.parseInt(value);
    }

    public boolean getBoolean(String key) {
        return Boolean.valueOf(this.map.get(key));
    }

    public long getLong(String key) {
        return Long.valueOf(this.map.get(key));
    }

    public boolean containsKey(String key) {
        return this.map.containsKey(key);
    }

    public Map<String, String> getMap() {
        return map;
    }
}


package com.copula.functionality.restlite;

import android.util.Log;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by heeleeaz on 7/11/16.
 */
public class HttpMediaStream {
    private static final String TAG = HttpMediaStream.class.getName();

    private static final int HTTP_PARTIAL_CONTENT = 206;
    private static final int HTTP_STATUS_NOT_FOUND = 404;

    private String mediaType, mediaFile;
    private HttpExchange mHttpExchange;
    private double mediaDuration;

    private HttpMediaStream() {
    }

    public static HttpMediaStream getInstance() {
        return new HttpMediaStream();
    }

    public void setMediaDuration(double mediaDuration) {
        this.mediaDuration = mediaDuration;
    }

    public void setMediaFile(String file) {
        this.mediaFile = file;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public void setHttpExchange(HttpExchange exchange) {
        this.mHttpExchange = exchange;
    }

    public void stream() {
       /* check media null state, if null. send http error message  */

        int fileLength = 0;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(mediaFile));
            if ((fileLength = bis.available()) <= 0) {
                mHttpExchange.sendResponseHeaders(HTTP_STATUS_NOT_FOUND, 0);
                Log.d(TAG, "Media file not available");
                return;
            }
        } catch (IOException e) {
            Log.d(TAG, "@fileOpening: " + e.getMessage());
            if (bis == null) return;
        }

        Headers requestHeaders = mHttpExchange.getRequestHeaders();
        //for (String key : requestHeaders.keySet()) {
        //    Log.d("Hello", key + " : " + requestHeaders.getFirst(key));
        //}

        //get media stream start and end position from client
        int startReading = 0, endReading = fileLength;
        try {
            String range = requestHeaders.getFirst("Range");
            String[] split = ((range.split("="))[1]).split("-");
            startReading = Integer.valueOf(split[0]);
            endReading = Integer.valueOf(split[1]);
        } catch (Exception e) {
        }

        /* set http header */
        Headers responseHeader = mHttpExchange.getResponseHeaders();
        responseHeader.set("Accept-Ranges", "bytes");
        responseHeader.set("Content-Length", "" + fileLength);
        responseHeader.set("Content-Type", mediaType);
        responseHeader.set("X-Content-Duration", "" + mediaDuration);
        responseHeader.set("Content-Duration", "" + mediaDuration);
        responseHeader.set("Content-Range", ("bytes " + startReading + "-" +
                (endReading - 1) + "/" + fileLength));

        /** start writing media dataPath */
        OutputStream os = mHttpExchange.getResponseBody();
        int readLength = endReading - startReading;
        try {
            mHttpExchange.sendResponseHeaders(HTTP_PARTIAL_CONTENT, 0);
        } catch (IOException e) {
            Log.d(TAG, "@sendResponseHeaders: " + e.getMessage());
        }

        //optimized buffer length.. with a max of 1MB
        byte[] buffer = null;
        try {
            buffer = new byte[Util.chunkOptimize(readLength, 1024 * 1024)];
            bis.skip(startReading);//skip to range server-reading position

            int read;
            while ((read = bis.read(buffer)) != -1) {
                os.write(buffer, 0, read);
            }

        } catch (Exception e) {
            Log.d(TAG, "@write: " + e.getMessage());
        } finally {
            try {
                if (os != null) os.close();
                bis.close();
            } catch (IOException e) {
                Log.d(TAG, "@close: " + e.getMessage());
            }
        }//end finally
    }
}//END

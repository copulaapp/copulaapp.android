package com.copula.functionality.util;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import com.copula.functionality.downloader.DaoUMediaDownload;
import com.copula.functionality.downloader.MediaDownloadEntry;

import java.util.List;

/**
 * Created by heeleaz on 3/19/17.
 */
public class MediaContentResolver {
    public static final int ACTION_DOWNLOAD = 0, ACTION_DELETE = 1;
    private Context context;
    private DaoUMediaDownload daoMediaDownload;
    private int action;

    public MediaContentResolver(Context context) {
        this.context = context;
        daoMediaDownload = new DaoUMediaDownload(context);
    }

    public static void updateContentProvider(Context context, String[] paths) {
        MediaScannerConnection.scanFile(context, paths, null, null);
    }

    public void updateInDownload(final MediaDownloadEntry media) {
        this.action = ACTION_DOWNLOAD;
        MediaScannerConnection.scanFile(context, new String[]{media.downloadTo},
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        if (uri == null) return;

                        List<String> paths = uri.getPathSegments();
                        if (paths == null || paths.size() < 1) return;

                        try {
                            int id = Integer.valueOf(paths.get(paths.size() - 1));
                            updateIdInDownload(media.downloadId, id);
                        } catch (NumberFormatException e) {
                        }
                    }
                });//END
    }

    private void updateIdInDownload(long downloadId, int mediaId) {
        daoMediaDownload.updateMediaId(downloadId, mediaId);
    }
}

package com.copula.functionality.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by heeleeaz on 7/10/16.
 */
public class DatabaseContext extends SQLiteOpenHelper {
    private String dbName, tbName;
    private int version;
    private Context mContext;

    public DatabaseContext(Context context, String dbName, String tbName, int version) {
        super(context, dbName, null, version);
        this.dbName = dbName;
        this.tbName = tbName;
        this.version = version;
        this.mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public boolean truncateTable() {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            return db.delete(tbName, null, null) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }
}

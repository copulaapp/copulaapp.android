package com.copula.functionality.util;


public class ConnectionConstant {
    public static final String LOOPBACK_ADDRESS = "127.0.0.1";
    public static final int MEDIA_QUERY_PORT = 32451;
    public static final int MEDIA_STREAM_PORT = 13234;
    public static final int MEDIA_DOWNLOAD_PORT = 27243;
    public static final int COPULA_APK_DOWNLOAD_PORT = 55555;
    public static final int APP_WIFI_CONNECTION = 0xAA;
    public static boolean HOST_ONLINE_STATUS = false;

    /**
     * renewed connection to be set true when server start/restart
     */
    public static boolean SERVER_REFRESHED = true;
}

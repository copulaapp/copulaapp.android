package com.copula.functionality.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.copula.functionality.downloader.MediaDownloadClient;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.support.validator.UrlValidator;

import java.io.File;
import java.util.Locale;

/**
 * Created by eliasigbalajobi on 3/6/16.
 */
public class MediaUtility {
    public static final int DP_UPLOAD_DIM = 400;

    public static int chunkOptimize(int filesize, int maxByteSize) {
        double factor = 1, readSize;
        while ((readSize = (filesize / factor)) > maxByteSize) {
            ++factor;//INCREMENT FACTOR TO DIVIDE FILESIZE
        }
        int result = (int) Math.ceil(readSize);
        return result % 2 == 0 ? result : result + 1;
    }

    public static long checkMediaFileSize(int maxFileSize, File mediaFile) {
        /** Get length of file in bytes */
        long fileSizeInBytes = mediaFile.length();

        /** Convert the bytes to Kilobytes (1 KB = 1024 Bytes) */
        long fileSizeInKB = fileSizeInBytes / 1024;

        /** Convert the KB to MegaBytes (1 MB = 1024 KBytes) */
        long fileSizeInMB = fileSizeInKB / 1024;

        if (maxFileSize == 0 || fileSizeInMB <= maxFileSize) {
            return fileSizeInBytes;
        }
        return -1;
    }

    public static String formatDuration(long millis) {
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        if (hour == 0) {
            return String.format(Locale.getDefault(), "%02d:%02d", minute, second);
        } else {
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minute, second);
        }
    }

    public static String filesizeToString(long filesize) {
        return (filesize > 1048576f) ?
                String.format(Locale.getDefault(),
                        "%.2f%s", (filesize / 1048576f), "MB") :
                String.format(Locale.getDefault(),
                        "%.2f%s", (filesize / 1024f), "KB");
    }

    private static String getSubDirectory(int mediaType) {
        if (mediaType == 1) return "Image";
        else if (mediaType == 2) return "Audio";
        else if (mediaType == 3) return "Video";
        else return "Media";
    }

    public static File getDownloadMediaDir(int mediaType, String filename) {
        String subDir = getSubDirectory(mediaType);
        File dir = Environment.getExternalStoragePublicDirectory("Copula/Downloads/" + subDir);

        dir.mkdirs();//make app dir for  media download
        if (filename != null) dir = new File(dir.getAbsolutePath() + "/" + filename);

        return dir;
    }

    public static File getProfileImageDir() {
        File dir = Environment.getExternalStoragePublicDirectory("Copula/Profile Images");
        dir.mkdirs();//make app dir for profile image
        return dir;
    }

    public static File tmpDir() {
        File dir = Environment.getExternalStoragePublicDirectory("Copula/tmp");
        dir.mkdirs();
        return dir;
    }

    public static File tmpTimelineResourceDir() {
        File dir = Environment.getExternalStoragePublicDirectory("Copula/tmp/t");
        dir.mkdirs();
        return dir;
    }

    public static File getTimelineImage(String filename) {
        return new File(tmpTimelineResourceDir().getAbsolutePath() + "/" + filename);
    }

    public static String appendSize(String url, int maxSize) {
        return url + "&maxSize=" + maxSize;
    }

    public static File makeTmpFile(String url) {
        if (url == null) return null;
        if (!UrlValidator.getInstance().isValid(url)) return null;

        String toPath = MediaUtility.tmpDir() + "/im.tmp";

        try {
            MediaDownloadClient.download(url, toPath);
            File file = new File(toPath);
            file.deleteOnExit();
            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isSDCardMounted() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static int getMediaType(String mediaType) {
        switch ((mediaType.split("/"))[0]) {
            case "image":
                return MediaModel.IMAGE;
            case "audio":
                return MediaModel.AUDIO;
            case "video":
                return MediaModel.VIDEO;
            default:
                return 0;
        }
    }

    public static String getMimeType(int mediaType) {
        switch (mediaType) {
            case MediaModel.IMAGE:
                return "image/*";
            case MediaModel.AUDIO:
                return "audio/*";
            case MediaModel.VIDEO:
                return "video/*";
            default:
                return "*/*";
        }
    }

    public static int getMediaIdFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri,
                    proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
            cursor.moveToFirst();
            return cursor.getInt(column_index);
        } catch (Exception e) {
            return -1;
        } finally {
            if (cursor != null) cursor.close();
        }
    }
}

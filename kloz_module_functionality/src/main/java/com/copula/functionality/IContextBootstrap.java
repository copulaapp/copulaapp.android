package com.copula.functionality;

/**
 * Created by heeleaz on 7/9/17.
 */
public interface IContextBootstrap {
    void boot(BootstrapListener callback);

    interface BootstrapListener {
        void onBootstrapStarted(Class<?> contextName);

        void onBootstrapCompleted(Class<?> contextName);
    }
}

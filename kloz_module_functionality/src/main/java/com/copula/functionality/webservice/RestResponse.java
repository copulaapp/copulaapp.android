package com.copula.functionality.webservice;

import java.io.Serializable;

/**
 * Created by eliasigbalajobi on 3/23/16.
 */
public class RestResponse<T> implements Serializable {
    private static final long serialVersionUID = -1172749069852898552L;
    public String statusMessage, nextPageToken;
    public int statusCode;
    public T result;
}

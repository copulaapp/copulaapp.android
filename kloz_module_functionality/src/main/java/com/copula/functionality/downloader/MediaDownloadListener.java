package com.copula.functionality.downloader;


import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by eliasigbalajobi on 2/15/16.
 */
public abstract class MediaDownloadListener {
    private ExecutorService executorService = Executors.newFixedThreadPool(2);
    private HashSet<Listener> mListeners = new HashSet<>();

    public void addListener(Listener listener) {
        mListeners.add(listener);
    }

    public void removeListener(Listener listener) {
        mListeners.remove(listener);
    }

    void dispatchDownloadEvent(final MediaDownloadEntry download, final int state) {
        for (final Listener listener : mListeners) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    listener.downloadEvent(download, state);
                }
            });
        }
    }

    public interface Listener {
        void downloadEvent(MediaDownloadEntry download, int state);
    }
}

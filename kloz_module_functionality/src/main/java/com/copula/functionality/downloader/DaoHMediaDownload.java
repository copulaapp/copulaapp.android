package com.copula.functionality.downloader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliasigbalajobi on 2/25/16.
 */
public class DaoHMediaDownload extends DatabaseContext {
    private static final String TB_NAME = "host_download_history";

    public DaoHMediaDownload(Context context) {
        super(context, "host_download_history.db", TB_NAME, 2);
    }

    public boolean newDownload(HMediaDownloadEntry download) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("media_id", download.mediaId);
            values.put("media_type", download.mediaType);
            values.put("created_at", download.createdAt);
            values.put("user_id", download.userId);
            return db.insert(TB_NAME, null, values) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    private HMediaDownloadEntry populateEntry(Cursor cu) {
        HMediaDownloadEntry entry = new HMediaDownloadEntry();
        entry.mediaType = cu.getInt(cu.getColumnIndex("media_type"));
        entry.mediaId = cu.getInt(cu.getColumnIndex("media_id"));

        entry.createdAt = cu.getInt(cu.getColumnIndex("created_at"));
        entry._id = cu.getInt(cu.getColumnIndex("_id"));
        entry.userId = cu.getString(cu.getColumnIndex("user_id"));
        return entry;
    }

    public List<HMediaDownloadEntry> getAllDownloads(String limit) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TB_NAME, null, null, null, null, null, "_id desc", limit);
            if (cu.getCount() <= 0) return null;

            List<HMediaDownloadEntry> list = new ArrayList<>();
            while (cu.moveToNext()) list.add(populateEntry(cu));

            return list;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public boolean removeDownload(HMediaDownloadEntry entry) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            return db.delete(TB_NAME, "_id=" + entry._id, null) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME;
        sql += "(_id integer primary key autoincrement,";
        sql += "media_type integer(1),";
        sql += "media_id integer,";
        sql += "user_id varchar(30),";
        sql += "created_at integer)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

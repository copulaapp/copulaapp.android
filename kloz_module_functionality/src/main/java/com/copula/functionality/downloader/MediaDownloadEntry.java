package com.copula.functionality.downloader;

import com.copula.functionality.localservice.media.MediaModel;

/**
 * Created by eliasigbalajobi on 2/9/16.
 */
public class MediaDownloadEntry extends MediaModel {
    public static final int ERROR = -1;
    public static final int STATE_QUEUED = 0;
    public static final int STATE_PROGRESS = 1;
    public static final int STATE_START = 1;
    public static final int STATE_COMPLETED = 2;

    public int downloadCount, downloadState;
    public int downloadId;
    public String downloadUrl, downloadTo;

    @Override
    public boolean equals(Object o) {
        return ((MediaDownloadEntry) o).downloadId == downloadId;
    }
}

package com.copula.functionality.downloader;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.localservice.media.feeds.DaoLocalMediaCaption;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.util.MediaContentResolver;

public class MediaDownloadService extends IntentService implements MediaDownloadListener.Listener,
        NotificationHelper.NotificationPushListener {

    private static final String TAG = MediaDownloadService.class.getSimpleName();
    private static PowerManager.WakeLock mWakeLock;
    private DaoUMediaDownload daoMediaDownload;
    private DaoHMediaDownload hMediaDownloadDAO;
    private MyBinder myBinder = new MyBinder();

    public MediaDownloadService() {
        super(TAG);
    }

    public static void startService(Context context) {
        context.startService(new Intent(context, MediaDownloadService.class));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        daoMediaDownload = new DaoUMediaDownload(this);
        hMediaDownloadDAO = new DaoHMediaDownload(this);

        MediaDownloadClient downloader = MediaDownloadClient.getInstance();
        downloader.addListener(this);

        //prevent device processor sleep for data share
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        mWakeLock.acquire();//do rightful action.. holding declared wakelock

        NotificationHelper.getInstance().addListener(this);

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public void downloadEvent(MediaDownloadEntry download, int state) {
        if (state == MediaDownloadEntry.STATE_QUEUED) {
            daoMediaDownload.queueDownload(download);
        } else if (state == MediaDownloadEntry.STATE_START) {
            daoMediaDownload.startDownload(download);
        } else if (state == MediaDownloadEntry.STATE_COMPLETED) {
            daoMediaDownload.completeDownload(download);
            MediaDownloadEntry d = daoMediaDownload.getDownload(download.downloadId);
            NotificationHelper.getInstance().dispatchDownload(d, state);

            new MediaContentResolver(this).updateInDownload(download);
        } else {
            download.downloadState = state;
            daoMediaDownload.completeDownload(download);
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        try {
            if (mWakeLock != null) mWakeLock.release();
        } catch (Exception e) {
        }
    }

    @Override
    public void onPushNotification(int broadcast, NotificationHelper.NotificationModel entry) {
        if (broadcast == NotificationHelper.MEDIA_ACTION_NOTIFICATION) {
            //keep history of media my host downloaded
            MediaCaptionEntry model = (MediaCaptionEntry) entry.data;
            if (model.action == MediaCaptionEntry.ACTION_DOWNLOAD) {
                HMediaDownloadEntry hostDownload = new HMediaDownloadEntry();
                hostDownload.setMedia(model.getMedia());
                hostDownload.userId = model.actorId;
                hMediaDownloadDAO.newDownload(hostDownload);
            } else new DaoLocalMediaCaption(this).newAction(model);
        }//END
    }

    public class MyBinder extends Binder {
        public MediaDownloadService getService() {
            return MediaDownloadService.this;
        }
    }
}

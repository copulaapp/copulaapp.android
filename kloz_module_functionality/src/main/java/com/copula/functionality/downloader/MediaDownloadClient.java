package com.copula.functionality.downloader;

import com.copula.functionality.localservice.media.MediaModel;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


/**
 * Created by eliasigbalajobi on 2/7/16.
 */
public class MediaDownloadClient extends MediaDownloadListener implements Runnable {
    private static final int DOWNLOAD_TRACKER_BREAK = 500; //ms

    private static MediaDownloadClient singleton;

    private final Object lock = new Object();
    private OkHttpClient mHttpClient;
    private Thread downloadThread;//main download thread
    private ExecutorService threadPool = Executors.newCachedThreadPool();
    private boolean downloading = false, running = false;

    private Queue<MediaDownloadEntry> downloadQueue = new ArrayDeque<>();

    private int stopDownloadId = 0;

    private MediaDownloadClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(15000, TimeUnit.SECONDS);
        builder.readTimeout(15000, TimeUnit.SECONDS);
        mHttpClient = builder.build();
    }

    public static MediaDownloadClient getInstance() {
        if (singleton == null) {
            singleton = new MediaDownloadClient();
            singleton.start();
        }
        return singleton;
    }


    public static boolean download(String url, String toPath) throws IOException {
        if (url == null || toPath == null) return false;

        OkHttpClient okHttpClient = new OkHttpClient();
        Request.Builder builder = new Request.Builder().url(url);
        Response response = okHttpClient.newCall(builder.build()).execute();
        if (!response.isSuccessful()) return false;


        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(response.body().byteStream());
            bos = new BufferedOutputStream(new FileOutputStream(toPath));

            byte[] buffer = new byte[1024 * 200];
            int read;
            while ((read = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, read);
            }
        } finally {
            if (bos != null) bos.close();
            if (bis != null) bis.close();

        }
        return true;
    }

    public void addListener(Listener listener) {
        super.addListener(listener);
    }

    public void removeListener(Listener listener) {
        super.removeListener(listener);
    }

    public void queue(MediaModel item, String toPath) {
        MediaDownloadEntry download = new MediaDownloadEntry();
        download.setMedia(item);

        //serving as downloadId.. to keep track of the download
        download.downloadId = 2 + new Random().nextInt(Integer.MAX_VALUE);
        download.downloadTo = toPath;
        download.downloadState = MediaDownloadEntry.STATE_QUEUED;
        downloadQueue.add(download);

        dispatchDownloadEvent(download, MediaDownloadEntry.STATE_QUEUED);
        synchronized (lock) {
            lock.notify();
        }
    }

    public void queue(String url, String fileName, String toPath, int mediaType) {
        MediaDownloadEntry model = new MediaDownloadEntry();
        model.dataUrl = url;
        model.mediaType = mediaType;
        model.fileName = fileName;

        queue(model, toPath);
    }

    @Override
    public void run() {
        while (running) {
            synchronized (lock) {
                while (downloading || downloadQueue.size() == 0) {
                    try {
                        lock.wait();
                    } catch (Exception ignored) {
                    }
                }

                try {
                    MediaDownloadEntry queue = downloadQueue.poll();
                    if (queue != null) {
                        downloading = true;
                        download(queue);
                    }
                } catch (Exception ignored) {
                }
                downloading = false;
                lock.notify();
            }
        }
    }

    private void start() {
        if (downloadThread == null)
            downloadThread = new Thread(this);

        downloadThread.start();
        running = true;
    }

    private void download(MediaDownloadEntry queue) {
        DownloadTracker tracker = new DownloadTracker(queue);
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            Request.Builder builder = new Request.Builder().url(queue.dataUrl);
            Response response = mHttpClient.newCall(builder.build()).execute();
            bis = new BufferedInputStream(response.body().byteStream());
            bos = new BufferedOutputStream(new FileOutputStream(queue.downloadTo));

            if (queue.fileSize == 0) {
                String len = response.header("Content-Length", "0");
                queue.fileSize = Integer.parseInt(len);
            }
            if (queue.fileName == null) {
                queue.fileName = response.header("Content-Name", "file");
            }
            if (queue.mimeType == null) {
                queue.mimeType = response.header("Content-Type", "media");
            }

            queue.downloadState = MediaDownloadEntry.STATE_START;
            dispatchDownloadEvent(queue, MediaDownloadEntry.STATE_START);

            downloadTracker(tracker);//start download tracking
            byte[] buffer = new byte[1024 * 1000];//1MB buffer allocated
            while ((tracker.read = bis.read(buffer)) != -1) {
                if (stopDownloadId == queue.downloadId) {
                    queue.downloadState = MediaDownloadEntry.ERROR;
                    dispatchDownloadEvent(queue, MediaDownloadEntry.ERROR);
                    break;
                }

                tracker.count += tracker.read;
                bos.write(buffer, 0, tracker.read);
            }

            queue.downloadState = MediaDownloadEntry.STATE_COMPLETED;
            dispatchDownloadEvent(queue, MediaDownloadEntry.STATE_COMPLETED);
        } catch (Exception e) {
            tracker.read = -1;//to terminate tracker
            queue.downloadState = MediaDownloadEntry.ERROR;
            dispatchDownloadEvent(queue, MediaDownloadEntry.ERROR);
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (Exception ignored) {
            }
        }
    }

    public void stopDownload(int downloadId) {
        stopDownloadId = downloadId;
    }

    private void downloadTracker(final DownloadTracker tracker) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(DOWNLOAD_TRACKER_BREAK); //pause to allow first updateLike
                    while (tracker.read != -1) {
                        tracker.download.downloadCount = tracker.count;
                        tracker.download.downloadState = MediaDownloadEntry.STATE_PROGRESS;
                        dispatchDownloadEvent(tracker.download, MediaDownloadEntry.STATE_PROGRESS);

                        Thread.sleep(DOWNLOAD_TRACKER_BREAK); //break for another 300ms
                    }
                } catch (Exception ignored) {
                }
            }//end run
        });
    }

    public void terminate() {
        running = false;
        downloadQueue = null;
    }

    private class DownloadTracker {
        int read, count;
        MediaDownloadEntry download;

        DownloadTracker(MediaDownloadEntry model) {
            this.download = model;
        }
    }
}

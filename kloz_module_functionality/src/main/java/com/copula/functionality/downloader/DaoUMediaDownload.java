package com.copula.functionality.downloader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliasigbalajobi on 2/25/16.
 */
public class DaoUMediaDownload extends DatabaseContext {
    private static final String TB_NAME = "user_download_history";

    public DaoUMediaDownload(Context context) {
        super(context, "user_download_history.db", TB_NAME, 1);
    }

    public boolean queueDownload(MediaDownloadEntry download) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("download_url", download.downloadUrl);
            values.put("download_to", download.downloadTo);
            values.put("download_id", download.downloadId);
            values.put("download_state", download.downloadState);
            values.put("file_size", download.fileSize);
            values.put("file_name", download.fileName);
            values.put("media_type", download.mediaType);
            return db.insert(TB_NAME, null, values) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean startDownload(MediaDownloadEntry download) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("file_size", download.fileSize);
            values.put("file_name", download.fileName);
            values.put("mime_type", download.mimeType);
            values.put("download_state", download.downloadState);

            String[] wArgs = new String[]{download.downloadId + ""};
            return db.update(TB_NAME, values, "download_id=?", wArgs) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean completeDownload(MediaDownloadEntry download) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("download_state", download.downloadState);

            String[] whereArgs = new String[]{"" + download.downloadId};
            return db.update(TB_NAME, values, "download_id=?", whereArgs) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean updateMediaId(long downloadId, int mediaId) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("media_id", mediaId);

            String[] whereArgs = new String[]{"" + downloadId};
            return db.update(TB_NAME, values, "download_id=?", whereArgs) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    private MediaDownloadEntry populateEntry(Cursor cu) {
        MediaDownloadEntry e = new MediaDownloadEntry();
        e.downloadTo = cu.getString(cu.getColumnIndex("download_to"));
        e.downloadUrl = cu.getString(cu.getColumnIndex("download_url"));
        e.fileSize = cu.getInt(cu.getColumnIndex("file_size"));
        e.fileName = cu.getString(cu.getColumnIndex("file_name"));
        e.downloadState = cu.getInt(cu.getColumnIndex("download_state"));
        e.downloadId = cu.getInt(cu.getColumnIndex("download_id"));
        e.mimeType = cu.getString(cu.getColumnIndex("mime_type"));
        e.mediaType = cu.getInt(cu.getColumnIndex("media_type"));
        e.dataPath = e.downloadTo;
        return e;
    }

    public MediaDownloadEntry getDownload(long downloadId) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            String[] wArgs = new String[]{"" + downloadId};
            cu = db.query(TB_NAME, null, "download_id=?", wArgs, null, null, null);
            if (cu == null || cu.getCount() <= 0) return null;

            if (cu.moveToFirst()) return populateEntry(cu);
            return null;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public List<MediaDownloadEntry> getAllDownloads(String limit) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TB_NAME, null, null, null, null, null, "_id desc", limit);
            if (cu.getCount() <= 0) return null;

            List<MediaDownloadEntry> list = new ArrayList<>();
            while (cu.moveToNext()) list.add(populateEntry(cu));

            return list;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public boolean removeDownload(MediaDownloadEntry entry) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            String[] whereArgs = new String[]{"" + entry.downloadId};
            return db.delete(TB_NAME, "download_id=?", whereArgs) > -1;

        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME;
        sql += "(_id integer primary key autoincrement,";
        sql += "file_size integer,";
        sql += "file_name varchar(100),";
        sql += "download_state integer(1),";
        sql += "download_url varchar(500),";
        sql += "download_to varchar(500),";
        sql += "mime_type varchar(30),";
        sql += "media_type integer(1),";
        sql += "download_id integer)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

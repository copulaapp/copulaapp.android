package com.copula.functionality.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by heeleaz on 12/12/16.
 */
public class MediaPlayerInterceptor extends BroadcastReceiver {
    public static final int HEADSET = 0x1;
    public static final int TELEPHONY = 0x2;

    private Context context;
    private InterceptorListener mListener;

    public MediaPlayerInterceptor(Context context) {
        this.context = context;
    }

    public void register(InterceptorListener listener) {
        Log.d("Hello", "SDids");

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_HEADSET_PLUG);
        filter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);

        this.mListener = listener;
        context.registerReceiver(this, filter);
    }

    public void unregister() {
        try {
            context.unregisterReceiver(this);
        } catch (Exception e) {
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Hello", intent.getAction());
        if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
            int state = intent.getIntExtra("state", -1);
            if (state == 0) mListener.onHoldIntercept(HEADSET);
            else if (state == 1) mListener.onFreeIntercept(HEADSET);
        } else {
            String state = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            if (state == null) return;

            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                mListener.onFreeIntercept(TELEPHONY);
            } else mListener.onHoldIntercept(TELEPHONY);
        }
    }

    public interface InterceptorListener {
        void onHoldIntercept(int actor);

        void onFreeIntercept(int actor);
    }
}

package com.copula.functionality.media;

import android.media.MediaPlayer;
import android.util.Log;
import com.copula.functionality.localservice.media.VideoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleeaz on 8/10/16.
 */
public class VideoController extends MediaPlayer implements MediaPlayer.OnPreparedListener {
    public static final int STREAM = 0, LOCAL = 1;
    private static String TAG = VideoController.class.getSimpleName();
    private int playPosition = 0;

    private int streamType = LOCAL;//default should be local media.

    private OnPreparedListener mOnPreparedListener;
    private List<VideoModel> playList = new ArrayList<>();

    public void setPlayList(List<VideoModel> mediaList, int position) {
        this.playList = mediaList;
        this.playPosition = position;
    }

    public void setStreamType(int streamType) {
        this.streamType = streamType;
    }

    public VideoModel getNextMedia() {
        if (playList.size() <= 0) return null;

        if (playList.size() - 1 > playPosition) {
            return playList.get(playPosition + 1);
        } else return playList.get(playPosition);
    }

    public List<VideoModel> getPlayList() {
        return playList;
    }

    public void setPlayList(VideoModel media) {
        List<VideoModel> mediaList = new ArrayList<>(1);
        mediaList.add(media);

        setPlayList(mediaList, 0);
    }

    public VideoModel getPrevMedia() {
        if (playList.size() <= 0) return null;

        if (playPosition > 0 && playPosition < playList.size()) {
            return playList.get(playPosition - 1);
        } else return playList.get(playPosition);
    }

    public boolean prepareNextMedia() {
        VideoModel nextMedia = getNextMedia();
        if (nextMedia == null) return false;

        if (playList.size() - 1 > playPosition) playPosition++;
        prepare(nextMedia);

        return true;
    }

    public boolean preparePrevMedia() {
        VideoModel prevMedia = getPrevMedia();
        if (prevMedia == null) return false;

        if (playPosition > 0 && playPosition < playList.size()) --playPosition;
        prepare(prevMedia);
        return true;
    }

    public void prepare(VideoModel media) {
        try {
            if (media == null) return;

            String url = (streamType == LOCAL) ? media.dataPath : media.streamUrl;
            Log.d("Hello", url);
            super.stop();
            super.reset();
            super.setDataSource(url);

            super.prepare();
            onPrepared(this);
        } catch (Exception e) {
            Log.d(TAG, "MediaPlayError: " + e.getMessage());
        }
    }

    public void prepareAsync(VideoModel media) {
        try {
            if (media == null) return;

            String url = (streamType == LOCAL) ? media.dataPath : media.streamUrl;
            super.stop();
            super.reset();
            super.setDataSource(url);

            super.prepareAsync();
        } catch (Exception e) {
            Log.d(TAG, "MediaPlayError: " + e.getMessage());
        }
    }

    public VideoModel getCurrentTrack() {
        try {
            return playList.get(playPosition);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void seekTo(int i) throws IllegalStateException {
        if (i > 0) super.seekTo(i);
    }

    public int getTracksCount() {
        return playList.size();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        VideoModel m = getCurrentTrack();
        if (m != null) {
            m.duration = (m.duration == 0)
                    ? mediaPlayer.getDuration() : m.duration;
            int w = getVideoWidth();
            int h = getVideoHeight();

            if ((w & h) != 0) {
                m.width = w;
                m.height = h;
            }
        }
        if (mOnPreparedListener != null) mOnPreparedListener.onPrepared(mediaPlayer);
    }

    @Override
    public void setOnPreparedListener(OnPreparedListener listener) {
        super.setOnPreparedListener(this);
        this.mOnPreparedListener = listener;
    }

    public void destroy() {
        playList = new ArrayList<>();
        playPosition = 0;

        if (super.isPlaying()) super.stop();
        super.release();
    }
}

package com.copula.functionality.media;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.util.Log;
import com.copula.functionality.localservice.media.AudioModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by eliasigbalajobi on 4/30/16.
 */
public class AudioController extends MediaPlayer implements MediaPlayer.OnCompletionListener, MediaPlayer
        .OnPreparedListener {
    public static final int STREAM = 0, LOCAL = 1;
    /**
     * audio player broadcast states
     **/
    public static final int EXTRA_PLAY = 1, EXTRA_STOP = 2, EXTRA_PAUSE = 3;
    public static final int EXTRA_NEXT = 4, EXTRA_PREV = 5, EXTRA_PREPARING = 6;
    public static final int EXTRA_PLAY_COMPLETED = 7, EXTRA_MEDIA_ERROR = -1;

    public static final int REPEAT_PLAYLIST = 0;
    public static final int REPEAT_TRACK = 1;
    public static final int NO_REPEAT = 2;
    /**
     * broadcast extras
     **/
    public static final String INTENT_ACTION = "action";
    public static final String INTENT_AUDIO_CONTROLLER = "AudioController";
    private static final String TAG = "AudioController";
    private static AudioController sInstance;

    private List<AudioModel> queue = new ArrayList<>();

    private int streamType = LOCAL;//default should be local media.
    private Randomize randomize;
    private int playerState;
    private int playPosition = 0;//current media play position
    private int repeatMode = NO_REPEAT;

    private Context mContext;
    private SharedPreferences preferences;

    private AudioController(Context context) {
        this.mContext = context;
        super.setOnCompletionListener(this);
        super.setOnPreparedListener(this);

        preferences = context.getSharedPreferences("audio_controller", 0);
    }

    public static AudioController getInstance(Context ctx) {
        return (sInstance == null) ? sInstance = new AudioController(ctx) : sInstance;
    }

    public static AudioController newInstance(Context ctx) {
        return new AudioController(ctx);
    }

    public int getStreamType() {
        return streamType;
    }

    public void setStreamType(int type) {
        this.streamType = type;
    }

    @Override
    public void pause() throws IllegalStateException {
        super.pause();
        sendBroadcast(EXTRA_PAUSE);
    }

    @Override
    public void start() throws IllegalStateException {
        super.start();
        sendBroadcast(EXTRA_PLAY);
    }

    @Override
    public void stop() throws IllegalStateException {
        super.stop();
        sendBroadcast(EXTRA_STOP);
    }

    private void sendBroadcast(int state) {
        this.playerState = state;

        Intent intent = new Intent(INTENT_AUDIO_CONTROLLER);
        intent.putExtra(INTENT_ACTION, state);
        mContext.sendBroadcast(intent);
    }

    public int getPlayerState() {
        return playerState;
    }

    public void setQueue(List<AudioModel> media) {
        if (media == null || media.size() < 1) return;
        this.queue = media;
    }

    public void startPlay(int pos, boolean async) {
        try {
            AudioModel media = queue.get(playPosition = pos);
            if (media == null) return;

            this.sendBroadcast(EXTRA_PREPARING);
            super.stop();
            super.reset();

            String url = (streamType == LOCAL) ? media.dataPath : media.streamUrl;
            super.setDataSource(url);
            if (async) this.prepareAsync();
            else this.prepare();

        } catch (Exception e) {
            sendBroadcast(EXTRA_MEDIA_ERROR);
            Log.d(TAG, "MediaPlayError: " + e.getMessage());
        }
    }

    public void startPlay() {
        playPosition = getNextPosition(playPosition - 1);
        startPlay(playPosition, true);
    }

    public int getLastTrackMediaId() {
        return preferences.getInt("last_track_id", 0);
    }

    private int getNextPosition(int currentPosition) {
        if (randomize != null) return randomize.next();
        else if (queue.size() - 1 > currentPosition)
            return ++currentPosition;
        else return currentPosition;
    }

    private int getPrevPosition(int currentPosition) {
        if (randomize != null) return randomize.prev();
        else if (currentPosition > 0) return --currentPosition;
        else return currentPosition;
    }

    public boolean playNext(boolean async) {
        if (queue.size() <= 0) return false;

        startPlay(playPosition = getNextPosition(playPosition), async);

        this.sendBroadcast(EXTRA_NEXT);
        return true;
    }

    public boolean playPrev(boolean async) {
        if (queue.size() <= 0) return false;

        if (getCurrentPosition() < 3000) {
            startPlay(playPosition = getPrevPosition(playPosition), async);
        } else startPlay(playPosition, async);

        this.sendBroadcast(EXTRA_PREV);
        return true;
    }

    public void togglePlay(boolean async) {
        //if media is playing, then pause, else play
        if (isPlaying()) pause();//pause play
        else resume(async);
    }

    private void resume(boolean async) {
        if (getCurrentPosition() > 1) start();
        else startPlay(playPosition, async);
    }

    public void shuffle(Randomize randomize) {
        this.randomize = randomize;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        sendBroadcast(EXTRA_PLAY_COMPLETED);

        //check if playlist has been exhausted.
        // restart playlist if repeat_playlist is enabled
        if (repeatMode == REPEAT_PLAYLIST && playPosition == queue.size() - 1) {
            startPlay(playPosition = getNextPosition(playPosition), true);
            return;
        }

        if (repeatMode == NO_REPEAT) {
            startPlay(playPosition = getNextPosition(playPosition), true);
        } else if (repeatMode == REPEAT_TRACK) {
            startPlay(playPosition, true);
        }
    }

    public AudioModel getCurrentTrack() {
        try {
            return queue.get(playPosition);
        } catch (Exception e) {
            return null;
        }
    }

    public int findPlayPositionInQueue() {
        return queue.indexOf(getCurrentTrack());
    }

    public void setPlayPosition(int playPosition) {
        this.playPosition = playPosition;
    }

    public int findPlayPositionInList(List<AudioModel> m) {
        return m.indexOf(getCurrentTrack());
    }

    public Randomize getShuffle() {
        return randomize;
    }

    public int getRepeatMode() {
        return repeatMode;
    }

    public void setRepeatMode(int repeatMode) {
        this.repeatMode = repeatMode;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        AudioModel t = getCurrentTrack();
        t.duration = t.duration == 0 ? mp.getDuration() : t.duration;
        t.mediaComposer = t.mediaComposer == null ? t.fileName : t.mediaComposer;
        t.mediaTitle = t.mediaTitle == null ? t.fileName : t.mediaTitle;

        preferences.edit().putInt("last_track_id", t.mediaId).apply();

        super.start();
        this.sendBroadcast(EXTRA_PLAY);
    }

    public int getPlayListCount() {
        return queue.size();
    }

    public List<AudioModel> getPlayList() {
        return queue;
    }

    public AudioController setQueue(AudioModel play) {
        queue.clear();
        queue.add(play);
        return this;
    }

    public boolean isPlaylistEmpty() {
        return queue == null || queue.size() == 0;
    }

    public static class Randomize {
        private int[] indexes;
        private Random random;

        private int currentPosition = 0;

        public Randomize(int playCount) {
            indexes = new int[playCount];
            random = new Random();

            for (int i = 0; i < playCount; i++) indexes[i] = i;

            for (int i = 0; i < playCount; i++) {
                int randomIndex = random.nextInt(playCount);

                int currentValue = indexes[i];
                indexes[i] = indexes[randomIndex];
                indexes[randomIndex] = currentValue;
            }
        }

        public int next() {
            if (currentPosition == indexes.length - 1)
                return indexes[currentPosition];
            return indexes[++currentPosition];
        }

        public int prev() {
            if (currentPosition > 0) return indexes[--currentPosition];
            return indexes[currentPosition];
        }
    }
}

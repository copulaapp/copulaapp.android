package com.copula.functionality;

/**
 * Created by heeleaz on 10/3/16.
 */
public class CopulaUrl {
    public static final String COPULA_WEB = "http://copulaapp.com";
    public static final String ABOUT_URL = COPULA_WEB + "/about";
    public static final String HELP_URL = COPULA_WEB + "/help";
    public static final String TERMS_URL = COPULA_WEB + "/legal/terms";
    public static final String BLOG_URL = COPULA_WEB + "/blog";
    public static final String APP_URL = COPULA_WEB + "/appurl";

    public static String getAppStoreUrl() {
        return "https://play.google.com/store/apps/details?id=com.copula.android";
    }
}

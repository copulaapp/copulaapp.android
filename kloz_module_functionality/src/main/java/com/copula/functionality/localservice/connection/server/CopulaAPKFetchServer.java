package com.copula.functionality.localservice.connection.server;

import android.content.Context;
import android.util.Log;
import com.copula.functionality.localservice.connection.server.controller.SystemAPKsController;
import com.copula.functionality.restlite.LiteRouter;
import com.copula.functionality.util.ConnectionConstant;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Created by heeleaz on 5/16/17.
 */
public class CopulaAPKFetchServer implements HttpHandler {
    private static final String TAG = LocalDataFetcherServer.class.getSimpleName();
    private static CopulaAPKFetchServer singleton = null;

    private HttpServer mHttpServer;
    private LiteRouter mRouter;

    private CopulaAPKFetchServer(Context context) {
        mRouter = new LiteRouter();
        mRouter.addObject(new SystemAPKsController(context));
    }

    public static CopulaAPKFetchServer getInstance(Context context) {
        if (singleton == null) singleton = new CopulaAPKFetchServer(context);
        return singleton;
    }

    private void startServer(String hostAddress, int port) {
        try {
            mHttpServer = HttpServer.create(new InetSocketAddress(port), 0);
            mHttpServer.setExecutor(Executors.newCachedThreadPool());
            mHttpServer.createContext(RequestTable.DOWNLOAD.COPULA_HTMLPAGE, this);
            mHttpServer.start();//startServer server;
        } catch (IOException e) {
            Log.d(TAG, "Server Error: " + e.getMessage());
        }
    }

    public void startServer(String hostAddress) {
        startServer(hostAddress, ConnectionConstant.COPULA_APK_DOWNLOAD_PORT);
    }

    @Override
    public void handle(HttpExchange httpExchange) {
        mRouter.route(httpExchange);
    }

    public void stopServer() {
        if (mHttpServer != null) mHttpServer.stop(0);
    }
}

package com.copula.functionality.localservice.connection.server;

import android.content.Context;
import android.util.Log;
import com.copula.functionality.localservice.connection.server.controller.MediaStreamController;
import com.copula.functionality.restlite.LiteRouter;
import com.copula.functionality.util.ConnectionConstant;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;


public class LocalMediaStreamServer implements HttpHandler {
    private static final String TAG = LocalMediaStreamServer.class.getSimpleName();
    private static LocalMediaStreamServer singleton = null;

    private HttpServer mHttpServer;
    private LiteRouter mRouter;

    private LocalMediaStreamServer(Context context) {
        mRouter = new LiteRouter();
        mRouter.addObject(new MediaStreamController(context));
    }

    public static LocalMediaStreamServer getInstance(Context ctx) {
        if (singleton == null) singleton = new LocalMediaStreamServer(ctx);
        return singleton;
    }

    private void startServer(String hostAddress, int port) {
        try {
            mHttpServer = HttpServer.create(new InetSocketAddress(port), 0);
            mHttpServer.setExecutor(Executors.newCachedThreadPool());

            for (final String route : RequestTable.STREAM.getRouteList()) {
                mHttpServer.createContext(route, this);
            }
            mHttpServer.start();
        } catch (IOException e) {
            Log.d(TAG, "Server Error: " + e.getMessage());
        }
    }

    public void startServer(String hostAddress) {
        startServer(hostAddress, ConnectionConstant.MEDIA_STREAM_PORT);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        mRouter.route(httpExchange);
    }

    public void stopServer() {
        if (mHttpServer != null) mHttpServer.stop(0);
    }
}
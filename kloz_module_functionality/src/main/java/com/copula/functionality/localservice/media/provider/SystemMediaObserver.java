package com.copula.functionality.localservice.media.provider;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.copula.functionality.localservice.media.MediaModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 12/28/16.
 */
public class SystemMediaObserver {
    private SharedPreferences preference;
    private Context mContext;

    public SystemMediaObserver(Context context) {
        mContext = context;
        preference = context.getSharedPreferences("recent_media_monitor", Context.MODE_PRIVATE);
    }

    public boolean hasAddedNewMedia() {
        return getLatestMediaCount(MediaModel.IMAGE) > 0
                || getLatestMediaCount(MediaModel.AUDIO) > 0
                || getLatestMediaCount(MediaModel.VIDEO) > 0;
    }

    private long __getTopMediaAddedDate(int mediaType) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT 1";

        Uri uri = SystemMediaProvider.getMediaUrl(mediaType);
        Cursor cu = SystemMediaProvider
                .runQuery(mContext, uri, null, null, null, orderBy, null);

        if (cu != null && cu.moveToNext()) {
            return cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));
        }
        return -1;
    }

    private List<?> __getTheLastMedia(int mediaType) {
        String selection = MediaStore.MediaColumns.DATE_ADDED + ">"
                + getLastDateAdded(mediaType);
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC";

        if (mediaType == MediaModel.AUDIO) {
            return SystemMediaProvider.getAudios(mContext, selection, orderBy);
        } else if (mediaType == MediaModel.IMAGE) {
            return SystemMediaProvider.getImages(mContext, selection, orderBy);
        } else {
            return SystemMediaProvider.getVideos(mContext, selection, orderBy);
        }
    }

    public int getLatestMediaCount(int mediaType) {
        Uri mediaUrl = SystemMediaProvider.getMediaUrl(mediaType);
        String selection = MediaStore.MediaColumns.DATE_ADDED + ">"
                + getLastDateAdded(mediaType);
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC";

        Cursor cu = SystemMediaProvider.runQuery(mContext,
                mediaUrl, null, selection, null, orderBy, null);
        return (cu != null) ? cu.getCount() : 0;
    }

    public List<MediaModel> createShuffledLatestMedia(int length) {
        List<List<?>> modelsReference = new ArrayList<>();
        modelsReference.add(__getTheLastMedia(MediaModel.IMAGE));
        modelsReference.add(__getTheLastMedia(MediaModel.AUDIO));
        modelsReference.add(__getTheLastMedia(MediaModel.VIDEO));

        List<MediaModel> model = new ArrayList<>();
        int referenceLength = modelsReference.size();
        for (int i = 0; i < length; i++) {
            int reference = (referenceLength + i) % referenceLength;
            List<?> mediaList = modelsReference.get(reference);
            if (mediaList.size() > 0) {
                MediaModel media = (MediaModel) mediaList.get(0);
                model.add(media);

                mediaList.remove(0);
            }

            if (mediaList.size() == 0) modelsReference.remove(reference);
            if ((referenceLength = modelsReference.size()) == 0) break;
        }

        return model;
    }

    private long getLastDateAdded(int mediaType) {
        return preference.getLong("recent_media_date_" + mediaType, 0);
    }

    private void setLastDateAdded(int mediaType, long date) {
        if (date == -1) return;

        SharedPreferences.Editor editor = preference.edit();
        editor.putLong("recent_media_date_" + mediaType, date);
        editor.apply();
    }

    private SystemMediaObserver __ackLatestMedia(int mediaType) {
        setLastDateAdded(mediaType, __getTopMediaAddedDate(mediaType));
        return this;
    }

    public void ackLatestMedia() {
        __ackLatestMedia(MediaModel.IMAGE);
        __ackLatestMedia(MediaModel.VIDEO).__ackLatestMedia(MediaModel.AUDIO);
    }
}

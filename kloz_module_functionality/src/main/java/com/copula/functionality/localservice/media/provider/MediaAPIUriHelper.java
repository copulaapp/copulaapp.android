package com.copula.functionality.localservice.media.provider;

import com.copula.functionality.localservice.connection.server.RequestTable;
import com.copula.functionality.util.ConnectionConstant;

/**
 * Created by heeleeaz on 7/11/16.
 */
public class MediaAPIUriHelper {
    static String getAudioDownloadUrl(String sAddr, int id) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_DOWNLOAD_PORT;
        return addr + RequestTable.DOWNLOAD.GET_AUDIO_FILE + "?mediaId=" + id;
    }

    static String getVideoDownloadUrl(String sAddr, int id) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_DOWNLOAD_PORT;
        return addr + RequestTable.DOWNLOAD.GET_VIDEO_FILE + "?mediaId=" + id;
    }

    static String getAudioThumbnailUrl(String sAddr, int id) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_DOWNLOAD_PORT;
        return addr + RequestTable.DOWNLOAD.GET_AUDIO_THUMBNAIL_FILE + "?mediaId=" + id;
    }

    static String getVideoThumbnailUrl(String sAddr, int id) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_DOWNLOAD_PORT;
        return addr + RequestTable.DOWNLOAD.VIDEO_THUMBNAIL_FILE + "?mediaId=" + id;
    }

    static String getImageDownloadUrl(String sAddr, int id) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_DOWNLOAD_PORT;
        return addr + RequestTable.DOWNLOAD.GET_IMAGE_FILE + "?mediaId=" + id;
    }

    static String getAudioStreamUrl(String sAddr, int id) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_STREAM_PORT;
        return addr + RequestTable.STREAM.AUDIO_STREAM + "?mediaId=" + id;
    }

    static String getVideoStreamUrl(String sAddr, int id) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_STREAM_PORT;
        return addr + RequestTable.STREAM.VIDEO_STREAM + "?mediaId=" + id;
    }

    public static String getUserProfileImage(String sAddr, String userId) {
        String addr = "http://" + sAddr + ":" + ConnectionConstant.MEDIA_DOWNLOAD_PORT;
        return addr + RequestTable.DOWNLOAD.GET_USER_PROFILE_IMAGE + "?userId=" + userId;
    }

}
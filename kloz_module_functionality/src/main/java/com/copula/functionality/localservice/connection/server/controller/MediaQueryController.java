package com.copula.functionality.localservice.connection.server.controller;

import android.content.Context;
import android.graphics.Bitmap;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.localservice.connection.server.RequestTable;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaStatsModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.feeds.DaoLocalMediaCaption;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.localservice.media.provider.SystemImageProvider;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.localservice.media.provider.V2SystemMediaQuery;
import com.copula.functionality.localservice.profile.DaoPeepPermission;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.restlite.MediaType;
import com.copula.functionality.restlite.annotation.RouteContext;
import com.copula.functionality.restlite.annotation.RoutePath;
import com.copula.functionality.restlite.annotation.RouteProduces;
import com.copula.functionality.restlite.QueryParam;

import java.io.File;
import java.util.List;

/**
 * Created by heeleeaz on 7/10/16.
 */
public class MediaQueryController extends ParentController {
    private V2SystemMediaQuery v2SystemMediaQuery;
    private DaoLocalMediaCaption daoMediaCaption;
    private SystemImageProvider systemImageProvider;
    private DaoPeepPermission DaoPeepsPermission;

    private Context context;

    private String appUserId;

    public MediaQueryController(Context context) {
        this.context = context;
        v2SystemMediaQuery = new V2SystemMediaQuery(context);
        daoMediaCaption = new DaoLocalMediaCaption(context);
        systemImageProvider = new SystemImageProvider(context);
        DaoPeepsPermission = new DaoPeepPermission(context);

        this.appUserId = new UserAccountBase(context).getUserId();
    }

    private boolean isAppUser(String user) {
        return (user != null && user.equals(appUserId));
    }

    @RoutePath(RequestTable.Route.PING_SERVER)
    @RouteProduces(MediaType.TEXT_PLAIN)
    @Override
    public int pingServer(@RouteContext QueryParam pathArg) {
        return UserProfileEntry.ONLINE;
    }

    @RoutePath(RequestTable.MEDIA.GET_IMAGE_LIBRARY)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<ImageModel> getImageLibrary(@RouteContext QueryParam argMap) {
        String selection = argMap.getString("selection");
        String orderBy = argMap.getString("orderBy");
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.queryImageLibrary(selection, orderBy);
        else
            return v2SystemMediaQuery.queryImageLibrary_VUser(selection, orderBy);
    }

    @RoutePath(RequestTable.MEDIA.GET_IMAGE_MEDIA)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<ImageModel> getImageMedias(@RouteContext QueryParam argMap) {
        String selection = argMap.getString("selection");
        String orderBy = argMap.getString("orderBy");
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.getImageMedia(selection, orderBy);
        else
            return v2SystemMediaQuery.queryImageMedia_VUser(selection, orderBy, peeperId);
    }

    @RoutePath(RequestTable.MEDIA.GET_AUDIO_MEDIA)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<AudioModel> getAudioMedias(@RouteContext QueryParam argMap) {
        String selection = argMap.getString("selection");
        String orderBy = argMap.getString("orderBy");
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.getAudioMedia(selection, orderBy);
        else
            return v2SystemMediaQuery.getAudioMedia_Host(selection, orderBy, peeperId);
    }

    @RoutePath(RequestTable.MEDIA.GET_AUDIO_LIBRARY)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<AudioModel> getAudioLibrary(@RouteContext QueryParam argMap) {
        String selection = argMap.getString("selection");
        String orderBy = argMap.getString("orderBy");
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.getAudioAlbum(selection, orderBy);
        else
            return v2SystemMediaQuery.getAudioAlbum_Host(selection, orderBy);
    }

    @RoutePath(RequestTable.MEDIA.GET_VIDEO_MEDIA)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<VideoModel> getVideoMedias(@RouteContext QueryParam argMap) {
        String selection = argMap.getString("selection");
        String orderBy = argMap.getString("orderBy");
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.getVideoMedia(selection, orderBy);
        else
            return v2SystemMediaQuery.getVideoMedia_Host(selection, orderBy, peeperId);
    }

    @RoutePath(RequestTable.MEDIA.GET_MEDIA_COUNT)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public MediaStatsModel getMediaCount(@RouteContext QueryParam argMap) {
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.getMediaCount();
        else return v2SystemMediaQuery.getMediaCount_VUser();
    }

    @RoutePath(RequestTable.MEDIA.GET_MEDIA_CAPTIONS)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<MediaCaptionEntry> getMediaCaptions(@RouteContext QueryParam argMap) {
        String limit = argMap.getString("limit");
        String peeperId = argMap.getString("peeperId");

        return daoMediaCaption.getCaptions(-1, peeperId, limit);
    }

    @RoutePath(RequestTable.MEDIA.SEARCH_AUDIO_MEDIA)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<AudioModel> searchAudioMedia(@RouteContext QueryParam argMap) {
        String query = argMap.getString("query");
        String orderBy = argMap.getString("orderBy");
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.searchAudioMedia(query, orderBy);
        else return v2SystemMediaQuery.searchAudioMedia_Host(query, orderBy);
    }

    @RoutePath(RequestTable.MEDIA.SEARCH_VIDEO_MEDIA)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public List<VideoModel> searchVideoMedia(@RouteContext QueryParam argMap) {
        String query = argMap.getString("query");
        String orderBy = argMap.getString("orderBy");
        String peeperId = argMap.getString("peeperId");

        if (isAppUser(peeperId))
            return v2SystemMediaQuery.searchVideoMedia(query, orderBy);
        else return v2SystemMediaQuery.searchVideoMedia_VUser(query, orderBy);
    }

    @RoutePath(RequestTable.MEDIA.MEDIA_ACTION)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public boolean mediaAction(@RouteContext QueryParam argMap) {
        MediaCaptionEntry model = new MediaCaptionEntry();
        model.actorId = argMap.getString("actorId");
        model.action = argMap.getInt("action");
        model.mediaType = argMap.getInt("mediaType");
        model.mediaId = argMap.getInt("mediaId");
        model.createdAt = System.currentTimeMillis();
        return NotificationHelper.getInstance().dispatchMediaAction(model);
    }

    @RoutePath(RequestTable.PROFILE.REQ_PERMISSION_UPDATE)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public int requestPermissionUpdate(@RouteContext QueryParam argMap) {
        String rpAddress = argMap.getString("rpAddress");
        String userId = argMap.getString("userId");
        int permission = argMap.getInt("permission");

        NotificationHelper helper = NotificationHelper.getInstance();
        return helper.dispatchPermissionUpdate(userId, rpAddress, permission);
    }

    @RoutePath(RequestTable.PROFILE.PERMISSION_UPDATED_BEEP)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public boolean permissionUpdatedBeep(@RouteContext QueryParam argMap) {
        String permitterId = argMap.getString("permitterId");
        int permission = argMap.getInt("permission");

        NotificationHelper helper = NotificationHelper.getInstance();
        return helper.dispatchPermissionUpdated(permitterId, permission);
    }

    @RoutePath(RequestTable.PROFILE.GET_PEEP_PERMISSION_MASK)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public int getPeepPermissionMask(@RouteContext QueryParam argMap) {
        String userId = argMap.getString("userId");
        return DaoPeepsPermission.getPermission(userId).permissionMask;
    }

    @RoutePath(RequestTable.DOWNLOAD.GET_AUDIO_FILE)
    @RouteProduces(MediaType.BINARY_FILE)
    public File getAudioFile(@RouteContext QueryParam argMap) {
        int mediaId = argMap.getInt("mediaId");
        AudioModel model = SystemMediaProvider.getAudio(context, mediaId);
        if (model != null) {
            return new File(model.dataPath);
        } else return null;
    }

    @RoutePath(RequestTable.DOWNLOAD.GET_VIDEO_FILE)
    @RouteProduces(MediaType.BINARY_FILE)
    public File getVideoFile(@RouteContext QueryParam argMap) {
        int mediaId = argMap.getInt("mediaId");

        VideoModel model = SystemMediaProvider.getVideo(context, mediaId);
        if (model != null) return new File(model.dataPath);
        else return null;
    }

    @RoutePath(RequestTable.DOWNLOAD.GET_IMAGE_FILE)
    @RouteProduces(MediaType.IMAGE_BITMAP)
    public Bitmap getImageFile(@RouteContext QueryParam argMap) {
        int mediaId = argMap.getInt("mediaId");
        int maxSize = argMap.getInt("maxSize");

        ImageModel model = SystemMediaProvider.getImage(context, mediaId);
        if (model != null) {
            return systemImageProvider.getOptImageBitmap(model.dataPath, maxSize);
        } else return null;
    }

    @RoutePath(RequestTable.DOWNLOAD.GET_AUDIO_THUMBNAIL_FILE)
    @RouteProduces(MediaType.IMAGE_BITMAP)
    public Bitmap getAudioAlbumArt(@RouteContext QueryParam argMap) {
        int mediaId = argMap.getInt("mediaId");
        int maxSize = argMap.getInt("maxSize");

        AudioModel model = SystemMediaProvider.getAudio(context, mediaId);
        if (model != null) {
            return systemImageProvider.getOptAlbumThumbnail(model.albumId, maxSize);
        } else return null;
    }

    @RoutePath(RequestTable.DOWNLOAD.VIDEO_THUMBNAIL_FILE)
    @RouteProduces(MediaType.IMAGE_BITMAP)
    public Bitmap getVideoAlbumArt(@RouteContext QueryParam argMap) {
        int mediaId = argMap.getInt("mediaId");
        int maxSize = argMap.getInt("maxSize");
        VideoModel model = SystemMediaProvider.getVideo(context, mediaId);
        if (model != null) {
            return systemImageProvider.getOptVideoThumbnail(model.dataPath, maxSize);
        } else return null;
    }
}

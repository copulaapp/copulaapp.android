package com.copula.functionality.localservice.connection.client;


import com.copula.functionality.localservice.connection.server.RequestTable;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.restlite.ObjectJsonMapper;
import com.copula.functionality.util.ConnectionConstant;
import com.copula.functionality.restlite.QueryParam;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.util.List;

public class LocalAPIProvider {
    private final int port = ConnectionConstant.MEDIA_QUERY_PORT;

    private OkHttpClient mConnection;
    private String hostAddress;

    public LocalAPIProvider(OkHttpClient client) {
        this.mConnection = client;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    private APIResponse getString(int port, String path, QueryParam routeArgs) {
        String url = "http://" + hostAddress + ":" + port + path + "?" + routeArgs.toString();
        try {
            Request.Builder builder = new Request.Builder().url(url);
            Response response = mConnection.newCall(builder.build()).execute();

            APIResponse apiResponse = new APIResponse();
            apiResponse.isSuccessful = response.isSuccessful();
            if (apiResponse.isSuccessful)
                apiResponse.result = response.body().string();

            return apiResponse;
        } catch (Exception e) {
            return new APIResponse();
        }
    }

    public List<ImageModel> qImageMedia(String s, String orderBy, String pp, String limit) {
        QueryParam argMap = new QueryParam().put("selection", s);
        argMap.put("orderBy", orderBy);
        argMap.put("peeperId", pp).put("limit", limit);
        APIResponse result = getString(port, RequestTable.MEDIA.GET_IMAGE_MEDIA, argMap);
        try {
            return ObjectJsonMapper.mapList(ImageModel.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public List<AudioModel>
    qAudioMedia(String selection, String orderBy, String peeperId, String limit) {
        QueryParam argMap = new QueryParam();
        argMap.put("selection", selection).put("orderBy", orderBy);
        argMap.put("limit", limit).put("peeperId", peeperId);
        APIResponse result =
                getString(port, RequestTable.MEDIA.GET_AUDIO_MEDIA, argMap);
        try {
            return ObjectJsonMapper.mapList(AudioModel.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public List<AudioModel> searchAudio(String query, String peeperId) {
        QueryParam argMap = new QueryParam();
        argMap.put("query", query).put("orderBy", null);
        argMap.put("limit", null).put("peeperId", peeperId);
        APIResponse result =
                getString(port, RequestTable.MEDIA.SEARCH_AUDIO_MEDIA, argMap);
        try {
            return ObjectJsonMapper.mapList(AudioModel.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public List<VideoModel> searchVideo(String query, String peeperId) {
        QueryParam argMap = new QueryParam();
        argMap.put("query", query).put("orderBy", null);
        argMap.put("limit", null).put("peeperId", peeperId);
        APIResponse result =
                getString(port, RequestTable.MEDIA.SEARCH_VIDEO_MEDIA, argMap);
        try {
            return ObjectJsonMapper.mapList(VideoModel.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public List<VideoModel> qVideoMedia(String s, String orderBy, String pp, String limit) {
        QueryParam arg = new QueryParam();
        arg.put("selection", s).put("orderBy", orderBy);
        arg.put("limit", limit).put("peeperId", pp);
        APIResponse result =
                getString(port, RequestTable.MEDIA.GET_VIDEO_MEDIA, arg);
        try {
            return ObjectJsonMapper.mapList(VideoModel.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public List<AudioModel> qAudioMedia(String selection, String orderBy, String peeperId) {
        QueryParam arg = new QueryParam();
        arg.put("selection", selection).put("orderBy", orderBy);
        arg.put("peeperId", peeperId);
        APIResponse result =
                getString(port, RequestTable.MEDIA.GET_AUDIO_LIBRARY, arg);
        try {
            return ObjectJsonMapper.mapList(AudioModel.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean mediaAction(String likerId, int mediaType, int mediaId, int action) {
        QueryParam arg = new QueryParam();
        arg.put("mediaId", mediaId).put("actorId", likerId);
        arg.put("mediaType", mediaType).put("action", action);
        APIResponse result = getString(port, RequestTable.MEDIA.MEDIA_ACTION, arg);
        return Boolean.valueOf(result.result);
    }

    public int requestPermissionUpdate(String userId, String rpAddress, int permission) {
        QueryParam arg = new QueryParam();
        arg.put("userId", userId).put("permission", permission);
        arg.put("rpAddress", rpAddress);
        APIResponse result =
                getString(port, RequestTable.PROFILE.REQ_PERMISSION_UPDATE, arg);
        try {
            return Integer.valueOf(result.result);
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean permissionUpdateBeep(String userId, int permission) {
        QueryParam arg = new QueryParam();
        arg.put("permitterId", userId).put("permission", permission);
        APIResponse result =
                getString(port, RequestTable.PROFILE.PERMISSION_UPDATED_BEEP, arg);
        return Boolean.getBoolean(result.result);
    }

    public int getPeepPermissionMask(String userId) {
        QueryParam arg = new QueryParam().put("userId", userId);
        APIResponse result =
                getString(port, RequestTable.PROFILE.GET_PEEP_PERMISSION_MASK, arg);
        try {
            return Integer.valueOf(result.result);
        } catch (Exception e) {
            return 0;
        }
    }

    public UserProfileEntry getUserProfile() {
        QueryParam argMap = new QueryParam();
        APIResponse result =
                getString(port, RequestTable.PROFILE.GET_USER_PROFILE, argMap);
        try {
            return ObjectJsonMapper.mapOne(UserProfileEntry.class, result.result);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean hostConnectBeep(UserProfileEntry model) {
        QueryParam arg = new QueryParam();
        arg.put("userId", model.userId).put("ipAddress", model.ipAddress);
        arg.put("bio", model.bio).put("username", model.username);
        arg.put("imgUrl", model.imgUrl).put("connectStatus", model.connectStatus);
        APIResponse result = getString(port, RequestTable.PROFILE.HOST_CONNECT_BEEP, arg);
        return Boolean.valueOf(result.result);
    }

    public boolean userAction(UserProfileEntry model) {
        QueryParam arg = new QueryParam();
        arg.put("userId", model.userId).put("action", model.action);
        arg.put("ipAddress", model.ipAddress);
        APIResponse result = getString(port, RequestTable.PROFILE.USER_ACTION, arg);
        return Boolean.valueOf(result.result);
    }

    public int pingServer(int serverPort) {
        try {
            QueryParam param = new QueryParam();
            APIResponse result =
                    getString(serverPort, RequestTable.Route.PING_SERVER, param);
            return Integer.valueOf(result.result);
        } catch (Exception e) {
            return -1;
        }
    }

    private class APIResponse {
        boolean isSuccessful;
        String result;
    }
}
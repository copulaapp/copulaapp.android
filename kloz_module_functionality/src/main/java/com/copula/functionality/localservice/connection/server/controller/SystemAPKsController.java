package com.copula.functionality.localservice.connection.server.controller;

import android.content.Context;
import com.copula.functionality.localservice.connection.server.RequestTable;
import com.copula.functionality.localservice.media.provider.SystemAPKsProvider;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.restlite.MediaType;
import com.copula.functionality.restlite.annotation.RouteContext;
import com.copula.functionality.restlite.annotation.RoutePath;
import com.copula.functionality.restlite.annotation.RouteProduces;
import com.copula.functionality.restlite.QueryParam;

import java.io.File;

/**
 * Created by heeleaz on 1/2/17.
 */
public class SystemAPKsController extends ParentController {
    private Context context;

    public SystemAPKsController(Context context) {
        this.context = context;
    }

    @RoutePath(RequestTable.Route.PING_SERVER)
    @RouteProduces(MediaType.TEXT_PLAIN)
    @Override
    public int pingServer(@RouteContext QueryParam pathArg) {
        return UserProfileEntry.ONLINE;
    }

    @RoutePath(RequestTable.DOWNLOAD.COPULA_HTMLPAGE)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public String getCopulaHtmlPage(@RouteContext QueryParam argMap) {
        String html = "<html><title>Receive Copula</title>";
        html += "<h1 align=center><a href=\"/directdownload\">Receive Copula(apk)</a></h1>";
        return html + "</html>";
    }

    @RoutePath(RequestTable.DOWNLOAD.GET_COPULA)
    @RouteProduces(MediaType.BINARY_FILE)
    public File getCopulaAPK(@RouteContext QueryParam argMap) {
        return SystemAPKsProvider.getCopulaAPK(context);
    }
}

package com.copula.functionality.localservice.connection.server.controller;

import com.copula.functionality.localservice.connection.server.RequestTable;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.restlite.MediaType;
import com.copula.functionality.restlite.annotation.RouteContext;
import com.copula.functionality.restlite.annotation.RoutePath;
import com.copula.functionality.restlite.annotation.RouteProduces;
import com.copula.functionality.restlite.QueryParam;

/**
 * Created by heeleaz on 1/2/17.
 */
public class ParentController {
    @RoutePath(RequestTable.Route.PING_SERVER)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public int pingServer(@RouteContext QueryParam pathArg) {
        return UserProfileEntry.ONLINE;
    }
}

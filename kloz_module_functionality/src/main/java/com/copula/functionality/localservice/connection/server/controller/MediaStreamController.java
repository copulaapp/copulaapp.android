package com.copula.functionality.localservice.connection.server.controller;

import android.content.Context;
import com.copula.functionality.localservice.connection.server.RequestTable;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.restlite.HttpMediaStream;
import com.copula.functionality.restlite.MediaType;
import com.copula.functionality.restlite.annotation.RouteContext;
import com.copula.functionality.restlite.annotation.RoutePath;
import com.copula.functionality.restlite.annotation.RouteProduces;
import com.copula.functionality.restlite.QueryParam;
import com.sun.net.httpserver.HttpExchange;

/**
 * Created by heeleeaz on 7/11/16.
 */

public class MediaStreamController extends ParentController {
    private Context mContext;

    public MediaStreamController(Context context) {
        this.mContext = context;
    }

    @RoutePath(RequestTable.Route.PING_SERVER)
    @RouteProduces(MediaType.TEXT_PLAIN)
    @Override
    public int pingServer(@RouteContext QueryParam pathArg) {
        return UserProfileEntry.ONLINE;
    }

    @RoutePath(RequestTable.STREAM.AUDIO_STREAM)
    @RouteProduces(MediaType.STREAM_HTTP)
    public HttpMediaStream streamAudio(@RouteContext QueryParam argMap, @RouteContext HttpExchange exchange) {
        int mediaId = argMap.getInt("mediaId");

        AudioModel model = SystemMediaProvider.getAudio(mContext, mediaId);
        if (model == null) return null;

        String mimeType = MediaType.getMIME(model.dataPath);
        HttpMediaStream streamer = HttpMediaStream.getInstance();
        streamer.setHttpExchange(exchange);
        streamer.setMediaDuration(model.duration / 1000f);
        streamer.setMediaFile(model.dataPath);
        streamer.setMediaType((mimeType == null) ? "audio/mp3" : mimeType);
        return streamer;
    }


    @RoutePath(RequestTable.STREAM.VIDEO_STREAM)
    @RouteProduces(MediaType.STREAM_HTTP)
    public HttpMediaStream streamVideo(@RouteContext QueryParam argMap, @RouteContext HttpExchange exchange) {
        int mediaId = argMap.getInt("mediaId");

        VideoModel model = SystemMediaProvider.getVideo(mContext, mediaId);
        if (model == null) return null;

        String mimeType = MediaType.getMIME(model.dataPath);
        HttpMediaStream streamer = HttpMediaStream.getInstance();
        streamer.setHttpExchange(exchange);
        streamer.setMediaDuration(model.duration / 1000f);
        streamer.setMediaFile(model.dataPath);
        streamer.setMediaType((mimeType == null) ? "video/mp4" : mimeType);
        return streamer;
    }
}

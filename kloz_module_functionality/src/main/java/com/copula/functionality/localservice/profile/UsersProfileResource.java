package com.copula.functionality.localservice.profile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.copula.functionality.downloader.MediaDownloadClient;
import com.copula.functionality.util.MediaUtility;
import com.copula.support.android.media.ImageBitmapOptimizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by heeleeaz on 7/11/16.
 */
public class UsersProfileResource {
    private static String encryptUserIdForImageFile(String userId) {
        return userId + ".jpg";//return the last string
    }

    public static boolean putProfileImage(String userId, String imageUrl) {
        if (imageUrl == null || userId == null) return false;

        try {
            String saveTo = getProfileImagePath(userId);
            return MediaDownloadClient.download(imageUrl, saveTo);
        } catch (IOException e) {
            return false;
        }
    }

    public static String getProfileImagePath(String userId) {
        String filename = encryptUserIdForImageFile(userId);
        String dir = MediaUtility.getProfileImageDir().getAbsolutePath();
        return dir + "/" + filename;
    }

    public static Bitmap getProfileImage(String userId, int maxSize) {
        try {
            File imgFile = new File(getProfileImagePath(userId));
            if (!imgFile.exists()) return null;

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = ImageBitmapOptimizer
                    .calcInSampleSize(new FileInputStream(imgFile), maxSize);
            return BitmapFactory.decodeStream(new FileInputStream(imgFile), null, opts);
        } catch (Exception e) {
            return null;
        }
    }//END
}

package com.copula.functionality.localservice.media;


public class ImageModel extends MediaModel {
    public int bucketId;
    public String bucketUrl, mimeType;

    public ImageModel() {
    }

    public ImageModel(MediaModel item) {
        super.setMedia(item);
    }
}

package com.copula.functionality.localservice.media.provider;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.ImageModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;
import com.copula.functionality.localservice.profile.UserAccountBase;

import java.util.ArrayList;
import java.util.List;

public class SystemMediaProvider {
    public static Uri IMAGE_URL = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    public static Uri AUDIO_URL = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    public static Uri VIDEO_URL = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

    public static Cursor runQuery(Context ctx, Uri uri, String[] projection, String selection
            , String[] selectionArgs, String sortOrder, String limit) {
        try {
            ContentResolver cr = ctx.getContentResolver();
            return cr.query(uri, projection, selection, selectionArgs, sortOrder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Uri getMediaUrl(int mediaType) {
        switch (mediaType) {
            case MediaModel.AUDIO:
                return AUDIO_URL;
            case MediaModel.IMAGE:
                return IMAGE_URL;
            case MediaModel.VIDEO:
                return VIDEO_URL;
            default:
                return AUDIO_URL;
        }
    }

    public static int getCount(Context ctx, int mediaType, String selection) {
        int count = 0;
        Cursor cu = runQuery(ctx, getMediaUrl(mediaType),
                new String[]{"_id"}, selection, null, null, null);
        if (cu != null) {
            count = cu.getCount();
            cu.close();
        }
        return count;
    }

    private static List<Integer> getDistinctProjection(Context ctx, String orderBy) {
        String projection = "DISTINCT " + Images.Media.BUCKET_ID;

        Cursor cu = runQuery(ctx, IMAGE_URL,
                new String[]{projection}, null, null, orderBy, null);
        if (cu == null) return new ArrayList<>();


        List<Integer> ids = new ArrayList<>();
        while (cu.moveToNext()) {
            ids.add(cu.getInt(cu.getColumnIndex(Images.Media.BUCKET_ID)));
        }
        return ids;
    }

    public static List<ImageModel> getDeviceImagesBucket(Context ctx, String sortOrder) {
        Cursor cu = runQuery(ctx, IMAGE_URL, null, null, null, sortOrder, null);
        if (cu == null || cu.getCount() <= 0) return null;

        String serverIP = UserAccountBase.systemAddress(ctx);
        List<ImageModel> entries = new ArrayList<>(cu.getCount());
        while (cu.moveToNext()) {
            ImageModel e = new ImageModel();
            e._id = cu.getInt(cu.getColumnIndex(MediaStore.MediaColumns._ID));
            e.mimeType = cu.getString(cu.getColumnIndex(Images.Media.MIME_TYPE));
            e.mediaLibrary = cu.getString(cu.getColumnIndex(Images.Media.BUCKET_DISPLAY_NAME));

            e.mediaType = MediaModel.IMAGE; //image media type specified as 1
            e.mediaId = e._id;
            e.dataUrl = MediaAPIUriHelper.getImageDownloadUrl(serverIP, e._id);
            e.thumbnailUrl = e.bucketUrl = e.dataUrl;
            entries.add(e);
        }
        cu.close();
        return entries;
    }

    public static List<ImageModel> getImages(Context ctx, String selection, String sortOrder) {
        Cursor cu = runQuery(ctx, IMAGE_URL, null, selection, null, sortOrder, null);
        if (cu == null || cu.getCount() <= 0) return new ArrayList<>();

        List<ImageModel> entries = new ArrayList<>(cu.getCount());
        String serverIP = UserAccountBase.systemAddress(ctx);
        while (cu.moveToNext()) {
            ImageModel e = new ImageModel();

            e._id = cu.getInt(cu.getColumnIndex(MediaStore.MediaColumns._ID));
            e.dataPath = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DATA));
            e.mediaLibrary = cu.getString(cu.getColumnIndex(Images.Media.BUCKET_DISPLAY_NAME));
            e.mimeType = cu.getString(cu.getColumnIndex(Images.Media.MIME_TYPE));
            e.bucketId = cu.getInt(cu.getColumnIndex(Images.Media.BUCKET_ID));
            e.fileName = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            e.mediaTitle = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.TITLE));
            e.fileSize = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.SIZE));
            e.createdAt = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));

            e.mediaType = MediaModel.IMAGE; //image media type specified as 1
            e.mediaId = e._id;
            e.dataUrl = MediaAPIUriHelper.getImageDownloadUrl(serverIP, e._id);
            e.thumbnailUrl = e.bucketUrl = e.dataUrl;
            entries.add(e);
        }
        cu.close();
        return entries;
    }

    public static List<AudioModel> getAudios(Context ctx, String selection, String sortOrder) {
        Cursor cu = runQuery(ctx, AUDIO_URL, null, selection, null, sortOrder, null);
        if (cu == null || cu.getCount() <= 0) return new ArrayList<>();

        List<AudioModel> entries = new ArrayList<>(cu.getCount());
        String sysIP = UserAccountBase.systemAddress(ctx);
        while (cu.moveToNext()) {
            AudioModel e = new AudioModel();
            e._id = cu.getInt(cu.getColumnIndex(MediaStore.MediaColumns._ID));
            e.mediaComposer = cu.getString(cu.getColumnIndex(Audio.Media.ARTIST));
            e.mediaLibrary = cu.getString(cu.getColumnIndex(Audio.Media.ALBUM));
            e.albumId = cu.getInt(cu.getColumnIndex(Audio.Albums.ALBUM_ID));
            e.duration = cu.getInt(cu.getColumnIndex(Audio.Media.DURATION));
            e.mediaTitle = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.TITLE));
            e.dataPath = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DATA));
            e.fileSize = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.SIZE));
            e.fileName = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            e.createdAt = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));
            e.mediaType = MediaModel.AUDIO; //audio media type specified as 2

            //request fetch from local network
            e.dataUrl = MediaAPIUriHelper.getAudioDownloadUrl(sysIP, e.mediaId = e._id);
            e.thumbnailUrl = MediaAPIUriHelper.getAudioThumbnailUrl(sysIP, e.mediaId);
            e.streamUrl = MediaAPIUriHelper.getAudioStreamUrl(sysIP, e.mediaId);
            entries.add(e);
        }
        cu.close();
        return entries;
    }


    public static List<VideoModel> getVideos(Context ctx, String selection, String sortOrder) {
        Cursor cu = runQuery(ctx, VIDEO_URL, null, selection, null, sortOrder, null);
        if (cu == null || cu.getCount() <= 0) return new ArrayList<>();

        List<VideoModel> entries = new ArrayList<>();
        String sysIP = UserAccountBase.systemAddress(ctx);
        while (cu.moveToNext()) {
            VideoModel e = new VideoModel();
            e._id = cu.getInt(cu.getColumnIndex(MediaStore.MediaColumns._ID));
            e.duration = cu.getInt(cu.getColumnIndex(Video.Media.DURATION));
            e.mediaTitle = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.TITLE));
            e.fileSize = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.SIZE));
            e.dataPath = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DATA));
            e.fileName = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            e.createdAt = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                e.width = cu.getInt(cu.getColumnIndex(Video.Media.WIDTH));
                e.height = cu.getInt(cu.getColumnIndex(Video.Media.HEIGHT));
            }

            e.mediaType = MediaModel.VIDEO; //video media specified 3
            e.mediaId = e._id;//mediaId specified as _id
            e.streamUrl = MediaAPIUriHelper.getVideoStreamUrl(sysIP, e._id);
            e.thumbnailUrl = MediaAPIUriHelper.getVideoThumbnailUrl(sysIP, e._id);
            e.dataUrl = MediaAPIUriHelper.getVideoDownloadUrl(sysIP, e._id);
            entries.add(e);
        }
        cu.close();
        return entries;
    }

    public static AudioModel getAudio(Context ctx, int id) {
        List<AudioModel> e = getAudios(ctx, "_id=" + id, null);
        return (e != null && e.size() > 0) ? e.get(0) : null;
    }

    public static VideoModel getVideo(Context ctx, int id) {
        List<VideoModel> e = getVideos(ctx, "_id=" + id, null);
        return (e != null && e.size() > 0) ? e.get(0) : null;
    }

    public static ImageModel getImage(Context ctx, int id) {
        List<ImageModel> e = getImages(ctx, "_id=" + id, null);
        return (e != null && e.size() > 0) ? e.get(0) : null;
    }

    public static MediaModel getMedia(Context ctx, int type, int id) {
        if (type == MediaModel.IMAGE) {
            return SystemMediaProvider.getImage(ctx, id);
        } else if (type == MediaModel.AUDIO) {
            return SystemMediaProvider.getAudio(ctx, id);
        } else if (type == MediaModel.VIDEO) {
            return SystemMediaProvider.getVideo(ctx, id);
        } else {
            return null;
        }
    }
}
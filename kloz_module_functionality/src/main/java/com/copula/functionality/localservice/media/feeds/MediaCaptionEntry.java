package com.copula.functionality.localservice.media.feeds;


import com.copula.functionality.localservice.media.MediaModel;

/**
 * Created by heeleaz on 8/27/16.
 */
public class MediaCaptionEntry extends MediaModel {
    public static final int ACTION_UNLIKE = 0, ACTION_LIKE = 1, ACTION_SHARE = 2, ACTION_DOWNLOAD = 3;
    public String actorId = "";
    public int action;

    public MediaCaptionEntry() {
    }

    public MediaCaptionEntry(MediaCaptionEntry model) {
        setMedia(model);
        this.actorId = model.actorId;
        this.action = model.action;
    }

    public MediaCaptionEntry(int mediaType, int mediaId) {
        this.mediaType = mediaType;
        this.mediaId = mediaId;
    }

    public MediaCaptionEntry(MediaModel m) {
        super(m);
    }
}

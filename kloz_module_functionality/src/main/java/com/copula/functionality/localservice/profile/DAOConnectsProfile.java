package com.copula.functionality.localservice.profile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.copula.functionality.restlite.DaoModelMapper;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

public class DaoConnectsProfile extends DatabaseContext {
    private static final String TAG = DaoConnectsProfile.class.getName();
    private String tbName;

    public DaoConnectsProfile(Context context) {
        this(context, "user_profile.db", "user_profile");
    }

    protected DaoConnectsProfile(Context context, String dbName, String tbName) {
        super(context, dbName, tbName, 1);
        this.tbName = tbName;
    }

    private UserProfileEntry populateProfile(Cursor cu) {
        UserProfileEntry e = DaoModelMapper.mapOne(cu, UserProfileEntry.class);
        e.isFollowing = cu.getInt(cu.getColumnIndex("isFollowing")) == 1;
        return e;
    }

    public boolean putConnect(UserProfileEntry model) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            if (this.transactionIsExists(db, model.userId)) {
                return this.transactionUpdProfile(db, model);
            } else return this.transactionPutFriend(db, model);
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    private List<UserProfileEntry> getConnects(String limit) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(tbName, null, null, null,
                    null, null, null, limit);
            if (cu == null || cu.getCount() <= 0) return null;

            List<UserProfileEntry> handlers = new ArrayList<>(cu.getCount());
            while (cu.moveToNext()) handlers.add(populateProfile(cu));
            return handlers;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if ((db != null)) db.close();
        }
    }

    public List<UserProfileEntry> getConnects(String limit, boolean removeSelf) {
        List<UserProfileEntry> friends = getConnects(limit);
        if (removeSelf && friends != null) {
            String userId = new UserAccountBase(getContext()).getUserId();
            friends.remove(new UserProfileEntry(userId));
        }
        return friends;
    }

    public UserProfileEntry getConnect(String userId) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            String[] whereArgs = new String[]{userId};
            cu = db.query(tbName, null, "userId=?", whereArgs,
                    null, null, null);
            if (cu.moveToFirst()) return populateProfile(cu);
            else return null;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if ((db != null)) db.close();
        }
    }

    public String getUserId(String username) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            String[] whereArgs = new String[]{username};
            cu = db.query(tbName, new String[]{"userId"}, "username=?",
                    whereArgs, null, null, null);
            if (cu.moveToFirst()) {
                return cu.getString(cu.getColumnIndex("userId"));
            } else return null;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if ((db != null)) db.close();
        }
    }

    public boolean isExists(String userId) {
        SQLiteDatabase db = null;
        try {
            return transactionIsExists(db = getReadableDatabase(), userId);
        } catch (Exception e) {
            return false;
        } finally {
            if ((db != null)) db.close();
        }
    }

    private boolean transactionUpdProfile(SQLiteDatabase db, UserProfileEntry model) {
        ContentValues values = new ContentValues();
        values.put("username", model.username);
        values.put("bio", model.bio);
        values.put("isFollowing", model.isFollowing ? 1 : 0);
        return db.update(tbName, values, "userId=?", new String[]{model.userId}) > -1;
    }

    private boolean transactionPutFriend(SQLiteDatabase db, UserProfileEntry model) {
        ContentValues values = new ContentValues();
        values.put("userId", model.userId);
        values.put("username", model.username);
        values.put("bio", model.bio);
        values.put("isFollowing", model.isFollowing ? 1 : 0);
        return db.insert(tbName, null, values) > -1;
    }

    private boolean transactionIsExists(SQLiteDatabase db, String userId) {
        Cursor cu = null;
        try {
            cu = db.query(tbName, null, "userId=?",
                    new String[]{userId}, null, null, null);
            return cu != null && cu.getCount() > 0;
        } finally {
            if (cu != null) cu.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + tbName;
        sql += "(_id integer primary key autoincrement,";
        sql += "userId varchar,";
        sql += "username varchar(60),";
        sql += "bio varchar(200),";
        sql += "isFollowing integer(1),";
        sql += "createdAt DATETIME DEFAULT CURRENT_TIMESTAMP)";
        db.execSQL(sql);
    }
}
package com.copula.functionality.localservice.connection.client;

/**
 * Created by eliasigbalajobi on 3/27/16.
 */

public interface LocalServiceAPIListener {
    void onConnected(LocalAPIProvider provider);
}

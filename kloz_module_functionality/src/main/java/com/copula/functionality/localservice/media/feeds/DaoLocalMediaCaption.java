package com.copula.functionality.localservice.media.feeds;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.provider.SystemMediaProvider;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

public class DaoLocalMediaCaption extends DatabaseContext {

    private static final String TAG = DaoLocalMediaCaption.class.getSimpleName();
    private static final String TB_NAME = "local_caption_media";
    private DaoMediaActionRegister actionRegister;

    public DaoLocalMediaCaption(Context context) {
        super(context, "local_caption_media.db", TB_NAME, 1);
        actionRegister = new DaoMediaActionRegister(context);
    }

    /* prepare media info.. this prepare action is ignored
         * if media has already been prepared
    */
    private boolean _initMediaAsCaption(int mediaType, int mediaId) {
        SQLiteDatabase db = null;
        Cursor cu = null;

        try {
            db = getReadableDatabase();
            String whereStmt = "media_type=? AND media_id=?";
            String[] whereArgs = new String[]{"" + mediaType, "" + mediaId};
            long currentTime = System.currentTimeMillis();

            cu = db.query(TB_NAME, new String[]{"_id"}, whereStmt, whereArgs, null, null, null);

            ContentValues values = new ContentValues();
            values.put("created_at", currentTime);
            if (cu.getCount() > 0) {//update media
                return db.update(TB_NAME, values, whereStmt, whereArgs) > -1;
            } else {//add the media
                values.put("media_id", mediaId);
                values.put("media_type", mediaType);
                values.put("created_at", currentTime);
                return db.insert(TB_NAME, null, values) > -1;
            }
        } catch (Exception e) {
            return false;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    private MediaCaptionEntry populateCaption(Cursor cu) {
        MediaCaptionEntry model = new MediaCaptionEntry();
        model.mediaType = cu.getInt(cu.getColumnIndex("media_type"));
        model.mediaId = cu.getInt(cu.getColumnIndex("media_id"));

        if (model.mediaType == 3) {
            model.setMedia(SystemMediaProvider
                    .getVideo(getContext(), model.mediaId));
        } else if (model.mediaType == 2) {
            model.setMedia(SystemMediaProvider
                    .getAudio(getContext(), model.mediaId));
        } else {
            model.setMedia(SystemMediaProvider
                    .getImage(getContext(), model.mediaId));
        }

        //these guyz comes after media_model_setter.. to avoid override
        model._id = cu.getInt(cu.getColumnIndex("_id"));
        model.createdAt = cu.getLong(cu.getColumnIndex("created_at"));
        model.likeCount = cu.getInt(cu.getColumnIndex("like_count"));
        model.shareCount = cu.getInt(cu.getColumnIndex("share_count"));
        model.mediaCaption = cu.getString(cu.getColumnIndex("media_desc"));
        return model;
    }

    private MediaCaptionEntry getMediaCaption(int mediaType, int mediaId) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            String[] whereArgs = new String[]{"" + mediaType, "" + mediaId};
            cu = db.query(TB_NAME, null, "media_type=? AND media_id=?", whereArgs,
                    null, null, null);
            if (cu.moveToNext()) return populateCaption(cu);
            else return null;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    private List<MediaCaptionEntry> getCaption(String limit) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TB_NAME, null, null, null, null, null, null, limit);
            if (cu == null || cu.getCount() <= 0) return new ArrayList<>();

            List<MediaCaptionEntry> entries = new ArrayList<>(cu.getColumnCount());
            while (cu.moveToNext()) {
                entries.add(populateCaption(cu));
            }
            return entries;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    private boolean updateCount(int _id, String _countColumn, int c) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues(1);
            values.put(_countColumn, c);
            int b = db.update(TB_NAME, values, "_id=" + _id, null);
            return b > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean newAction(MediaCaptionEntry model) {
        if (model.action == MediaCaptionEntry.ACTION_LIKE) {
            return newLikeAction(model, model.actorId);
        } else if (model.action == MediaCaptionEntry.ACTION_SHARE) {
            return newShareAction(model, model.actorId);
        } else return unlikeAction(model, model.actorId);
    }


    private boolean newShareAction(MediaModel model, String actorId) {
        try {
            _initMediaAsCaption(model.mediaType, model.mediaId);
            actionRegister.registerAction(model, actorId, MediaCaptionEntry.ACTION_SHARE);

            MediaCaptionEntry mm = getMediaCaption(model.mediaType, model.mediaId);
            return updateCount(mm._id, "share_count", mm.shareCount + 1);
        } catch (Exception e) {
            Log.d(TAG, "Media Share action failed, " + e.getMessage());
            return false;//error like failed
        }
    }

    private boolean newLikeAction(MediaCaptionEntry model, String actorId) {
        try {
            _initMediaAsCaption(model.mediaType, model.mediaId);
            if (!actionRegister.hasActed(model, model.actorId, MediaCaptionEntry.ACTION_LIKE)) {
                actionRegister.registerAction(model, model.actorId, MediaCaptionEntry.ACTION_LIKE);

                MediaCaptionEntry mm = getMediaCaption(model.mediaType, model.mediaId);
                return updateCount(mm._id, "like_count", 1 + mm.likeCount);
            }
            return true;
        } catch (Exception e) {
            Log.d(TAG, "Media Liked action failed, " + e.getMessage());
            return false;//error like failed
        }
    }

    private boolean unlikeAction(MediaCaptionEntry model, String actorId) {
        try {
            if (actionRegister.hasActed(model, model.actorId, MediaCaptionEntry.ACTION_LIKE)) {
                actionRegister.removeAction(model, model.actorId, MediaCaptionEntry.ACTION_LIKE);

                MediaCaptionEntry mm = getMediaCaption(model.mediaType, model.mediaId);
                return updateCount(mm._id, "like_count", mm.likeCount - 1);
            }
            return true;
        } catch (Exception e) {
            Log.d(TAG, "Media unlike action failed, " + e.getMessage());
            return false;//error like failed
        }
    }

    public List<MediaCaptionEntry> getCaptions(int mediaType, String peeperId, String limit) {
        List<DaoMediaActionRegister.MediaActRegisterModel>
                register = actionRegister.getActionRegister(mediaType, limit);
        if (register == null) return new ArrayList<>();

        List<MediaCaptionEntry> captions = getCaption(null);
        List<MediaCaptionEntry> outModel = new ArrayList<>();

        for (DaoMediaActionRegister.MediaActRegisterModel m : register) {
            int index = captions.indexOf(new MediaCaptionEntry(m.mediaType, m.mediaId));
            if (index == -1) continue;

            MediaCaptionEntry model = new MediaCaptionEntry(captions.get(index));
            model.createdAt = m.createdAt;//override to like time
            model.actorId = m.actorId;
            model.action = m.action;
            model.isLiked = actionRegister.hasActed(model, peeperId, MediaCaptionEntry.ACTION_LIKE);
            outModel.add(model);
        }
        return outModel;
    }

    public boolean removeMediaCaptions(int mediaType, int mediaId) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            String[] wArgs = new String[]{"" + mediaType, "" + mediaId};
            return db.delete(TB_NAME, "media_type=? and media_id=?", wArgs) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME;
        sql += "(_id integer primary key autoincrement,";
        sql += "media_id integer,";
        sql += "media_type integer,";
        sql += "like_count integer,";
        sql += "share_count integer,";
        sql += "created_at integer,";
        sql += "media_desc varchar(200))";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
package com.copula.functionality.localservice.connection.server;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliasigbalajobi on 12/31/15.
 */
public class RequestTable {
    public static List<String> getRouteList(Class<?> cls) {
        List<String> routeList = new ArrayList<>();
        for (Field field : cls.getFields()) {
            if (field.getType().isAssignableFrom(String.class)) try {
                routeList.add((String) field.get(null));
            } catch (Exception e) {
            }
        }
        return routeList;
    }

    public static class MEDIA extends Route {
        public static final String MEDIA_ACTION = "/media/action";
        public static final String GET_MEDIA_CAPTIONS = "/media/liked";
        public static final String GET_MEDIA_COUNT = "/media/count";

        public static final String GET_IMAGE_LIBRARY = "/media/image/library";
        public static final String GET_IMAGE_MEDIA = "/media/image";

        public static final String GET_AUDIO_MEDIA = "/media/audio";
        public static final String GET_AUDIO_LIBRARY = "/media/audio/library";

        public static final String GET_VIDEO_MEDIA = "/media/video";
        public static final String SEARCH_VIDEO_MEDIA = "/media/search/video";
        public static final String SEARCH_AUDIO_MEDIA = "/media/search/audio";

        public static List<String> getRouteList() {
            return RequestTable.getRouteList(MEDIA.class);
        }
    }

    public static class PROFILE extends Route {
        public static final String GET_USER_PROFILE = "/profile/getProfile";
        public static final String HOST_CONNECT_BEEP = "/profile/notification/hostConnectBeep";
        public static final String REQ_PERMISSION_UPDATE = "/profile/permission/requestPermissionUpdate";
        public static final String PERMISSION_UPDATED_BEEP = "/profile/permission/permissionUpdateBeep";
        public static final String GET_PEEP_PERMISSION_MASK = "/profile/permission/getPeepPermission";

        public static final String USER_ACTION = "/profile/action";

        public static List<String> getRouteList() {
            return RequestTable.getRouteList(PROFILE.class);
        }
    }

    public static class DOWNLOAD extends Route {
        public static final String GET_VIDEO_FILE = "/mediadownload/video/download";
        public static final String GET_AUDIO_FILE = "/mediadownload/audio/download";
        public static final String VIDEO_THUMBNAIL_FILE = "/mediadownload/video/thumbnail";
        public static final String GET_AUDIO_THUMBNAIL_FILE = "/mediadownload/audio/albumart";
        public static final String GET_IMAGE_FILE = "/mediadownload/image/download";
        public static final String GET_USER_PROFILE_IMAGE = "/mediadownload/profile/image";

        public static final String GET_COPULA = "/directdownload";
        public static final String COPULA_HTMLPAGE = "/";

        public static List<String> getRouteList() {
            return RequestTable.getRouteList(DOWNLOAD.class);
        }
    }

    public static class STREAM extends Route {
        public static final String AUDIO_STREAM = "/mediastream/audio";
        public static final String VIDEO_STREAM = "/mediastream/video";

        public static List<String> getRouteList() {
            return RequestTable.getRouteList(STREAM.class);
        }
    }

    public static class Route {
        public static final String PING_SERVER = "/ps";
    }
}

package com.copula.functionality.localservice.media.provider;

import android.content.Context;
import com.copula.functionality.localservice.media.AudioModel;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.localservice.media.VideoModel;

import java.util.List;

/**
 * Created by heeleaz on 2/26/17.
 */
public class V2SystemMediaQuery extends V1SystemMediaQuery {
    public V2SystemMediaQuery(Context ctx) {
        super(ctx);
    }

    public List<AudioModel> searchAudioMedia(String query, String orderBy) {
        query = "\'%" + query + "%\'";
        String selection = "title LIKE " + query + " OR ARTIST LIKE " + query;

        List<AudioModel> device_m =
                SystemMediaProvider.getAudios(super.ctx, selection, orderBy);
        if (device_m == null) return null;

        setup(device_m, MediaModel.AUDIO, appUserId);

        return device_m;
    }

    public List<AudioModel> searchAudioMedia_Host(String query, String orderBy){
        List<AudioModel> result = searchAudioMedia(query, orderBy);
        if (result != null)
            result.retainAll(daoStudioMedia.getMedias(MediaModel.AUDIO));
        return result;
    }

    public List<VideoModel> searchVideoMedia(String query, String orderBy) {
        String selection = "title LIKE " + "\'%" + query + "%\'";
        List<VideoModel> device_m =
                SystemMediaProvider.getVideos(super.ctx, selection, orderBy);
        if (device_m == null) return null;

        setup(device_m, MediaModel.VIDEO, appUserId);
        return device_m;
    }

    public List<VideoModel> searchVideoMedia_VUser(String query, String orderBy) {
        List<VideoModel> result = searchVideoMedia(query, orderBy);
        if (result != null)
            result.retainAll(daoStudioMedia.getMedias(MediaModel.VIDEO));
        return result;
    }
}

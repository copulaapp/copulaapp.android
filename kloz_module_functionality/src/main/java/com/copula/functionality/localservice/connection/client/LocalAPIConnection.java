package com.copula.functionality.localservice.connection.client;


import com.copula.functionality.app.UserSession;
import okhttp3.OkHttpClient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class LocalAPIConnection {
    private static LocalAPIConnection singleton;
    private OkHttpClient connection;
    private LocalAPIProvider apiServiceProvider;
    private ExecutorService threadPool = Executors.newCachedThreadPool();

    private LocalAPIConnection(int connectTimeout) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(connectTimeout, TimeUnit.MILLISECONDS);
        connection = builder.build();
    }

    public static LocalAPIConnection getInstance() {
        if (singleton == null) {
            singleton = new LocalAPIConnection(5000);
        }
        return singleton;
    }

    public static LocalAPIConnection newInstance(int connectTimeout) {
        return new LocalAPIConnection(connectTimeout);
    }

    public boolean connect(LocalServiceAPIListener listener) {
        return connect(UserSession.getHostAddress(), listener);
    }

    public boolean connect(String hostAddress, final LocalServiceAPIListener listener) {
        final LocalAPIProvider provider = connect(hostAddress);
        if (provider == null) return false;

        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                listener.onConnected(provider);
            }
        });
        return true;
    }

    public LocalAPIProvider connect(String hostAddress) {
        if (hostAddress == null)
            throw new IllegalArgumentException("hostaddress cannot be null");

        if (apiServiceProvider == null) {
            apiServiceProvider = new LocalAPIProvider(connection);
        }

        apiServiceProvider.setHostAddress(hostAddress);
        return apiServiceProvider;
    }

    public LocalAPIProvider connect() {
        return connect(UserSession.getHostAddress());
    }
}
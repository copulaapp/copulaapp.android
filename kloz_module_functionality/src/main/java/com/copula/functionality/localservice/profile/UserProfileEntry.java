package com.copula.functionality.localservice.profile;

import com.copula.functionality.webservice.RestResponse;
import com.copula.functionality.restlite.annotation.DaoColumn;
import com.copula.functionality.restlite.annotation.JSONInner;

import java.io.Serializable;

/**
 * Created by eliasigbalajobi on 2/24/16.
 */
@JSONInner("result")
public class UserProfileEntry extends RestResponse implements Serializable {
    public static final int ONLINE = 1, OFFLINE = 0;
    public String bio, username, email, ipAddress, imgUrl, userId;
    public int feedPostCount = -1, followerCount = -1, followingCount;
    public boolean isLoggedUser;
    public int connectStatus, action;

    @DaoColumn(type = Boolean.class, name = "isFollowing")
    public boolean isFollowing;

    public UserProfileEntry(String userId) {
        this.userId = userId;
    }

    public UserProfileEntry() {
    }

    @Override
    public boolean equals(Object o) {
        String a = ((UserProfileEntry) o).userId;
        return (!(a == null && userId == null) && this.userId.equals(a));
    }
}


package com.copula.functionality.localservice.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import com.copula.functionality.downloader.DaoHMediaDownload;
import com.copula.functionality.downloader.DaoUMediaDownload;
import com.copula.functionality.localservice.media.feeds.DaoLocalMediaCaption;
import com.copula.functionality.localservice.media.feeds.DaoMediaActionRegister;
import com.copula.functionality.localservice.media.provider.MediaAPIUriHelper;
import com.copula.functionality.media.AudioController;
import com.copula.functionality.util.ConnectionConstant;
import com.copula.support.android.network.NetworkHelper;
import com.copula.support.android.util.UserEmailFetcher;
import com.copula.support.util.FileManager;

import java.io.File;
import java.net.InetAddress;
import java.util.Random;

public class UserAccountBase {
    private SharedPreferences preference;
    private Context context;

    public UserAccountBase(Context context) {
        this.context = context;
        this.preference = context.getSharedPreferences("profile_model", Context.MODE_PRIVATE);
    }

    public static void logoutUser(Context context) {
        new DaoConnectsProfile(context).truncateTable();
        new DaoMediaActionRegister(context).truncateTable();
        new DaoLocalMediaCaption(context).truncateTable();
        new DaoUMediaDownload(context).truncateTable();
        new DaoHMediaDownload(context).truncateTable();
        new DaoPeepPermission(context).truncateTable();

        AudioController.getInstance(context).stop();

        new UserAccountBase(context).clearPreference();
    }

    public static String systemAddress(Context context) {
        InetAddress address = NetworkHelper.getSystemAddress(context);
        if (address != null) return address.getHostAddress();
        return ConnectionConstant.LOOPBACK_ADDRESS;
    }

    private void clearPreference() {
        preference.edit().clear().apply();
    }

    public void setupLogged(String userId, String username, String bio) {
        setUsername(username);
        setUserId(userId);
        setProfileBio(bio);
        logged(true);

        int permissionMask = PermissionEntry.SHARE_PERM |
                PermissionEntry.READ_PERM |
                PermissionEntry.COPY_PERM;//all permission for root user
        new DaoPeepPermission(context).addPermission(userId, permissionMask);
    }

    public void setupNonLogged(String bio) {
        UserAccountBase accountBase = new UserAccountBase(context);
        String username = UserEmailFetcher.getEmailName(context);
        if (username == null) username = createTempUsername();

        String userId = "notreg_" + new Random().nextInt(99999);

        accountBase.setUsername(username);
        accountBase.setUserId(userId);
        accountBase.setProfileBio(bio);

        int permissionMask = PermissionEntry.SHARE_PERM |
                PermissionEntry.READ_PERM |
                PermissionEntry.COPY_PERM;//all permission for root user
        new DaoPeepPermission(context).addPermission(userId, permissionMask);
    }

    private String createTempUsername() {
        String email = UserEmailFetcher.getEmail(context);
        if (email != null) return (email.split("@"))[1];

        return Build.MODEL;
    }

    public SharedPreferences getPreference() {
        return preference;
    }

    public boolean isLogged() {
        return preference.getBoolean("logged", false);
    }

    private void logged(boolean logged) {
        Editor editor = preference.edit();
        editor.putBoolean("logged", logged).apply();
    }


    public void set3PAccount(String accountType, String tpUserId) {
        Editor editor = preference.edit();
        editor.putString("accountType", accountType);
        editor.putString("tpUserId", tpUserId);
        editor.apply();
    }

    public boolean hasInitNonLogged() {
        return preference.getBoolean("non_logged", false);
    }

    public String getUsername(String fallback) {
        return preference.getString("username", fallback);
    }

    public String getUsername() {
        return preference.getString("username", null);
    }

    public String getProfileBio(String fallBack) {
        String bio = preference.getString("profileBio", fallBack);
        if (bio == null || bio.equals("")) return fallBack;

        return bio;
    }

    public String getPNToken() {
        return preference.getString("pnToken", "");
    }

    public void setPNToken(String token) {
        preference.edit().putString("pnToken", token).apply();
    }

    public void setProfileBio(String bio) {
        preference.edit().putString("profileBio", bio).apply();
    }

    public String getUserId() {
        return preference.getString("userId", null);
    }

    public String getNRUserId() {
        if (!isLogged()) return getUserId();
        return null;
    }

    private boolean setUserId(String userId) {
        if (userId == null) return false;

        preference.edit().putString("userId", userId).apply();
        return true;
    }

    public boolean setUsername(String username) {
        if (username == null) return false;

        preference.edit().putString("username", username).apply();
        return true;
    }

    public boolean setProfileImage(File imgFile) {
        return setProfileImage(getUserId(), imgFile);
    }

    public boolean setProfileImage(String userId, File imgFile) {
        File saveTo = new File(UsersProfileResource.getProfileImagePath(userId));
        return FileManager.copyFile(imgFile, saveTo);
    }

    private String getProfileImageUrl() {
        return MediaAPIUriHelper.getUserProfileImage(systemAddress(context), getUserId());
    }

    public UserProfileEntry getUserProfile() {
        UserProfileEntry profile = new UserProfileEntry();
        profile.bio = getProfileBio("");
        profile.username = getUsername();
        profile.ipAddress = systemAddress(context);
        profile.userId = getUserId();
        profile.imgUrl = getProfileImageUrl();
        profile.connectStatus = UserProfileEntry.ONLINE;
        profile.isLoggedUser = isLogged();
        return profile;
    }
}
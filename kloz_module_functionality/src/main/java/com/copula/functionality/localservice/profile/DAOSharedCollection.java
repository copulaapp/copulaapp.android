package com.copula.functionality.localservice.profile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.restlite.DaoModelMapper;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 2/22/17.
 */
public class DaoSharedCollection extends DatabaseContext {
    private static final String TABLE = "shared_collection";

    public DaoSharedCollection(Context context) {
        super(context, "shared_collection.db", TABLE, 1);
    }

    public <T extends MediaModel> boolean addMediaList(List<T> entries) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            for (MediaModel model : entries) {
                values.put("mediaType", model.mediaType);
                values.put("mediaId", model.mediaId);
                db.insert(TABLE, null, values);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean addMedia(MediaModel entry) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("mediaType", entry.mediaType);
            values.put("mediaId", entry.mediaId);
            return db.insert(TABLE, null, values) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public List<MediaModel> getMedias(int mediaType) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TABLE, null, "mediaType=" + mediaType, null, null, null, null);
            if (cu == null || cu.getCount() <= 0) return new ArrayList<>();

            List<MediaModel> items = new ArrayList<>(cu.getCount());
            while (cu.moveToNext()) {
                items.add(DaoModelMapper.mapOne(cu, MediaModel.class));
            }
            return items;
        } catch (Exception e) {
            return new ArrayList<>();
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public MediaModel getTopModel(int mediaType) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TABLE, null,
                    "mediaType=" + mediaType, null, null, null, "_id desc", "0,1");
            if (cu.moveToFirst())
                return DaoModelMapper.mapOne(cu, MediaModel.class);
            return null;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public int getCount(int mediaType) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            String[] projection = new String[]{"_id"};
            cu = db.query(TABLE,
                    projection, "mediaType=" + mediaType, null, null, null, null);
            return cu.getCount();
        } catch (Exception e) {
            return 0;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public int getCount() {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            String[] projection = new String[]{"count(1)"};
            cu = db.query(TABLE, projection, null, null, null, null, null);
            if (cu.moveToFirst()) return cu.getInt(0);
            return -1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    public boolean removeMedia(MediaModel entry) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            String[] whereArgs = {"" + entry.mediaType, "" + entry.mediaId};
            return db.delete(TABLE, "mediaType=? AND mediaId=?", whereArgs) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean clearMedia(int mediaType) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            return db.delete(TABLE, "mediaType=?", new String[]{"" + mediaType}) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE;
        sql += "(_id integer primary key autoincrement,";
        sql += "mediaId integer(10),";
        sql += "mediaType integer(1))";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

package com.copula.functionality.localservice.connection.client;

import android.content.Context;
import android.util.Log;
import com.copula.functionality.util.ConnectionConstant;

/**
 * Created by heeleaz on 2/19/17.
 */
public class LocalServerActivePersist implements Runnable {
    private static LocalServerActivePersist singleton;
    private int pauseInterval = 2000;
    private LocalServerStateMonitor monitor;

    private boolean persist = true;
    private LocalAPIProvider api;

    private Thread persistorThread;

    private LocalServerActivePersist(Context context) {
        this.monitor = new LocalServerStateMonitor(context);
        this.api = LocalAPIConnection.getInstance().connect();
    }

    public static void resumePersistor(Context context) {
        if (singleton == null)
            singleton = new LocalServerActivePersist(context);

        singleton.resume();
    }

    public static void stopPersistor(Context context) {
        if (singleton == null)
            singleton = new LocalServerActivePersist(context);

        singleton.stop();
    }

    public void setPauseInterval(int pauseInterval) {
        this.pauseInterval = pauseInterval;
    }

    private void resume() {
        if (persistorThread == null || !persist) {
            persist = true;

            persistorThread = null;
            persistorThread = new Thread(this);
            persistorThread.start();
        }
    }

    private void stop() {
        persist = false;
    }

    @Override
    public void run() {
        while (persist) {
            Log.d("Hello", "Persisting");

            if (!isActive()) {
                monitor.startServer(true);
            }
            try {
                Thread.sleep(pauseInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isActive() {
        return api.pingServer(ConnectionConstant.MEDIA_QUERY_PORT) == 1;
    }
}

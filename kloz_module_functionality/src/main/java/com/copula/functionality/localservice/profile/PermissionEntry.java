package com.copula.functionality.localservice.profile;

import java.io.Serializable;

/**
 * Created by heeleaz on 8/27/16.
 */
public class PermissionEntry implements Serializable {
    public static final int READ_PERM = 4, COPY_PERM = 2, SHARE_PERM = 1;
    public int inContextPermission, permissionMask;

    public String userId, rpAddress;

    public static boolean checkPermission(int mask, int permission) {
        return (mask & permission) == permission;
    }

    public boolean isPermitted(int permission) {
        return (permissionMask & permission) == permission;
    }
}

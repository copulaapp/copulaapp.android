package com.copula.functionality.localservice.media.feeds;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.copula.functionality.localservice.media.MediaModel;
import com.copula.functionality.util.DatabaseContext;

import java.util.ArrayList;
import java.util.List;

public class DaoMediaActionRegister extends DatabaseContext {
    private static final String TB_NAME = "media_like_register";

    public DaoMediaActionRegister(Context context) {
        super(context, "media_like_register.db", TB_NAME, 1);
    }

    private int getRowId(SQLiteDatabase db, MediaModel model, int action) {
        Cursor cu = null;
        try {
            String[] selection = new String[]{"_id"};
            String[] wArgs = new String[]
                    {"" + model.mediaType, "" + model.mediaId, "" + action};
            cu = db.query(TB_NAME, selection, "media_type=? AND media_id=? AND action=?",
                    wArgs, null, null, null);
            if (cu.moveToNext()) return cu.getInt(cu.getColumnIndex("_id"));
            return -1;
        } catch (Exception e) {
            return -1;
        } finally {
            if (cu != null) cu.close();
        }
    }

    private boolean normalizeDataSize(int cleanupThreshold) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getWritableDatabase();
            cu = db.query(TB_NAME, new String[]{"_id"}, null, null, null, null, "_id desc");
            if (cu.getCount() <= cleanupThreshold) return false;

            //start deleting data from this position
            cu.moveToPosition(cleanupThreshold - 1);
            int thresholdId = cu.getInt(cu.getColumnIndex("_id"));
            return db.delete(TB_NAME, "_id<=" + thresholdId, null) > -1;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    boolean registerAction(MediaModel model, String actorId, int action) {
        this.normalizeDataSize(50);

        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("media_id", model.mediaId);
            values.put("media_type", model.mediaType);
            values.put("actor_id", actorId);
            values.put("action", action);
            values.put("created_at", model.createdAt);
            return db.insert(TB_NAME, null, values) > -1;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean removeAction(MediaModel model, String actorId, int action) {
        SQLiteDatabase db = null;
        try {
            int rid = getRowId(db = getWritableDatabase(), model, action);
            if (rid < 0) return false;//media not exists

            String[] wArgs = new String[]{"" + rid, actorId};
            return db.delete(TB_NAME, "_id=? AND actor_id=?", wArgs) > 0;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean hasActed(MediaModel model, String actorId, int action) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            int rid = getRowId(db = getWritableDatabase(), model, action);
            if (rid < 0) return false;//media not exists

            String[] wArgs = new String[]{"" + rid, actorId};
            cu = db.query(TB_NAME, new String[]{"_id"}, "_id=? AND actor_id=?", wArgs,
                    null, null, null);
            return cu.getCount() > 0;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    List<MediaActRegisterModel> getActionRegister(int mediaType, String limit) {
        SQLiteDatabase db = null;
        Cursor cu = null;

        try {
            db = getReadableDatabase();

            String whereStmt = null;
            if (mediaType != -1) whereStmt = "media_type=" + mediaType;
            cu = db.query(TB_NAME, null, whereStmt, null, null, null, "created_at desc", limit);
            if (cu.getCount() <= 0) return null;

            List<MediaActRegisterModel> list = new ArrayList<>();
            while (cu.moveToNext()) {
                MediaActRegisterModel m = new MediaActRegisterModel();
                m.createdAt = cu.getLong(cu.getColumnIndex("created_at"));
                m.mediaId = cu.getInt(cu.getColumnIndex("media_id"));
                m.mediaType = cu.getInt(cu.getColumnIndex("media_type"));
                m.actorId = cu.getString(cu.getColumnIndex("actor_id"));
                m.action = cu.getInt(cu.getColumnIndex("action"));
                list.add(m);
            }
            return list;
        } catch (Exception e) {
            return null;
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TB_NAME;
        sql += "(_id integer primary key autoincrement,";
        sql += "media_type integer(1),";
        sql += "media_id integer,";
        sql += "action integer,";
        sql += "actor_id varchar(45),";
        sql += "created_at integer)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table " + TB_NAME);
    }

    protected static class MediaActRegisterModel {
        public long createdAt;
        public int action, mediaId, mediaType;
        public String actorId;
    }
}

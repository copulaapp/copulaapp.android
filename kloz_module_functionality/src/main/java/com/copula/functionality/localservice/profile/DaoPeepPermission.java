package com.copula.functionality.localservice.profile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.copula.functionality.util.DatabaseContext;

/**
 * Created by heeleeaz on 8/10/16.
 */
public class DaoPeepPermission extends DatabaseContext {
    private static final String TAG = DaoConnectsProfile.class.getName();
    private static final String TABLE = "peeps_permission";

    public DaoPeepPermission(Context context) {
        super(context, "peeps_permission.db", TABLE, 1);
    }

    public boolean setPermission(String userId, int permission, boolean allow) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();

            int oldPermission;
            if ((oldPermission = getPermissionMask(db, userId)) == 0x0) {
                this.addPermission(db, userId, permission);
                return false;
            }

            if (allow) permission = permission | oldPermission;
            else permission = oldPermission & ~permission;

            ContentValues values = new ContentValues();
            values.put("permissionMask", permission);
            return db.update(TABLE, values, "userId=?", new String[]{userId}) > -1;
        } catch (Exception e) {
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public boolean addPermission(String userId, int permissionMask) {
        SQLiteDatabase db = null;
        try {
            return addPermission(db = getWritableDatabase(), userId, permissionMask);
        } catch (Exception e) {
            Log.d(TAG, "PermissionAdd Error: " + e.getMessage());
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    private boolean addPermission(SQLiteDatabase db, String userId, int permissionMask) {
        if (getPermissionMask(db, userId) > 0) return false;

        ContentValues values = new ContentValues();
        values.put("userId", userId);
        values.put("permissionMask", permissionMask);
        return db.insert(TABLE, null, values) > -1;
    }

    private int getPermissionMask(SQLiteDatabase db, String userId) {
        Cursor cu = null;
        try {
            String[] wArgs = new String[]{"" + userId};
            cu = db.query(TABLE, null, "userId=?", wArgs, null, null, null);
            if (cu.getCount() > 0 && cu.moveToNext()) {
                return cu.getInt(cu.getColumnIndex("permissionMask"));
            } else return 0x0;
        } finally {
            if (cu != null) cu.close();
        }
    }

    public PermissionEntry getPermission(String userId) {
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            String[] whereArgs = new String[]{userId};
            cu = db.query(TABLE, null, "userId=?", whereArgs, null, null, null);

            PermissionEntry model = new PermissionEntry();
            if (cu.getCount() > 0 && cu.moveToFirst()) {
                model.permissionMask = cu.getInt(cu.getColumnIndex("permissionMask"));
                model.userId = userId;
            }
            return model;
        } catch (Exception e) {
            return new PermissionEntry();
        } finally {
            if (cu != null) cu.close();
            if ((db != null)) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists " + TABLE;
        sql += "(_id integer primary key autoincrement,";
        sql += "userId varchar,";
        sql += "permissionMask integer(1))";
        db.execSQL(sql);
    }
}

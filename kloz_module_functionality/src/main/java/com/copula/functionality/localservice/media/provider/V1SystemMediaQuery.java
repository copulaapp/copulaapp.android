package com.copula.functionality.localservice.media.provider;

import android.content.Context;
import android.provider.MediaStore;
import com.copula.functionality.localservice.media.*;
import com.copula.functionality.localservice.media.feeds.DaoLocalMediaCaption;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.localservice.profile.DaoSharedCollection;
import com.copula.functionality.localservice.profile.UserAccountBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliasigbalajobi on 5/13/16.
 */
public class V1SystemMediaQuery {
    protected Context ctx;
    protected DaoLocalMediaCaption daoMediaCaption;
    protected DaoSharedCollection daoStudioMedia;

    protected String appUserId;

    public V1SystemMediaQuery(Context ctx) {
        this.ctx = ctx;
        this.daoMediaCaption = new DaoLocalMediaCaption(ctx);
        this.daoStudioMedia = new DaoSharedCollection(ctx);

        this.appUserId = new UserAccountBase(ctx).getUserId();
    }

    <T extends MediaModel> void setup(List<T> mediaList, int mediaType, String user) {
        List<MediaModel> studio_m = daoStudioMedia.getMedias(mediaType);
        List<MediaCaptionEntry> caption_m = daoMediaCaption.getCaptions(mediaType, user, null);

        for (MediaModel media : mediaList) {
            media.isGalleryMedia = studio_m.indexOf(media) > -1;

            int index = caption_m.indexOf(new MediaCaptionEntry(media));
            if (index > -1) {
                MediaCaptionEntry caption = caption_m.get(index);

                media.createdAt = caption.createdAt;
                media.likeCount = caption.likeCount;
                media.shareCount = caption.shareCount;
                media.isLiked = caption.isLiked;
            }
        }
    }

    public List<ImageModel> queryImageLibrary_VUser(String selection, String orderBy) {
        List<ImageModel> device_m =
                SystemMediaProvider.getImages(ctx, selection, orderBy);
        if (device_m == null) return null;

        device_m.retainAll(daoStudioMedia.getMedias(MediaModel.IMAGE));

        List<ImageModel> entries = new ArrayList<>();
        for (ImageModel media : device_m) {
            media.mediaId = media.bucketId; //unique key to avoid repetition
            if (!entries.contains(media)) entries.add(media);
        }

        return entries;
    }

    public List<ImageModel> queryImageLibrary(String selection, String orderBy) {
        List<ImageModel> device_m = SystemMediaProvider.getDeviceImagesBucket(ctx, orderBy);
        if (device_m == null) return null;

        List<ImageModel> entries = new ArrayList<>();
        for (ImageModel media : device_m) {
            media.mediaId = media.bucketId; //unique key to avoid repetition
            if (!entries.contains(media)) entries.add(media);
        }

        return entries;
    }

    public List<ImageModel> getImageMedia(String selection, String orderBy) {
        List<ImageModel> device_m =
                SystemMediaProvider.getImages(ctx, selection, orderBy);
        if (device_m == null) return null;

        setup(device_m, MediaModel.IMAGE, appUserId);

        return device_m;
    }

    public List<ImageModel> queryImageMedia_VUser(String selection, String orderBy, String vUser) {
        List<ImageModel> device_m =
                SystemMediaProvider.getImages(ctx, selection, orderBy);
        if (device_m == null) return null;

        device_m.retainAll(daoStudioMedia.getMedias(MediaModel.IMAGE));
        setup(device_m, MediaModel.IMAGE, vUser);

        return device_m;
    }

    public List<AudioModel> getAudioAlbum(String selection, String orderBy) {
        List<AudioModel> m = SystemMediaProvider.getAudios(ctx, selection, orderBy);
        if (m == null) return null;

        List<AudioModel> entries = new ArrayList<>();
        for (AudioModel media : m) {
            media.mediaId = media.albumId; //unique key to avoid repetition
            if (!entries.contains(media)) entries.add(media);
        }
        return entries;
    }

    public List<AudioModel> getAudioAlbum_Host(String selection, String orderBy) {
        selection = (selection == null)
                ? MediaStore.Audio.Media.IS_MUSIC + " = 1" : selection;

        List<AudioModel> m = SystemMediaProvider.getAudios(ctx, selection, orderBy);
        if (m == null) return null;

        //retain just media on this user media collection
        m.retainAll(daoStudioMedia.getMedias(MediaModel.AUDIO));
        List<AudioModel> entries = new ArrayList<>();
        for (AudioModel media : m) {
            media.mediaId = media.albumId; //unique key to avoid repetition
            if (!entries.contains(media)) entries.add(media);
        }
        return entries;
    }

    public List<AudioModel> getAudioMedia(String selection, String orderBy) {
        List<AudioModel> media = SystemMediaProvider.getAudios(ctx, selection, orderBy);
        if (media == null) return null;

        setup(media, MediaModel.AUDIO, appUserId);
        return media;
    }

    public List<AudioModel> getAudioMedia_Host(String selection, String orderBy, String vUser) {
        List<AudioModel> media = SystemMediaProvider.getAudios(ctx, selection, orderBy);
        if (media == null) return null;

        media.retainAll(daoStudioMedia.getMedias(MediaModel.AUDIO));
        setup(media, MediaModel.AUDIO, vUser);

        return media;
    }

    public List<VideoModel> getVideoMedia(String selection, String orderBy) {
        List<VideoModel> m = SystemMediaProvider.getVideos(ctx, selection, orderBy);
        if (m == null) return null;

        setup(m, MediaModel.VIDEO, appUserId);
        return m;

    }

    public List<VideoModel> getVideoMedia_Host(String selection, String orderBy, String vUser) {
        List<VideoModel> m = SystemMediaProvider.getVideos(ctx, selection, orderBy);
        if (m == null) return null;

        m.retainAll(daoStudioMedia.getMedias(MediaModel.VIDEO));
        setup(m, MediaModel.VIDEO, vUser);
        return m;
    }

    public MediaStatsModel getMediaCount() {
        MediaStatsModel model = new MediaStatsModel();
        String selection = MediaStore.Audio.Media.IS_MUSIC + "=1";
        model.audioMediaCount =
                SystemMediaProvider.getCount(ctx, MediaModel.AUDIO, selection);
        model.imageMediaCount =
                SystemMediaProvider.getCount(ctx, MediaModel.IMAGE, null);
        model.videoMediaCount =
                SystemMediaProvider.getCount(ctx, MediaModel.VIDEO, null);
        return model;
    }

    public MediaStatsModel getMediaCount_VUser() {
        MediaStatsModel model = new MediaStatsModel();
        model.audioMediaCount -= daoStudioMedia.getCount(MediaModel.AUDIO);
        model.imageMediaCount -= daoStudioMedia.getCount(MediaModel.IMAGE);
        model.videoMediaCount -= daoStudioMedia.getCount(MediaModel.VIDEO);
        return model;
    }
}

package com.copula.functionality.localservice.media.provider;

import android.content.Context;

import java.io.File;

/**
 * Created by heeleaz on 5/16/17.
 */
public class SystemAPKsProvider {
    public static File getCopulaAPK(Context context) {
        String data = context.getApplicationInfo().publicSourceDir;
        return new File(data);
    }

}

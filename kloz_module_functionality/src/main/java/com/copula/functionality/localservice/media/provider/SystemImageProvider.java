package com.copula.functionality.localservice.media.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import com.copula.support.android.media.ImageBitmapOptimizer;

import java.io.File;
import java.io.InputStream;

public class SystemImageProvider {
    private Context mContext;

    public SystemImageProvider(Context mContext) {
        this.mContext = mContext;
    }

    public Bitmap getOptImageBitmap(String url, int maxSize) {
        if (url == null) return null;
        try {
            File imFile = new File(url);
            if (imFile.exists()) return ImageBitmapOptimizer.decodeFile(imFile, maxSize);
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public Bitmap getOptAlbumThumbnail(long albumId, int maxSize) {
        try {
            Uri albumArtUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(albumArtUri, albumId);
            ContentResolver res = mContext.getContentResolver();
            InputStream is = res.openInputStream(uri);
            if (is != null && is.available() > 0) {
                Options opts = new Options();
                opts.inSampleSize = ImageBitmapOptimizer.calcInSampleSize(is, maxSize);
                return BitmapFactory.decodeStream(res.openInputStream(uri), null, opts);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public Uri getAlbumThumbnailUri(long albumId) {
        Uri albumArtUri = Uri.parse("content://media/external/audio/albumart");
        return ContentUris.withAppendedId(albumArtUri, albumId);
    }

    public Bitmap getOptVideoThumbnail(String videoUrl, int maxSize) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = null;
        try {
            retriever = new MediaMetadataRetriever();
            retriever.setDataSource(videoUrl);

            bitmap = retriever.getFrameAtTime(-1);

            int w = maxSize;
            int h = (maxSize / bitmap.getHeight()) * maxSize;
            bitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);
        } catch (Exception e) {
        } finally {
            if (retriever != null) retriever.release();
        }
        return bitmap;
    }
}
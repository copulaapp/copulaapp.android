package com.copula.functionality.localservice.connection.server.controller;

import android.content.Context;
import android.graphics.Bitmap;
import com.copula.functionality.app.NotificationHelper;
import com.copula.functionality.localservice.connection.server.RequestTable;
import com.copula.functionality.localservice.profile.DaoConnectsProfile;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.localservice.profile.UsersProfileResource;
import com.copula.functionality.restlite.MediaType;
import com.copula.functionality.restlite.annotation.RouteContext;
import com.copula.functionality.restlite.annotation.RoutePath;
import com.copula.functionality.restlite.annotation.RouteProduces;
import com.copula.functionality.restlite.QueryParam;

/**
 * Created by heeleeaz on 7/11/16.
 */
public class UserProfileController extends ParentController {
    private UserAccountBase userAccountBase;
    private DaoConnectsProfile daoConnectsProfile;

    public UserProfileController(Context context) {
        daoConnectsProfile = new DaoConnectsProfile(context);
        userAccountBase = new UserAccountBase(context);
    }

    @RoutePath(RequestTable.PROFILE.GET_USER_PROFILE)
    @RouteProduces(MediaType.APPLICATION_JSON)
    public UserProfileEntry getUserProfile(@RouteContext QueryParam pathArg) {
        String userId = pathArg.getString("userId");
        if (userId != null) {
            return daoConnectsProfile.getConnect(userId);
        } else return userAccountBase.getUserProfile();
    }

    @RoutePath(RequestTable.PROFILE.HOST_CONNECT_BEEP)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public boolean hostConnectBeep(@RouteContext QueryParam pathArg) {
        UserProfileEntry profileModel = new UserProfileEntry();
        profileModel.userId = pathArg.getString("userId");
        profileModel.username = pathArg.getString("username");
        profileModel.bio = pathArg.getString("bio");
        profileModel.ipAddress = pathArg.getString("ipAddress");
        profileModel.imgUrl = pathArg.getString("imgUrl");
        profileModel.connectStatus = pathArg.getInt("connectStatus");

        NotificationHelper helper = NotificationHelper.getInstance();
        return helper.dispatchHostConnect(profileModel);
    }

    @RoutePath(RequestTable.PROFILE.USER_ACTION)
    @RouteProduces(MediaType.TEXT_PLAIN)
    public boolean userAction(@RouteContext QueryParam pathArg) {
        UserProfileEntry profileModel = new UserProfileEntry();
        profileModel.userId = pathArg.getString("userId");
        profileModel.ipAddress = pathArg.getString("ipAddress");
        profileModel.action = pathArg.getInt("action", -1);

        NotificationHelper helper = NotificationHelper.getInstance();
        return helper.dispatchUserAction(profileModel);
    }

    @RoutePath(RequestTable.DOWNLOAD.GET_USER_PROFILE_IMAGE)
    @RouteProduces(MediaType.IMAGE_BITMAP)
    public Bitmap getProfileImage(@RouteContext QueryParam pathArg) {
        String userId = pathArg.getString("userId");
        int maxSize = pathArg.getInt("maxSize");

        return UsersProfileResource.getProfileImage(userId, maxSize);
    }
}

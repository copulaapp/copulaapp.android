package com.copula.functionality.localservice.connection.client;

import android.content.Context;
import android.util.Log;
import com.copula.functionality.localservice.connection.server.LocalDataFetcherServer;
import com.copula.functionality.localservice.connection.server.LocalMediaQueryServer;
import com.copula.functionality.localservice.connection.server.LocalMediaStreamServer;
import com.copula.functionality.util.ConnectionConstant;

/**
 * Created by heeleaz on 1/2/17.
 */
public class LocalServerStateMonitor {
    private Context context;

    public LocalServerStateMonitor(Context context) {
        this.context = context;
    }

    public void stopServer() {
        LocalMediaStreamServer.getInstance(context).stopServer();
        LocalDataFetcherServer.getInstance(context).stopServer();
        LocalMediaQueryServer.getInstance(context).stopServer();

        ConnectionConstant.HOST_ONLINE_STATUS = false;
    }

    public void startServer(boolean restart) {
        startServer(null, restart);
    }

    private void startServer(String address, boolean restart) {
        if (restart) stopServer();

        Log.d("Hello", "serverStarted:" + address);

        LocalMediaQueryServer.getInstance(context).startServer(address);
        LocalDataFetcherServer.getInstance(context).startServer(address);
        LocalMediaStreamServer.getInstance(context).startServer(address);

        ConnectionConstant.HOST_ONLINE_STATUS = true;
    }
}

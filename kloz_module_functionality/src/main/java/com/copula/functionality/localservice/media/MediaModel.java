package com.copula.functionality.localservice.media;


import com.copula.functionality.restlite.ObjectJsonMapper;
import com.copula.functionality.restlite.annotation.DaoColumn;

import java.io.Serializable;

public class MediaModel implements Serializable {
    public static final int AUDIO = 2, IMAGE = 1, VIDEO = 3;

    public String dataPath, dataUrl, streamUrl, fileName;
    public boolean status = false, isGalleryMedia;
    public long fileSize;
    public int width, height;

    @DaoColumn(name = "mediaType", type = Integer.class)
    public int mediaType;
    @DaoColumn(name = "mediaId", type = Integer.class)
    public int mediaId;
    @DaoColumn(name = "_id", type = Integer.class)
    public int _id;
    @DaoColumn(name = "likeCount", type = Integer.class)
    public int likeCount;
    @DaoColumn(name = "shareCount", type = Integer.class)
    public int shareCount;
    @DaoColumn(name = "createdAt", type = Long.class)
    public long createdAt;
    @DaoColumn(type = Long.class, name = "updatedAt")
    public long updatedAt;
    @DaoColumn(type = Boolean.class, name = "isLiked")
    public boolean isLiked;

    public String thumbnailUrl, mediaTitle, mediaComposer, mediaLibrary;
    public String mimeType, mediaCaption;

    private Object tag;

    public MediaModel(int mediaType, int mediaId) {
        this.mediaType = mediaType;
        this.mediaId = mediaId;
    }

    public MediaModel() {
    }

    public MediaModel(MediaModel m) {
        setMedia(m);
    }

    public MediaModel getMedia() {
        return this;
    }

    public void setMedia(MediaModel entry) {
        if (entry == null) return;
        try {
            String j = ObjectJsonMapper.toJSON(entry);
            ObjectJsonMapper.mapOne(this, j);
        } catch (Exception e) {
        }
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        MediaModel item = (MediaModel) o;
        return item.mediaId == mediaId && item.mediaType == mediaType;
    }
}

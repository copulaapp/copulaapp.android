package com.copula.functionality.localservice.media;


public class AudioModel extends MediaModel {
    public int albumId;
    public long duration;

    public AudioModel() {
    }

    public AudioModel(int mediaId) {
        this.mediaId = mediaId;
        this.mediaType = MediaModel.AUDIO;
    }

    public AudioModel(MediaModel item) {
        super.setMedia(item);
    }
}

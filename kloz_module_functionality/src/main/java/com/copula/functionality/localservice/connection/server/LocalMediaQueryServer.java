package com.copula.functionality.localservice.connection.server;

import android.content.Context;
import android.util.Log;
import com.copula.functionality.localservice.connection.server.controller.MediaQueryController;
import com.copula.functionality.localservice.connection.server.controller.UserProfileController;
import com.copula.functionality.restlite.LiteRouter;
import com.copula.functionality.util.ConnectionConstant;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * @author eliasigbalajobi
 *         <p/>
 *         this server only getFeeds request and send response to client
 */
public class LocalMediaQueryServer implements HttpHandler {
    private static final String TAG = LocalMediaQueryServer.class.getSimpleName();
    private static LocalMediaQueryServer singleton = null;

    private HttpServer mHttpServer;
    private LiteRouter mRouter;

    private LocalMediaQueryServer(Context context) {
        mRouter = new LiteRouter();
        mRouter.addObject(new UserProfileController(context));
        mRouter.addObject(new MediaQueryController(context));
    }

    public static LocalMediaQueryServer getInstance(Context context) {
        if (singleton == null) singleton = new LocalMediaQueryServer(context);
        return singleton;
    }

    private void startServer(String hostAddress, int port) {
        try {
            mHttpServer = HttpServer.create(new InetSocketAddress(port), 0);
            mHttpServer.setExecutor(Executors.newCachedThreadPool());

            List<String> l = new ArrayList<>();
            l.addAll(RequestTable.MEDIA.getRouteList());
            l.addAll(RequestTable.PROFILE.getRouteList());
            for (final String route : l) {
                mHttpServer.createContext(route, this);
            }
            mHttpServer.start();//startServer server
        } catch (Exception e) {
            Log.d(TAG, "Server Error: " + e.getMessage());
        } //catch (OutOfMemoryError e) {
        //e.printStackTrace();
        //}
    }

    public void startServer(String hostAddress) {
        this.startServer(hostAddress, ConnectionConstant.MEDIA_QUERY_PORT);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        mRouter.route(httpExchange);
    }

    public void stopServer() {
        if (mHttpServer != null) mHttpServer.stop(0);
    }
}

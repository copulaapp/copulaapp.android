package com.copula.functionality.app;


import android.content.Context;
import com.copula.functionality.localservice.profile.UserAccountBase;
import com.copula.functionality.localservice.profile.UserProfileEntry;

/**
 * Created by eliasigbalajobi on 2/27/16.
 */
public class UserSession {
    private static UserProfileEntry hostProfile;
    private static String hostAddress = "127.0.0.1";

    public static String getHostAddress() {
        return hostAddress;
    }

    public static UserProfileEntry getHostProfile() {
        if (hostProfile == null) hostProfile = new UserProfileEntry();
        return hostProfile;
    }

    public static void setHostProfile(UserProfileEntry hostProfile) {
        UserSession.hostProfile = hostProfile;
        UserSession.hostAddress = hostProfile.ipAddress;
    }

    public static String getAppUserId(Context ctx) {
        return getAppProfile(ctx).userId;
    }

    public static UserProfileEntry getAppProfile(Context ctx) {
        return new UserAccountBase(ctx).getUserProfile();
    }
}

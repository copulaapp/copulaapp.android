package com.copula.functionality.app;


import com.copula.functionality.downloader.MediaDownloadEntry;
import com.copula.functionality.localservice.media.feeds.MediaCaptionEntry;
import com.copula.functionality.localservice.profile.PermissionEntry;
import com.copula.functionality.localservice.profile.UserProfileEntry;
import com.copula.functionality.util.ConnectionConstant;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by eliasigbalajobi on 5/24/16.
 */
public class NotificationHelper {
    public static final int HOST_CONNECT_NOTIFICATION = 0;
    public static final int USER_DOWNLOAD_NOTIFICATION = 1;
    public static final int MEDIA_ACTION_NOTIFICATION = 2;
    public static final int PERMISSION_UPDATE_NOTIFICATION = 3;
    public static final int PERMISSION_UPDATED_NOTIFICATION = 4;
    public static final int USER_ACTION_NOTIFICATION = 5;
    public static final int APP_WIFI_NOTIFICATION = ConnectionConstant.APP_WIFI_CONNECTION;
    private static NotificationHelper singleton;
    private ExecutorService threadPool = Executors.newCachedThreadPool();
    private Set<NotificationPushListener> pushListeners = new HashSet<>();

    private NotificationHelper() {
    }

    public static NotificationHelper getInstance() {
        if (singleton == null) singleton = new NotificationHelper();
        return singleton;
    }

    public boolean dispatchMediaAction(MediaCaptionEntry model) {
        notifyListeners(MEDIA_ACTION_NOTIFICATION, new NotificationModel(model));
        return true;
    }

    public boolean dispatchHostConnect(UserProfileEntry profile) {
        notifyListeners(HOST_CONNECT_NOTIFICATION, new NotificationModel(profile));
        return true;
    }

    public boolean dispatchUserAction(UserProfileEntry profile) {
        notifyListeners(USER_ACTION_NOTIFICATION, new NotificationModel(profile));
        return true;
    }

    public boolean dispatchDownload(MediaDownloadEntry download, int state) {
        if (download == null) return false;

        notifyListeners(USER_DOWNLOAD_NOTIFICATION, new NotificationModel(download));
        return true;
    }

    public int dispatchPermissionUpdate(String userId, String rpAddress, int permission) {
        PermissionEntry permissionEntry = new PermissionEntry();
        permissionEntry.inContextPermission = permission;
        permissionEntry.userId = userId;
        permissionEntry.rpAddress = rpAddress;

        notifyListeners(PERMISSION_UPDATE_NOTIFICATION, new NotificationModel(permissionEntry));
        return 1;//awaiting response from user
    }

    public boolean dispatchPermissionUpdated(String permitterId, int permission) {
        PermissionEntry permissionEntry = new PermissionEntry();
        permissionEntry.permissionMask = permission;
        permissionEntry.userId = permitterId;

        notifyListeners(PERMISSION_UPDATED_NOTIFICATION, new NotificationModel(permissionEntry));
        return true;
    }

    public void dispatch(int broadcast, Object data) {
        notifyListeners(broadcast, new NotificationModel(data));
    }

    public void addListener(NotificationPushListener listener) {
        pushListeners.add(listener);
    }

    public void removeListener(NotificationPushListener listener) {
        pushListeners.remove(listener);
    }

    private void notifyListeners(final int broadcast, final NotificationModel entry) {
        for (final NotificationPushListener l : pushListeners) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    l.onPushNotification(broadcast, entry);
                }
            });
        }
    }

    public interface NotificationPushListener {
        void onPushNotification(int broadcast, NotificationModel entry);
    }

    public class NotificationModel {
        public Object data;

        private NotificationModel(Object data) {
            this.data = data;
        }
    }
}

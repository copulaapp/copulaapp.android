package com.copula.functionality.app;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleeaz on 7/24/16.
 */
public class V23PermissionHelper {
    public static final int RATIONALE_PERMISSION = 2, REQUEST_PERMISSION = 0, PERMITTED = 1;
    private String[] coreRationalePermission;
    private Activity context;

    public V23PermissionHelper(Activity activity) {
        this.context = activity;
    }

    private static boolean isPermitted(Activity context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static int wifiScanAccessPermission(Activity context, int requestCode) {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                return RATIONALE_PERMISSION;
            } else {
                ActivityCompat.requestPermissions(context, permissions, requestCode);
                return REQUEST_PERMISSION;
            }
        } else return PERMITTED;
    }//end

    public static boolean checkSettingsPermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return Settings.System.canWrite(context);
        }
        return true;
    }

    public static boolean checkPermission(Activity context, String permission) {
        return isPermitted(context, permission);
    }

    private String[] checkCorePermRationale() {
        List<String> permString = new ArrayList<>();
        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permString.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permString.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                Manifest.permission.READ_PHONE_STATE)) {
            permString.add(Manifest.permission.READ_PHONE_STATE);
        }

        return permString.toArray(new String[permString.size()]);
    }

    public int coreAppPermission(int resultCode) {
        List<String> permission = new ArrayList<>();
        permission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permission.add(Manifest.permission.READ_PHONE_STATE);

        if (!isPermitted(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                || !isPermitted(context, Manifest.permission.READ_PHONE_STATE)) {
            if ((coreRationalePermission = checkCorePermRationale()).length == 0) {
                ActivityCompat.requestPermissions(context,
                        permission.toArray(new String[permission.size()]), resultCode);
                return REQUEST_PERMISSION;
            } else return RATIONALE_PERMISSION;
        } else {
            return PERMITTED;
        }
    }//end

    public String[] getCoreRationalePermission() {
        return coreRationalePermission;
    }
}
